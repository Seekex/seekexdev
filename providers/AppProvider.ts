import {ApplicationContract} from '@ioc:Adonis/Core/Application'
import { DateTime } from 'luxon'
import { string } from '@ioc:Adonis/Core/Helpers'
//import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'

export default class AppProvider {
public static needsApplication = true

  constructor (protected app: ApplicationContract) {
    
  }

  public register () {
    // Register your own bindings
    //this.app.container.bind('Binding', () => {})
  }

  public async boot () {
    const View = (await import('@ioc:Adonis/Core/View')).default
    const Auth = (await import('@ioc:Adonis/Addons/Auth')).default
    View.global('outDate', (dt) => {
      return DateTime.fromISO(dt).setZone('Asia/Kolkata').toFormat('dd, LLL yyyy ')
    })

    View.global('outDateFull', (dt) => {
      return DateTime.fromISO(dt).setZone('Asia/Kolkata').toFormat('dd, LLL yyyy t')
    })
    View.global('outDateFormat', (dt) => {
     const d= new Date(dt) 
     if (isNaN(d.getTime())) {
      return dt
     }else{
       return  DateTime.fromISO(new Date(Date.parse(dt)).toISOString()).setZone('Asia/Kolkata').toFormat('dd, LLL yyyy t')
     }
    })

    View.global('nameFromUrl', (url) => {
      return (url = url.substr(1 + url.lastIndexOf("/")).split('?')[0])
    })
    View.global('youtubeImg', (data) => {
      return JSON.parse(data)[0].url
    })

    View.global('captialize', (str) => {
      //return str
      return string.capitalCase(str)
    })

    View.global('acls',(url,aclurl,user) => {
      if(user!='admin'){
        const userAcl = JSON.parse(aclurl)
        //const url = request.ctx?.route?.pattern
        return userAcl.indexOf(url)>-1?true:false
      }else{
        return true
      }
     // return {url,userAcl,user}
    })
 
  }

  public shutdown () {
    // Cleanup, since app is going down
  }

  public async ready () {

    //*****************All Crons ********************************//
    // (await import('App/Tasks/UserWalletUpdate')).default;
    // (await import('App/Tasks/CronAtEveryDay')).default;
    // (await import('App/Tasks/IncompleteExpert')).default;
    // (await import('App/Tasks/CronAtEveryFiveMin')).default;
  }
}
