Vue.component('project-list', {
  props: ['post'],
  template: '<div class="card-box m-b-10"><div class="table-box opport-box"><div class="table-detail"><img src="" v-bind:src="base_url+\'/uploaded/\'+user+\'/\'+post.cover_image" alt="contact-img" title="contact-img" class="rounded-circle thumb-sm"></div><div class="table-detail"><div class="member-info"><h4 class="m-t-0"><b>{{post.title}}</b></h4></div></div><div class="table-detail"><p class="text-dark m-b-5"><b>From Date: {{post.from_date|dateFormat}}</b><span class="text-muted"></span></p><p class="text-dark m-b-0"><b>To Date: {{post.to_date|dateFormat}}</b> <span class="text-muted"></span></p></div><div class="table-detail table-actions-bar"><a href="javascript:void(0)" class="table-action-btn" data-id=""><i class="md md-edit"></i></a><a href="javascript:void(0)" class="table-action-btn" ><i class="md md-close"></i></a></div></div></div>'
})
Vue.filter('dateFormat', function (date) {
  if (!date) return ''
  return moment(date).format('MMMM Do YYYY');
})
var proj=new Vue({ el: '#project-app',
        data: function(){
          $.get(base_url+'/profile-admin/get-project').then((response)=>{
            this.posts = response;
          });
            return{
              posts:''
            }
         }
});
function editProject(pid){
  alert(pid);
}
function deleteProject(obj){
  let id=$(obj).data('id');
 //alert(id);
}
