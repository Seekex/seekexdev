Vue.prototype.$http = axios
Vue.component('survey', {
  props: ['survey','datas'],
  template: '<div class="new-survey">\
            <div class="form-group-custom">\
            <div class="row">\
              <div class="col-sm-8">\
              <div class="form-group-custom">\
                <input id="survey_title" type="text" class="" name="title" value="" v-model="survey.title" required />\
                <label class="control-label text-dark" for="survey_title">Survey Title</label><i class="bar"></i>\
                <span class="err-msg" v-if="survey.title==\'\'">Please enter survey title.</span>\
              </div>\
              <div class="form-group-custom">\
              <textarea id="survey_notes" class="" name="notes" value="" v-model="survey.notes"></textarea>\
              <label class="control-label text-dark" for="survey_notes">Survey Notes (Optional)</label><i class="bar"></i>\
              </div>\
              </div>\
              <div class="col-sm-4">\
                <div class="form-group-custom">\
                  <select class="custom-select" name="survey_type" v-model="survey.survey_type" required >\
                    <option value="">Select Type</option>\
                    <option v-for="item in datas.surveyCategories" :value="item.name" :key="item._id">{{item.name}}</option>\
                  </select>\
                  <i class="bar"></i>\
                  <span class="err-msg" v-if="survey.survey_type==\'\'">Select a type.</span>\
                </div>\
              </div>\
            </div>\
            </div>\
            <app-question v-for="(question,index) in survey.questions"  v-bind:question="question" v-bind:index="index"  v-bind:questionTypes="datas.questionTypes" ></app-question>\
            <div class="buttons">\
                <a href="javascript:void(0)" @click="$parent.addQuestion()" class="add-another"><i class="fa fa-plus"></i> ADD QUESTION</a>\
            </div>\
          </div>'
})
Vue.component('app-question', {
  props: ['index','question','questionTypes'],
  template: '<div class="form-group-custom">\
              <div class="row">\
              <strong class="ques-count">{{index+1}}</strong>\
              <div class="col-sm-8">\
              <input type="text" class="" name="question" value="" v-model="question.question" required />\
              <label class="control-label text-dark">Question</label><i class="bar"></i>\
              <span class="err-msg"  v-if="question.question==\'\'">Please enter question.</span>\
            </div>\
            <div class="col-sm-3">\
              <div class="form-group-custom">\
                <select class="custom-select" @change="surveyApp.onQustType(question.type,index)" name="question_type" v-model="question.type" required >\
                  <option v-for="item in questionTypes" :value="item.type" :key="item.type">{{item.lable}}</option>\
                </select>\
                <i class="bar"></i>\
              </div>\
              <span class="err-msg"  v-if="question.type==\'\'">Please select question type.</span>\
            </div>\
            <div class="remove-btn col-sm-1">\
              <a href="javascript:void(0)" @click="surveyApp.removeQuestion(index)" class="add-another"><i class="fa fa-minus"></i></a>\
            </div>\
            <div class="required-option col-sm-12">\
            <div class="checkbox checkbox-purple" style="display: flex;">\
            <input class="required-checkbox" type="checkbox" name="required" v-model="question.required">\
            <label>Require an Answer to this Question</label>\
            </div>\
            </div>\
            <div class="col-sm-8" v-if="question.type==\'checkbox\' || question.type==\'radiobutton\'">\
            <label class="text-dark">All Options</label>\
            <question-option  v-for="(option,indx) in question.options"  v-bind:option="option" v-bind:index="indx" v-bind:qindex="index"></question-option>\
            <div class="buttons">\
                <a href="javascript:void(0)" @click="surveyApp.addOption(index)" class="add-another"><i class="fa fa-plus"></i> Add Option</a>\
            </div>\
            </div>\
            <div class="col-sm-8" v-if="question.type==\'starrating\'">\
            <star-option  v-bind:question="question" v-bind:index="index"></star-option>\
            </div>\
            </div>\
          </div>'
})
Vue.component('question-option', {
  props: ['qindex','index','option'],
  template: '<div class="form-group-custom"><div class="row">\
              <div class="col-sm-10">\
              <input type="text" class="opt" name="option" value="" v-model="option.text" required />\
              <label class="control-label text-dark">Option Text</label><i class="bar"></i>\
              <span class="err-msg"  v-if="option.text==\'\'">Please enter option value.</span>\
              </div>\
              <div class="remove-btn col-sm-2">\
              <a href="javascript:void(0)" @click="surveyApp.removeOption(qindex,index)" class="add-another"><i class="fa fa-minus"></i></a>\
            </div>\
            </div>'
})

Vue.component('star-option', {
  props: ['index','question'],
  template: '<div class="form-group-custom">\
            <div class="row">\
            <div class="col-sm-2">\
            <label class="text-dark">Scale</label>\
            <select class="custom-select" name="scale" v-model="question.scale">\
              <option value="2">2</option>\
              <option value="3">3</option>\
              <option value="4">4</option>\
              <option value="5">5</option>\
              <option value="6">6</option>\
              <option value="7">7</option>\
              <option value="8">8</option>\
              <option value="9">9</option>\
              <option value="10">10</option>\
            </select>\
            <i class="bar"></i>\
            </div>\
            <div class="col-sm-2">\
            <label class="text-dark">Shape</label>\
            <select class="custom-select"  name="shape" v-model="question.shape">\
              <option value="star">Star</option>\
              <option value="smile-o">Smiley</option>\
              <option value="heart">Heart</option>\
              <option value="thumbs-up">Thumb</option>\
            </select>\
            <i class="bar"></i>\
            </div>\
            <div class="col-sm-2">\
            <label class="text-dark">Color</label>\
            <input class="color" type="color" name="color" v-model="question.color">\
            </div>\
            <div class="col-sm-2">\
            <label v-bind:style="{fontSize:\'36px\',color:question.color,paddingTop:\'5px\'}"><i :class="\'fa fa-\'+question.shape"></i></label>\
            </div>\
            </div>\
            </div>'
})


//SLoadingBtn.start();
var surveyApp=new Vue({ el: '#addsurvey',
        data: function(){
          this.$http.get("/get-survey-categories").then((response)=>{
            this.datas = response.data;
          });
          if(surveyId){
            this.$http.get("/get-survey/"+surveyId).then((response)=>{
              this.survey = response.data;
            });
          }
            return{
              survey:{survey_type:'Community feedback',questions:[]},
              datas:'',
              error:false
            }
         },
        methods: {
          onQustType:function(value,indx){
            if(value=='radiobutton' ||value=='checkbox'){
              this.$set(this.survey.questions[indx],'options',[{},{}]);
            }else if(value=='starrating'){
              this.$set(this.survey.questions[indx],'scale',5);
              this.$set(this.survey.questions[indx],'shape','star');
              this.$set(this.survey.questions[indx],'color','#d0021b');
              
              this.$set(this.survey.questions[indx],'options',[])
            }else{
              this.$set(this.survey.questions[indx],'options',[])
            }
          },
          addQuestion:function(){
            if(this.survey.questions.length>=20){
              alert('Sorry! You can\'t add more question.');
              return false;
            }
            this.survey.questions.push({type:'textbox'});
          },
          addOption:function(indx){
            this.survey.questions[indx].options.push({});
          },
          removeQuestion:function(indx){
            this.survey.questions.splice(indx,1);
          },
          removeOption:function(qindx,indx){
            this.survey.questions[qindx].options.splice(indx,1);
          },
          saveSurvey:function(){
          var SLoadingBtn = Ladda.create(document.querySelector( '#save' ));
          surveyApp.error=false;
           if(!this.survey.title){
              this.$set(this.survey,'title','');
              surveyApp.error=true;
           }
           if(this.survey.questions.length>0){
            this.survey.questions.forEach((ques,indx) => {
                if(!ques.question){
                  surveyApp.$set(surveyApp.survey.questions[indx],'question','');
                  surveyApp.error=true;
                }
                if(surveyApp.survey.questions[indx].type=='checkbox' || surveyApp.survey.questions[indx].type=='radiobutton'){
                  surveyApp.survey.questions[indx].options.forEach((opt,OptIndx)=>{
                    if(!opt.text){
                      surveyApp.$set( surveyApp.survey.questions[indx].options[OptIndx],'text','');
                      surveyApp.error=true;
                    }
                  })
                }
            });
           }
           if(surveyApp.error){
              return false;
           }
          SLoadingBtn.start();
          this.$http.post('/save-survey',{
            data:surveyApp.survey
          }).then(function(response){
            SLoadingBtn.stop();
           // console.log(response.data);
            swal({
							title: '',
							text: response.data.msg,
							type: response.data.status,
							confirmButtonColor: '#4fa7f3',
							timer: 10000
						}).catch(swal.noop);
					if(response.data.status=='success'){
						window.location.href='/survey-list';
					}
          });
          }
         },
         computed: {
            options:function(indx){
              console.log(indx);
            }
         }
});
