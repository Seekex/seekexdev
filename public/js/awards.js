Vue.prototype.$http = axios

Vue.component('awards-list', {
  props: ['post'],
  template: '<div class="card-box m-b-10"><div class="table-box opport-box"><div class="table-detail"><div class="member-info"><h4 class="m-t-0"><b>{{post.title}}</b></h4></div></div><div class="table-detail"><div class="member-info">{{post.description|limitedStr}}</div></div><div class="table-detail"><p class="text-dark m-b-5"><b>{{post.from_date|dateFormat}}</b><span class="text-muted"></span></p></div><div class="table-detail table-actions-bar"><a href="javascript:void(0)" class="table-action-btn" @click="$parent.edit(post)"><i class="md md-edit"></i></a><a href="javascript:void(0)" class="table-action-btn" @click="$parent.delete(post)"><i class="md md-close"></i></a></div></div></div>'
});

Vue.filter('dateFormat', function (date) {
  if (!date) return ''
  return moment(date).format('MMMM Do YYYY');
})

Vue.filter('limitedStr', function (date) {
  if (!date) return ''
  var maxLength = 150;
  var trimmedString = date.substr(0, maxLength);
  trimmedString = trimmedString.substr(0, Math.min(trimmedString.length, trimmedString.lastIndexOf(" ")))
  return trimmedString;
})

var app=new Vue({ el: '#award_app',
        data: function(){
          this.$http.get("/get-awards").then((response)=>{
            this.datas = response.data.datas;
          });
            return{
              datas:'',
              award:{title:'',description:'',from_date:''}
            }
         },
         methods: {
           delete:function(data){
            this.$http.post('/delete-award',{
                data:{
                  id:data._id
                }
              }).then(function(response){
                  this.datas=response.datas;
                  swal({
                    title: '',
                    text: response.data.msg,
                    type: response.data.status,
                    confirmButtonColor: '#4fa7f3'
                  });
                  if(response.status=='success'){
                    this.$http.get("/get-awards").then((response)=>{
                      this.datas = response.datas;
                    });
                  }
              });
           },
           edit:function(data){
             app.award=data;
             $('#editAward').toggleClass('active');
           },
           saveEditData:function(){
             this.$http.post('/edit-awards',{
              data:app.award
            }).then(function(response){
              if(response.data.status=='success'){
                $('.right-card').removeClass('active');
              }
              swal({
                title: '',
                text: response.data.msg,
                type: response.data.status,
                confirmButtonColor: '#4fa7f3'
              });
              if(msg.status=='success'){
                $.get("/get-awards").then((response)=>{
                  this.datas = response.datas;
                });
              }
            });
            return false;
           }
         }
});

function addNewAward(){
	$('.right-card').toggleClass('active');
}
function cancel(){
	$('.right-card').removeClass('active');
}
$(function(){
	$('#dob').datepicker({format: "dd-mm-yyyy", endDate: '-18y'});
	$('.date').datepicker({format: "dd-mm-yyyy"});
});


var awardLoadingBtn = Ladda.create(document.querySelector( '#save_awards' ));
	$("#awards_data").submit(function(event) {
		var frm=$('#awards_data').serializeArray();
		$.ajax({
				  method: "POST",
				  url: "/save-awards",
				  data:frm,
				  beforeSend:function(){
					awardLoadingBtn.start();
					},
					headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
					}
				}).done(function( msg ) {
					awardLoadingBtn.stop();
					
					swal({
							title: '',
							text: msg.msg,
							type: msg.status,
							confirmButtonColor: '#4fa7f3',
							timer: 2000
						}).catch(swal.noop);
					if(msg.status=='success'){
						$('#awards_data')[0].reset();
						$('.right-card').toggleClass('active');
						$.get("/get-awards").then((response)=>{
							app.datas = response.datas;
						  });
					}
				}).fail(function( jqXHR, textStatus ) {  
					awardLoadingBtn.stop();
				});
		return false;		
	 });