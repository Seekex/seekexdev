$(function(a) {
    "use strict";
    a(".menu-toggle").click(function(e) {
        e.preventDefault();
        a("#sidebar-wrapper").toggleClass("active");
        a(".menu-toggle > .fa-bars, .menu-toggle > .fa-times").toggleClass("fa-bars fa-times");
        a(this).toggleClass("active");
        a('.menu-toggle-dashboard').toggle();
    })
})

function updateQueryStringParameter(uri, key, value) {
    var re = new RegExp("([?&])" + key + "=.*?(&|$)", "i");
    var separator = uri.indexOf('?') !== -1 ? "&" : "?";
    if (uri.match(re)) {
        return uri.replace(re, '$1' + key + "=" + value + '$2');
    } else {
        return uri + separator + key + "=" + value;
    }
}

function dateFormatter(date) {
    var date = new Date(date);
    var options = {
        weekday: 'long',
        year: 'numeric',
        month: 'short',
        day: 'numeric',
        hour: "numeric",
        minute: "numeric",
        second: "numeric",
        dayPeriod: "short"
    };
    return new Intl.DateTimeFormat('en-IN', options).format(date)
}