Vue.prototype.$http = axios
//import VueRateIt from 'vue-star-rating';
//Vue.component('star-rating', VueStarRating.default);


Vue.component('question-item', {
	template: '<div class="quest">\
						<span v-html="index+1"></span>\
						<p v-html="question.question"></p>\
						<div v-if="question.type==\'textbox\' " class="form-group-custom">\
							<input type="text" class="answer" name="answer" value="" v-model="question.answer" required />\
              <label class="control-label text-dark">Write Answer </label><i class="bar"></i>\
              <span v-if="question.required && (question.answer==null || question.answer==\'\')"  class="err-msg">Answer is required.</span>\
            </div>\
            <div v-if="question.type==\'commentbox\' " class="form-group-custom">\
							<textarea class="answer" name="answer" value="" v-model="question.answer" /></textarea>\
              <label class="control-label text-dark">Write Answer </label><i class="bar"></i>\
              <span v-if="question.required && (question.answer==null || question.answer==\'\')"  class="err-msg">Answer is required.</span>\
            </div>\
            <div v-if="question.type==\'checkbox\'" class="form-group-custom">\
            <div class="checkbox checkbox-purple" style="display: flex;" v-for="item in question.options">\
            <input class="required-checkbox" type="checkbox" :value="item.text" v-model="question.answer">\
            <label>{{item.text}}</label>\
            </div>\
            <span v-if="question.required && (question.answer==null || question.answer==\'\')"  class="err-msg">Answer is required.</span>\
            </div>\
            <div v-if="question.type==\'radiobutton\'" class="form-group-custom">\
						<div class="radio radio-purple" style="display: flex;" v-for="item in question.options">\
            <input class="required-radio" type="radio" name="option" :value="item.text" v-model="question.answer">\
            <label>{{item.text}}</label>\
            </div>\
            <span v-if="question.required && (question.answer==null || question.answer==\'\')"  class="err-msg">Answer is required.</span>\
            </div>\
            <div v-if="question.type==\'starrating\'" class="form-group-custom" style="display: flex;">\
            <div class="star-rating">\
            <fa-rating :item-size="30" :glyph="VueRateIt.Glyphs[question.shape]" :max-rating="parseInt(question.scale)" :active-color="question.color" v-model="question.answer" :show-rating="false"></fa-rating>\
            <span v-if="question.required && (question.answer==null || question.answer==\'\')"  class="err-msg">Answer is required.</span>\
            </div>\
            </div>\
						</div>',
  props: ['question','index']
})


var surveyApp=new Vue({ el: '#question_list',
        data: function(){
				return{
            datas:'',
            answers:[],
            error:false,
            submitBtn:true,
            user:{name:'',email:'',phone:''}
					}
        },
        created(){

        },
        methods: {
					submit:function(){
            surveyApp.error=false;
            this.datas.questions.forEach((element,indx) => {
                if(element.required && (element.answer==null || element.answer=='')){
                  surveyApp.error=true;
                }
              surveyApp.answers.push({answer:element.answer,type:element.type})
            });
            if(surveyApp.error){
              return false;
            }
            $('#UserModalCenter').modal();
          },
          validEmail: function (email) {
            var re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return re.test(email);
          },
          submitSurvey:function(){
            if(this.user.name==''){
              return false;
            }
            if(this.user.email=='' || !this.validEmail(this.user.email)){
              return false;
            }
            var SLoadingBtn = Ladda.create(document.querySelector( '#submit'));
            SLoadingBtn.start();
            this.$http.post('/save-survey-answer',{
              data:{answers:surveyApp.answers,user:surveyApp.user}
            }).then(function(response){
              SLoadingBtn.stop();
            if(response.data.status=='success'){
              window.location.href='/survey/thanks';
            }
            });
            //console.log('final data',data);
          }
				},
				computed: {

        },
        mounted:function(){
          this.$http.get("/get-survey/"+surveyId).then((response)=>{
						response.data.questions.map((elm,indx)=>{
              if(elm.type=='checkbox'){
                this.$set(elm,'answer',[]);
              }else{
                this.$set(elm,'answer',null);
              }            
            });
            this.datas=response.data
					});
        }
});
$('#UserModalCenter').on('shown.bs.modal', function (e) {
  surveyApp.submitBtn=false;
})
$('#UserModalCenter').on('hidden.bs.modal', function (e) {
  surveyApp.submitBtn=true;
})