import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class Users extends BaseSchema {
  protected tableName = 'users'

  public async up () {
    this.schema.createTable(this.tableName, (table) => {
      table.increments('id')
      table.boolean('expert')
      table.boolean('verified')
	    table.boolean('requested')
	    table.boolean('emailverified')
	    table.boolean('mobileverified')	
	    table.string('name')
	    table.string('email')
	    table.string('contactId')
	    table.string('mobile')
	    table.string('genderId')
      table.string('profilePic')
	    table.string('password')
      table.string('dob')
      table.text('aboutMe')
      table.bigInteger('age')
      table.bigInteger('views')
      table.float('audioCallRate')
      table.float('videoCallRate')
      table.boolean('show_mobile')
      table.integer('countryCode')
      table.bigInteger('incomingCallSetting')
	    table.boolean('available')
      table.string('resume')
      table.string('intro')
      table.boolean('expertmode')
      table.bigInteger('followers')
      table.bigInteger('following')
      table.bigInteger('conversations')
      table.string('remember_me_token')
      table.timestamps(true)
    })
  }

  public async down () {
    this.schema.dropTable(this.tableName)
  }
}
