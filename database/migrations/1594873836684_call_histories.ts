import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class CallHistories extends BaseSchema {
  protected tableName = 'call_histories'

  public async up () {
    this.schema.createTable(this.tableName, (table) => {
      table.increments('id')
      table.integer('requestId')
      table.dateTime('callTime')
      table.dateTime('startTime')
      table.dateTime('endTime')
      table.string('initiaterFeedback')
      table.float('initiaterRating')
      table.bigInteger('initiater')
      table.bigInteger('receiver')
      table.string('receiverFeedback')
      table.float('receiverRating')
      table.string('recordingUrl')
      table.integer('endedBy')
      table.string('endedReason')
      table.float('rate')
      table.float('duration')
      table.float('estimatedDuration')
      table.boolean('video')
      table.boolean('isigst')
      table.timestamps(true)
    })
  }

  public async down () {
    this.schema.dropTable(this.tableName)
  }
}
