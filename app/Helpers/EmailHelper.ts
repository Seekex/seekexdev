import Mail from '@ioc:Adonis/Addons/Mail'
import Env from '@ioc:Adonis/Core/Env'
const From=Env.get('FROM_EMAIL') as string
import Application from '@ioc:Adonis/Core/Application'
import EmailContent from 'App/Models/EmailContent'
import WebServiceLog from 'App/Models/WebServiceLog'
import SendNewsletter from 'App/Models/SendNewsletter'
const replaceContent = (str,obj)=>{
  var retStr = str
      retStr =retStr.replace(/\{{(.*?)\}}/g, function(match, property) {
        //  console.log('prop',property)
            return obj[property]
        })
  return retStr
}

export default class EmailHelper {
  
  async sendEmail(data){
    try {
      await Mail.sendLater((message) => {
        message
          .from(From)
          .to(data.to)
          .subject(data.subject)
          .htmlView('emails/common', {data})
      })
    }catch(error) {
      console.log('email send error',error)
    }
  }

  async sendRegEmail(udata){
    const emailContent:any = await EmailContent.query().where({slug:'registration'}).first()
    const to = udata?.email
    const subject = replaceContent(emailContent?.subject,{name:udata?.name,email:udata?.email})
    const appImage = '<img src="http://seekex.in/html/images/resource/mokeup-1.png">'
    const message = replaceContent(emailContent?.content,{name:udata?.name,email:udata?.email,images:appImage,appLink:'',policyLink:'',pcPercentage:'50%',yourProfile:'your profile'})
    const data={subject:subject,message:message}
    // console.log('subject',subject)
    // console.log('mes',message)
    // return 
    try {
      await Mail.send((message) => {
        message
          .from(From,'Seekex')
          .to(to,udata?.name)
          .subject(subject)
          .htmlView('emails/common', {data})
      })
    }catch(error) {
      console.log('email send error',error)
    }
  }
  async sendEmailInvoice(data,file){
    try {
      const opt={filename:file}
      await Mail.sendLater((message) => {
        message.attach(Application.publicPath(file),opt)
              .from(From)
              .to(data.to)
              .subject(data.subject)
              .htmlView('emails/invoice', {data})
          })
    } catch (error) {
      console.log('email send error',error)
    }
  }

  async sendOtpEmail(data:any){
    const subject='Seekex OTP'
    if(!data?.name){
      data.name=''
    }
    try{
      await Mail.send((message) => {
        message
          .from(From,'Seekex')
          .to(data?.email,data?.name)
          .subject(subject)
          .htmlView('emails/reset', {data})
      })
      return true
    }catch(error) {
      //console.log('email send error',error)
      await WebServiceLog.create({request:JSON.stringify(data),response:JSON.stringify(error),service_name:'Send Otp Email Error',status:1,uid:data?.id,time_taken:0})
      return false
    }
  }

  async sendExpertApprovalEmail(data){
    const emailContent:any = await EmailContent.query().where({slug:'expert-approved'}).first()
    const to = data?.expertEmail
    const subject = replaceContent(emailContent?.subject,data)
    const message = replaceContent(emailContent?.content,data)
    const emaildata={subject:subject,message:message}
    try{
      await Mail.sendLater((message) => {
        message
          .from(From,'Seekex')
          .to(to,data?.expertName)
          .subject(subject)
          .htmlView('emails/common', {data:emaildata})
      })
      return true
    }catch(error) {
      await WebServiceLog.create({request:JSON.stringify(data),response:JSON.stringify(error),service_name:'Send Expert Approval Email',status:1,uid:data?.id,time_taken:0})
      return error
    }
  }

  async sendScheduleACallForExpertEmail(data){
    const emailContent:any = await EmailContent.query().where({slug:'schedule-a-call-for-expert'}).first()
    const to = data?.expertEmail
    const subject = replaceContent(emailContent?.subject,data)
    const message = replaceContent(emailContent?.content,data)
    const emaildata={subject:subject,message:message}
    try {
      await Mail.send((message) => {
        message
          .from(From,'Seekex')
          .to(to,data?.expertName)
          .subject(subject)
          .htmlView('emails/common', {data:emaildata})
      })
    }catch(error) {
      await WebServiceLog.create({request:JSON.stringify(data),response:JSON.stringify(error),service_name:'Send Schedule A Call For Expert Email Error',status:1,uid:data?.id,time_taken:0})
      console.log('email send error',error)
    }
  }

  async sendScheduleACallForSeekerEmail(data){
    const emailContent:any = await EmailContent.query().where({slug:'schedule-a-call-for-seeker'}).first()
    const to = data?.seekerEmail
    const subject = replaceContent(emailContent?.subject,data)
    const message = replaceContent(emailContent?.content,data)
    const emaildata={subject:subject,message:message}
    try {
      await Mail.send((message) => {
        message
          .from(From,'Seekex')
          .to(to,data?.seekerName)
          .subject(subject)
          .htmlView('emails/common', {data:emaildata})
      })
    }catch(error) {
      await WebServiceLog.create({request:JSON.stringify(data),response:JSON.stringify(error),service_name:'Send Schedule A Call For Seeker Email Error',status:1,uid:data?.id,time_taken:0})
      console.log('email send error',error)
    }
  }

  async sendRefundRejectEmail(data){
    const emailContent:any = await EmailContent.query().where({slug:'refund-rejection'}).first()
    const to = data?.seekerEmail
    const subject = replaceContent(emailContent?.subject,data)
    const message = replaceContent(emailContent?.content,data)
    const emaildata={subject:subject,message:message}
    try{
      await Mail.sendLater((message) => {
        message
          .from(From,'Seekex')
          .to(to,data?.seekerName)
          .subject(subject)
          .htmlView('emails/common', {data:emaildata})
      })
      return true
    }catch(error) {
      await WebServiceLog.create({request:JSON.stringify(data),response:JSON.stringify(error),service_name:'Send Refund Reject Email',status:1,uid:data?.id,time_taken:0})
      return error
    }
  }


  async sendUserPasswordEmail(data){
    const emailContent:any = await EmailContent.query().where({slug:'forgot-password'}).first()
    const to = data?.email
    const subject = replaceContent(emailContent?.subject,data)
    const message = replaceContent(emailContent?.content,data)
    const emaildata={subject:subject,message:message}
    try{
      await Mail.sendLater((message) => {
        message
          .from(From,'Seekex')
          .to(to,data?.name)
          .subject(subject)
          .htmlView('emails/common', {data:emaildata})
      })
      return true
    }catch(error) {
      await WebServiceLog.create({request:JSON.stringify(data),response:JSON.stringify(error),service_name:'Send User Password Email',status:1,uid:data?.id,time_taken:0})
      return error
    }
  }

  async sendNewsletter(data,emails){
    const subject = data?.title
    const messageContent:any = data?.content
    if(emails.length>0){
      for await (const item of emails) {
        await Mail.sendLater((message) => {
          message
            .from(From,'Seekex')
            .to(item)
            .subject(subject)
            .html(messageContent)
        })
      }
      await SendNewsletter.query().where('id',data?.id).update({output:'all email sent',totle:emails?.length})
    }
  }
}