import Application from '@ioc:Adonis/Core/Application'
const fs = require('fs');
const filesize = require('filesize')
import AppMedia from 'App/Models/AppMedia'
import WebServiceLog from 'App/Models/WebServiceLog'
import Env from '@ioc:Adonis/Core/Env'
const AWS = require('aws-sdk');
const request = require('request')
//const END_POINT_URL=Env.get('AWS_EndpointUrl') as string
const KEY=Env.get('AWS_AccessKey') as string
const SECRET=Env.get('AWS_SecretKey') as string
const BUCKET_NAME =Env.get('BucketName') as string
const s3 = new AWS.S3({
  accessKeyId: KEY,
  secretAccessKey: SECRET
});
const fileName = (url)=>{
  return (url = url.substr(1 + url.lastIndexOf("/")).split('?')[0])
}
const Accomplishments_Detail=1
const Certificates_Detail=2
const Projects_Detail=3
const REQUEST_MEDIA=4
const POSTS_MEDIA=5
const PROFILE_MEDIA=7
const PROFILE_RESUME=8
const PROFILE_INTRO=9
const CHAT_IMAGE=10
const CHAT_AUDIO=11
const CHAT_VIDEO=12
const CHAT_FILE=13
const CHAT_MESSAGE=14
const RECORDINGS_PATHS=15

export default class UploadHelper {

  public async uploadFile(file,contentType,uid,mtype,pid,folder){
    const fileContent = fs.readFileSync(Application.publicPath(file));
    const params = {
      Bucket: BUCKET_NAME,
      Key: folder+'/'+file, // File name you want to save as in S3
      Body: fileContent,
      ACL: "public-read",
      ContentType:contentType
  };
    s3.upload(params, async function(err, res) {
      if (err) {
        await WebServiceLog.create({request:JSON.stringify(params),response:JSON.stringify(err),service_name:'Upload On S3 Service',status:1,uid:uid,time_taken:1})
        return false
      }
      if(res.Location){
        var mediaData={media_for:pid,media_type:mtype,url:res.Location}
        await AppMedia.create(mediaData)
      }
      await WebServiceLog.create({request:JSON.stringify({media_for:pid,media_type:mtype,file:file}),response:JSON.stringify(res),service_name:'Upload On S3 Service',status:1,uid:uid,time_taken:1})
      //console.log(`File uploaded successfully. ${res.Location}`)
      return true
    });
  }

  public async uploadChatFile(file,contentType,uid,mtype,pid,folder){
    const fileContent = fs.readFileSync(Application.tmpPath('uploads/'+file));
    const params = {
      Bucket: BUCKET_NAME,
      Key: folder+'/'+file, // File name you want to save as in S3
      Body: fileContent,
      ACL: "public-read",
      ContentType:contentType
  };
    try {
      var uploadFile = await s3.upload(params).promise()
      return uploadFile
    } catch (error) {
      await WebServiceLog.create({request:JSON.stringify({media_for:pid,media_type:mtype,file:file}),response:JSON.stringify(error),service_name:'Upload On S3 Done',status:1,uid:uid,time_taken:1})
      return false
    }
  }

  public async uploadFileToTemp(file,contentType,folder,uid){
    const fileContent = fs.readFileSync(Application.tmpPath('uploads/'+file));
    const newFopt= contentType=='audio'?'recordings':folder
    const params = {
      Bucket: BUCKET_NAME,
      Key: newFopt+'/'+file, // File name you want to save as in S3
      Body: fileContent,
      ACL: "public-read",
      ContentType:contentType=='audio'?'audio/x-wav':contentType
    }
    try {
      var uploadFile = await s3.upload(params).promise()
      return uploadFile
    } catch (error) {
      await WebServiceLog.create({request:JSON.stringify({file:file}),response:JSON.stringify(error),service_name:'Upload On S3 temp Done',status:1,uid:uid,time_taken:1})
      return false
    }
  }

  async deleteFile(folder,file){
    const params = {
      Bucket: BUCKET_NAME,
      Key: folder+'/'+file
    }
    try {
      var deleteFile = await s3.deleteObject(params).promise()
      return deleteFile
    } catch (error) {
      await WebServiceLog.create({request:JSON.stringify({file:file}),response:JSON.stringify(error),service_name:'Upload On S3 temp Done',status:1,uid:0,time_taken:0})
      return false
    }
  }

  async getFileSize(file){
    var stats = await fs.statSync(file)
    var fileSize =  filesize(stats.size, {round: 0})
    return fileSize
  }

  async uploadRecording(file,folder,uid){
    const fileContent = fs.readFileSync(Application.publicPath('recordings/'+file));
    const params = {
      Bucket: BUCKET_NAME,
      Key: folder+'/'+file, // File name you want to save as in S3
      Body: fileContent,
      ACL: "public-read",
      ContentType:'audio/x-wav'
    }
    try {
      var uploadFile = await s3.upload(params).promise()
      return uploadFile
    } catch (error) {
      await WebServiceLog.create({request:JSON.stringify({file:file}),response:JSON.stringify(error),service_name:'Upload On S3 Recording file Done',status:1,uid:uid,time_taken:1})
      return false
    }
  }

  async uploadRecordingExotel(file,folder,uid){
    const fileContent = fs.readFileSync(Application.publicPath('recordings/exotel/'+file));
    const params = {
      Bucket: BUCKET_NAME,
      Key: folder+'/'+file, // File name you want to save as in S3
      Body: fileContent,
      ACL: "public-read",
      ContentType:'audio/x-wav'
    }
    try {
      var uploadFile = await s3.upload(params).promise()
      return uploadFile
    } catch (error) {
      await WebServiceLog.create({request:JSON.stringify({file:file}),response:JSON.stringify(error),service_name:'Upload On S3 Recording file Done',status:1,uid:uid,time_taken:1})
      return false
    }
  }

  public async uploadFileToFolder(file,contentType,folder){
    const fileContent = fs.readFileSync(Application.publicPath((folder+'/'+file)))
    const params = {
      Bucket: BUCKET_NAME,
      Key: folder+'/'+file, // File name you want to save as in S3
      Body: fileContent,
      ACL: "public-read",
      ContentType:contentType
    }
    try {
      var uploadFile = await s3.upload(params).promise()
      return uploadFile
    } catch (error) {
      await WebServiceLog.create({request:JSON.stringify({file:file}),response:JSON.stringify(error),service_name:'Upload image On S3 folder error',status:1,uid:0,time_taken:1})
      return false
    }
  }
}