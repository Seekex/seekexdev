import Application from '@ioc:Adonis/Core/Application'
import { exec } from 'child_process'
import User from 'App/Models/User';
export default class UtilityHelper {
  
  async restartPm2Server(){
    const cmd = new Promise( function(resolve,reject) {
      exec("pm2 status", (error, stdout, stderr) => {
        if (error) {
            console.log(`error: ${error.message}`);
            reject(error)
            return;
        }
        if (stderr) {
            console.log(`stderr: ${stderr}`);
            reject(stderr)
            return;
        }
        console.log(`stdout: ${stdout}`)
        resolve(stdout)
      })
    })
    return cmd
  }
}