import * as admin from 'firebase-admin'
import WebServiceLog from 'App/Models/WebServiceLog'
import AppNotification from 'App/Models/AppNotification'
import LoginHistory from 'App/Models/LoginHistory'
const perf = require('execution-time')()
import FirebaseHelper from 'App/Helpers/FirebaseHelper'
import Post from 'App/Models/Post'
const firebaseHelper = new FirebaseHelper()
import truncatise from 'truncatise'
import User from 'App/Models/User'
import FollowPost from 'App/Models/FollowPost'
import Database from '@ioc:Adonis/Lucid/Database'
import Comment from 'App/Models/Comment'
import UserVideo from 'App/Models/UserVideo'
import VideoComment from 'App/Models/VideoComment'
import LedgerAccount from 'App/Models/LedgerAccount'
import AudienceCampaign from 'App/Models/AudienceCampaign'
import AppUserEvent from 'App/Models/AppUserEvent'
const replaceContent = (str,obj)=>{
  var retStr = str
      retStr =retStr.replace(/\{{(.*?)\}}/g, function(match, property) {
        //  console.log('prop',property)
            return obj[property]
        })
  return retStr
}
const NOTIFICATION_TYPE_LOGOUT = 1
const NOTIFICATION_TYPE_CALL = 2
const NOTIFICATION_TYPE_STATUS_OFFLINE = 3
const NOTIFICATION_TYPE_CHAT = 4
const NOTIFICATION_TYPE_STATUS_CHANGED = 5
const NOTIFICATION_TYPE_POST = 6
const NOTIFICATION_TYPE_REQUEST = 7
const NOTIFICATION_TYPE_LIKE_POST = 8
const NOTIFICATION_TYPE_COMMENT = 9
const NOTIFICATION_TYPE_FOLLOW = 10
const NOTIFICATION_TYPE_RATING = 11
const NOTIFICATION_TYPE_RECORDING_READY = 12
const NOTIFICATION_TYPE_USER_BUSY = 13
const NOTIFICATION_TYPE_TRANSACTION = 14
const NOTIFICATION_TYPE_REFUND = 15
const NOTIFICATION_TYPE_REPORT = 16
const NOTIFICATION_TYPE_APPROVE = 17
const NOTIFICATION_TYPE_FEATURED_EXPERT = 18
const NOTIFICATION_TYPE_LIKE_COMMENT = 19
const NOTIFICATION_TYPE_USER_BUSY_CALL = 20
const NOTIFICATION_TYPE_NEW_DEVICE = 21
const NOTIFICATION_INCOMPLETE_EXPERT = 22
const NOTIFICATION_TYPE_PROMO = 23
const NOTIFICATION_TYPE_LIKE_VIDEO = 24
const NOTIFICATION_TYPE_FOLLOW_VIDEO = 25
const NOTIFICATION_TYPE_LIKE_VIDEO_COMMENT = 26
const NOTIFICATION_TYPE_VIDEO_COMMENT = 27
const NOTIFICATION_TYPE_RATING_POUP = 28
const seekexIcon = 'https://seekex.in/front/web/simages/aboutus/175x175%20logo.png'
const authKey = "AAAAtK_aByM:APA91bHvyNiokQq9rHVvHgxEXmRPxGijzWSLCrz-UJe7Jj0pQEcOSkeP4MnoKZiAqqHR3ryrhGPG-YxQgYmLL_q1tLeAZ0-_EpMt9mytPnOncL70SpBmJhDoHr-30TdhFYPm40RNz6r6" // You FCM AUTH key
const fmcurl = "https://fcm.googleapis.com/fcm/send"
const HomeActivity="com.seekx.module.home.HomeActivity"

export default class NotificationHelper {
   
  async sendNotifiction(msg,to){
    const status = await firebaseHelper.sendNotifiction(msg,to)
    return status
  }


  async sendNotifictionForCall(data,fromUser){
    perf.start()
    const payload:any={}
    const info:any={}
   // const fcm = data?.receiverFcm||data?.fcm
    if(data?.receiver==fromUser?.id){
      var receiverFcm = await LoginHistory.query().where("uid",data?.initiater).orderBy("id","desc").first()
      const tTime = perf.stop()
      await WebServiceLog.create({request:JSON.stringify('Call from Expert'),response:JSON.stringify(receiverFcm),service_name:'Send Notification From '+fromUser.name,status:1,uid:fromUser?.id,time_taken:tTime.time})
    }else{
      var receiverFcm = await LoginHistory.query().where("uid",data?.receiver).orderBy("id","desc").first()
      const tTime = perf.stop()
      await WebServiceLog.create({request:JSON.stringify('Call from User'),response:JSON.stringify(receiverFcm),service_name:'Send Notification From '+fromUser.name,status:1,uid:fromUser?.id,time_taken:tTime.time})
    }
   
  // console.log(receiverFcm)
  // return receiverFcm
    const fcm = receiverFcm?.fcmToken
    payload.callDto=data
    payload.activity="com.seekx.webrtcSdk.call.CallActivity"
    payload.profile=fromUser?fromUser:{}
    info.title='Incoming Call Notification'
    info.body = 'Incoming Call Notification From '+fromUser.name
    info.payload=JSON.stringify(payload)
    info.type=''+NOTIFICATION_TYPE_CALL
    info.room_id=data?.id+'_room'
    info.sound ="default"
    const notifId= await this.insertNotification(info,fromUser?.id,NOTIFICATION_TYPE_CALL,data?.id)
    info.notification_id=''+notifId
    info.priority='high'
    try {
      return await this.sendNotifiction(info,fcm)
    } catch (error) {
      const tTime = perf.stop()
      await WebServiceLog.create({request:JSON.stringify(info),response:JSON.stringify(error),service_name:'Send Notification Service error',status:1,uid:0,time_taken:tTime.time})
      return false
    }
  }

  async sendNotifictionForBussy(data,fromUser){
    const payload:any={}
    const info:any={}
    payload.callDto=data
    payload.profile=fromUser?fromUser:{}
    info.title='Incoming Call'
    info.body = 'Incoming Call From '+fromUser.name
    info.payload=payload
    info.type=NOTIFICATION_TYPE_USER_BUSY
    const notifId= await this.insertNotification(info,fromUser?.id,NOTIFICATION_TYPE_USER_BUSY,data?.id)
    info.notification_id=notifId
    try {
     return  await this.sendNotifiction(info,data?.receiverFcm)
    } catch (error) {
      await WebServiceLog.create({request:JSON.stringify(info),response:JSON.stringify(error),service_name:'Send Notification Service',status:1,uid:0,time_taken:0})
    }
  }

  async sendNotifictionForRefundByAdmin(data,uid){
    //var notifyUser= await LoginHistory.query().where("uid",uid).orderBy("id","desc").first()
    var notifyUser= await LoginHistory.getUserFcm(uid)
    // return notifyUser
     const fcm = notifyUser?.fcmToken
     const payload={}
     const info:any={}
     info.title='Refund Approved'
     info.body = 'Kudos! We have approved your refund request for call id: '+data?.id
     info.payload=JSON.stringify(payload)
     info.type=''+NOTIFICATION_TYPE_REFUND
     info.sound ="default"
     const notifId= await this.insertNotification(info,uid,NOTIFICATION_TYPE_REFUND,data?.id)
     info.notification_id=''+notifId
     info.priority='high'
     //return info
     try {
       const tTime = perf.stop()
       const notify = await firebaseHelper.sendNotifictionNew(info,fcm)
       await WebServiceLog.create({request:JSON.stringify(info),response:JSON.stringify(fcm+'=>Status:'+notify),service_name:'Send Notification Service success for refund approved by expert',status:1,uid:0,time_taken:tTime.time})
      return true
     } catch (error) {
       const tTime = perf.stop()
       await WebServiceLog.create({request:JSON.stringify(info),response:JSON.stringify(error),service_name:'Send Notification Service error for refund approved by expert',status:1,uid:0,time_taken:tTime.time})
       return false
     }
  }

  async sendNotifictionForApplyRefund(data,fromUser){
    //return data
    //var notifyUser= await LoginHistory.query().where("uid",data?.receiver).orderBy("id","desc").first()
    var notifyUser= await LoginHistory.getUserFcm(data?.receiver)
   // return notifyUser
    const fcm = notifyUser?.fcmToken
    const payload:any={}
    const info:any={}
    info.title='Refund Applied'
    info.body = 'Ooo!Oh! '+fromUser?.name+' has applied for refund for call id : '+data?.id
    payload.activity= "com.seekx.module.home.HomeActivity"
    payload.fragment= "callDetails"
    info.payload=JSON.stringify(payload)
    info.type=''+NOTIFICATION_TYPE_REFUND
    info.sound ="default"
    const notifId= await this.insertNotification(info,data?.receiver,NOTIFICATION_TYPE_REFUND,data?.id)
    info.notification_id=''+notifId
    info.priority='high'
    //return info
    try {
     return await firebaseHelper.sendNotifictionNew(info,fcm)
    } catch (error) {
      const tTime = perf.stop()
      await WebServiceLog.create({request:JSON.stringify(info),response:JSON.stringify(error),service_name:'Send Notification Service error',status:1,uid:0,time_taken:tTime.time})
      return false
    }
  }

  async sendNotifictionForApproveRefund(data,fromUser,amount){
    //var notifyUser= await LoginHistory.query().where("uid",data?.initiater).orderBy("id","desc").first()
    var notifyUser= await LoginHistory.getUserFcm(data?.initiater)
   // return notifyUser
    const fcm = notifyUser?.fcmToken
    const payload={}
    const info:any={}
    info.title='Refund Approved'
    info.body = 'Kudos! '+fromUser?.name+' has acknowledged your refund request worth '+amount+' and it has now been approved. '
    info.payload=JSON.stringify(payload)
    info.type=''+NOTIFICATION_TYPE_REFUND
    info.sound ="default"
    const notifId= await this.insertNotification(info,data?.initiater,NOTIFICATION_TYPE_REFUND,data?.id)
    info.notification_id=''+notifId
    info.priority='high'
    //return info
    try {
      const tTime = perf.stop()
      const notify = await firebaseHelper.sendNotifictionNew(info,fcm)
      await WebServiceLog.create({request:JSON.stringify(info),response:JSON.stringify(fcm+'=>Status:'+notify),service_name:'Send Notification Service success for refund approved by expert',status:1,uid:0,time_taken:tTime.time})
     return true
    } catch (error) {
      const tTime = perf.stop()
      await WebServiceLog.create({request:JSON.stringify(info),response:JSON.stringify(error),service_name:'Send Notification Service error for refund approved by expert',status:1,uid:0,time_taken:tTime.time})
      return false
    }
  }

  async sendTestNotification(){
    const info:any={}
    const fcm = 'csi-YJA5QciOZ9W_VnSbsa:APA91bFiXB6Hxa0tK4E3oe8zfsJcDqjh65z7Pn1yUvVmEb7Q6NzxcX50TQ9c4BSG4srkAd6NHZPXYlTusZkHiFFx97ionKGaWoSQgfWjHrpegTWLcZWlsAXxoVdXRlQs_lpU4vKsOfcl'
    info.title='Test notification'
    info.body = 'Incoming Call From User '
    info.payload={}
    info.type=1
    info.notification_id=12

    try {
      return await this.sendNotifiction(info,fcm)
    } catch (error) {
      console.log(error)
    }
  }

  async insertNotification(info,uid,type,targetId=0){
      const appNotification:any=new AppNotification()
      appNotification.status=false
      appNotification.type=type
      appNotification.uid=uid
      appNotification.targetId=targetId||0
      appNotification.title=info.title
      appNotification.message=info.body
      appNotification.payload=JSON.stringify(info)
      await appNotification.save()
      return appNotification?.id
  }

  async sendNotifictionForRequest(data,fromUser){
    const bookingDate=new Date(data?.date)
    const date= bookingDate.getDate()+'-'+bookingDate.getMonth()+'-'+bookingDate.getFullYear()
    //var notifyUser= await LoginHistory.query().where("uid",data?.toUid).orderBy("id","desc").first()
    var notifyUser= await LoginHistory.getUserFcm(data?.toUid)
    const payload:any={}
    const info:any={}
    payload.profile=fromUser?fromUser:{}
    info.title='Request Received'
    info.body = 'Kudos! '+fromUser?.name+' booked an appointment with you for '+date
    payload.activity='com.seekx.module.home.HomeActivity'
    payload.fragment='RequestFragment'
    payload.tabType=1
    payload.postReqId=data?.id
    info.payload=payload
    info.type=NOTIFICATION_TYPE_REQUEST
    const notifId= await this.insertNotification(info,data?.toUid,NOTIFICATION_TYPE_REQUEST,data?.id)
    info.notification_id=notifId
    try {
     return  await firebaseHelper.sendNotifictionNew(info,notifyUser?.fcmToken)
    } catch (error) {
      await WebServiceLog.create({request:JSON.stringify(info),response:JSON.stringify(error),service_name:'Send Notification Service error from request',status:1,uid:0,time_taken:0})
    }
  }

  async sendNotifictionForAcceptRequest(data,fromUser){
    const payload:any={}
    const info:any={}
   // payload.callDto=data
    payload.profile=fromUser?fromUser:{}
    payload.activity=HomeActivity
    info.title='Request has been accepted'
    info.body = fromUser.name+' has confirmed your appointment'
    payload.activity='com.seekx.module.home.HomeActivity'
    payload.fragment='RequestFragment'
    payload.tabType=2
    payload.postReqId=data?.id
    info.payload=payload
    info.type=NOTIFICATION_TYPE_REQUEST
    const notifId= await this.insertNotification(info,data?.toUid,NOTIFICATION_TYPE_REQUEST,data?.id)
    info.notification_id=notifId
    try {
      return  await firebaseHelper.sendNotifictionNew(info,data?.fcm)
     } catch (error) {
       await WebServiceLog.create({request:JSON.stringify(info),response:JSON.stringify(error),service_name:'Send Notification Service error from request Accepted',status:1,uid:0,time_taken:0})
     }
  }

  async sendNotifictionForCancelRequest(data,fromUser){
    const payload:any={}
    const info:any={}
    payload.activity=HomeActivity
    payload.profile=fromUser?fromUser:{}
    info.title='Request has been accepted'
    info.body = 'Ooo!Oh! '+fromUser?.name+' cancelled a booked slot for '+data?.date
    payload.activity='com.seekx.module.home.HomeActivity'
    payload.fragment='RequestFragment'
    payload.tabType=3
    payload.postReqId=data?.id
    info.payload=payload
    info.type=NOTIFICATION_TYPE_REQUEST
    const notifId= await this.insertNotification(info,fromUser?.id,NOTIFICATION_TYPE_REQUEST,data?.id)
    info.notification_id=notifId
    try {
      return  await firebaseHelper.sendNotifictionNew(info,data?.fcm)
     } catch (error) {
       await WebServiceLog.create({request:JSON.stringify(info),response:JSON.stringify(error),service_name:'Send Notification Service error from request Cancel',status:1,uid:0,time_taken:0})
     }
  }

  async sendNotifictionForChat(data,fromUser){
    const payload:any={}
    const info:any={}
    payload.profile=fromUser?fromUser:{}
    payload.chatDto=data?.chatMsg
    payload.activity="com.seekx.module.activities.ChatActivity"
    info.title=fromUser?.name
    info.body = data?.msg
    info.payload=payload
    info.type=NOTIFICATION_TYPE_CHAT
    const notifId= await this.insertNotification(info,data?.uid,NOTIFICATION_TYPE_CHAT,data?.id)
    info.notification_id=notifId
   // const notifyUser= await LoginHistory.query().where("uid",data?.uid).orderBy("id","desc").first()
    const notifyUser= await LoginHistory.getUserFcm(data?.uid)
    
    try {
      if(notifyUser?.fcmToken){
        const notify=await firebaseHelper.sendNotifictionNew(info,notifyUser?.fcmToken)
        data.notifyStatus=notify
      }
      await WebServiceLog.create({request:JSON.stringify(info),response:JSON.stringify(data),service_name:'Send Notification Service for chat',status:1,uid:0,time_taken:0})
     } catch (error) {
       await WebServiceLog.create({request:JSON.stringify(info),response:JSON.stringify(error),service_name:'Send Notification Service for chat error',status:1,uid:0,time_taken:0})
     }
  }

  async sendNotificationForLikesFollow(data,fromUser,type){
    const payload:any={}
    const info:any={}
    const post:any = await Post.find(data?.targetId)
    if(post?.createdBy==fromUser?.id){
      return
    }
    payload.profile=fromUser?fromUser:{}
    payload.activity=HomeActivity
    var notifyUser
    if(type =='1'){
       //notifyUser= await LoginHistory.query().where("uid",post?.createdBy).orderBy("id","desc").first()
      notifyUser= await LoginHistory.getUserFcm(post?.createdBy)
      info.title=fromUser?.name+' liked your post:'
      info.body = truncatise(post?.description, { TruncateLength: 15 })
      payload.activity="com.seekx.module.home.HomeActivity"
      payload.fragment='TimeLineFragment'
      payload.postReqId=post?.id
      info.payload=payload
      payload.type=8
      info.type=NOTIFICATION_TYPE_LIKE_POST
      const existNotify = await AppNotification.getExistNotify(post?.createdBy,NOTIFICATION_TYPE_LIKE_POST,data?.targetId)
      if(existNotify){
        return
      }
      const notifId= await this.insertNotification(info,post?.createdBy,NOTIFICATION_TYPE_LIKE_POST,data?.targetId)
      info.notification_id=notifId
    }
    if(type =='3'){
       //notifyUser= await LoginHistory.query().where("uid",post?.createdBy).orderBy("id","desc").first()
       notifyUser= await LoginHistory.getUserFcm(post?.createdBy)
      info.title=fromUser?.name+' followed your post:'
      info.body = truncatise(post?.description, { TruncateLength: 15 })
      payload.activity="com.seekx.module.home.HomeActivity"
      payload.fragment='TimeLineFragment'
      payload.postReqId=post?.id
      payload.type=8
     // payload.uid=fromUser?.id
      payload.tabType=2
      info.payload=payload
      info.type=NOTIFICATION_TYPE_FOLLOW
      const existNotify = await AppNotification.getExistNotify(post?.createdBy,NOTIFICATION_TYPE_FOLLOW,data?.targetId)
      if(existNotify){
        return
      }
      const notifId= await this.insertNotification(info,post?.createdBy,NOTIFICATION_TYPE_FOLLOW,data?.targetId)
      info.notification_id=notifId
    }
    if(type =='2'){
      var comment:any = await Comment.find(data?.targetId)
      if(comment?.uid==fromUser?.id){
        return
      }
      // notifyUser= await LoginHistory.query().where("uid",comment?.uid).orderBy("id","desc").first()
       notifyUser= await LoginHistory.getUserFcm(comment?.uid)
      info.title=fromUser?.name+' liked your Comment:'
      info.body = truncatise(comment?.comment, { TruncateLength: 15 })
      payload.activity="com.seekx.module.home.HomeActivity"
      payload.fragment='TimeLineFragment'
      payload.postReqId=post?.id
      payload.type=8
      info.payload=payload
      info.type=NOTIFICATION_TYPE_LIKE_COMMENT
      const existNotify = await AppNotification.getExistNotify(comment?.uid,NOTIFICATION_TYPE_LIKE_COMMENT,data?.targetId)
      if(existNotify){
        return
      }
      const notifId= await this.insertNotification(info,comment?.uid,NOTIFICATION_TYPE_LIKE_COMMENT,data?.targetId)
      info.notification_id=notifId
    }

    if(type =='4'){
      //notifyUser= await LoginHistory.query().where("uid",data?.targetId).orderBy("id","desc").first()
      notifyUser= await LoginHistory.getUserFcm(data?.targetId)
      info.title=fromUser?.name+' Followed your'
      info.body = 'Yaay! You have found a new follower in '+fromUser?.name
      payload.activity="com.seekx.module.home.HomeActivity"
      payload.fragment="ExpertFragment"
      payload.uid=fromUser?.id
      payload.tabType=2
      info.payload=payload
      info.type=NOTIFICATION_TYPE_FOLLOW
      const existNotify = await AppNotification.getExistNotify(data?.targetId,NOTIFICATION_TYPE_FOLLOW,data?.targetId)
      if(existNotify){
        return
      }
      const notifId= await this.insertNotification(info,data?.targetId,NOTIFICATION_TYPE_FOLLOW,data?.targetId)
      info.notification_id=notifId
    }
     
      try {
        const notify=await firebaseHelper.sendNotifictionNew(info,notifyUser?.fcmToken)
        data.notifyStatus=notify
        await WebServiceLog.create({request:JSON.stringify(info),response:JSON.stringify(data),service_name:'Send Notification Service for LikesFollow',status:1,uid:0,time_taken:0})
        return  
       } catch (error) {
         await WebServiceLog.create({request:JSON.stringify(info),response:JSON.stringify(error),service_name:'Send Notification Service for LikesFollow',status:1,uid:0,time_taken:0})
       }
  }

  async sendNotificationForComment(data,fromUser){
    const payload:any={}
    const info:any={}
    const post:any = await Post.find(data?.postId)
    if(post?.createdBy==fromUser?.id){
      return
    }
    //var notifyUser= await LoginHistory.query().where("uid",post?.createdBy).orderBy("id","desc").first()
    var notifyUser= await LoginHistory.getUserFcm(post?.createdBy)
    payload.profile=fromUser?fromUser:{}
    payload.activity=HomeActivity
    info.title=fromUser?.name+' commented on your post:'
    info.body = truncatise(post?.description, { TruncateLength: 15 })
    payload.fragment='TimeLineFragment'
    payload.postReqId=post?.id
    payload.type=9

    info.payload=payload
    info.type=NOTIFICATION_TYPE_COMMENT
    const notifId= await this.insertNotification(info,post?.createdBy,NOTIFICATION_TYPE_COMMENT,data?.id)
    info.notification_id=notifId
    try {
      const notify=await firebaseHelper.sendNotifictionNew(info,notifyUser?.fcmToken)
      data.notifyStatus=notify
      await WebServiceLog.create({request:JSON.stringify(info),response:JSON.stringify(data),service_name:'Send Notification Service for Comment',status:1,uid:0,time_taken:0})
      return  
     } catch (error) {
       await WebServiceLog.create({request:JSON.stringify(info),response:JSON.stringify(error),service_name:'Send Notification Service for Comment',status:1,uid:0,time_taken:0})
     }
  }

  async sendNotificationForLikesFollowVideo(data,fromUser,type){
    const payload:any={}
    const info:any={}
    payload.profile=fromUser?fromUser:{}
    payload.activity=HomeActivity
    data = data.serialize()
    var notifyUser
    if(type =='1'){
      const post:any = await UserVideo.find(data?.userVideoId)
      if(post?.createdBy==fromUser?.id){
        return
      }
      notifyUser= await LoginHistory.getUserFcm(post?.createdBy)
      info.title=fromUser?.name+' liked your video:'
      info.body = truncatise(post?.description, { TruncateLength: 15 })
      payload.activity="com.seekx.module.home.HomeActivity"
      payload.fragment='HomeFragment'
      payload.videoId=post?.id
      info.payload=payload
      payload.type=NOTIFICATION_TYPE_LIKE_VIDEO
      info.type=NOTIFICATION_TYPE_LIKE_VIDEO
      const existNotify = await AppNotification.getExistNotify(post?.createdBy,NOTIFICATION_TYPE_LIKE_VIDEO,data?.userVideoId||data?.targetId)
      if(existNotify){
        return
      }
      const notifId= await this.insertNotification(info,post?.createdBy,NOTIFICATION_TYPE_LIKE_VIDEO,data?.userVideoId||data?.targetId)
      info.notification_id=notifId
    }
    if(type =='3'){
      const post:any = await UserVideo.find(data?.userVideoId)
      if(post?.createdBy==fromUser?.id){
        return
      }
      notifyUser= await LoginHistory.getUserFcm(post?.createdBy)
      info.title=fromUser?.name+' followed your video:'
      info.body = truncatise(post?.description, { TruncateLength: 15 })
      payload.activity="com.seekx.module.home.HomeActivity"
      payload.fragment='HomeFragment'
      payload.videoId=post?.id
      payload.type=NOTIFICATION_TYPE_FOLLOW_VIDEO
     // payload.uid=fromUser?.id
      payload.tabType=2
      info.payload=payload
      info.type=NOTIFICATION_TYPE_FOLLOW_VIDEO
      const existNotify = await AppNotification.getExistNotify(post?.createdBy,NOTIFICATION_TYPE_FOLLOW_VIDEO,data?.userVideoId||data?.targetId)
      if(existNotify){
        return
      }
      const notifId= await this.insertNotification(info,post?.createdBy,NOTIFICATION_TYPE_FOLLOW_VIDEO,data?.userVideoId||data?.targetId)
      info.notification_id=notifId
    }
    if(type =='2'){
      var comment:any = await VideoComment.find(data?.targetId)
      if(comment?.uid==fromUser?.id){
        return
      }
      // notifyUser= await LoginHistory.query().where("uid",comment?.uid).orderBy("id","desc").first()
       notifyUser= await LoginHistory.getUserFcm(comment?.uid)
      info.title=fromUser?.name+' liked your comment for video:'
      info.body = truncatise(comment?.comment, { TruncateLength: 15 })
      payload.activity="com.seekx.module.home.HomeActivity"
      payload.fragment='HomeFragment'
      payload.videoId=comment?.userVideoId
      payload.type=NOTIFICATION_TYPE_LIKE_COMMENT
      info.payload=payload
      info.type=NOTIFICATION_TYPE_LIKE_COMMENT
      const existNotify = await AppNotification.getExistNotify(comment?.uid,NOTIFICATION_TYPE_LIKE_VIDEO_COMMENT,data?.userVideoId||data?.targetId)
      if(existNotify){
        return
      }
      const notifId= await this.insertNotification(info,comment?.uid,NOTIFICATION_TYPE_LIKE_VIDEO_COMMENT,data?.userVideoId||data?.targetId)
      info.notification_id=notifId
    }
     
      try {
        const notify=await firebaseHelper.sendNotifictionNew(info,notifyUser?.fcmToken)
        data.notifyStatus=notify
        await WebServiceLog.create({request:JSON.stringify(info),response:JSON.stringify(data),service_name:'Send Notification Service for LikesFollow',status:1,uid:0,time_taken:0})
        return
       } catch (error) {
         await WebServiceLog.create({request:JSON.stringify(info),response:JSON.stringify(error),service_name:'Send Notification Service for LikesFollow',status:1,uid:0,time_taken:0})
       }
  }

  async sendNotificationForVideoComment(data,fromUser){
    const payload:any={}
    const info:any={}
    const post:any = await UserVideo.find(data?.userVideoId)
    if(post?.createdBy==fromUser?.id){
      return
    }
    //var notifyUser= await LoginHistory.query().where("uid",post?.createdBy).orderBy("id","desc").first()
    var notifyUser= await LoginHistory.getUserFcm(post?.createdBy)
    payload.profile=fromUser?fromUser:{}
    payload.activity=HomeActivity
    info.title=fromUser?.name+' commented on your video:'
    info.body = truncatise(post?.description, { TruncateLength: 15 })
    payload.fragment='HomeFragment'
    payload.videoId=post?.id
    payload.type=NOTIFICATION_TYPE_VIDEO_COMMENT
    info.payload=payload
    info.type=NOTIFICATION_TYPE_VIDEO_COMMENT
    const notifId= await this.insertNotification(info,post?.createdBy,NOTIFICATION_TYPE_VIDEO_COMMENT,data?.id)
    info.notification_id=notifId
    try {
      const notify=await firebaseHelper.sendNotifictionNew(info,notifyUser?.fcmToken)
      data.notifyStatus=notify
      await WebServiceLog.create({request:JSON.stringify(info),response:JSON.stringify(data),service_name:'Send Notification Service for Comment',status:1,uid:0,time_taken:0})
      return  
     } catch (error) {
       await WebServiceLog.create({request:JSON.stringify(info),response:JSON.stringify(error),service_name:'Send Notification Service for Comment',status:1,uid:0,time_taken:0})
     }
  }

  async sendNotificationForReportPost(data,fromUser){  
    const payload:any={}
    const info:any={}
    const post:any = await Post.find(data?.targetId)
    if(post?.createdBy==fromUser?.id){
      return
    }
   // var notifyUser= await LoginHistory.query().where("uid",post?.createdBy).orderBy("id","desc").first()
    var notifyUser= await LoginHistory.getUserFcm(post?.createdBy)
    payload.profile=fromUser?fromUser:{}
    payload.activity=HomeActivity
    info.title=''
    info.body ='Your post "'+truncatise(post?.description, { TruncateLength: 15 })+'..." has been reported by another user of our community'
    info.payload=payload
    info.type=NOTIFICATION_TYPE_REPORT
    const notifId= await this.insertNotification(info,post?.createdBy,NOTIFICATION_TYPE_REPORT,data?.targetId)
    info.notification_id=notifId
    try {
      const notify=await firebaseHelper.sendNotifictionNew(info,notifyUser?.fcmToken)
      data.notifyStatus=notify
      await WebServiceLog.create({request:JSON.stringify(info),response:JSON.stringify(data),service_name:'Send Notification Service for Report Post',status:1,uid:0,time_taken:0})
      return  
     } catch (error) {
       await WebServiceLog.create({request:JSON.stringify(info),response:JSON.stringify(error),service_name:'Send Notification Service for Report Post',status:1,uid:0,time_taken:0})
     }
  }

  async sendNotificationForReportUser(data,fromUser){
    const payload:any={}
    const info:any={}
    const user:any = await User.find(data?.targetId)
    if(user?.createdBy==fromUser?.id){
      return
    }
   // var notifyUser= await LoginHistory.query().where("uid",user?.id).orderBy("id","desc").first()
    var notifyUser= await LoginHistory.getUserFcm(user?.id)
    payload.profile=fromUser?fromUser:{}
    payload.activity=HomeActivity
    info.title=''
    info.body ='An anonymous user has reported your profile.'
    info.payload=payload
    info.type=NOTIFICATION_TYPE_REPORT
    const notifId= await this.insertNotification(info,user?.id,NOTIFICATION_TYPE_REPORT,data?.targetId)
    info.notification_id=notifId
    try {
      const notify=await firebaseHelper.sendNotifictionNew(info,notifyUser?.fcmToken)
      data.notifyStatus=notify
      await WebServiceLog.create({request:JSON.stringify(info),response:JSON.stringify(data),service_name:'Send Notification Service for Report User',status:1,uid:0,time_taken:0})
      return  
     } catch (error) {
       await WebServiceLog.create({request:JSON.stringify(info),response:JSON.stringify(error),service_name:'Send Notification Service for Report User',status:1,uid:0,time_taken:0})
     }
  }

  async sendNotificationForApprovedExpert(data){
    const payload:any={}
    const info:any={}
    const user:any = await User.find(data?.uid)
   // var notifyUser= await LoginHistory.query().where("uid",user?.id).orderBy("id","desc").first()
    var notifyUser= await LoginHistory.getUserFcm(user?.id)
    payload.profile={}
    payload.activity=HomeActivity
    info.title='Expert approved'
    info.body ="Kudos! Your profile has now been approved as a verified expert.You can now start earning from your calls!"
    info.payload=payload
    info.type=NOTIFICATION_TYPE_APPROVE
    const notifId= await this.insertNotification(info,user?.id,NOTIFICATION_TYPE_APPROVE,data?.uid)
    info.notification_id=notifId
    try {
      const notify=await firebaseHelper.sendNotifictionNew(info,notifyUser?.fcmToken)
      data.notifyStatus=notify
      await WebServiceLog.create({request:JSON.stringify(info),response:JSON.stringify(data),service_name:'Send Notification Service for Approved Expert',status:1,uid:0,time_taken:0})
      return  
     }catch (error){
       await WebServiceLog.create({request:JSON.stringify(info),response:JSON.stringify(error),service_name:'Send Notification Service for Approved Expert',status:1,uid:0,time_taken:0})
     }
  }

  async sendNotificationForCommentToFollowers(data,fromUser){
    const commentUser= await User.find(data?.uid)
    const allFollowers = await (await FollowPost.query().where('targetId',data?.postId))
                                .map((user:any)=>{
                                  user=user?.uid
                                  return user
                                })
    if(allFollowers.length>0){
      const queryRaw = "SELECT lh1.* FROM login_histories lh1 WHERE lh1.id = (SELECT MAX(lh2.id) FROM login_histories lh2 WHERE lh2.uid = lh1.uid) AND uid IN (?)"
    const fcmOfAllUsers = await Database.rawQuery(queryRaw,[allFollowers])
    const allTokens= await fcmOfAllUsers[0].map((token:any)=>{
                   // token=token?.fcm_token
                    return token
                })
   // const fcmOfAllUsers= await LoginHistory.query().whereIn('uid',allFollowers).where('id','MAX(id)')
      if(allTokens.length>0){
        const post:any = await Post.find(data?.postId)
        const payload:any={}
        const info:any={}
        payload.profile={}
        payload.activity=HomeActivity
        info.title=commentUser?.name+' has commented on your followed post'
        info.body =truncatise(post?.description, { TruncateLength: 15 })
        info.payload=payload
        info.type=NOTIFICATION_TYPE_COMMENT
        for await(const udata of allTokens){
          if(udata?.uid==fromUser?.id){
            return
          }
          const notifId= await this.insertNotification(info,udata?.uid,NOTIFICATION_TYPE_COMMENT,data?.postId)
          info.notification_id=notifId
            try {
              const notify=await firebaseHelper.sendNotifictionNew(info,udata?.fcm_token)
              data.notifyStatus=notify
              await WebServiceLog.create({request:JSON.stringify(info),response:JSON.stringify(udata),service_name:'Send Notification for followed post',status:1,uid:0,time_taken:0})
              return  
            } catch (error) {
              await WebServiceLog.create({request:JSON.stringify(info),response:JSON.stringify(error),service_name:'Send Notification for followed post faield',status:1,uid:0,time_taken:0})
            }
        }
      }
      return allTokens
    }
    
    return
   // console.log('Commented By',commentUser)
  }

  async logOutNotification(user,udi){
    const payload:any={}
    const info:any={}
    //var notifyUser= await LoginHistory.query().where("uid",user?.id).orderBy("id","desc").first()
    var notifyUser= await LoginHistory.getUserFcm(user?.id)
    if(udi==notifyUser?.deviceId){
      await WebServiceLog.create({request:JSON.stringify(info),response:JSON.stringify(user),service_name:'Send Notification Service for Logout App Same device',status:1,uid:0,time_taken:0})
      return false
    }
    info.title=user?.name+' Logout App'
    info.body = 'Logout'
    info.payload=payload
    info.type=NOTIFICATION_TYPE_LOGOUT
    const notifId= await this.insertNotification(info,user?.id,NOTIFICATION_TYPE_LOGOUT,user?.id)
    info.notification_id=notifId
    try {
      const notify=await firebaseHelper.sendNotifictionNew(info,notifyUser?.fcmToken)
      user.notifyStatus=notify
      await WebServiceLog.create({request:JSON.stringify(info),response:JSON.stringify(user),service_name:'Send Notification Service for Logout App',status:1,uid:0,time_taken:0})
      return  
     } catch (error) {
       await WebServiceLog.create({request:JSON.stringify(info),response:JSON.stringify(error),service_name:'Send Notification Service for Logout App faield',status:1,uid:0,time_taken:0})
     }
  }

  async busyUserNotification(data){
    const payload:any={}
    const info:any={}
    //var notifyUser= await LoginHistory.query().where("uid",data?.expertId).orderBy("id","desc").first()
    var notifyUser= await LoginHistory.getUserFcm(data?.expertId)
    payload.profile={}
    payload.activity=HomeActivity
    info.title='Incoming Call notification'
    info.body = 'Getting a call for a booked Appointment, please discontent your current call.'
    info.payload=payload
    info.type=NOTIFICATION_TYPE_USER_BUSY_CALL
    const notifId= await this.insertNotification(info,data?.expertId,NOTIFICATION_TYPE_USER_BUSY_CALL,data?.expertId)
    info.notification_id=notifId
    try {
      const notify=await firebaseHelper.sendNotifictionNew(info,notifyUser?.fcmToken)
      data.notifyStatus=notify
      await WebServiceLog.create({request:JSON.stringify(info),response:JSON.stringify(data),service_name:'Send Notification for busy expert',status:1,uid:0,time_taken:0})
      return  
     } catch (error) {
       await WebServiceLog.create({request:JSON.stringify(info),response:JSON.stringify(error),service_name:'Send Notification for busy expert faield',status:1,uid:0,time_taken:0})
     }
  }

  async feedbackRating(data,fromUser){
    const payload:any={}
    const info:any={}
    var notifyUser= await LoginHistory.getUserFcm(data?.uid)
    payload.profile={}
    payload.activity=HomeActivity
    info.title='New feedback'
    info.body = fromUser?.name+' has dropped you a '+data?.userRating+' stars feedback for your consultation!'
    payload.fragment='RatingFragment'
    info.payload=payload
    info.type=NOTIFICATION_TYPE_RATING
    const notifId= await this.insertNotification(info,data?.uid,NOTIFICATION_TYPE_RATING,data?.id||data?.callId)
    info.notification_id=notifId
    try {
      const notify=await firebaseHelper.sendNotifictionNew(info,notifyUser?.fcmToken)
      data.notifyStatus=notify
      await WebServiceLog.create({request:JSON.stringify(info),response:JSON.stringify(data),service_name:'Send Notification for expert feedback',status:1,uid:0,time_taken:0})
      return
     } catch (error) {
       await WebServiceLog.create({request:JSON.stringify(info),response:JSON.stringify(error),service_name:'Send Notification for expert feedback faield',status:1,uid:0,time_taken:0})
       return false
     }
  }

  async sendBulkNotifiction(msg,devices){
    const info:any={}
    const payload:any={}
    info.title=truncatise(msg, { TruncateLength: 5 })
    info.body=msg
    payload.activity=HomeActivity
    info.payload=payload
    info.type=NOTIFICATION_TYPE_NEW_DEVICE
    try {
      const notify=await firebaseHelper.sendMulticastNotifictionWithSdk(info,devices)
      await WebServiceLog.create({request:JSON.stringify(info),response:JSON.stringify(devices),service_name:'Send Bulk Notification to new Device',status:1,uid:0,time_taken:0})
      return
     } catch (error) {
       await WebServiceLog.create({request:JSON.stringify(info),response:JSON.stringify(error),service_name:'Send Bulk Notification to new Device faield',status:1,uid:0,time_taken:0})
       return false
     }
  }


  async sendNotificationToIncompleteExpert(){
    const payload:any={}
    const info:any={}
   // payload.profile={}
    payload.activity="com.seekx.module.home.HomeActivity"
    info.title='Complete your profile'
    info.body = 'Please complete the profile details to start receiving calls.'
    payload.fragment='EditProfileFragment'
    info.payload=payload
    info.type=NOTIFICATION_INCOMPLETE_EXPERT
    const incompleteExperts = await User.query().select('id').where(q=>{
      q.where('audioCallRate','<=',0).orWhere('videoCallRate','<=',0).orWhereNull('audioCallRate').orWhereNull('videoCallRate')
    }).where('expert',true)
    if(incompleteExperts.length>0){
      for await (const item of incompleteExperts) {
        const userFcm = await LoginHistory.getUserFcm(item?.id)
        if(userFcm){
          await firebaseHelper.sendNotifictionNew(info,userFcm?.fcmToken)
        }  
      }
    }
    await WebServiceLog.create({request:JSON.stringify(info),response:JSON.stringify(incompleteExperts),service_name:'Send Notification for expert feedback',status:1,uid:0,time_taken:0})
    return incompleteExperts
  }

  async sendBulkNotifictionWithImg(data,devices){
    
    if(devices.length>0){
      const evntsAct=await AppUserEvent.eventsActions()
      const notifyAction = evntsAct[data?.action]
      await AudienceCampaign.query().where('id',data?.campaignId).update({status:2})
      var allUDatas:any=[]
      for await (const user of devices) {
       // console.log('add money Date',data,user)
        if(data?.addMoney==1 && data?.amount>0){
          await LedgerAccount.addPromoAmount({uid:user?.id,amount:data?.amount,description:'',campaignId:data?.campaignId})
        }
        const info:any={}
        const payload:any={}
        info.title=replaceContent(data?.title||truncatise(data?.message, { TruncateLength: 5 }),{name:user?.name,amount:data?.amount,coupon:data?.couponCode})
        info.body=replaceContent(data?.message,{name:user?.name,amount:data?.amount,coupon:data?.couponCode})
        payload.activity=notifyAction?.activity||HomeActivity
        payload.fragment=notifyAction?.fragment||''
        if(data?.actionId){
          if(data.action==1){
            payload.videoId=data?.actionId||0
          }
          if(data.action==2){
            payload.postId=data?.actionId||0
            payload.tabType=1
          }
          if(data.action==3){
            payload.expertId=data?.actionId||0
          }
          if(data.action==6){
            payload.tabType=2
          }
          if(data.action==7){
            payload.tabType=1
          }
          if(data.action==8){
            payload.tabType=2
          }
          if(data.action==9){
            payload.tabType=1
          }
          // if(data.action==10){
          //   payload.tabType=1
          // }
        }
        payload.campaignId=data?.campaignId||0
        payload.uid=user?.id||0
        payload.banner = data?.image
        info.payload=JSON.stringify(payload)
        info.type=''+NOTIFICATION_TYPE_PROMO
        const notification = {
          "title": info?.title||"",
          'body': info?.body||"",
          "image":data?.image||'',
          //"icon":seekexIcon
        }
        const msg=info
        allUDatas.push({
          notification: notification,
          data: msg,
          token:user?.userFcm?.fcmToken
        })
      }
      // console.log(allUDatas)
      // return allUDatas
      try {
       const notify = await firebaseHelper.sendMulticastNotifictionWithImgSdk(allUDatas)
        await AudienceCampaign.query().where('id',data?.campaignId).update({status:3})
        await WebServiceLog.create({request:JSON.stringify(allUDatas),response:JSON.stringify(notify),service_name:'Send Bulk Notification with Img',status:1,uid:0,time_taken:0})
        return true
       } catch (error) {
         await WebServiceLog.create({request:JSON.stringify(allUDatas),response:JSON.stringify(error),service_name:'Send Bulk Notification with Img error',status:1,uid:0,time_taken:0})
         return false
       }
    }
    return
  }

  async sendRatingPopupNotification(data,fromUid){
    //NOTIFICATION_TYPE_RATING_POUP
    const fromUser = await User.find(fromUid)
    const payload:any={}
    const info:any={}
    //var notifyUser= await LoginHistory.query().where("uid",data?.uid).orderBy("id","desc").first()
    var notifyUser= await LoginHistory.getUserFcm(fromUid)
    payload.profile={}
    payload.userRatingDto=data
    payload.activity='com.seekx.module.activities.RatingActivity'
    info.title='Give feedback'
    info.body = fromUser?.name+' give stars feedback for your consulting!'
    payload.fragment='RatingFragment'
    info.payload=payload
    info.type=NOTIFICATION_TYPE_RATING_POUP
    const notifId= await this.insertNotification(info,data?.uid,NOTIFICATION_TYPE_RATING_POUP,data?.id||data?.callId)
    info.notification_id=notifId
    try {
      const notify=await firebaseHelper.sendNotifictionNew(info,notifyUser?.fcmToken)
      data.notifyStatus=notify
      await WebServiceLog.create({request:JSON.stringify(info),response:JSON.stringify(data),service_name:'Send Notification for take feedback',status:1,uid:0,time_taken:0})
      return
     } catch (error) {
       await WebServiceLog.create({request:JSON.stringify(info),response:JSON.stringify(error),service_name:'Send Notification for take feedback faield',status:1,uid:0,time_taken:0})
       return false
     }
  }

}