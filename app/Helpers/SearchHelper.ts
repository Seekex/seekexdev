// import Application from '@ioc:Adonis/Core/Application'
import algoliasearch from 'algoliasearch'
import User from 'App/Models/User';
import ResumeHelper from './ResumeHelper';
const resumeHelper = new ResumeHelper()
const client = algoliasearch('Y6RMW16559', '723ccd885af314753db8d3fbd832f3d5');
const index = client.initIndex('dev_seekex')
const searchData=client.initIndex('dev_search_keys')
const searchType = ['expert','post']
const fileName = (url)=>{
  return (url = url.substr(1 + url.lastIndexOf("/")).split('?')[0])
}
const onlyUnique=(value, index, self)=>{
  return self.indexOf(value) === index
}
export default class SearchHelper {

  async addData(data){
      const objects = {
        name:data?.name,
        title: data?.title,
        type: data?.type,
        dataId:data?.dataId,
        userId:data?.userId,
        minCallRate:data?.minCallRate||0,
        maxCallRate:data?.maxCallRate||0,
        data:data,
        createdAt:new Date()
      }
    return index.saveObject(objects, { autoGenerateObjectIDIfNotExist: true })
  }
  async addExpertData(data){
    return await index.saveObject(data, { autoGenerateObjectIDIfNotExist: true })
  }

  async addExpertsListData(data){
    return await index.saveObjects(data, { autoGenerateObjectIDIfNotExist: true })
  }

  async clear(){
    return await index.clearObjects()
  }

  async search(str,type,uid){
    const stype = type==2?'post':type==3?'expert':'other'
    searchData.saveObject({
      keyword:str,
      search:str,
      keywordType:type,
      type:stype,
      userId:uid||0,
      createdAt:new Date()
    }, { autoGenerateObjectIDIfNotExist: true })
    // return index.search(str,{
    //   filters: 'type:'+stype
    // })
  }

  async searchStrInKeywordsWithUserId(str){
    const datas= await searchData.search(str)
    return datas?.hits.map((elm:any)=>{
      return elm?.userId
    }).filter(onlyUnique)
  }

  async expertData(uid){
    if(uid){
      var user:any=await User.query()
                .select('id','verified','featured','name','genderId','dob','age','aboutMe','views','audioCallRate','videoCallRate','audioCallRatePerSec','videoCallRatePerSec','resumeText','profilePic','created')
                // .preload('accomplishmentsList')
                // .preload('certificatesList')
                // .preload('projectList')
                .preload('country')
                .preload('state')
                .preload('highestEducation')
                .preload('collegeUniversity')
                .preload('occupation')
                .preload('currentOrg')
                .preload('skillsList')
                .preload('intExpertiseList')
                .where('id',uid).first()
      user=user.serialize()
      user.country=user?.country?.name
      user.state=user?.state?.name
      user.highestEducation=user.highestEducation?.name
      user.collegeUniversity=user.collegeUniversity?.name
      user.occupation=user.occupation?.name
      user.currentOrg=user.currentOrg?.name
      user.skillsList=await(user.skillsList).map(elm=>{
        return elm?.name
      })
      user.intExpertiseList=await(user.intExpertiseList).map(elm=>{
        return elm?.name
      })
      user.createdAt=new Date()
      user.type='expert'
     // await this.addExpertData(user)
      return user
    }
  }
}