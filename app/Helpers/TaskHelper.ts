//import Mail from '@ioc:Adonis/Addons/Mail'
//import Env from '@ioc:Adonis/Core/Env'
import AppSetting from 'App/Models/AppSetting'
import LedgerAccount from 'App/Models/LedgerAccount'
import TransactionHistory from 'App/Models/TransactionHistory'
import WebServiceLog from 'App/Models/WebServiceLog'
import CallHistory from 'App/Models/CallHistory'
//const From=Env.get('FROM_EMAIL') as string
import { DateTime,Duration } from 'luxon'
import User from 'App/Models/User'
import CommonHelper from 'App/Helpers/CommonHelper'
import Request from 'App/Models/Request'
const commonHelper = new CommonHelper()
import LedgerHelper from 'App/Helpers/LedgerHelper'
import BookedSlot from 'App/Models/BookedSlot'
import MasterGodown from 'App/Models/MasterGodown'
import Refund from 'App/Models/Refund'
import RefundHistory from 'App/Models/RefundHistory'
import ReferralHistory from 'App/Models/ReferralHistory'
const ledgerHelper = new LedgerHelper()
const perf = require('execution-time')()
const fetch = require('node-fetch')
export default class TaskHelper {
  
  async callHoldAmountTransferToAvailable(){
    perf.start()
    const holdTime = await AppSetting.query().where({fieldKey:'refund_time'}).first() 
    const currentDate= DateTime.local()
    const durTo = Duration.fromObject({days:holdTime?.value||0})
    const durFrom = Duration.fromObject({days:(holdTime?.value+1)||0});
    const callDtFrom=currentDate.minus(durFrom).toSQLDate()
    const callDtTo=currentDate.minus(durTo).toSQLDate()
    var calls:any = await CallHistory.query().where({callMoneyTransactionStatus:'hold'}).where('duration','>',0).whereBetween('callTime',[callDtFrom,callDtTo]).doesntHave('refund')
    if(calls){
      var allCallsIds:any=[]
      for(let i = 0; i < calls.length; i++){
        const call = calls[i]
       // console.log('data',i)
        const expertWallet:any = await LedgerAccount.query().where({'uid':call?.receiver,name:'User'}).first()
        const transectionHistoryForCall = await TransactionHistory.query().where({callId:call?.id,transactionStatus:'hold',toWallet:expertWallet?.id}).first()
        if(transectionHistoryForCall){
          if(expertWallet?.holdAmount >= transectionHistoryForCall?.transactionAmount){
            const addToExpertWallet=new TransactionHistory()
            addToExpertWallet.callId=call?.id
            addToExpertWallet.description=transectionHistoryForCall?.transactionAmount+' amount added in Expert Wallet from hold amount for call ID => '+call?.id
            addToExpertWallet.fromWallet=expertWallet?.id
            addToExpertWallet.toWallet=expertWallet?.id
            addToExpertWallet.isdebit=true
            addToExpertWallet.status=1
            addToExpertWallet.transactionAmount=transectionHistoryForCall?.transactionAmount
            addToExpertWallet.transactionType=1
            addToExpertWallet.transactionWalletId=expertWallet?.id
            addToExpertWallet.transactionTo='Expert'
            addToExpertWallet.transactionAction='plus'
            addToExpertWallet.transactionStatus='released'
            addToExpertWallet.uid=call?.receiver
            addToExpertWallet.payout=false
            await addToExpertWallet.save()
            expertWallet.holdAmount=expertWallet.holdAmount-transectionHistoryForCall?.transactionAmount
            expertWallet.availableAmount=expertWallet.availableAmount+transectionHistoryForCall?.transactionAmount
            await expertWallet.save()
            const callData:any=await CallHistory.find(call?.id)
            callData.callMoneyTransactionStatus='transferred'
            await callData.save()
            allCallsIds.push(call?.id)
          }
        }
       }
       const exeTime = perf.stop()
       await WebServiceLog.create({request:JSON.stringify(allCallsIds),response:JSON.stringify({from:callDtFrom,to:callDtTo}),service_name:'Call Hold Amount Transfer To Expert wallet from hold to Available amount',status:1,uid:0,time_taken:exeTime.time})
    }
  }

  async newFn(){
    const call:any = await CallHistory.find(1)
    call.duration=1
    await call?.save()
  }
  async updateResponseRate(){
      const allExperts= await User.query().where({expert:1})
      if(allExperts.length>0){
        for await (const exp of allExperts) {
          const expRespo=await commonHelper.expertResponceRate(exp?.id)
          if(expRespo>0){
            const expert:any= await User.find(exp?.id)
            expert.callResponseRate=expRespo
            await expert.save()
          }
        }
      }
  }

  async moveUnAcceptedRequests(){
    const date = new Date()
    date.setDate(date.getDate() - 1)
    date.setUTCHours(23,59,0,0)
    const allRequests= await Request.query().whereIn('status',[1,2]).where('bookingDate','<',date)
    if(allRequests){
      for await (const data of allRequests) {
        const request:any=data
        if(request?.estimatedCost>0){
          await ledgerHelper.releaseAmountForRequest(request)
        }
        request.comment = 'Canceled by system (Auto canceled)'
        request.canceledBy = 0
        request.status = 4
        request.canceledDate = new Date()
        await request.save()
        await BookedSlot.query().where("requestId",request?.id).update({status:3})
        await WebServiceLog.create({request:JSON.stringify(request),response:JSON.stringify({}),service_name:'Old Request Move =>'+request?.id,status:1,uid:0,time_taken:0})
      }
    }
    return allRequests
  }

  async approveUnActionRefund(){
    const refundDay= await AppSetting.getValueByKey('refund_time')
    const currentDate= DateTime.local()
    const durTo = Duration.fromObject({days:refundDay||0})
    const durFrom = Duration.fromObject({days:(refundDay+1)||0});
    const callDtFrom=currentDate.minus(durFrom).toSQLDate()
    const callDtTo=currentDate.minus(durTo).toSQLDate()
    const allRefunds = await Refund.query()
                                  .preload('callDetail')
                                  .where({status:1,expertApprovalStatus:0,autoRefund:0})
                                  .whereBetween('created',[callDtFrom,callDtTo])
    if(allRefunds){
      for await (const refundData of allRefunds){
        const rfStatus= await ledgerHelper.refundAmountByExpert(refundData)
        var refundHistory:any=new RefundHistory()
        refundHistory.status='Approved'
        refundHistory.Type='Borne From Expert'
        refundHistory.comment='User Wallet '+rfStatus+' Auto Approved Refund => '+refundData?.amt
        refundHistory.approveBy=0
        refundHistory.refundId=refundData?.id
        await Refund.query().where({id:refundData?.id}).update({status:2,autoRefund:1})
        await refundHistory.save()
      }
    }
    await WebServiceLog.create({request:JSON.stringify(allRefunds),response:JSON.stringify(new Date()),service_name:'Auto  Approved Refunds Service',status:1,uid:0,time_taken:0})
  }

  async freeImageApiForInterest(){
      const insts= await MasterGodown.query().where("masterType",5).where('imageUrl','')
      if(insts){
         for await(const data of insts){
            if(data?.name){
              const url ="https://api.pexels.com/v1/search?query="+data?.name+"&per_page=1"
              const key="563492ad6f91700001000001cd70fb50535b42bea7cd58106cf4c570"
              const res= await fetch(url, {
                  'method': 'GET',
                  'headers': {
                    'Authorization':key,
                    'Content-Type': 'application/json',
                    "Accept": "application/json",
                  }
                }).then((res)=>{
                  return res.json()
                })
                await WebServiceLog.create({request:JSON.stringify(data),response:JSON.stringify(res),service_name:'Master Godown Url service',status:1,uid:0,time_taken:0})
              if(res?.photos[0]?.src?.tiny){
                const url=res?.photos[0]?.src?.tiny
                 //Image crop in 200X200
                await commonHelper.imageCropAndUpload(url,data)
                // data.imageUrl=res?.photos[0]?.src?.tiny
                // await data.save()
              }else{
                await commonHelper.genTextImage(data?.name,data)
              }
            }
        }
      }
  }

  async freeImageApiForInterestById(id){
    const data= await MasterGodown.query().where("id",id).first()
          if(data?.name){
            const url ="https://api.pexels.com/v1/search?query="+data?.name+"&per_page=1"
            const key="563492ad6f91700001000001cd70fb50535b42bea7cd58106cf4c570"
            const res= await fetch(url, {
                'method': 'GET',
                'headers': {
                  'Authorization':key,
                  'Content-Type': 'application/json',
                  "Accept": "application/json",
                }
              }).then((res)=>{
                return res.json()
              })
              await WebServiceLog.create({request:JSON.stringify(data),response:JSON.stringify(res),service_name:'Master Godown Url service',status:1,uid:0,time_taken:0})
            if(res?.photos[0]?.src?.tiny){
              const url=res?.photos[0]?.src?.tiny
               //Image crop in 200X200
              await commonHelper.imageCropAndUpload(url,data)
              // data.imageUrl=res?.photos[0]?.src?.tiny
              // await data.save()
            }else{
              await commonHelper.genTextImage(data?.name,data)
            }
          }
  }

  async transferUserReferalAmount(){
    //whereHas
    const refMinAmount = await AppSetting.getValueByKey('min_referral_transfer')
    const users = await User.query()
                        .select('id','referralCode')
                        // .preload('earnReferrals',(query)=>{
                        //     query.where('status',1)
                        //         .whereColumn('uid','=','refer_by')
                        //         //.pojo<{ totalAmount: number}>()
                        //         //.sum('amount as totalAmount')
                        // })
                        .whereHas('earnReferrals',(query)=>{
                          query.where('status',1)
                              .whereColumn('uid','=','refer_by')
                              .sum('amount')
                        },'>=',refMinAmount)
    
    if(users){
      for await (const user of users) {
        const referralEarned = await ReferralHistory.query()
                                                .where('uid',user?.id)
                                                .where('status',1)
                                                .whereColumn('uid','=','refer_by')
                                                .pojo<{ totalAmount: number,count:number}>()
                                                .sum('amount as totalAmount')
                                                .count('id as count').first()
        const userWallet = await LedgerAccount.query().where({name:'User',uid:user?.id}).first() 
        const userPromoAmount:any = userWallet?.promoAmount
        const refAmount:any = referralEarned?.totalAmount
        if(refAmount>userPromoAmount || userPromoAmount==0){
          await WebServiceLog.create({request:JSON.stringify({referralEarned,userWallet}),response:JSON.stringify('Not valid Referral amount for tranfer service error'),service_name:'Auto Referral amout tranfer error',status:1,uid:user?.id,time_taken:0})
        }else{
          const promoTransfer = await ledgerHelper.referalAmountTranfer(userWallet,refAmount)
          if(promoTransfer){
            await ReferralHistory.query()
                                  .where('referralCode',user?.referralCode)
                                  .where('status',1)
                                  .whereColumn('uid','=','refer_by')
                                  .update({status:2})
            await WebServiceLog.create({request:JSON.stringify({referralEarned,userWallet,promoTransfer}),response:JSON.stringify('Referral amount tranfer service success'),service_name:'Auto Referral amout tranfer success',status:1,uid:user?.id,time_taken:0})                   
          }
        }            
      }
    }
    return users
  }
  
}