import Env from '@ioc:Adonis/Core/Env'
import Invoice from 'App/Models/Invoice'
import LedgerHelper from "App/Helpers/LedgerHelper"
import User from 'App/Models/User'
import PendingRating from 'App/Models/PendingRating'
import AppSetting from 'App/Models/AppSetting'
import Request from 'App/Models/Request'
import CallHistory from 'App/Models/CallHistory'
import WebServiceLog from 'App/Models/WebServiceLog'
const Ledger:any=new LedgerHelper()
// import Application from '@ioc:Adonis/Core/Application'
var AccessToken = require('twilio').jwt.AccessToken
var VideoGrant = AccessToken.VideoGrant
//const durTime = async()=>{ return await AppSetting.getValueByKey('duration_time')}
//const API_KEY_SID = 'ACcc0b3dcec8f5d0b88d3a67f8277b2f91'
//const API_KEY_SID = 'SKe9a5d8108f1f5959bbb4f31d942a747a'
//const API_KEY_SECRET = 'fcacd6d77173de2d1b92829cb3fb3567'
// const ACCOUNT_SID = 'ACb18460164ea512ef95a9cb136464f6b3'
// const API_KEY_SID = 'SKc81f8ddcec067b0e5519bf9a50ed172f'
// const API_KEY_SECRET = '2eUlYXe1Z5kD3V03C0z25ABvRMfP9L9I'

// const ACCOUNT_SID = 'AC77cf6512e7b1ea5f87750197dd0957c7'
// const API_KEY_SID = 'SK3fae51e1cf0b8e65ac1e35a73a2e1cac'
// const API_KEY_SECRET = 'yFXDZ8OEGP2yEjcoHDzrz9HOSqMl5ICF'

const ACCOUNT_SID = Env.get('TWILIO_ACCOUNT_SID') as string
const API_KEY_SID = Env.get('TWILIO_API_KEY_SID') as string
const API_KEY_SECRET = Env.get('TWILIO_API_KEY_SECRET') as string
const TWILIO_AUTH_TOKEN = Env.get('TWILIO_AUTH_TOKEN') as string

const accessToken = new AccessToken(
  ACCOUNT_SID,
  API_KEY_SID,
  API_KEY_SECRET
)

const client = require('twilio')(ACCOUNT_SID, TWILIO_AUTH_TOKEN)

export default class TwilioHelper {
  
  async createUserToken(uid,room){
    accessToken.identity = uid
    var grant = new VideoGrant()
    grant.room = room
    accessToken.addGrant(grant)
    var jwt = accessToken.toJwt()
    return jwt
  }

  async updateRoomDetails(call,roomId,expertId){
    
    await client.video.rooms(roomId).participants
      .each({status: 'disconnected'},async (participant) => {
        //console.log(participant)
        if(participant?.identity==expertId){
          call.duration=participant?.duration||0
          if(call.duration>0){
            call.duration=parseInt(call?.duration)
          }
          await WebServiceLog.create({request:JSON.stringify({call,roomId,expertId}),response:JSON.stringify(participant),service_name:'updateRoomDetails =>'+participant?.duration,status:1,uid:0,time_taken:0})
          
          await call.save()
          var pRating= await PendingRating.query().where('callId',call?.id).first()
          if(!pRating && call?.duration>0){
            await PendingRating.create({callId:call?.id,skip:0,status:0,uid:call?.initiater})
          }
          var receiverExpert:any = await User.find(call?.receiver)
					if(receiverExpert?.verified){
            var invoice = await Invoice.query().where({callId:call?.id}).first()
            if(!invoice){
              const trans=await Ledger.calculateAndTransferAmountForCall(call)
            }
         }else{
          if(call?.requestId && call?.rate==0){
            await Request.completeRequest(call?.requestId)
          }
         }
        }
      })
  }

  async retryUpdateCallDetails(){
    const allUnUpdatedCalls = await CallHistory.query()
                                    .where((q)=>{
                                      q.where('duration',0).orWhereNull('duration')
                                    })
                                    .where('callingType','VOIP')
                                    //.where('rate','>',0)
                                    .where('payStatus',0)
                                    .where('finishedStatus',1)
    if(allUnUpdatedCalls){
      for await (const call of allUnUpdatedCalls) {
        const roomId=''+call?.roomSid
        const expertId=call?.receiver
        if(roomId){
          await this.updateRoomDetails(call,roomId,expertId)
        }
        call.payStatus=true
        await call.save()
      }
    } 
    await WebServiceLog.create({request:JSON.stringify(allUnUpdatedCalls),response:JSON.stringify({}),service_name:'Retry Update Call Duration =>',status:1,uid:0,time_taken:0})            
  }
}