import Application from '@ioc:Adonis/Core/Application'
var reader = require('any-text')
const request = require('request')
const fs = require('fs')
const download = async (uri, filename, callback)=>{
  await request.head(uri, async (err, res, body)=>{
    // console.log('err',err)
    // console.log('res',res)
    // console.log('body',body)
    await request(uri).pipe(fs.createWriteStream(filename)).on('close', callback)
  })
}
const fileName = (url)=>{
  return (url = url.substr(1 + url.lastIndexOf("/")).split('?')[0])
}
export default class ResumeHelper {

  async parseResume(file){
    const text = await reader.getText(Application.tmpPath('uploads/'+file))
    return text
  }

  async parseResumeFromUrl(url){
    if(url && url !=''){
      const fileNme = fileName(url)
      if(!fileNme){
        return ''
      }
      const resumetext =  new Promise( async function(resolve,reject) {
        await download(url, './public/'+fileNme, async function(){
          const text = await reader.getText('./public/'+fileNme)
          resolve(text)
        })
      })
      return resumetext
    }
    return ''
  }
}