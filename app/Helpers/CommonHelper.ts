import AppSetting from "App/Models/AppSetting"
import User from "App/Models/User"
import ReferralHistory from "App/Models/ReferralHistory"
//import Database from '@ioc:Adonis/Lucid/Database'
import Application from '@ioc:Adonis/Core/Application'
import WebServiceLog from "App/Models/WebServiceLog"
const perf = require('execution-time')()
import Env from '@ioc:Adonis/Core/Env'
const key=Env.get('EXOTEL_KEY') as string
const token=Env.get('EXOTEL_TOKEN') as string
const sid=Env.get('EXOTEL_SID') as string
const axios = require('axios')
import LedgerAccount from "App/Models/LedgerAccount"
import EmailHelper from 'App/Helpers/EmailHelper'
const emailHelper = new EmailHelper()
import CallHistory from "App/Models/CallHistory"
import UploadHelper from 'App/Helpers/UploadHelper'
const uploadHelper = new UploadHelper()
import FirebaseHelper from 'App/Helpers/FirebaseHelper'
import BlockList from "App/Models/BlockList"
import Request from "App/Models/Request"
import FollowUser from "App/Models/FollowUser"
import UserRating from "App/Models/UserRating"
const firebaseHelper = new FirebaseHelper()
const request = require('request')
import { DateTime } from 'luxon'
const fs = require('fs')
var gm = require('gm')
const ext = (url)=>{
  return (url = url.substr(1 + url.lastIndexOf("/")).split('?')[0]).split('#')[0].substr(url.lastIndexOf("."))
}
const fileName = (url)=>{
  return (url = url.substr(1 + url.lastIndexOf("/")).split('?')[0])
}
const download = async (uri, filename, callback)=>{
    await request.head(uri, async (err, res, body)=>{
      console.log('err',err)
      console.log('res',res)
      console.log('body',body)
      await request(uri).pipe(fs.createWriteStream(filename)).on('close', callback)
    })
}

const downloadNew = async (uri, filename, callback)=>{
  await request(uri).pipe(fs.createWriteStream(filename)).on('close', callback)
  // await request.head(uri, async (err, res, body)=>{
  //   console.log('err',err)
  //   console.log('res',res)
  //   console.log('body',body)
  //    res.pipe(fs.createWriteStream(filename)).on('close', callback)
  // })
}

const getNextDayOfTheWeek=(day, excludeToday = true, refDate )=>{
  const dayOfWeek = day-1
  if (dayOfWeek < 0) return
  const date=refDate
  date.setHours(0,0,0,0)
  date.setDate(date.getDate() + +!!excludeToday + 
               (dayOfWeek + 7 - date.getDay() - +!!excludeToday) % 7)  
  //console.log('date',date)
  return date
}
import NotificationHelper from 'App/Helpers/NotificationHelper'
import UserCountryState from "App/Models/UserCountryState"
import Chat from "App/Models/Chat"
import Database from "@ioc:Adonis/Lucid/Database"
import UserMasterGodown from "App/Models/UserMasterGodown"
import MasterGodown from "App/Models/MasterGodown"
import YoutubeVideo from "App/Models/YoutubeVideo"
import Post from "App/Models/Post"

const notificationHelper = new NotificationHelper()
const ytch = require('yt-channel-info')
const nativeRound=(num)=>{
    var n = num||0
    const dc= n % 1
    var v = Math.floor(n)
    if(dc>=0.5){
      n=v+0.5
    }else{
      n=v
    }
    return n
}

const profileColor=['#36689B','#FF0000','#8B0000','#C0C0C0','#808080','#800000','#808000','#00FF00','#00FFFF','#008080','#000080','#FF00FF','#800080','#CD5C5C','#F08080','#FA8072','#E9967A','#FFA07A']

export default class CommonHelper {


  public async userConversations(uid){
    const tcall:any= await CallHistory.query().where((query)=>{
      query.where("duration",'>',1).whereNotNull('duration')
    }).where((query)=>{
      query.where({'receiver':uid}).orWhere({'initiater':uid})
    })
    .pojo<{ count: number}>()
    .count('* as count')
    const queryRaw="SELECT count(*) as count  FROM chats c JOIN ( SELECT MAX(id) as id, CASE WHEN `receiver_id` = ? THEN sender_id WHEN `sender_id` = ? THEN receiver_id END AS users FROM chats GROUP BY CASE WHEN `receiver_id` = ? THEN sender_id WHEN `sender_id` = ? THEN receiver_id END HAVING users IS NOT NULL ) msg ON c.id = msg.id order by c.id desc;"
    var rawChats = await Database.rawQuery(queryRaw,[uid,uid,uid,uid])

    //const chatHead = await Chat.query().where({receiverId:uid}).groupBy('receiverId').distinct('sender_id')
    // const user = await Database
    //                   .from('users')
    //                   .leftJoin('chats', 'users.id', 'chats.receiver_id')
    //                   .where("chats.receiver_id",uid)
    //                   .groupBy('users.id')
    //                   .count('users.id as count')
    //return rawChats[0]
    const chatHead = rawChats[0]
   // return {tcall,chatHead}
    const tCount:any=parseInt(tcall[0]?tcall[0].count:0)+parseInt(chatHead[0]?chatHead[0].count:0)
    await WebServiceLog.create({request:JSON.stringify(uid),response:JSON.stringify({tcall,chatHead}),service_name:'userConversations',status:1,uid:uid,time_taken:0})
    return tCount||0
    
  }
  public  async expertResponceRate(expertId){
    const callRH= await CallHistory.query().where({'receiver':expertId}).where("duration",'>',1)
    .pojo<{ count: number}>()
    .count('* as count')
    const tcall= await CallHistory.query().where({'receiver':expertId})
    .pojo<{ count: number}>()
    .count('* as count')

    const totalCalls=tcall[0].count
    const respondCalls=callRH[0].count
    return Math.round((respondCalls/totalCalls)*100)
  }

  public async userRating(uid){
    const userRat:any= await UserRating.query().where('uid',uid)
    .pojo<{ rating: number}>()
    .sum('user_rating as rating')
    .pojo<{ count: number}>()
    .count('* as count')
    const rat=nativeRound(parseFloat((userRat[0]?.rating/userRat[0]?.count).toFixed(1)))
    return {rating:rat||0,count:userRat[0]?.count}
  }

  public async callRating(callid){
    const userRat= await UserRating.query().where('callId',callid).first()
    return {rating:nativeRound(userRat?.userRating),count:0}
  }

  public async durationFormat(seconds){
    if(seconds==0){
      return '0 sec'
    }
    var d = Number(seconds)
    var h = Math.floor(d / 3600)
    var m = Math.floor(d % 3600 / 60)
    var s = Math.floor(d % 3600 % 60)

    var hDisplay = h > 0 ? h + (h == 1 ? " hr " : " hr ") : ""
    var mDisplay = m > 0 ? m + (m == 1 ? " min " : " min ") : ""
    var sDisplay = s > 0 ? s + (s == 1 ? " sec" : " sec") : ""
    return hDisplay + mDisplay + sDisplay;
  } 
  

  public async referralAmount(refCode,userId){
    perf.start()
    const getMaxReferalCodeUsed = await AppSetting.getValueByKey('max_referral')
    const getMaxUsed = ReferralHistory.referalCodeUsedCount(refCode)
    var status=0
    if(getMaxUsed < getMaxReferalCodeUsed){
      status=1
    }
      const referralUser= await User.query().where({referralCode:refCode}).first()
      const referAmountForUser= await AppSetting.query().where({field_key:'user_ref_amount_all'}).first() // This amount for user who user referral code
      const referAmountForReferral = await AppSetting.query().where({field_key:'user_ref_amount_ref'}).first() // Refer By user referral Amount
      const referByUser = {
        referBy:referralUser?.id,
        referTo:userId,
        referralCode:refCode,
        uid:referralUser?.id,
        amount:referAmountForReferral?.value,
        status:status
      }
      const referToUser = {
        referBy:referralUser?.id,
        referTo:userId,
        referralCode:refCode,
        uid:userId,
        amount:referAmountForUser?.value,
        status:status
      }
    //  const trx = await Database.transaction()
      try {
      // await trx.transaction() 
      await WebServiceLog.create({request:JSON.stringify(referByUser),response:JSON.stringify(referToUser),service_name:'Referral Amount history',status:1,uid:userId,time_taken:0})
        await ReferralHistory.createMany([referByUser,referToUser])
      //  await trx.commit()
      } catch (error) {
      // await trx.rollback()
        const exeTime = perf.stop()
        await WebServiceLog.create({request:JSON.stringify(referByUser)+JSON.stringify(referToUser),response:JSON.stringify(error),service_name:'Referral Amount history',status:1,uid:userId,time_taken:exeTime.time}) 
      }
  }

  async showCallAmount(amt,isVideo,type){
    //VOIP_AUDIO_CALL_CHARGE VOIP_VIDEO_CALL_CHARGE

    // const IGST=parseFloat(Env.get('IGST_EXCLUSIVE') as string)
    // const marginAmount=parseFloat(Env.get('COMMISSION_AMOUNT') as string)

    const IGST=await AppSetting.getValueByKey('gst')/100
    const marginAmount=await AppSetting.getValueByKey('commission')/100

    var totalAmount=0
    if(type=='VOIP'){
      // const voipAudio=parseFloat(Env.get('VOIP_AUDIO_CALL_CHARGE') as string)
      // const voipVideo=parseFloat(Env.get('VOIP_VIDEO_CALL_CHARGE') as string)
      const voipAudio=await AppSetting.getValueByKey('voip_audio_charge')
      const voipVideo=await AppSetting.getValueByKey('voip_video_charge')
      if(isVideo){
        totalAmount = (amt+(amt*marginAmount)+(voipVideo*5))+(amt+(amt*marginAmount)+(voipVideo*5))*IGST
      }else{
        totalAmount = (amt+(amt*marginAmount)+(voipAudio*5))+(amt+(amt*marginAmount)+(voipAudio*5))*IGST
      }
    }else if(type=='Exotel'){
      //const exotelCahrge=parseFloat(Env.get('TELEPHONY_CHARGE') as string)
      const exotelCahrge=await AppSetting.getValueByKey('exotel_charge')
      totalAmount = (amt+(amt*marginAmount)+(exotelCahrge*5))+(amt+(amt*marginAmount)+(exotelCahrge*5))*IGST
    }
    //return {totalAmount,amt}
    return parseFloat((totalAmount/(5*60)).toFixed(5))
  }

  // Old Code
  async estimatedDuration(uid,expId,isVideo,type){
    const durTime = await AppSetting.getValueByKey('duration_time')||0
    const expert = await User.find(expId)
    if(isVideo){
      var rate = expert?.videoCallRate
    }else{
      var rate = expert?.audioCallRate
    }
    //return rate
    const userWallet:any=await LedgerAccount.query().where({'uid':uid,name:'User'}).first()

    const avlAmount = userWallet?.totalAmount-userWallet?.disputedAmount
    
    const IGST=await AppSetting.getValueByKey('gst')/100
    const marginAmount=await AppSetting.getValueByKey('commission')/100
    const voipAudio=await AppSetting.getValueByKey('voip_audio_charge')
    const voipVideo=await AppSetting.getValueByKey('voip_video_charge')
    const exotelCahrge=await AppSetting.getValueByKey('exotel_charge')
    var totalAmount=0
    var amt:any = rate
    //return {rate,avlAmount}
    if(type=='VOIP'){
      if(isVideo){
        totalAmount = parseFloat(((amt+(amt*marginAmount)+(voipVideo*5))+(amt+(amt*marginAmount)+(voipVideo*5))*IGST).toFixed(3))
      }else{
        totalAmount = parseFloat(((amt+(amt*marginAmount)+(voipAudio*5))+(amt+(amt*marginAmount)+(voipAudio*5))*IGST).toFixed(3))
      }
    }else if(type=='Exotel'){
      totalAmount = parseFloat(((amt+(amt*marginAmount)+(exotelCahrge*5))+(amt+(amt*marginAmount)+(exotelCahrge*5))*IGST).toFixed(3))
     // console.log('exotel')
    }
    //  return totalAmount
   // return {avlAmount,totalAmount,amt}
    await WebServiceLog.create({request:JSON.stringify({avlAmount,totalAmount,amt,durTime}),response:JSON.stringify(Math.floor((avlAmount/(totalAmount/5))*60)),service_name:'Estimated Duration',status:1,uid:uid,time_taken:0})
    return Math.floor((avlAmount/(totalAmount/5))*60)-durTime

    //
  }

  // New Function for Call estimated duration acording to seeker wallet amount
  async estimatedCallDuration(uid,data,type){
    const durTime = await AppSetting.getValueByKey('duration_time')||0
    const expId=data?.receiver
    const isVideo=data?.video
    const requestId=data?.requestId
    var avlAmount=0
    const expert = await User.find(expId)
    if(isVideo){
      var rate = expert?.videoCallRate
    }else{
      var rate = expert?.audioCallRate
    }
    const userWallet:any=await LedgerAccount.query().where({'uid':uid,name:'User'}).first()
    if(requestId){
      const requestAmount = await Request.getAmount(requestId)
      avlAmount = userWallet?.totalAmount-(userWallet?.disputedAmount+userWallet?.holdAmount)+requestAmount
    }else{
      avlAmount = userWallet?.totalAmount-(userWallet?.disputedAmount+userWallet?.holdAmount)
    }

    const IGST=await AppSetting.getValueByKey('gst')/100
    const marginAmount=await AppSetting.getValueByKey('commission')/100
    const voipAudio=await AppSetting.getValueByKey('voip_audio_charge')
    const voipVideo=await AppSetting.getValueByKey('voip_video_charge')
    const exotelCahrge=await AppSetting.getValueByKey('exotel_charge')
    var totalAmount=0
    var amt:any = rate
    //return {rate,avlAmount}
    if(type=='VOIP'){
      if(isVideo){
        totalAmount = parseFloat(((amt+(amt*marginAmount)+(voipVideo*5))+(amt+(amt*marginAmount)+(voipVideo*5))*IGST).toFixed(3))
      }else{
        totalAmount = parseFloat(((amt+(amt*marginAmount)+(voipAudio*5))+(amt+(amt*marginAmount)+(voipAudio*5))*IGST).toFixed(3))
      }
    }else if(type=='Exotel'){
      totalAmount = parseFloat(((amt+(amt*marginAmount)+(exotelCahrge*5))+(amt+(amt*marginAmount)+(exotelCahrge*5))*IGST).toFixed(3))
     // console.log('exotel')
    }
    //  return totalAmount
   // return {avlAmount,totalAmount,amt}
    await WebServiceLog.create({request:JSON.stringify({avlAmount,totalAmount,amt}),response:JSON.stringify(Math.floor((avlAmount/(totalAmount/5))*60)),service_name:'Estimated Duration New Function',status:1,uid:uid,time_taken:0})
    const estDuration = Math.floor((avlAmount/(totalAmount/5))*60)-durTime
    return estDuration>0?estDuration:0
    //
  }

  async newOtp(data,type){
    var otp:any=Math.floor(123456 + Math.random()*487654)
      //otp=parseInt(otp.toString().slice(6))

    if(type=='onmobile'){
      var user:any = await User.query().where({mobile:data}).first()
      if(user){
        user.mobileOtp=otp
        await user.save()
      }else{
        await User.create({mobile:data,mobileOtp:otp})
      }
    }
    if(type=='onemail'){
      var user:any = await User.query().where({email:data}).first()
      if(user){
        user.emailOtp=otp
        await user.save()
      }else{
        await User.create({email:data,emailOtp:otp})
      }
    }
    return otp
  }

  async sendMobileOtp(mobile){
    const apikey=Env.get('SMS_API_KEY') as string
    const url=Env.get('SMS_API_URL') as string
    const sender=Env.get('SENDER') as string
    const otp = await this.newOtp(mobile,'onmobile')
    //return otp
    const newMobNo=''+'91'+mobile.replace(/\D/g, '').slice(-10)
    const message='Your OTP is '+otp
    var data ='?apikey='+apikey+'&numbers='+encodeURI(newMobNo)+"&sender="+encodeURI(sender)+"&message="+encodeURI(message)
    const res = await axios.get(url+data)
    await WebServiceLog.create({request:JSON.stringify(data),response:JSON.stringify(res?.data),service_name:'OTP SMS send Api',status:1,uid:0,time_taken:0})
    return res?.data?.status
  }

  async sendPasswordOnMobile(mobile,password){
    const apikey=Env.get('SMS_API_KEY') as string
    const url=Env.get('SMS_API_URL') as string
    const sender=Env.get('SENDER') as string
    //return otp
    const newMobNo=''+'91'+mobile.replace(/\D/g, '').slice(-10)
    const message='Your new password is '+password
    var data ='?apikey='+apikey+'&numbers='+encodeURI(newMobNo)+"&sender="+encodeURI(sender)+"&message="+encodeURI(message)
    const res = await axios.get(url+data)
    await WebServiceLog.create({request:JSON.stringify(data),response:JSON.stringify(res?.data),service_name:'Password SMS send Api',status:1,uid:0,time_taken:0})
    return res?.data?.status
  }


  async sendEmailOtp(email){
    await this.newOtp(email,'onemail')
    var user:any = await User.query().where({email:email}).first()
    const res = await emailHelper.sendOtpEmail(user)
    return res
  }

  async sendMobleOtpOnEmail(email,mobile){
    var user:any = await User.query().where({mobile:mobile}).first()
    user.email=email
    user.emailOtp=user?.mobileOtp||123456
    const res = await emailHelper.sendOtpEmail(user)
    return res
  }

  async downloadExotelFileAndUploadOnSeekex(url,call){
   // const getMP3Duration = require('get-mp3-duration')
    const fileNme = fileName(url)
    await download(url, './public/recordings/exotel/'+fileNme, async function(){
      var uploadFile= await uploadHelper.uploadRecordingExotel(fileNme,'recordings',call?.uid)
      if(uploadFile?.Location){
        call.recordingUrl=uploadFile?.Location||''
      }
      // const buffer = await fs.readFileSync('./public/recordings/exotel/'+fileNme)
      // const duration = getMP3Duration(buffer)
      // call.duration=Math.round(duration/1000)
      await call.save()
      //  console.log('Call duration',uploadFile)
      //  console.log('Call Upload',duration)
       await WebServiceLog.create({request:JSON.stringify(url)+JSON.stringify(call),response:JSON.stringify({uploadFile}),service_name:'Download Exotel File And Upload On Seekex',status:1,uid:0,time_taken:0})
      // console.log(Math.round(duration/1000))
      //  return duration
      //fs.unlink(Application.publicPath(fileNme))
    })
  }

  async updateExotelDuration(call,CallSid){
    const apiUrl ='https://'+key+':'+token+'@api.exotel.in/v1/Accounts/'+sid+'/Calls/'+CallSid+'.json?details=true'
    try {
       axios.get(apiUrl).then( async(response) => {
        //  console.log(response.data)
          const duration=response?.data?.Call?.Details?.ConversationDuration||0
          call.duration=duration
          await call.save()
          await WebServiceLog.create({request:JSON.stringify({call,CallSid}),response:JSON.stringify(response.data),service_name:'Exotel Call Service  with call details success',status:1,uid:0,time_taken:0})
        })
    } catch (error) {
       await WebServiceLog.create({request:JSON.stringify({call,CallSid}),response:JSON.stringify(error),service_name:'Exotel Call Service  with call details failed',status:1,uid:0,time_taken:0})
    }
  }

  async checkUserCallStatusWithExpert(data,type){
    // var userStatus = await firebaseHelper.checkUserStatus(data?.receiver)
    // if(userStatus?.status==0){
    //   return {status:1,msg:'Expert is offline, not available for call'}
    // }
     // Block User Status.
     var userBlockStatus= await BlockList.query().where({byUid:data?.receiver,toUid:data?.initiater}).first()
     if(userBlockStatus){
        return {status:1,msg:'Expert blocked to user.'}
     }
     var expertBlockStatus= await BlockList.query().where({byUid:data?.initiater,toUid:data?.receiver}).first()
     if(expertBlockStatus){
       return {status:1,msg:'User blocked to Expert.'}
     }
     var receiverExpert:any = await User.find(data?.receiver)
     if(receiverExpert?.incomingCallSetting==2){ // Nobody can call
      return {status:1,msg:'Nobody can call to Expert.'}
     }else if(receiverExpert?.incomingCallSetting==4){ // No one call call without appointment
      const currentDate=new Date().toISOString().split('T')[0]
      var callrequest = await Request.query().preload('slots',function(query){
                                  query.preload('schedule')
                              })
                              .where({'requestedBy':data?.initiater,'toUid':data?.receiver,status:2})
                              .whereRaw("DATE(booking_date) =?",[currentDate]).first()
      if(callrequest){
          var requestStatus= await this.bookingRequestSlotValidate(callrequest)
          if(!requestStatus){
            return {status:1,msg:'No appointment with this Expert, please request this expert for call!'}
          }
      }else{
        return {status:1,msg:'No appointment with this Expert, please request this expert for call!'}
      }
     }else if(receiverExpert?.incomingCallSetting==1 && receiverExpert?.incomingCallSettingOption==1){ // Any one can call with appointment
      const currentDate=new Date().toISOString().split('T')[0]
      var callrequest = await Request.query().preload('slots',function(query){
                                  query.preload('schedule')
                              })
                              .where({'requestedBy':data?.initiater,'toUid':data?.receiver,status:2})
                              .whereRaw("DATE(booking_date) =?",[currentDate]).first()
      if(callrequest){
          var requestStatus= await this.bookingRequestSlotValidate(callrequest)
          if(!requestStatus){
            return {status:1,msg:'No appointment with this Expert, please request this expert for call!'}
          }
      }else{
        return {status:1,msg:'No appointment with this Expert, please request this expert for call!'}
      }
     }else if(receiverExpert?.incomingCallSetting==1 && receiverExpert?.incomingCallSettingOption==2){ // Any one can call without appointment

     }else if(receiverExpert?.incomingCallSetting==3 && receiverExpert?.incomingCallSettingOption==1){ // only favourite can call with appointment
      const favUser = await FollowUser.query().where({targetId:data?.initiater,uid:data?.receiver}).first()
      if(favUser){
        const currentDate=new Date().toISOString().split('T')[0]
        var callrequest = await Request.query().preload('slots',function(query){
                                    query.preload('schedule')
                                })
                                .where({'requestedBy':data?.initiater,'toUid':data?.receiver,status:2})
                                .whereRaw("DATE(booking_date) =?",[currentDate]).first()
        if(callrequest){
            var requestStatus= await this.bookingRequestSlotValidate(callrequest)
            if(!requestStatus){
              return {status:1,msg:'No appointment with this Expert, please request this expert for call!'}
            }else{
              //return requestStatus
            }
        }else{
          return {status:1,msg:'No appointment with this Expert, please request this expert for call!'}
        }
      }else{
        return {status:1,msg:'You can not call this Expert.'}
      }
      return 
     }else if(receiverExpert?.incomingCallSetting==3 && receiverExpert?.incomingCallSettingOption==2){ // only favourite can call without appointment
      const favUser = await FollowUser.query().where({targetId:data?.initiater,uid:data?.receiver}).first()
      if(!favUser){
        return {status:1,msg:'You can not call this Expert.'}
      }
     }
    var onGoingCall=await firebaseHelper.checkOnGoingCallWithExpert(data?.receiver)
    if(onGoingCall>0){
      const currentDate=new Date().toISOString().split('T')[0]
      var callrequest = await Request.query().preload('slots',function(query){
                                      query.preload('schedule')
                              })
                              .where({'requestedBy':data?.initiater,'toUid':data?.receiver,status:2})
                              .whereRaw("DATE(booking_date) =?",[currentDate]).first()
      if(callrequest){
        var requestStatus= await this.bookingRequestSlotValidate(callrequest)
        if(requestStatus){
          await notificationHelper.busyUserNotification({expertId:data?.receiver})
        }
        return {status:1,msg:'Expert is on another call, please wait and call after some time'}
      }
      return {status:1,msg:'Expert is on another call, please wait and call after some time'}
    }
		if(receiverExpert?.verified){
      var callDur = await this.estimatedCallDuration(data?.initiater,data,type)//VOIP/Exotel
      await WebServiceLog.create({request:JSON.stringify({data,type}),response:JSON.stringify(callDur),service_name:'User call duration',status:1,uid:0,time_taken:0})
			if(callDur<1){
        return {status:1,msg:'You have no enough balance for calling, please recharge your wallet'}
      }
    }
    return {status:0}
  }

  async bookingRequestSlotValidate(request){

      if(request){
        var slots=request?.slots
        const fSlot=slots[0]
        const lSlot=slots[slots.length-1]
        const fromTime = fSlot?.schedule?.fromTime.split(':')
        const toTime = lSlot?.schedule?.toTime.split(':')
        const bookingDate=DateTime.fromISO(new Date(Date.parse(request?.bookingDate)).toISOString())
        const bookingDateStart = bookingDate.setZone('Asia/Kolkata').set({hour: fromTime[0],minute: fromTime[1]-1,second:0})
        const bookingDateEnd = bookingDate.setZone('Asia/Kolkata').set({hour: toTime[0],minute: toTime[1],second:0})
        const bookingDateStartNew=bookingDateStart.toUTC().toMillis()
        const bookingDateEndNew=bookingDateEnd.toUTC().toMillis()
        var currentDate=DateTime.local().set({second:0,millisecond:0}).toUTC()
        const newcurrentDate = currentDate.toMillis()
       // return {bookingDateStart,bookingDateStartNew,bookingDateEnd,bookingDateEndNew,currentDate,newcurrentDate}
        const valid=bookingDateStartNew < newcurrentDate && bookingDateEndNew > newcurrentDate
        await WebServiceLog.create({request:JSON.stringify(request),response:JSON.stringify({bookingDateStart,currentDate, bookingDateEnd,fromTime,toTime,bookingDateStartNew,bookingDateEndNew,newcurrentDate}),service_name:'Request Slot validation',status:1,uid:0,time_taken:0})
        return valid
      }
  }

  async userProfile(uid){

    try {
      var user:any= await User.query()
                .where('id',uid).first()
      // var userAll:any= await User.query()
      //           .preload('accomplishmentsList')
      //           .preload('certificatesList')
      //           .preload('projectList')
      //           .preload('country')
      //           .preload('state')
      //           .preload('highestEducation')
      //           .preload('occupation')
      //           .preload('skillsList')
      //           .preload('intExpertiseList')
      //           .where('id',uid).first()
                
     // return response.status(200).json(userAll)
      /** Rating And Responce Count */
      const rat= await this.userRating(uid)
      const respo=await this.expertResponceRate(uid)
      user.rating=rat?.rating||0
      user.ratingCount=rat?.count||0
      user.callResponseRate=respo||0
      user.userAudioCallRate=user?.audioCallRate||0
      user.userVideoCallRate=user?.videoCallRate||0
      if(user.audioCallRate>0){
        user.audioCallRate= await this.showCallAmount(user?.audioCallRate||0,false,'VOIP')
      }
      if(user.videoCallRate>0){
        user.videoCallRate=await this.showCallAmount(user?.videoCallRate||0,true,'VOIP')
      }
      //user.conversations=await this.userConversations(uid)||0
       /** Rating And Responce Count */

      // if(!userAll?.country){
      //   userAll.country={id:0,name:'',parentId:0,masterType:0,imageUrl:''}
      // }else{
      //   userAll.country={
      //     id:userAll.country?.countryStateId||0,
      //     name:userAll.country?.name||'',
      //     parentId:userAll.country?.parentId||0,
      //     masterType:userAll.country?.masterType||0,
      //     imageUrl:userAll.country?.imageUrl||'',
      //     used: userAll.country?.used||0,
      //   }
      // }
      // if(!userAll?.state){
      //   userAll.state={id:0,name:'',parentId:0,masterType:0,imageUrl:''}
      // }else{
      //   userAll.state={
      //     id:userAll.state?.countryStateId||0,
      //     name:userAll.state?.name||'',
      //     parentId:userAll.state?.parentId||0,
      //     masterType:userAll.state?.masterType||0,
      //     imageUrl:userAll.state?.imageUrl||'',
      //     used: userAll.state?.used||0,
      //   }
      // }
      // if(userAll.intExpertiseList){
      //   userAll.intExpertiseList.map(elm=>{
      //     elm.id=elm.masterGodownId
      //     return elm
      //   })
      // }
      // if(userAll.skillsList){
      //   userAll.skillsList.map(elm=>{
      //     elm.id=elm.masterGodownId
      //     return elm
      //   })
      // }
      // if(userAll.highestEducation){
      //   userAll.highestEducation.id=userAll.highestEducation.masterGodownId
      // }
      // if(userAll.occupation){
      //   userAll.occupation.id=userAll.occupation.masterGodownId
      // }
      // var newAcc:any=[]
      // if(userAll.accomplishmentsList){
      //  for (const accElement of userAll.accomplishmentsList) {
      //   let mdImg:any=[]
      //     var newAccElement:any=accElement.serialize()
      //     newAccElement.medias.forEach(elm => {
      //       mdImg.push(elm?.url) 
      //     })
      //     newAccElement.medias=mdImg
      //     newAccElement.media=mdImg
      //     newAcc.push(newAccElement) 
      //  }
      // }
      // var newCer:any=[]
      // if(userAll.certificatesList){ 
      //   for (const accElement of userAll.certificatesList) {
      //     let mdImg:any=[]
      //      var newCerElement:any=accElement.serialize()
      //      newCerElement.medias.forEach(elm => {
      //        mdImg.push(elm?.url) 
      //      })
      //      newCerElement.medias=mdImg
      //      newCerElement.media=mdImg
      //      newCer.push(newCerElement) 
      //   }
      //  }
      //  var newPro:any=[]
      // if(userAll.projectList){
      //   for (const accElement of userAll.projectList) {
      //     let mdImg:any=[]
      //      var newProElement:any=accElement.serialize()
      //      newProElement.medias.forEach(elm => {
      //        mdImg.push(elm?.url) 
      //      })
      //      newProElement.medias=mdImg
      //      newProElement.media=mdImg
      //      newPro.push(newProElement) 
      //   }
      //  }


     
      // // var block= await BlockList.query().where({byUid:userAll?.id,toUid:uid}).first()?true:false
      // // var blocked= await BlockList.query().where({byUid:uid,toUid:userAll?.id}).first()?true:false
      // var blocked= await BlockList.query().where({byUid:userAll?.id,toUid:uid}).first()?true:false
      // var block= await BlockList.query().where({byUid:uid,toUid:userAll?.id}).first()?true:false

      // var followStatus = await FollowUser.query().where({uid:uid,targetId:userAll?.id}).first()?true:false
      
      // var userdata:any = {
      //             accomplishmentsList:newAcc,
      //             certificatesList:newCer,
      //             projectList:newPro,
      //             profile:user,
      //             country:userAll?.country,
      //             state:userAll?.state,
      //             highestEducation:userAll?.highestEducation,
      //             intExpertiseList:userAll?.intExpertiseList,
      //             occupation:userAll?.occupation,
      //             skillsList:userAll?.skillsList,
      //             viewBy:[],
      //             followstatus:followStatus,
      //             block: block,
      //             blocked: blocked,
      //             rating:rat?.rating,
      //             ratingCount:rat?.count||0,
      //             callResponseRate:respo||0
      //         }             
      return user
    } catch (error) {
      await WebServiceLog.create({request:JSON.stringify(uid),response:JSON.stringify(error),service_name:'get user profile Service by helper',status:1,uid:uid,time_taken:0})
        return false
    }

  }
  
  async getAllDates(day,noDate){
    var allDates=new Array()
    for (var i = 0; i < noDate; i++ ){
      var date
      if((i-1)>=0){
        date= new Date(allDates[i-1])
        date.setHours(0,0,0,0)
        date.setDate(date.getDate() + +!!true + 
        (day + 7 - date.getDay() - +!!true) % 7)
        allDates[i]=date
      }else{
        date=new Date()
        date.setHours(0,0,0,0)
        date.setDate(date.getDate() + +!!false + 
        (day + 7 - date.getDay() - +!!false) % 7)
        allDates[i]=date
      }
    }
    return allDates
  }

  async timeComp(time){
    // var d = new Date()
    // var m = d.getMinutes()
    // var h = d.getHours()
    var d = DateTime.local().setZone('Asia/Kolkata')
    var m = d.minute
    var h = d.hour
    if(h == 0) {h = 24}
    var currentTime:any = h+"."+m
    var time = time.split(":")
    var hour = time[0]
    if(hour == '00') {hour = 24}
    var min = time[1]
    var inputTime:any = hour+"."+min
    var totalTime:any = currentTime - inputTime
    //console.log(currentTime,inputTime,totalTime)
    if (totalTime > 0) {
      return true
    }else{
      return false
    }
  }

  async igstCgst(initiaterId){
    const headquarterState = await AppSetting.getValueByKey('headquarter_state')
    const userSatate = await UserCountryState.getValueByUid(initiaterId)
    if(userSatate==headquarterState){
      await WebServiceLog.create({request:JSON.stringify({userSatate,headquarterState}),response:JSON.stringify(0),service_name:'Gst type Sgst',status:1,uid:initiaterId,time_taken:0})
      return false
    }else{
      await WebServiceLog.create({request:JSON.stringify({userSatate,headquarterState}),response:JSON.stringify(1),service_name:'Gst type Igst',status:1,uid:initiaterId,time_taken:0})
      return true
    }
    //return {headquarterState,userSatate}
  }

  async imageCropAndUpload(url,tag){
    const tagId=tag?.id
    if(url){
      const fileExtName=ext(url)
      const newFileName = `${new Date().getTime()+tagId}${fileExtName}`
      var readStream = request(url)
      if(fileExtName=='.png' || fileExtName=='.jpg' ||fileExtName=='.jpeg'){
        await gm(readStream)
          .resize('200', '200')
          .write('./public/appimages/'+newFileName, async function (err) {
            await WebServiceLog.create({request:JSON.stringify(url),response:JSON.stringify(err),service_name:'Image resize service',status:1,uid:0,time_taken:0})
            if(err){
              await WebServiceLog.create({request:JSON.stringify(url),response:JSON.stringify(err),service_name:'Image resize service error',status:1,uid:0,time_taken:0})
            }
            if(!err){
              const contentType=fileExtName=='.png'?'image/png':'image/jpeg'
              const uploadedFile=await uploadHelper.uploadFileToFolder(newFileName,contentType,'appimages')
              if(uploadedFile){
                if(fileExtName=='.png' || fileExtName=='.jpg' ||fileExtName=='.jpeg'){
                  tag.imageUrl=uploadedFile?.Location
                }else{
                  tag.imageUrl=''
                }
                await tag.save()
                fs.unlink(Application.publicPath((+'appimages/'+newFileName)))
              }
            }
        })
      }
    }
  }

  async genTextImage(text,tag){
    const tagId=tag?.id||1
    const newFileName = "text_"+`${new Date().getTime()+tagId}.${'jpg'}`
    const str = (text.charAt(0)+text.charAt(text.length-1)).toUpperCase()
    await gm(200, 200, "#36689B")
    .font("Helvetica.ttf", 80,)
    .fill('#ffffff')
    .drawText(45, 125, str,10)
    .write('./public/appimages/'+newFileName, async function (err) {
      if(err){
        await WebServiceLog.create({request:JSON.stringify(text),response:JSON.stringify(err),service_name:'Text Image create service error',status:1,uid:0,time_taken:0})
      }
      if(!err){
        const contentType='image/jpg'
        const uploadedFile=await uploadHelper.uploadFileToFolder(newFileName,contentType,'appimages')
        if(uploadedFile){
            tag.imageUrl=uploadedFile?.Location
            await tag.save()
         // fs.unlink(Application.publicPath((+'appimages/'+newFileName)))
        }
      }
    })
  }

  async extractHashTag(str){
    var regexp = /(\s|^)\#\w\w+\b/gm
    var result = str.match(regexp)
    if (result) {
        result = result.map(function(s){ return s.trim()})
        return result
    }else{
        return ''
    }
  }

  async matchHashTagAndSave(data,uid){
    if(data.length>0){
      for await (const tag of data) {
        const existTag= await MasterGodown.query().where('name',tag).first()
        if(!existTag){
          const newMData= new MasterGodown()
          newMData.imageUrl=''
          newMData.masterType=6
          newMData.name=tag
          newMData.createdBy=uid
          newMData.isVerified=false
          newMData.used=1
          await newMData.save()
        }else{
          await MasterGodown.query().where('id',existTag?.id).increment('used',1)
        }
      }
    }
  }

  async checkMobileDevice(userAgent){
      let check = false;
      (function(a){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4))) check = true;})(userAgent);
      return check;
  }

  async localImageCropAndUpload(file,fileExtName,tag){
    const tagId=tag?.id||123
    if(file){
      const newFileName = `${new Date().getTime()+tagId}${fileExtName}`
      var readStream = fs.createReadStream('./public/appimages/'+file)
     // var readStream = fs.createReadStream('/path/to/my/img.jpg')
      if(fileExtName=='.png' || fileExtName=='.jpg' ||fileExtName=='.jpeg'){
        await gm(readStream,file)
          .resize('200', '200')
          .write('./public/appimages/'+newFileName, async function (err) {
            await WebServiceLog.create({request:JSON.stringify({readStream,newFileName}),response:JSON.stringify(err),service_name:'Image resize service',status:1,uid:0,time_taken:0})
            if(err){
              await WebServiceLog.create({request:JSON.stringify({readStream,newFileName}),response:JSON.stringify(err),service_name:'Image resize service error',status:1,uid:0,time_taken:0})
            }
            if(!err){
              const contentType=fileExtName=='.png'?'image/png':'image/jpeg'
              const uploadedFile=await uploadHelper.uploadFileToFolder(newFileName,contentType,'appimages')
              if(uploadedFile){
                if(fileExtName=='.png' || fileExtName=='.jpg' ||fileExtName=='.jpeg'){
                  tag.imageUrl=uploadedFile?.Location
                  tag.image_url=uploadedFile?.Location
                }else{
                  tag.imageUrl=''
                  tag.image_url=''
                }
                await tag.save()
                fs.unlink(Application.publicPath((+'appimages/'+newFileName)))
              }
            }
        })
      }
    }
  }

  async genTextImageForProfile(text,user){
    const tagId=user?.id||1
    const newFileName = "text_"+`${new Date().getTime()+tagId}.${'jpg'}`
    const str = (text.charAt(0)+text.charAt(text.length-1)).toUpperCase()
    await gm(200, 200,"#36689B")
    .font("Helvetica.ttf",80)
    .fill('#ffffff')
    .drawText(45,120,str,10)
    .write('./public/profile/'+newFileName, async function (err) {
      if(err){
        await WebServiceLog.create({request:JSON.stringify(text),response:JSON.stringify(err),service_name:'Text Image create service For User Profile error',status:1,uid:0,time_taken:0})
      }
      if(!err){
        const contentType='image/jpg'
        const uploadedFile=await uploadHelper.uploadFileToFolder(newFileName,contentType,'profile')
        await WebServiceLog.create({request:JSON.stringify(text),response:JSON.stringify(uploadedFile),service_name:'Text Image For Profile Upload',status:1,uid:0,time_taken:0})
        if(uploadedFile){
          user.profilePic=uploadedFile?.Location
          await user.save()
          await WebServiceLog.create({request:JSON.stringify(uploadedFile),response:JSON.stringify(user),service_name:'Text Image For Profile Upload User Data',status:1,uid:0,time_taken:0})
          //fs.unlink(Application.publicPath(('profile/'+newFileName)))
        }else{
          await WebServiceLog.create({request:JSON.stringify({newFileName,contentType}),response:JSON.stringify(err),service_name:'Text Image For Profile Upload error',status:1,uid:0,time_taken:0})
        }
      }
    })
  }

  async genTextImageForProfileNew(text,user){
    const tagId=user?.id||1
    const newFileName = "text_"+`${new Date().getTime()+tagId}.${'jpg'}`
    text = text.split(" ")
    var str
    if(text.length>=2){
      str = (text[0]?.charAt(0)+text[1]?.charAt(0)).toUpperCase()
    }else{
      str = (text[0].charAt(0)+text[0].charAt(1)).toUpperCase()
    }
    const color = profileColor[Math.floor(Math.random() * (18 - 1 + 1) + 1)]
    const textImage =  new Promise( function(resolve,reject) {
                            let img
                            try {
                              img =  gm(500, 500,color)
                              .font("Helvetica.ttf",160)
                              .fill('#ffffff')
                              .gravity('Center')
                              .drawText(0,0,str)
                              .write('./public/profile/'+newFileName, async(err)=>{
                                  if(err) {
                                    throw(err)
                                  }
                                  const contentType='image/jpg'
                                  const uploadedFile=await uploadHelper.uploadFileToFolder(newFileName,contentType,'profile')
                                  resolve(uploadedFile)
                                });
                            }catch (err) {
                              reject(err)
                            }
                             //resolve(img)
                          })
        await WebServiceLog.create({request:JSON.stringify(textImage),response:JSON.stringify({}),service_name:'Text Image For Profile Upload 1',status:1,uid:0,time_taken:0})  
        if(textImage){
          return textImage
        }else{
          return false
        }
  }

  async imageCropAndUploadByUrl(url){
    const tagId=Math.floor(Math.random() * (99 - 1 + 1) + 1)
    if(url){
      var fileExtName=ext(url)||'.jpg'
      if(fileExtName=='.png' || fileExtName=='.jpg' ||fileExtName=='.jpeg'){

      }else{
        fileExtName='.jpg'
      }
      const newFileName = `${new Date().getTime()+tagId}${fileExtName}`
      var readStream = request(url)
      
      // await downloadNew(url, './public/appimages/'+newFileName, async function(){ 

      // })
      // return readStream
      //  return fileExtName
      if(fileExtName=='.png' || fileExtName=='.jpg' ||fileExtName=='.jpeg'){
        const Image =  new Promise( function(resolve,reject) {
          let img
          try {
            img = gm(readStream)
            .resize('200', '200')
            .write('./public/appimages/'+newFileName, async function (err) {
              await WebServiceLog.create({request:JSON.stringify(url),response:JSON.stringify(err),service_name:'Image resize service By Url',status:1,uid:0,time_taken:0})
              if(err){
                await WebServiceLog.create({request:JSON.stringify(url),response:JSON.stringify(err),service_name:'Image resize service By Url error',status:1,uid:0,time_taken:0})
                throw(err)
              }
              if(!err){
                const contentType=fileExtName=='.png'?'image/png':'image/jpeg'
                const uploadedFile=await uploadHelper.uploadFileToFolder(newFileName,contentType,'appimages')
                resolve(uploadedFile)
              }
          })
          }catch (err) {
            reject(err)
          }
        })
        if(Image){
          return Image
        }else{
          return false
        }
      }
    }
  }

  async chanalIdByUrl(url){
    var pattern = new RegExp(
      '^(https?://)?(www.)?youtube.com/(user/)?([a-z-_0-9]+)/?([?#]?.*)',
      'i',
    )
    var matches = url.match(pattern)
    if (matches.length>1) {
      if(matches[4]=='c'){
        return {cId:matches[matches.length-1],type:3}
      }
      return {cId:matches[matches.length-1],type:1}
    }
    return url
  }

  async youtubeVideos(url,uid){
    perf.start()
    const chId = await this.chanalIdByUrl(url)
    const sortBy = 'popular' // most views video
    const chanalType = chId?.type||0
    const chVideos:any = new Promise( async function(resolve,reject) {
     await ytch.getChannelVideos(chId?.cId, sortBy,chanalType).then((response) => {
      const time = perf.stop()
        WebServiceLog.create({request:JSON.stringify(url),response:JSON.stringify(response),service_name:'Youtube Videos By chanal Url success',status:1,uid:uid,time_taken:time?.time})
        resolve(response)
      }).catch((err) => {
        const time = perf.stop()
         WebServiceLog.create({request:JSON.stringify(url),response:JSON.stringify(err),service_name:'Youtube Videos By chanal Url error',status:1,uid:uid,time_taken:time?.time})
        reject(err)
      })
    })
    return chVideos
  }
  async addYoutubeVideos(url,uid){
    const videos = await this.youtubeVideos(url,uid)
    perf.start()
    if(videos.items.length>0){
      const firstSixVideos = videos.items.splice(0, 6)
      await YoutubeVideo.query().where('uid',uid).delete()
      for await (const item of firstSixVideos) {
        const yVideo = new YoutubeVideo()
        yVideo.uid=uid
        yVideo.title=item?.title||''
        yVideo.description=item?.title||''
        yVideo.videoId=item?.videoId||''
        yVideo.videoThumbnails=JSON.stringify(item?.videoThumbnails)||''
        yVideo.viewCount=item?.viewCount||0
        yVideo.videoLength=item?.lengthSeconds||0
        await yVideo.save()
      }
      const time = perf.stop()
      await WebServiceLog.create({request:JSON.stringify(videos),response:JSON.stringify(firstSixVideos),service_name:'Youtube Videos Save success',status:1,uid:uid,time_taken:time?.time})
    }
  }
    
  async userPostsAndCallsCount(uid){
    const postCount= await Post.query().where('createdBy',uid)
                          .pojo<{ count: number}>()
                          .count('id as count').first()
    const msgCount = await Chat.query().where(q=>{
                            q.where('senderId',uid).orWhere('receiverId',uid)
                          })
                          .pojo<{ count: number}>()
                          .count('id as count').first()
    const recCalls = await CallHistory.query().where((query)=>{
                      query.where("duration",'>',1).whereNotNull('duration')
                    }).where((query)=>{
                      query.where({'receiver':uid})
                    })
                    .pojo<{ count: number}>()
                    .count('* as count').first()
    
    const intCalls = await CallHistory.query().where((query)=>{
                      query.where("duration",'>',1).whereNotNull('duration')
                    }).where((query)=>{
                      query.where({'initiater':uid})
                    })
                    .pojo<{ count: number}>()
                    .count('* as count').first()
      return {totalPost:postCount?.count,totalMsg:msgCount?.count,recivedCalls:recCalls?.count,initiatCalls:intCalls?.count}                                                
  }
}