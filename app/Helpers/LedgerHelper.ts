//import Mail from '@ioc:Adonis/Addons/Mail'
//import Env from '@ioc:Adonis/Core/Env'
//const From=Env.get('FROM_EMAIL') as string
import Database from '@ioc:Adonis/Lucid/Database'
import Razorpay from "razorpay"
import Env from '@ioc:Adonis/Core/Env'
import LedgerAccount from "App/Models/LedgerAccount"
import TransactionHistory from 'App/Models/TransactionHistory'
import WebServiceLog from 'App/Models/WebServiceLog'
import Invoice from 'App/Models/Invoice'
import AppSetting from 'App/Models/AppSetting'

import CallHistory from 'App/Models/CallHistory'
import Request from 'App/Models/Request'
const RazorPayKey=Env.get('RazorPayKey') as string
const RazorPaySecret=Env.get('RazorPaySecret') as string
const AccountNumber=Env.get('AccountNumber') as string
//const merchant_account_id=Env.get('MerchantAccountNumber') as string
//const axios = require('axios')
const RZP = new Razorpay({
  key_id: RazorPayKey,
  key_secret: RazorPaySecret
})
const perf = require('execution-time')()
const calculateDays:any= async (date)=>{
    const oneDay = 24 * 60 * 60 * 1000; // hours*minutes*seconds*milliseconds
    const firstDate:any = new Date()
    const secondDate:any = new Date(date)
    return Math.round(Math.abs((firstDate - secondDate) / oneDay))
}

const calculateCallAmount:any= async(callData)=>{
  const callRate=callData?.rate
  const duration=callData?.duration
  return parseFloat((duration*callRate).toFixed(2))
}
//var request = require('request')
var rp = require('request-promise')
export default class LedgerHelper {
  
  async createGatewayToUserWallate(data){
    perf.start()
    const transId=data.razorPayId
    const amount=data.amt
    const uid=data.uid
    const razorpayWallet:any=await LedgerAccount.query().where({name:'Razorpay',uid:1}).first()
    const seekexWallet:any = await LedgerAccount.query().where({name:'Seekex',uid:2}).first()
    try {
      var paymentDetail = await RZP.payments.fetch(transId)
      if(paymentDetail?.status=='authorized'){
        paymentDetail = await RZP.payments.capture(transId, paymentDetail?.amount, paymentDetail?.currency)
      }
     // return paymentDetail
      if(amount==(paymentDetail?.amount/100) && paymentDetail?.status=='captured' && paymentDetail?.status!='failed' && paymentDetail?.status!='refunded'){
        var userWallet:any=await LedgerAccount.query().where({name:'User',uid:uid}).first()
       const trns_id= await Database.transaction(async (trx) => {
          /***************************Payment Gateway To Razorpay Wallet Transaction****************************/
          const inRazToRazWallet=new TransactionHistory()
          inRazToRazWallet.description=(amount)+' add to Razorpay Wallet'
          inRazToRazWallet.fromWallet=0
          inRazToRazWallet.toWallet=razorpayWallet?.id
          inRazToRazWallet.isdebit=false
          inRazToRazWallet.razorPayPaymentId=transId
          inRazToRazWallet.bankId=paymentDetail?.acquirer_data?.bank_transaction_id||0
          inRazToRazWallet.status=1
          inRazToRazWallet.transactionAmount=paymentDetail?.amount/100
          inRazToRazWallet.transactionType=1
          inRazToRazWallet.transactionWalletId=razorpayWallet?.id
          inRazToRazWallet.transactionTo='Razorpay'
          inRazToRazWallet.transactionAction='plus'
          inRazToRazWallet.uid=uid
          inRazToRazWallet.payout=false
          inRazToRazWallet.useTransaction(trx)
          await inRazToRazWallet.save()

        /***************************Deduct To Razorpay Wallet For Seekex Wallet****************************/
          const outRazWallet=new TransactionHistory()
          outRazWallet.description=(amount)+' minus to Razorpay Wallet'
          outRazWallet.fromWallet=razorpayWallet?.id
          outRazWallet.toWallet=0
          outRazWallet.isdebit=true
          outRazWallet.status=1
          outRazWallet.transactionAmount=paymentDetail?.amount/100
          outRazWallet.transactionType=1
          outRazWallet.transactionWalletId=razorpayWallet?.id
          outRazWallet.transactionTo='Razorpay'
          outRazWallet.transactionAction='minus'
          outRazWallet.uid=uid
          outRazWallet.payout=false
          outRazWallet.useTransaction(trx)
          await outRazWallet.save()
        /***************************Add To Seekex Wallet From  Razorpay Wallet****************************/
          const inRazWalletToSeekexWallet=new TransactionHistory()
          inRazWalletToSeekexWallet.description=(amount)+' Add To Seekex Wallet From  Razorpay Wallet'
          inRazWalletToSeekexWallet.fromWallet=razorpayWallet?.id
          inRazWalletToSeekexWallet.toWallet=seekexWallet?.id
          inRazWalletToSeekexWallet.isdebit=false
          inRazWalletToSeekexWallet.status=1
          inRazWalletToSeekexWallet.transactionAmount=paymentDetail?.amount/100
          inRazWalletToSeekexWallet.transactionType=1
          inRazWalletToSeekexWallet.transactionWalletId=seekexWallet?.id
          inRazWalletToSeekexWallet.transactionTo='Seekex'
          inRazWalletToSeekexWallet.transactionAction='plus'
          inRazWalletToSeekexWallet.uid=uid
          inRazWalletToSeekexWallet.payout=false
          inRazWalletToSeekexWallet.useTransaction(trx)
          await inRazWalletToSeekexWallet.save()

        /***************************Deduct To Seekex Wallet For User Wallet****************************/
          const outSeekexWallet=new TransactionHistory()
          outSeekexWallet.description=(amount)+' minus to Seekex Wallet'
          outSeekexWallet.fromWallet=seekexWallet?.id
          outSeekexWallet.toWallet=0
          outSeekexWallet.isdebit=true
          outSeekexWallet.status=1
          outSeekexWallet.transactionAmount=paymentDetail?.amount/100
          outSeekexWallet.transactionType=1
          outSeekexWallet.transactionWalletId=seekexWallet?.id
          outSeekexWallet.transactionTo='Seekex'
          outSeekexWallet.transactionAction='minus'
          outSeekexWallet.uid=uid
          outSeekexWallet.payout=false
          outSeekexWallet.useTransaction(trx)
          await outSeekexWallet.save()
        /***************************Add To User Wallet From  Seekex Wallet****************************/ 
          const inSeekexWalletToUserWallet:any=new TransactionHistory()
          inSeekexWalletToUserWallet.description=(amount)+' Add To User Wallet From Seekex Wallet'
          inSeekexWalletToUserWallet.fromWallet=seekexWallet?.id
          inSeekexWalletToUserWallet.toWallet=userWallet?.id
          inSeekexWalletToUserWallet.isdebit=false
          inSeekexWalletToUserWallet.status=1
          inSeekexWalletToUserWallet.transactionAmount=paymentDetail?.amount/100
          inSeekexWalletToUserWallet.transactionType=1 // 1 Fow add money to wallet
          inSeekexWalletToUserWallet.transactionWalletId=userWallet?.id
          inSeekexWalletToUserWallet.transactionTo='User'
          inSeekexWalletToUserWallet.transactionAction='plus'
          inSeekexWalletToUserWallet.uid=uid
          inSeekexWalletToUserWallet.payout=false
          inSeekexWalletToUserWallet.useTransaction(trx)
          await inSeekexWalletToUserWallet.save()
          userWallet.availableAmount=userWallet.availableAmount+(paymentDetail?.amount/100)
          userWallet.totalAmount=userWallet.totalAmount+(paymentDetail?.amount/100)
          await userWallet.save()
        /****************************** Invoice for transaction amount *******************************************/
          const invoice:any = new Invoice()
          invoice.callId=0
          invoice.commissionAmount=0
          invoice.transactionId=inSeekexWalletToUserWallet?.id
          invoice.finalAmount=paymentDetail?.amount/100
          invoice.salesAmount=paymentDetail?.amount/100
          invoice.totalAmount=paymentDetail?.amount/100
          invoice.invoiceDate = new Date()
          invoice.isigst = false
          invoice.gstAmount  = 0
          invoice.gstCommissionAmount  = 0
          invoice.useTransaction(trx)
          await invoice.save()
        /************************************************************************************************/
          return inSeekexWalletToUserWallet?.id||0
        })
        return {status:'success',statusCode:200,amount:paymentDetail?.amount/100,transaction_id:trns_id}
      }else{
        return {status:'error',statusCode:200,msg:paymentDetail.error_description?paymentDetail.error_description:'Invalid transaction.'}
      }
     // return paymentDetail
    } catch (error) {
      const results = perf.stop()
      await WebServiceLog.create({request:JSON.stringify(data),response:JSON.stringify(error),service_name:'Wallet recharge Service Helper error',status:1,uid:uid,time_taken:results.time})
      return {status:'error',statusCode:error.statusCode,msg:error.error.description}
    }
  }

  async calculateAndTransferAmountForCall(data){
    perf.start()
    const uid=data?.uid||data?.initiater
    const expertId=data?.receiver||data?.receiver
    const callId=data?.callId||data?.id
    var userWallet:any=await LedgerAccount.query().where({'uid':uid,name:'User'}).first()
    var expertWallet:any=await LedgerAccount.query().where({'uid':expertId,name:'User'}).first()
    const seekexWallet:any = await LedgerAccount.query().where({name:'Seekex',uid:2}).first()
    var sgstWallet:any=await LedgerAccount.query().where({'uid':3,name:'Sgst'}).first()
    var cgstWallet:any=await LedgerAccount.query().where({'uid':4,name:'Cgst'}).first()
    var igstWallet:any=await LedgerAccount.query().where({'uid':5,name:'Igst'}).first()
    var commissionWallet:any=await LedgerAccount.query().where({'uid':6,name:'Commission'}).first()
    var exotelWallet:any=await LedgerAccount.query().where({'uid':7,name:'Exotel'}).first()
    var voipWallet:any=await LedgerAccount.query().where({'uid':8,name:'VOPI'}).first()

    // const IGST=parseFloat(Env.get('IGST_EXCLUSIVE') as string)
    // const commission=parseFloat(Env.get('COMMISSION_AMOUNT') as string)

    const IGST=(data?.gstPercentage/100)||await AppSetting.getValueByKey('gst')/100
    const commission=(data?.commissionPercentage/100)||await AppSetting.getValueByKey('commission')/100
    
    const callDuration = parseInt(data.duration)
    const callRate = parseFloat(((data?.callRate||data?.rate)).toFixed(5))
    const totalCalls = Math.ceil(callDuration)
    var callAmount=parseFloat((totalCalls*callRate).toFixed(2))
    if(callAmount<=0){
      if(data?.requestId && data?.rate==0){
        await Request.completeRequest(data?.requestId)
      }
      return {status:'error',statusCode:20,msg:'Call Amount not valid'}
    }
    const calgstAmount = parseFloat((callAmount-(callAmount/(100+(100*IGST)))*100).toFixed(2))

    const commissionAmount=parseFloat((callAmount*commission).toFixed(2))

    const remAmount = parseFloat((callAmount-calgstAmount-commissionAmount).toFixed(2))
    
    var exotelAmount=0
    var vopiAmount=0
    var payAmount=0
    var otherCharge=0
    if(data.callingType == 'Exotel'){
      //const exotelCahrge=parseFloat(Env.get('TELEPHONY_CHARGE') as string)
      const exotelCahrge=await AppSetting.getValueByKey('exotel_charge')
      if(exotelCahrge>0){
        exotelAmount = parseFloat((totalCalls*(exotelCahrge/60)).toFixed(2))
        payAmount = parseFloat((remAmount-(exotelAmount)).toFixed(2))
        otherCharge=exotelAmount
      }else{
        payAmount = parseFloat((remAmount).toFixed(2))
      }
      
    }else if(data.callingType == 'VOIP'){
      // const voipAudio=parseFloat(Env.get('VOIP_AUDIO_CALL_CHARGE') as string)
      // const voipVideo=parseFloat(Env.get('VOIP_VIDEO_CALL_CHARGE') as string)
      const voipAudio=await AppSetting.getValueByKey('voip_audio_charge')
      const voipVideo=await AppSetting.getValueByKey('voip_video_charge')

      if(data?.video && voipVideo>0){
        vopiAmount=parseFloat((totalCalls*(voipVideo/60)).toFixed(2))
      }else if(voipAudio>0){
        vopiAmount=parseFloat((totalCalls*(voipAudio/60)).toFixed(2))
      }
      otherCharge=vopiAmount
      payAmount = parseFloat((remAmount-(vopiAmount)).toFixed(2))
    }
    const commissionGST=parseFloat((commissionAmount*IGST).toFixed(2))
    await WebServiceLog.create({request:JSON.stringify(data),response:JSON.stringify({IGST,commission,callAmount,callRate,calgstAmount,commissionAmount,otherCharge,remAmount,payAmount,commissionGST}),service_name:'Call Amount Transfer Service Helper All Calculated Amounts',status:1,uid:uid,time_taken:0})
    //return {IGST,commission,callAmount,callRate,calgstAmount,commissionAmount,otherCharge,remAmount,payAmount,commissionGST}
    try {
    const trns_id= await Database.transaction(async (trx) => {
      /************************Update User Waallet******************************/
        if(data?.requestId){
          const requestAmount = await Request.getAmount(data?.requestId)
          if(callAmount>requestAmount){
            var extraAmount = callAmount-requestAmount
            userWallet.holdAmount=userWallet.holdAmount-requestAmount
            userWallet.totalAmount=userWallet.totalAmount-extraAmount
          }else{
            var remAmount = parseFloat((requestAmount-callAmount).toFixed(2))
           //userWallet.holdAmount=userWallet.holdAmount-callAmount
            userWallet.holdAmount=userWallet.holdAmount-requestAmount
            userWallet.totalAmount=userWallet.totalAmount-callAmount
            if(remAmount>0){
              await this.releaseRemAmountAfterCall(data,remAmount)
            }
          }
          await Request.completeRequest(data?.requestId)
        }else{
            userWallet.totalAmount=userWallet.totalAmount-callAmount
        }
        if(userWallet?.promoAmount>0){
          if(callAmount<userWallet?.promoAmount){
            userWallet.promoAmount=userWallet?.promoAmount-callAmount
          }else{
            userWallet.promoAmount=0
          }
        }

      /************************Update User Waallet [End]******************************/
      const addToSeekexWallet=new TransactionHistory()
      addToSeekexWallet.callId=callId
      addToSeekexWallet.description=callAmount+' minus from User Wallet And Add to Seekex Wallet For call Id =>'+callId
      addToSeekexWallet.fromWallet=userWallet?.id
      addToSeekexWallet.toWallet=seekexWallet?.id
      addToSeekexWallet.isdebit=true
      addToSeekexWallet.status=1
      addToSeekexWallet.transactionAmount=callAmount||0
      addToSeekexWallet.transactionType=1
      addToSeekexWallet.transactionWalletId=userWallet?.id
      addToSeekexWallet.transactionTo='Seekex'
      addToSeekexWallet.transactionAction='minus'
      addToSeekexWallet.uid=uid
      addToSeekexWallet.payout=false
      addToSeekexWallet.useTransaction(trx)
      await addToSeekexWallet.save()
      await userWallet.save()
      
      const addToSeekexWalletFromUser=new TransactionHistory()
      addToSeekexWalletFromUser.callId=callId
      addToSeekexWalletFromUser.description=callAmount+' Add to Seekex Wallet For call Id =>'+callId
      addToSeekexWalletFromUser.fromWallet=userWallet?.id
      addToSeekexWalletFromUser.toWallet=seekexWallet?.id
      addToSeekexWalletFromUser.isdebit=false
      addToSeekexWalletFromUser.status=1
      addToSeekexWalletFromUser.transactionAmount=callAmount||0
      addToSeekexWalletFromUser.transactionType=3
      addToSeekexWalletFromUser.transactionWalletId=seekexWallet?.id
      addToSeekexWalletFromUser.transactionTo='Seekex'
      addToSeekexWalletFromUser.transactionAction='plus'
      addToSeekexWalletFromUser.uid=seekexWallet?.uid
      addToSeekexWalletFromUser.payout=false
      addToSeekexWalletFromUser.useTransaction(trx)
      await addToSeekexWalletFromUser.save()
      seekexWallet.availableAmount=seekexWallet.availableAmount+callAmount
      seekexWallet.totalAmount=seekexWallet.totalAmount+callAmount
      await seekexWallet.save()
      /**************************************************/

      /************************GST Amount Transection *****************************/
    if(data.isigst){
       var igstAmount=calgstAmount
       const addToIgstWallet=new TransactionHistory()
       addToIgstWallet.callId=callId
       addToIgstWallet.description=igstAmount+' Add to igst Wallet For call Id =>'+callId
       addToIgstWallet.fromWallet=seekexWallet?.id
       addToIgstWallet.toWallet=igstWallet?.id
       addToIgstWallet.isdebit=false
       addToIgstWallet.status=1
       addToIgstWallet.transactionAmount=igstAmount||0
       addToIgstWallet.transactionType=1
       addToIgstWallet.transactionWalletId=igstWallet?.id
       addToIgstWallet.transactionTo='Igst'
       addToIgstWallet.transactionAction='plus'
       addToIgstWallet.uid=uid
       addToIgstWallet.payout=false
       addToIgstWallet.useTransaction(trx)
       await addToIgstWallet.save()
       /***********************Update Wallet ****************************/
       igstWallet.availableAmount=igstWallet.availableAmount+igstAmount
       igstWallet.totalAmount=igstWallet.totalAmount+igstAmount
       await igstWallet.save()
     }else{
      var gstAmount=calgstAmount
       const addToCgstWallet=new TransactionHistory()
       addToCgstWallet.callId=callId
       addToCgstWallet.description=gstAmount/2+' Add to cgst Wallet For call Id =>'+callId
       addToCgstWallet.fromWallet=seekexWallet?.id
       addToCgstWallet.toWallet=cgstWallet?.id
       addToCgstWallet.isdebit=false
       addToCgstWallet.status=1
       addToCgstWallet.transactionAmount=gstAmount/2||0
       addToCgstWallet.transactionType=1
       addToCgstWallet.transactionWalletId=cgstWallet?.id
       addToCgstWallet.transactionTo='Igst'
       addToCgstWallet.transactionAction='plus'
       addToCgstWallet.uid=uid
       addToCgstWallet.payout=false
       addToCgstWallet.useTransaction(trx)
       await addToCgstWallet.save()
       /***********************Update Wallet ****************************/
       cgstWallet.availableAmount=cgstWallet.availableAmount+(gstAmount/2)
       cgstWallet.totalAmount=cgstWallet.totalAmount+(gstAmount/2)
       await cgstWallet.save()

       const addToSgstWallet=new TransactionHistory()
       addToSgstWallet.callId=callId
       addToSgstWallet.description=gstAmount/2+' Add to sgst Wallet'
       addToSgstWallet.fromWallet=seekexWallet?.id
       addToSgstWallet.toWallet=sgstWallet?.id
       addToSgstWallet.isdebit=false
       addToSgstWallet.status=1
       addToSgstWallet.transactionAmount=gstAmount/2||0
       addToSgstWallet.transactionType=1
       addToSgstWallet.transactionWalletId=sgstWallet?.id
       addToSgstWallet.transactionTo='Igst'
       addToSgstWallet.transactionAction='plus'
       addToSgstWallet.uid=uid
       addToSgstWallet.payout=false
       addToSgstWallet.useTransaction(trx)
       await addToSgstWallet.save()
       /***********************Update Wallet ****************************/
       sgstWallet.availableAmount=sgstWallet.availableAmount+(gstAmount/2)
       sgstWallet.totalAmount=sgstWallet.totalAmount+(gstAmount/2)
       await sgstWallet.save()
     }
     /************************Commission Amount Transection *****************************/
      const addToCommitionWallet=new TransactionHistory()
      addToCommitionWallet.callId=callId
      addToCommitionWallet.description=commissionAmount+' Add to commission Wallet For call Id =>'+callId
      addToCommitionWallet.fromWallet=seekexWallet?.id
      addToCommitionWallet.toWallet=commissionWallet?.id
      addToCommitionWallet.isdebit=false
      addToCommitionWallet.status=1
      addToCommitionWallet.transactionAmount=commissionAmount||0
      addToCommitionWallet.transactionType=1
      addToCommitionWallet.transactionWalletId=commissionWallet?.id
      addToCommitionWallet.transactionTo='Commission'
      addToCommitionWallet.transactionAction='plus'
      addToCommitionWallet.uid=commissionWallet?.uid
      addToCommitionWallet.payout=false
      addToCommitionWallet.useTransaction(trx)
       await addToCommitionWallet.save()
       /***********************Update Wallet ****************************/
       commissionWallet.availableAmount=commissionWallet.availableAmount+commissionAmount
       commissionWallet.totalAmount=commissionWallet.totalAmount+commissionAmount
       await commissionWallet.save()

       if(data.callingType == 'Exotel' && exotelAmount>0){
      /************************Exotel Amount Transection *****************************/
      const addToExotelWallet=new TransactionHistory()
      addToExotelWallet.callId=callId
      addToExotelWallet.description=exotelAmount+' Exotel charge Add to Exotel Wallet For call Id =>'+callId
      addToExotelWallet.fromWallet=seekexWallet?.id
      addToExotelWallet.toWallet=exotelWallet?.id
      addToExotelWallet.isdebit=false
      addToExotelWallet.status=1
      addToExotelWallet.transactionAmount=exotelAmount||0
      addToExotelWallet.transactionType=1
      addToExotelWallet.transactionWalletId=exotelWallet?.id
      addToExotelWallet.transactionTo='Exotel'
      addToExotelWallet.transactionAction='plus'
      addToExotelWallet.uid=exotelWallet?.uid
      addToExotelWallet.payout=false
      addToExotelWallet.useTransaction(trx)
       await addToExotelWallet.save()
       /***********************Update Wallet ****************************/
       exotelWallet.availableAmount=exotelWallet.availableAmount+exotelAmount
       exotelWallet.totalAmount=exotelWallet.totalAmount+exotelAmount
       await exotelWallet.save()
       }else if(vopiAmount>0){
        const addToVoipWallet=new TransactionHistory()
        addToVoipWallet.callId=callId
        addToVoipWallet.description=vopiAmount+' VOIP charge Add to VOIP Wallet For call Id =>'+callId
        addToVoipWallet.fromWallet=userWallet?.id
        addToVoipWallet.toWallet=voipWallet?.id
        addToVoipWallet.isdebit=false
        addToVoipWallet.status=1
        addToVoipWallet.transactionAmount=vopiAmount||0
        addToVoipWallet.transactionType=1
        addToVoipWallet.transactionWalletId=voipWallet?.id
        addToVoipWallet.transactionTo='VOIP'
        addToVoipWallet.transactionAction='plus'
        addToVoipWallet.uid=voipWallet?.uid
        addToVoipWallet.payout=false
        addToVoipWallet.useTransaction(trx)
       await addToVoipWallet.save()
       /***********************Update Wallet ****************************/
       voipWallet.availableAmount=voipWallet.availableAmount+voipWallet
       voipWallet.totalAmount=voipWallet.totalAmount+voipWallet
       await voipWallet.save()
       }

       /************************Expert Pay Amount Transection *****************************/
       const addToExpertWallet=new TransactionHistory()
       addToExpertWallet.callId=callId
       addToExpertWallet.description=payAmount+' Add to Expert Wallet For call Id =>'+callId
       addToExpertWallet.fromWallet=seekexWallet?.id
       addToExpertWallet.toWallet=expertWallet?.id
       addToExpertWallet.isdebit=false
       addToExpertWallet.status=1
       addToExpertWallet.transactionAmount=payAmount
       addToExpertWallet.transactionType=1
       addToExpertWallet.transactionWalletId=expertWallet?.id
       addToExpertWallet.transactionTo='Expert'
       addToExpertWallet.transactionAction='plus'
       addToExpertWallet.transactionStatus='hold'
       addToExpertWallet.uid=expertWallet?.uid
       addToExpertWallet.payout=false
       addToExpertWallet.useTransaction(trx)
       await addToExpertWallet.save()
       /***********************Update Wallet ****************************/
       expertWallet.holdAmount=expertWallet.holdAmount+payAmount
       expertWallet.totalAmount=expertWallet.totalAmount+payAmount
       await expertWallet.save()
       /****************************** Invoice for transaction amount *******************************************/
        const invoice:any = new Invoice()
        invoice.callId=callId
        invoice.commissionAmount=commissionAmount
        invoice.transactionId=addToExpertWallet?.id||0
        invoice.salesAmount=callAmount-calgstAmount
        invoice.finalAmount=payAmount
        invoice.totalAmount=callAmount
        invoice.invoiceDate = new Date()
        invoice.isigst = data.isigst?true:false
        invoice.gstAmount  = calgstAmount
        invoice.gstCommissionAmount  = commissionGST
        invoice.otherChargeValue  = otherCharge
        invoice.useTransaction(trx)
        await invoice.save()
        
     /************************************************************************************************/
       return addToExpertWallet?.id||0
    })
      const results = perf.stop()
      await WebServiceLog.create({request:JSON.stringify(data),response:JSON.stringify(trns_id),service_name:'Call Amount Transfer Service Helper',status:1,uid:uid,time_taken:results.time})
      return {status:'success',statusCode:200,amount:callAmount,transactionId:trns_id}
    } catch (error) {
      const results = perf.stop()
      await WebServiceLog.create({request:JSON.stringify(data),response:JSON.stringify(error),service_name:'Call Amount Transfer Service Helper error',status:1,uid:uid,time_taken:results.time})
      return {status:'error',statusCode:error.statusCode,msg:error.error.description}
    }
  }

  async holdAmountForCall(data){
    const uid=data?.uid
    var userWallet:any=await LedgerAccount.query().where({'uid':uid,name:'User'}).first()
   // userWallet.availableAmount=userWallet?.availableAmount-parseFloat(data?.amount)
    userWallet.holdAmount=userWallet.holdAmount+parseFloat(data.amount)
    await userWallet.save()
    const addToExpertWallet=new TransactionHistory()
    addToExpertWallet.callId=0
    addToExpertWallet.description=data?.amount+'-Hold for a request id - '+data?.id
    addToExpertWallet.fromWallet=userWallet?.id
    addToExpertWallet.toWallet=userWallet?.id
    addToExpertWallet.isdebit=true
    addToExpertWallet.status=1
    addToExpertWallet.transactionAmount=data?.amount
    addToExpertWallet.transactionType=1
    addToExpertWallet.transactionWalletId=userWallet?.id
    addToExpertWallet.transactionTo='Hold Amount'
    addToExpertWallet.transactionAction='NO'
    addToExpertWallet.uid=uid
    addToExpertWallet.payout=false
    await addToExpertWallet.save()
  }

  async releaseAmountForRequest(data){
    const uid=data?.uid||data?.requestedBy
    const amount=data?.estimatedCost
    var userWallet:any=await LedgerAccount.query().where({'uid':uid,name:'User'}).first()
   // return userWallet
    userWallet.holdAmount=userWallet.holdAmount-parseFloat(amount.toFixed(2))
    //userWallet.availableAmount=userWallet?.availableAmount+parseFloat(amount)

    await userWallet.save()

    const addToExpertWallet=new TransactionHistory()
    addToExpertWallet.callId=0
    addToExpertWallet.description=amount+'-Release Amount for request id '+(data?.id||0)
    addToExpertWallet.fromWallet=userWallet?.id
    addToExpertWallet.toWallet=userWallet?.id
    addToExpertWallet.isdebit=false
    addToExpertWallet.status=1
    addToExpertWallet.transactionAmount=amount
    addToExpertWallet.transactionType=1 //for release amount
    addToExpertWallet.transactionWalletId=userWallet?.id
    addToExpertWallet.transactionTo='Release Amount'
    addToExpertWallet.transactionAction='NO'
    addToExpertWallet.uid=uid
    addToExpertWallet.payout=false
    await addToExpertWallet.save()
  }

  /// New function for Releas Seekex remaining Hold amount after call end
  async releaseRemAmountAfterCall(data,amount){
    const uid=data?.uid||data?.initiater
    var userWallet:any=await LedgerAccount.query().where({'uid':uid,name:'User'}).first()
    await WebServiceLog.create({request:JSON.stringify({data,amount}),response:JSON.stringify(userWallet),service_name:'release Rem Amount After Call',status:1,uid:uid,time_taken:0})
   // return userWallet
    // userWallet.holdAmount=userWallet.holdAmount-parseFloat(amount.toFixed(2))
    // await userWallet.save()
    const addToExpertWallet=new TransactionHistory()
    addToExpertWallet.callId=data?.id
    addToExpertWallet.description=amount.toFixed(2)+'-Release Remaining Amount After call for call id '+(data?.id||0)
    addToExpertWallet.fromWallet=userWallet?.id
    addToExpertWallet.toWallet=userWallet?.id
    addToExpertWallet.isdebit=false
    addToExpertWallet.status=1
    addToExpertWallet.transactionAmount=parseFloat(amount.toFixed(2))
    addToExpertWallet.transactionType=1 //for release amount
    addToExpertWallet.transactionWalletId=userWallet?.id
    addToExpertWallet.transactionTo='Release Remaining Amount After Call'
    addToExpertWallet.transactionAction='NO'
    addToExpertWallet.uid=uid
    addToExpertWallet.payout=false
    await addToExpertWallet.save()
  }

  async applyRefundAmount(data){
    const applyForRefundDay= await AppSetting.getValueByKey('refund_time')
    const refundTime = parseInt(applyForRefundDay)
    const callID = data?.callId
    var callDetails:any = await CallHistory.find(callID)
    const callDay = parseInt(await calculateDays(callDetails?.callTime))
    const callAmount=await calculateCallAmount(callDetails)
    var refundAmount:any=0
    if(data?.type==2){
      refundAmount=parseFloat((callAmount*(data?.amt/100)).toFixed(2))
    }else{
      refundAmount=data?.amt
    }
    var expertId=data?.appliedFor||callDetails?.receiver
    var expertRefundAmount:any=0
    if(data?.type==1){
      const callTransectionForExpert:any= await TransactionHistory.query().where({callId:callID,uid:expertId}).first()
      expertRefundAmount=callTransectionForExpert?.transactionAmount||0
      if(expertRefundAmount<=0){
        const callInvoice:any= await Invoice.query().where({callId:callID}).first()
        expertRefundAmount=callInvoice?.finalAmount
      }
    }else if(data?.type==2){
      const IGST=(callDetails?.gstPercentage/100)||await AppSetting.getValueByKey('gst')/100
      const commission=await AppSetting.getValueByKey('commission')/100
      const commissionAmount=parseFloat((refundAmount*commission).toFixed(2))
      const gstAmount = parseFloat((refundAmount-(refundAmount/(100+(100*IGST)))*100).toFixed(2))
      expertRefundAmount = refundAmount-(commissionAmount+gstAmount)
    }
    expertRefundAmount=parseFloat(expertRefundAmount.toFixed(2))

    if(refundAmount>callAmount){
      await WebServiceLog.create({request:JSON.stringify(data),response:JSON.stringify({refundAmount,callAmount}),service_name:'Refund Apply data log '+callDay,status:1,uid:0,time_taken:0})
      return {status:'error',msg:'Your request amount greater than call amount'}
    }
    await WebServiceLog.create({request:JSON.stringify(data),response:JSON.stringify(callDetails),service_name:'Refund Apply data log'+callDay,status:1,uid:0,time_taken:0})
    if(callDay<=refundTime){
      const expertWallet:any = await LedgerAccount.query().where({'uid':expertId,name:'User'}).first()
       const addToExpertWallet=new TransactionHistory()
       addToExpertWallet.callId=callID
       addToExpertWallet.description=expertRefundAmount+' amount disputed in Expert Wallet for refund. Call Id =>'+data?.callId
       addToExpertWallet.fromWallet=expertWallet?.id
       addToExpertWallet.toWallet=expertWallet?.id
       addToExpertWallet.isdebit=true
       addToExpertWallet.status=1
       addToExpertWallet.transactionAmount=expertRefundAmount
       addToExpertWallet.transactionType=1
       addToExpertWallet.transactionWalletId=expertWallet?.id
       addToExpertWallet.transactionTo='Expert'
       addToExpertWallet.transactionAction='deputed'
       addToExpertWallet.transactionStatus='deputed'
       addToExpertWallet.uid=expertId
       addToExpertWallet.payout=false
       await addToExpertWallet.save()
       
      expertWallet.holdAmount=expertWallet.holdAmount-parseFloat(expertRefundAmount)
      expertWallet.disputedAmount=expertWallet.disputedAmount+parseFloat(expertRefundAmount)
      await expertWallet.save()
      return {status:'success',expertId:expertId,refundAmount:refundAmount,expertRefundAmount:expertRefundAmount}
    }else{
      await WebServiceLog.create({request:JSON.stringify(data),response:JSON.stringify({refundTime,callDay}),service_name:'Refund Apply data log'+callDay,status:1,uid:0,time_taken:0})
      return {status:'error',msg:'Refund not applied, your refund date is expired',data:{refundTime,callDay}}
    }
  }

  async withdrawFromWallet(data,bankDetails){
    perf.start()
    if(bankDetails.upi){
      var payOutData:any={
        "account_number":AccountNumber,
            "fund_account_id": bankDetails?.accountId,
            "amount": data.amt*100,
            "currency": "INR",
            "mode": "UPI",
            "purpose": "payout",
            "queue_if_low_balance":1,
            "reference_id": bankDetails?.uid,
            "narration": "Seekex Payout",
            "notes": {
              "notes_key_1":'User withdraw money from wallet'
            }
          }
    }else{
      var payOutData:any={
            "account_number":AccountNumber,
            "fund_account_id": bankDetails?.accountId,
            "amount": data.amt*100,
            "currency": "INR",
            "mode": "IMPS",
            "purpose": "payout",
            "queue_if_low_balance":1,
            "reference_id": bankDetails?.uid,
            "narration": "Seekex Payout",
            "notes": {
              "notes_key_1":'User withdraw money from wallet'
            }
          }
    }
//return payOutData
    try {
      var res = await  rp({
        method: 'POST',
        url: 'https://'+RazorPayKey+':'+RazorPaySecret+'@api.razorpay.com/v1/payouts',
        form: payOutData,
        headers: {
          'Content-Type': 'application/json'
       }
      }).promise()
      const results = perf.stop()
      await WebServiceLog.create({request:JSON.stringify(payOutData),response:JSON.stringify(res),service_name:'Razorpay create fund Api success',status:1,uid:0,time_taken:results.time})
      return {status:'success',data:JSON.parse(res)}
    } catch (error) {
      const results = perf.stop()
      await WebServiceLog.create({request:JSON.stringify(payOutData),response:JSON.stringify(error),service_name:'Razorpay create fund Api error',status:1,uid:0,time_taken:results.time})
      return {status:'error',data:error}
    }
  }

  async createContactOnPaymentGateway(user){
    perf.start()
    const contact:any={
      "name":user?.name,
      "email":user?.email,
      "contact":user?.mobile,
      "type":user.expert?'vendor':'customer',
      "reference_id":user?.id,
      "notes":{
        "notes_key_1":user.expert?'vendor':'customer'
      }
    }
      try {
       var res = await  rp({
          method: 'POST',
          url: 'https://'+RazorPayKey+':'+RazorPaySecret+'@api.razorpay.com/v1/contacts',
          form: contact,
          headers: {
            'Content-Type': 'application/json'
         }
        }).promise()

      return JSON.parse(res)
      } catch (error) {
        const results = perf.stop()
        await WebServiceLog.create({request:JSON.stringify(contact),response:JSON.stringify(error),service_name:'Razorpay add user account Api error',status:1,uid:user?.id,time_taken:results.time})
         return JSON.parse(error)
      } 
  }

  async fundAccount(data,user){
    perf.start()
    var faData:any
    if(data?.upi){
      faData={
        "account_type":"vpa",
        "contact_id":user?.contactId,
        "vpa":{
          "address":data?.upiAddress
        }
      }
    }else{
      faData={
        "contact_id":user?.contactId,
        "account_type":"bank_account",
        "bank_account":{
          "name":data?.holderName,
          "ifsc":data?.ifscCode,
          "account_number":data?.accountNo,
        }
      }
    }
    try {
      var res = await  rp({
         method: 'POST',
         url: 'https://'+RazorPayKey+':'+RazorPaySecret+'@api.razorpay.com/v1/fund_accounts',
         form: faData,
         headers: {
           'Content-Type': 'application/json'
        }
       }).promise()
       const results = perf.stop()
       await WebServiceLog.create({request:JSON.stringify({data,faData}),response:JSON.stringify(res),service_name:'Razorpay add fund account Api success',status:1,uid:user?.id,time_taken:results.time})
       return {status:'success',data:JSON.parse(res)}
      return JSON.parse(res)

     } catch (error) {
      const results = perf.stop()
      await WebServiceLog.create({request:JSON.stringify(data),response:JSON.stringify(error),service_name:'Razorpay add fund account Api error',status:1,uid:user?.id,time_taken:results.time})
      return {status:'error',data:error}
       return JSON.parse(error)

     } 
  }
  
  async addReferalAmount(referral){

     // const trx = await Database.transaction()
     const userWallet:any = await LedgerAccount.query().where({uid:referral?.uid,name:'User'}).first()
     const refWallet:any = await LedgerAccount.query().where({name:'Promotion'}).first()
     
    //*********************Promotion Amount Transection***************************//
    const transectionDataRef:any = new TransactionHistory()
    transectionDataRef.description=referral?.amount+' amount deduct from promotion for referral to User Id '+userWallet?.uid
    transectionDataRef.fromWallet=refWallet?.id
    transectionDataRef.toWallet=userWallet?.id
    transectionDataRef.isdebit=true
    transectionDataRef.razorPayPaymentId=''
    transectionDataRef.bankId=0
    transectionDataRef.status=1
    transectionDataRef.transactionAmount=referral?.amount
    transectionDataRef.transactionType=9
    transectionDataRef.transactionWalletId=refWallet?.id
    transectionDataRef.transactionTo='Promotion'
    transectionDataRef.transactionAction='minus'
    transectionDataRef.uid=refWallet?.uid||0
    transectionDataRef.payout=false
   // transectionDataRef.useTransaction(trx)
    await transectionDataRef.save()
   
    refWallet.availableAmount=refWallet?.availableAmount-referral?.amount
    refWallet.totalAmount=refWallet.totalAmount-referral?.amount
   // refWallet.useTransaction(trx)
    await refWallet.save()
    //*********************Promotion Amount Transection End***************************//
    //*********************User Transection***************************//
     const transectionData:any = new TransactionHistory()
     transectionData.description=referral?.amount+' referral amount add to User Wallet'
     transectionData.fromWallet=refWallet?.id
     transectionData.toWallet=userWallet?.id
     transectionData.isdebit=false
     transectionData.razorPayPaymentId=''
     transectionData.bankId=0
     transectionData.status=1
     transectionData.transactionAmount=referral?.amount
     transectionData.transactionType=9
     transectionData.transactionWalletId=userWallet?.id
     transectionData.transactionTo='User'
     transectionData.transactionAction='plus'
     transectionData.uid=userWallet?.uid
     transectionData.payout=false
    // transectionData.useTransaction(trx)
     await transectionData.save()
    // console.log('trans data',transectionData)
     userWallet.promoAmount=userWallet?.promoAmount+referral?.amount
     userWallet.totalAmount=userWallet.totalAmount+referral?.amount
    // userWallet.useTransaction(trx)
     await userWallet.save()
     referral.transectionId=transectionData?.id
    // referral.transectionDate=  new Date()
    // referral.useTransaction(trx)
     await referral.save()

     //*********************User Transection[End]***************************//
  }

  async refundAmountFromExpert(refund){
    const callData:any = await CallHistory.query().where({id:refund?.callId}).first()
    const uid = refund?.appliedBy
    const expertId = refund?.appliedFor
    const amount=refund?.amt
    var userWallet:any=await LedgerAccount.query().where({'uid':uid,name:'User'}).first()
    var expertWallet:any=await LedgerAccount.query().where({'uid':expertId,name:'User'}).first()
    const seekexWallet:any = await LedgerAccount.query().where({name:'Seekex'}).first()
    var sgstWallet:any=await LedgerAccount.query().where({name:'Sgst'}).first()
    var cgstWallet:any=await LedgerAccount.query().where({name:'Cgst'}).first()
    var igstWallet:any=await LedgerAccount.query().where({name:'Igst'}).first()
    var commissionWallet:any=await LedgerAccount.query().where({name:'Commission'}).first()
    
    // const IGST=parseFloat(Env.get('IGST_EXCLUSIVE') as string)
    // const NONIGST=parseFloat(Env.get('NON_IGST_EXCLUSIVE') as string)
    // const commission=parseFloat(Env.get('COMMISSION_AMOUNT') as string)
    const IGST=(callData?.gstPercentage/100)||await AppSetting.getValueByKey('gst')/100
    const commission=await AppSetting.getValueByKey('commission')/100
    const commissionAmount=parseFloat((amount*commission).toFixed(2))
    //const gstAmount=parseFloat((amount*IGST).toFixed(2))
    const gstAmount = parseFloat((amount-(amount/(100+(100*IGST)))*100).toFixed(2))
    const remeningAmount = amount-(commissionAmount+gstAmount)
    try {
      const trns_id= await Database.transaction(async (trx) => {
        if(callData?.isigst){
          var igstAmount=gstAmount
          const deductToIgstWallet=new TransactionHistory()
          deductToIgstWallet.callId=callData?.id
          deductToIgstWallet.description=igstAmount+' deducted to igst Wallet for refund => RefundId-'+refund?.id
          deductToIgstWallet.fromWallet=igstWallet?.id
          deductToIgstWallet.toWallet=igstWallet?.id
          deductToIgstWallet.isdebit=true
          deductToIgstWallet.status=1
          deductToIgstWallet.transactionAmount=igstAmount
          deductToIgstWallet.transactionType=10
          deductToIgstWallet.transactionWalletId=igstWallet?.id
          deductToIgstWallet.transactionTo='Igst'
          deductToIgstWallet.transactionAction='minus'
          deductToIgstWallet.uid=igstWallet?.uid
          deductToIgstWallet.payout=false
          deductToIgstWallet.useTransaction(trx)
          await deductToIgstWallet.save()
          /***********************Update Wallet ****************************/
          igstWallet.availableAmount=igstWallet.availableAmount-igstAmount
          igstWallet.totalAmount=igstWallet.totalAmount-igstAmount
          await igstWallet.save()
        }else{
          //var gstAmount=parseFloat((userCallingAmount*NONIGST).toFixed(2))
          const deductToCgstWallet=new TransactionHistory()
          deductToCgstWallet.callId=callData?.id
          deductToCgstWallet.description=gstAmount/2+' deducted to cgst Wallet for refund => RefundId-'+refund?.id
          deductToCgstWallet.fromWallet=cgstWallet?.id
          deductToCgstWallet.toWallet=cgstWallet?.id
          deductToCgstWallet.isdebit=true
          deductToCgstWallet.status=1
          deductToCgstWallet.transactionAmount=gstAmount/2
          deductToCgstWallet.transactionType=10
          deductToCgstWallet.transactionWalletId=cgstWallet?.id
          deductToCgstWallet.transactionTo='Igst'
          deductToCgstWallet.transactionAction='minus'
          deductToCgstWallet.uid=cgstWallet?.uid
          deductToCgstWallet.payout=false
          deductToCgstWallet.useTransaction(trx)
          await deductToCgstWallet.save()
          /***********************Update Wallet ****************************/
          cgstWallet.availableAmount=cgstWallet.availableAmount-(gstAmount/2)
          cgstWallet.totalAmount=cgstWallet.totalAmount-(gstAmount/2)
          await cgstWallet.save()
  
          const deductToSgstWallet=new TransactionHistory()
          deductToSgstWallet.callId=callData?.id
          deductToSgstWallet.description=gstAmount/2+' deducted to sgst Wallet for refund => RefundId-'+refund?.id
          deductToSgstWallet.fromWallet=sgstWallet?.id
          deductToSgstWallet.toWallet=sgstWallet?.id
          deductToSgstWallet.isdebit=true
          deductToSgstWallet.status=1
          deductToSgstWallet.transactionAmount=gstAmount/2
          deductToSgstWallet.transactionType=10
          deductToSgstWallet.transactionWalletId=sgstWallet?.id
          deductToSgstWallet.transactionTo='Igst'
          deductToSgstWallet.transactionAction='minus'
          deductToSgstWallet.uid=sgstWallet?.uid
          deductToSgstWallet.payout=false
          deductToSgstWallet.useTransaction(trx)
          await deductToSgstWallet.save()
          /***********************Update Wallet ****************************/
          sgstWallet.availableAmount=sgstWallet.availableAmount-(gstAmount/2)
          sgstWallet.totalAmount=sgstWallet.totalAmount-(gstAmount/2)
          await sgstWallet.save()
        }
        const deductToCommitionWallet=new TransactionHistory()
        deductToCommitionWallet.callId=callData?.id
        deductToCommitionWallet.description=commissionAmount+' deducted to commission Wallet for refund => RefundId-'+refund?.id
        deductToCommitionWallet.fromWallet=commissionWallet?.id
        deductToCommitionWallet.toWallet=commissionWallet?.id
        deductToCommitionWallet.isdebit=true
        deductToCommitionWallet.status=1
        deductToCommitionWallet.transactionAmount=commissionAmount
        deductToCommitionWallet.transactionType=10
        deductToCommitionWallet.transactionWalletId=commissionWallet?.id
        deductToCommitionWallet.transactionTo='Commission'
        deductToCommitionWallet.transactionAction='minus'
        deductToCommitionWallet.uid=commissionWallet?.uid
        deductToCommitionWallet.payout=false
        deductToCommitionWallet.useTransaction(trx)
          await deductToCommitionWallet.save()
          /***********************Update Wallet ****************************/
          commissionWallet.availableAmount=commissionWallet.availableAmount-commissionAmount
          commissionWallet.totalAmount=commissionWallet.totalAmount-commissionAmount
          await commissionWallet.save()
  
          /************************deducted amount from Expert [Amount Transection]  *****************************/
          const deductToExpertWallet=new TransactionHistory()
          deductToExpertWallet.callId=callData?.id
          deductToExpertWallet.description=remeningAmount+' deducted to Expert Wallet for refund => RefundId-'+refund?.id
          deductToExpertWallet.fromWallet=0
          deductToExpertWallet.toWallet=expertWallet?.id
          deductToExpertWallet.isdebit=true
          deductToExpertWallet.status=1
          deductToExpertWallet.transactionAmount=remeningAmount
          deductToExpertWallet.transactionType=10
          deductToExpertWallet.transactionWalletId=expertWallet?.id
          deductToExpertWallet.transactionTo='Expert'
          deductToExpertWallet.transactionAction='minus'
          deductToExpertWallet.uid=expertWallet?.uid||expertId
          deductToExpertWallet.payout=false
          deductToExpertWallet.useTransaction(trx)
          await deductToExpertWallet.save()
          /***********************Update Wallet ****************************/
          expertWallet.disputedAmount=expertWallet.disputedAmount-remeningAmount
          expertWallet.totalAmount=expertWallet.totalAmount-remeningAmount
          await expertWallet.save()
        
          /************************add amount to User [Amount Transection]  *****************************/
          const addToUserWallet=new TransactionHistory()
          addToUserWallet.callId=callData?.id
          addToUserWallet.description=(remeningAmount+gstAmount+commissionAmount)+' add to Seeker Wallet for refund => RefundId-'+refund?.id
          addToUserWallet.fromWallet=expertWallet?.id
          addToUserWallet.toWallet=userWallet?.id
          addToUserWallet.isdebit=false
          addToUserWallet.status=1
          addToUserWallet.transactionAmount=(remeningAmount+gstAmount+commissionAmount)
          addToUserWallet.transactionType=10
          addToUserWallet.transactionWalletId=userWallet?.id
          addToUserWallet.transactionTo='User'
          addToUserWallet.transactionAction='plus'
          addToUserWallet.uid=uid
          addToUserWallet.payout=false
          addToUserWallet.useTransaction(trx)
          await addToUserWallet.save()
          /***********************Update Wallet ****************************/
          userWallet.availableAmount=userWallet.availableAmount+(remeningAmount+gstAmount+commissionAmount)
          userWallet.totalAmount=userWallet.totalAmount+(remeningAmount+gstAmount+commissionAmount)
          await userWallet.save()
          return userWallet?.id
      })
      return {status:'success',statusCode:200,amount:amount,transaction_id:trns_id}
    } catch (error) {
      const results = perf.stop()
      await WebServiceLog.create({request:JSON.stringify(refund),response:JSON.stringify(error),service_name:'Refund Approved by Expert helper error',status:1,uid:uid,time_taken:results.time})
      return {status:'error',statusCode:error?.statusCode,msg:error?.error?.description}
    }
  }

  async refundAmountFromSeekex(refund){
    const callData:any = await CallHistory.query().where({id:refund?.callId}).first()
    const uid = refund?.appliedBy
    const expertId = refund?.appliedFor
   // const transectionsData = await TransactionHistory.query().where({callId:refund?.callId})
    var userWallet:any=await LedgerAccount.query().where({'uid':uid,name:'User'}).first()
    var expertWallet:any=await LedgerAccount.query().where({'uid':expertId,name:'User'}).first()
    const seekexWallet:any = await LedgerAccount.query().where({name:'Seekex'}).first()
    var sgstWallet:any=await LedgerAccount.query().where({name:'Sgst'}).first()
    var cgstWallet:any=await LedgerAccount.query().where({name:'Cgst'}).first()
    var igstWallet:any=await LedgerAccount.query().where({name:'Igst'}).first()
    var commissionWallet:any=await LedgerAccount.query().where({name:'Commission'}).first()
    const amount=refund?.amt

    // const IGST=parseFloat(Env.get('IGST_EXCLUSIVE') as string)
    // const NONIGST=parseFloat(Env.get('NON_IGST_EXCLUSIVE') as string)
    // const commission=parseFloat(Env.get('COMMISSION_AMOUNT') as string)
    const IGST=(callData?.gstPercentage/100)||await AppSetting.getValueByKey('gst')/100
    const commission=await AppSetting.getValueByKey('commission')/100
    const commissionAmount=parseFloat((amount*commission).toFixed(2))
    const gstAmount = parseFloat((amount-(amount/(100+(100*IGST)))*100).toFixed(2))
    const remeningAmount = amount-(commissionAmount+gstAmount)
   // return{commissionAmount,gstAmount,remeningAmount}
   const trx= await Database.transaction()
    if(callData?.isigst){
      var igstAmount=gstAmount
       const deductToIgstWallet=new TransactionHistory()
       deductToIgstWallet.callId=callData?.id
       deductToIgstWallet.description=igstAmount+' deducted to igst Wallet for refund => RefundId-'+refund?.id
       deductToIgstWallet.fromWallet=igstWallet?.id
       deductToIgstWallet.toWallet=igstWallet?.id
       deductToIgstWallet.isdebit=true
       deductToIgstWallet.status=1
       deductToIgstWallet.transactionAmount=igstAmount
       deductToIgstWallet.transactionType=10
       deductToIgstWallet.transactionWalletId=igstWallet?.id
       deductToIgstWallet.transactionTo='Igst'
       deductToIgstWallet.transactionAction='minus'
       deductToIgstWallet.uid=igstWallet?.uid
       deductToIgstWallet.payout=false
       deductToIgstWallet.useTransaction(trx)
       await deductToIgstWallet.save()
       /***********************Update Wallet ****************************/
       igstWallet.availableAmount=igstWallet.availableAmount-igstAmount
       igstWallet.totalAmount=igstWallet.totalAmount-igstAmount
       await igstWallet.save()
     }else{
      //var gstAmount=parseFloat((userCallingAmount*NONIGST).toFixed(2))
       const deductToCgstWallet=new TransactionHistory()
       deductToCgstWallet.callId=callData?.id
       deductToCgstWallet.description=gstAmount/2+' deducted to cgst Wallet for refund => RefundId-'+refund?.id
       deductToCgstWallet.fromWallet=cgstWallet?.id
       deductToCgstWallet.toWallet=cgstWallet?.id
       deductToCgstWallet.isdebit=true
       deductToCgstWallet.status=1
       deductToCgstWallet.transactionAmount=gstAmount/2
       deductToCgstWallet.transactionType=10
       deductToCgstWallet.transactionWalletId=cgstWallet?.id
       deductToCgstWallet.transactionTo='Igst'
       deductToCgstWallet.transactionAction='minus'
       deductToCgstWallet.uid=cgstWallet?.uid
       deductToCgstWallet.payout=false
       deductToCgstWallet.useTransaction(trx)
       await deductToCgstWallet.save()
       /***********************Update Wallet ****************************/
       cgstWallet.availableAmount=cgstWallet.availableAmount-(gstAmount/2)
       cgstWallet.totalAmount=cgstWallet.totalAmount-(gstAmount/2)
       await cgstWallet.save()

       const deductToSgstWallet=new TransactionHistory()
       deductToSgstWallet.callId=callData?.id
       deductToSgstWallet.description=gstAmount/2+' deducted to sgst Wallet for refund => RefundId-'+refund?.id
       deductToSgstWallet.fromWallet=sgstWallet?.id
       deductToSgstWallet.toWallet=sgstWallet?.id
       deductToSgstWallet.isdebit=true
       deductToSgstWallet.status=1
       deductToSgstWallet.transactionAmount=gstAmount/2
       deductToSgstWallet.transactionType=10
       deductToSgstWallet.transactionWalletId=sgstWallet?.id
       deductToSgstWallet.transactionTo='Igst'
       deductToSgstWallet.transactionAction='minus'
       deductToSgstWallet.uid=sgstWallet?.uid
       deductToSgstWallet.payout=false
       deductToSgstWallet.useTransaction(trx)
       await deductToSgstWallet.save()
       /***********************Update Wallet ****************************/
       sgstWallet.availableAmount=sgstWallet.availableAmount-(gstAmount/2)
       sgstWallet.totalAmount=sgstWallet.totalAmount-(gstAmount/2)
       await sgstWallet.save()
     }
     const deductToCommitionWallet=new TransactionHistory()
     deductToCommitionWallet.callId=callData?.id
     deductToCommitionWallet.description=commissionAmount+' deducted to commission Wallet for refund => RefundId-'+refund?.id
     deductToCommitionWallet.fromWallet=commissionWallet?.id
     deductToCommitionWallet.toWallet=commissionWallet?.id
     deductToCommitionWallet.isdebit=true
     deductToCommitionWallet.status=1
     deductToCommitionWallet.transactionAmount=commissionAmount
     deductToCommitionWallet.transactionType=10
     deductToCommitionWallet.transactionWalletId=commissionWallet?.id
     deductToCommitionWallet.transactionTo='Commission'
     deductToCommitionWallet.transactionAction='minus'
     deductToCommitionWallet.uid=commissionWallet?.uid
     deductToCommitionWallet.payout=false
     deductToCommitionWallet.useTransaction(trx)
      await deductToCommitionWallet.save()
      /***********************Update Wallet ****************************/
      commissionWallet.availableAmount=commissionWallet.availableAmount-commissionAmount
      commissionWallet.totalAmount=commissionWallet.totalAmount-commissionAmount
      await commissionWallet.save()

      /************************deducted amount from Expert [Amount Transection]  *****************************/
      const deductToSeekexWallet=new TransactionHistory()
      deductToSeekexWallet.callId=callData?.id
      deductToSeekexWallet.description=remeningAmount+' deducted to Seekex Wallet for refund => RefundId-'+refund?.id
      deductToSeekexWallet.fromWallet=0
      deductToSeekexWallet.toWallet=seekexWallet?.id
      deductToSeekexWallet.isdebit=true
      deductToSeekexWallet.status=1
      deductToSeekexWallet.transactionAmount=remeningAmount
      deductToSeekexWallet.transactionType=10
      deductToSeekexWallet.transactionWalletId=seekexWallet?.id
      deductToSeekexWallet.transactionTo='Expert'
      deductToSeekexWallet.transactionAction='minus'
      deductToSeekexWallet.uid=seekexWallet?.uid
      deductToSeekexWallet.payout=false
      deductToSeekexWallet.useTransaction(trx)
      await deductToSeekexWallet.save()
      /***********************Update Wallet ****************************/
      seekexWallet.availableAmount=seekexWallet.availableAmount-remeningAmount
      seekexWallet.totalAmount=seekexWallet.totalAmount-remeningAmount
      await seekexWallet.save()
     
      /************************add amount to User [Amount Transection]  *****************************/
      const addToUserWallet=new TransactionHistory()
      addToUserWallet.callId=callData?.id
      addToUserWallet.description=(remeningAmount+gstAmount+commissionAmount)+' add to Seeker Wallet for refund => RefundId-'+refund?.id
      addToUserWallet.fromWallet=seekexWallet?.id
      addToUserWallet.toWallet=userWallet?.id
      addToUserWallet.isdebit=false
      addToUserWallet.status=1
      addToUserWallet.transactionAmount=(remeningAmount+gstAmount+commissionAmount)
      addToUserWallet.transactionType=10
      addToUserWallet.transactionWalletId=userWallet?.id
      addToUserWallet.transactionTo='User'
      addToUserWallet.transactionAction='plus'
      addToUserWallet.uid=uid
      addToUserWallet.payout=false
      addToUserWallet.useTransaction(trx)
      await addToUserWallet.save()
      /***********************Update Wallet ****************************/
      userWallet.availableAmount=userWallet.availableAmount+(remeningAmount+gstAmount+commissionAmount)
      userWallet.totalAmount=userWallet.totalAmount+(remeningAmount+gstAmount+commissionAmount)
      await userWallet.save()
      expertWallet.disputedAmount=expertWallet.disputedAmount-remeningAmount
      await expertWallet.save()
  }
  
  async refundPartialAmountFromExpert(refund,amount){
    const callData:any = await CallHistory.query().where({id:refund?.callId}).first()
    const uid = refund?.appliedBy
    const expertId = refund?.appliedFor
   // const transectionsData = await TransactionHistory.query().where({callId:refund?.callId})
    var userWallet:any=await LedgerAccount.query().where({'uid':uid,name:'User'}).first()
    var expertWallet:any=await LedgerAccount.query().where({'uid':expertId,name:'User'}).first()
    const seekexWallet:any = await LedgerAccount.query().where({name:'Seekex'}).first()
    var sgstWallet:any=await LedgerAccount.query().where({name:'Sgst'}).first()
    var cgstWallet:any=await LedgerAccount.query().where({name:'Cgst'}).first()
    var igstWallet:any=await LedgerAccount.query().where({name:'Igst'}).first()
    var commissionWallet:any=await LedgerAccount.query().where({name:'Commission'}).first()


    // const IGST=parseFloat(Env.get('IGST_EXCLUSIVE') as string)
    // const NONIGST=parseFloat(Env.get('NON_IGST_EXCLUSIVE') as string)
    // const commission=parseFloat(Env.get('COMMISSION_AMOUNT') as string)
    const IGST=(callData?.gstPercentage/100)||await AppSetting.getValueByKey('gst')/100
    const commission=await AppSetting.getValueByKey('commission')/100
    const commissionAmount=parseFloat((amount*commission).toFixed(2))
    const gstAmount = parseFloat((amount-(amount/(100+(100*IGST)))*100).toFixed(2))
    const remeningAmount = amount-(commissionAmount+gstAmount)
   // return{commissionAmount,gstAmount,remeningAmount}
   const trx= await Database.transaction()
    if(callData?.isigst){
      var igstAmount=gstAmount
       const deductToIgstWallet=new TransactionHistory()
       deductToIgstWallet.callId=callData?.id
       deductToIgstWallet.description=igstAmount+' deducted to igst Wallet for refund => RefundId-'+refund?.id
       deductToIgstWallet.fromWallet=igstWallet?.id
       deductToIgstWallet.toWallet=igstWallet?.id
       deductToIgstWallet.isdebit=true
       deductToIgstWallet.status=1
       deductToIgstWallet.transactionAmount=igstAmount
       deductToIgstWallet.transactionType=10
       deductToIgstWallet.transactionWalletId=igstWallet?.id
       deductToIgstWallet.transactionTo='Igst'
       deductToIgstWallet.transactionAction='minus'
       deductToIgstWallet.uid=uid
       deductToIgstWallet.payout=false
       deductToIgstWallet.useTransaction(trx)
       await deductToIgstWallet.save()
       /***********************Update Wallet ****************************/
       igstWallet.availableAmount=igstWallet.availableAmount-igstAmount
       igstWallet.totalAmount=igstWallet.totalAmount-igstAmount
       await igstWallet.save()
     }else{
      //var gstAmount=parseFloat((userCallingAmount*NONIGST).toFixed(2))
       const deductToCgstWallet=new TransactionHistory()
       deductToCgstWallet.callId=callData?.id
       deductToCgstWallet.description=gstAmount/2+' deducted to cgst Wallet for refund => RefundId-'+refund?.id
       deductToCgstWallet.fromWallet=cgstWallet?.id
       deductToCgstWallet.toWallet=cgstWallet?.id
       deductToCgstWallet.isdebit=true
       deductToCgstWallet.status=1
       deductToCgstWallet.transactionAmount=gstAmount/2
       deductToCgstWallet.transactionType=10
       deductToCgstWallet.transactionWalletId=cgstWallet?.id
       deductToCgstWallet.transactionTo='Igst'
       deductToCgstWallet.transactionAction='minus'
       deductToCgstWallet.uid=uid
       deductToCgstWallet.payout=false
       deductToCgstWallet.useTransaction(trx)
       await deductToCgstWallet.save()
       /***********************Update Wallet ****************************/
       cgstWallet.availableAmount=cgstWallet.availableAmount-(gstAmount/2)
       cgstWallet.totalAmount=cgstWallet.totalAmount-(gstAmount/2)
       await cgstWallet.save()

       const deductToSgstWallet=new TransactionHistory()
       deductToSgstWallet.callId=callData?.id
       deductToSgstWallet.description=gstAmount/2+' deducted to sgst Wallet for refund => RefundId-'+refund?.id
       deductToSgstWallet.fromWallet=sgstWallet?.id
       deductToSgstWallet.toWallet=sgstWallet?.id
       deductToSgstWallet.isdebit=true
       deductToSgstWallet.status=1
       deductToSgstWallet.transactionAmount=gstAmount/2
       deductToSgstWallet.transactionType=10
       deductToSgstWallet.transactionWalletId=sgstWallet?.id
       deductToSgstWallet.transactionTo='Igst'
       deductToSgstWallet.transactionAction='minus'
       deductToSgstWallet.uid=uid
       deductToSgstWallet.payout=false
       deductToSgstWallet.useTransaction(trx)
       await deductToSgstWallet.save()
       /***********************Update Wallet ****************************/
       sgstWallet.availableAmount=sgstWallet.availableAmount-(gstAmount/2)
       sgstWallet.totalAmount=sgstWallet.totalAmount-(gstAmount/2)
       await sgstWallet.save()
     }
     const deductToCommitionWallet=new TransactionHistory()
     deductToCommitionWallet.callId=callData?.id
     deductToCommitionWallet.description=commissionAmount+' deducted to commission Wallet for refund => RefundId-'+refund?.id
     deductToCommitionWallet.fromWallet=commissionWallet?.id
     deductToCommitionWallet.toWallet=commissionWallet?.id
     deductToCommitionWallet.isdebit=true
     deductToCommitionWallet.status=1
     deductToCommitionWallet.transactionAmount=commissionAmount
     deductToCommitionWallet.transactionType=10
     deductToCommitionWallet.transactionWalletId=commissionWallet?.id
     deductToCommitionWallet.transactionTo='Commission'
     deductToCommitionWallet.transactionAction='minus'
     deductToCommitionWallet.uid=uid
     deductToCommitionWallet.payout=false
     deductToCommitionWallet.useTransaction(trx)
      await deductToCommitionWallet.save()
      /***********************Update Wallet ****************************/
      commissionWallet.availableAmount=commissionWallet.availableAmount-commissionAmount
      commissionWallet.totalAmount=commissionWallet.totalAmount-commissionAmount
      await commissionWallet.save()

      /************************deducted amount from Expert [Amount Transection]  *****************************/
      const deductToExpertWallet=new TransactionHistory()
      deductToExpertWallet.callId=callData?.id
      deductToExpertWallet.description=remeningAmount+' deducted to Expert Wallet for refund => RefundId-'+refund?.id
      deductToExpertWallet.fromWallet=0
      deductToExpertWallet.toWallet=expertWallet?.id
      deductToExpertWallet.isdebit=true
      deductToExpertWallet.status=1
      deductToExpertWallet.transactionAmount=remeningAmount
      deductToExpertWallet.transactionType=10
      deductToExpertWallet.transactionWalletId=expertWallet?.id
      deductToExpertWallet.transactionTo='Expert'
      deductToExpertWallet.transactionAction='minus'
      deductToExpertWallet.uid=uid
      deductToExpertWallet.payout=false
      deductToExpertWallet.useTransaction(trx)
      await deductToExpertWallet.save()
      /***********************Update Wallet ****************************/
      expertWallet.holdAmount=expertWallet.holdAmount-remeningAmount
      expertWallet.totalAmount=expertWallet.totalAmount-remeningAmount
      await expertWallet.save()
     
      /************************add amount to User [Amount Transection]  *****************************/
      const addToUserWallet=new TransactionHistory()
      addToUserWallet.callId=callData?.id
      addToUserWallet.description=(remeningAmount+gstAmount+commissionAmount)+' add to Seeker Wallet for refund => RefundId-'+refund?.id
      addToUserWallet.fromWallet=expertWallet?.id
      addToUserWallet.toWallet=userWallet?.id
      addToUserWallet.isdebit=false
      addToUserWallet.status=1
      addToUserWallet.transactionAmount=(remeningAmount+gstAmount+commissionAmount)
      addToUserWallet.transactionType=10
      addToUserWallet.transactionWalletId=userWallet?.id
      addToUserWallet.transactionTo='User'
      addToUserWallet.transactionAction='plus'
      addToUserWallet.uid=uid
      addToUserWallet.payout=false
      addToUserWallet.useTransaction(trx)
      await addToUserWallet.save()
      /***********************Update Wallet ****************************/
      userWallet.availableAmount=userWallet.availableAmount+(remeningAmount+gstAmount+commissionAmount)
      userWallet.totalAmount=userWallet.totalAmount+(remeningAmount+gstAmount+commissionAmount)
      await userWallet.save()
  }
  async refundPartialAmountFromSeekex(refund,amount){
    const callData:any = await CallHistory.query().where({id:refund?.callId}).first()
    const uid = refund?.appliedBy
    const expertId = refund?.appliedFor
   // const transectionsData = await TransactionHistory.query().where({callId:refund?.callId})
    var userWallet:any=await LedgerAccount.query().where({'uid':uid,name:'User'}).first()
    var expertWallet:any=await LedgerAccount.query().where({'uid':expertId,name:'User'}).first()
    const seekexWallet:any = await LedgerAccount.query().where({name:'Seekex'}).first()
    var sgstWallet:any=await LedgerAccount.query().where({name:'Sgst'}).first()
    var cgstWallet:any=await LedgerAccount.query().where({name:'Cgst'}).first()
    var igstWallet:any=await LedgerAccount.query().where({name:'Igst'}).first()
    var commissionWallet:any=await LedgerAccount.query().where({name:'Commission'}).first()


    // const IGST=parseFloat(Env.get('IGST_EXCLUSIVE') as string)
    // const NONIGST=parseFloat(Env.get('NON_IGST_EXCLUSIVE') as string)
    // const commission=parseFloat(Env.get('COMMISSION_AMOUNT') as string)
    const IGST=(callData?.gstPercentage/100)||await AppSetting.getValueByKey('gst')/100
    const commission=await AppSetting.getValueByKey('commission')/100
    const commissionAmount=parseFloat((amount*commission).toFixed(2))
    const gstAmount = parseFloat((amount-(amount/(100+(100*IGST)))*100).toFixed(2))
    const remeningAmount = amount-(commissionAmount+gstAmount)
   // return{commissionAmount,gstAmount,remeningAmount}
   const trx= await Database.transaction()
    if(callData?.isigst){
      var igstAmount=gstAmount
       const deductToIgstWallet=new TransactionHistory()
       deductToIgstWallet.callId=callData?.id
       deductToIgstWallet.description=igstAmount+' deducted to igst Wallet for refund => RefundId-'+refund?.id
       deductToIgstWallet.fromWallet=igstWallet?.id
       deductToIgstWallet.toWallet=igstWallet?.id
       deductToIgstWallet.isdebit=true
       deductToIgstWallet.status=1
       deductToIgstWallet.transactionAmount=igstAmount
       deductToIgstWallet.transactionType=10
       deductToIgstWallet.transactionWalletId=igstWallet?.id
       deductToIgstWallet.transactionTo='Igst'
       deductToIgstWallet.transactionAction='minus'
       deductToIgstWallet.uid=uid
       deductToIgstWallet.payout=false
       deductToIgstWallet.useTransaction(trx)
       await deductToIgstWallet.save()
       /***********************Update Wallet ****************************/
       igstWallet.availableAmount=igstWallet.availableAmount-igstAmount
       igstWallet.totalAmount=igstWallet.totalAmount-igstAmount
       await igstWallet.save()
     }else{
      //var gstAmount=parseFloat((userCallingAmount*NONIGST).toFixed(2))
       const deductToCgstWallet=new TransactionHistory()
       deductToCgstWallet.callId=callData?.id
       deductToCgstWallet.description=gstAmount/2+' deducted to cgst Wallet for refund => RefundId-'+refund?.id
       deductToCgstWallet.fromWallet=cgstWallet?.id
       deductToCgstWallet.toWallet=cgstWallet?.id
       deductToCgstWallet.isdebit=true
       deductToCgstWallet.status=1
       deductToCgstWallet.transactionAmount=gstAmount/2
       deductToCgstWallet.transactionType=10
       deductToCgstWallet.transactionWalletId=cgstWallet?.id
       deductToCgstWallet.transactionTo='Igst'
       deductToCgstWallet.transactionAction='minus'
       deductToCgstWallet.uid=uid
       deductToCgstWallet.payout=false
       deductToCgstWallet.useTransaction(trx)
       await deductToCgstWallet.save()
       /***********************Update Wallet ****************************/
       cgstWallet.availableAmount=cgstWallet.availableAmount-(gstAmount/2)
       cgstWallet.totalAmount=cgstWallet.totalAmount-(gstAmount/2)
       await cgstWallet.save()

       const deductToSgstWallet=new TransactionHistory()
       deductToSgstWallet.callId=callData?.id
       deductToSgstWallet.description=gstAmount/2+' deducted to sgst Wallet for refund => RefundId-'+refund?.id
       deductToSgstWallet.fromWallet=sgstWallet?.id
       deductToSgstWallet.toWallet=sgstWallet?.id
       deductToSgstWallet.isdebit=true
       deductToSgstWallet.status=1
       deductToSgstWallet.transactionAmount=gstAmount/2
       deductToSgstWallet.transactionType=10
       deductToSgstWallet.transactionWalletId=sgstWallet?.id
       deductToSgstWallet.transactionTo='Igst'
       deductToSgstWallet.transactionAction='minus'
       deductToSgstWallet.uid=uid
       deductToSgstWallet.payout=false
       deductToSgstWallet.useTransaction(trx)
       await deductToSgstWallet.save()
       /***********************Update Wallet ****************************/
       sgstWallet.availableAmount=sgstWallet.availableAmount-(gstAmount/2)
       sgstWallet.totalAmount=sgstWallet.totalAmount-(gstAmount/2)
       await sgstWallet.save()
     }
     const deductToCommitionWallet=new TransactionHistory()
     deductToCommitionWallet.callId=callData?.id
     deductToCommitionWallet.description=commissionAmount+' deducted to commission Wallet for refund => RefundId-'+refund?.id
     deductToCommitionWallet.fromWallet=commissionWallet?.id
     deductToCommitionWallet.toWallet=commissionWallet?.id
     deductToCommitionWallet.isdebit=true
     deductToCommitionWallet.status=1
     deductToCommitionWallet.transactionAmount=commissionAmount
     deductToCommitionWallet.transactionType=10
     deductToCommitionWallet.transactionWalletId=commissionWallet?.id
     deductToCommitionWallet.transactionTo='Commission'
     deductToCommitionWallet.transactionAction='minus'
     deductToCommitionWallet.uid=uid
     deductToCommitionWallet.payout=false
     deductToCommitionWallet.useTransaction(trx)
      await deductToCommitionWallet.save()
      /***********************Update Wallet ****************************/
      commissionWallet.availableAmount=commissionWallet.availableAmount-commissionAmount
      commissionWallet.totalAmount=commissionWallet.totalAmount-commissionAmount
      await commissionWallet.save()

      /************************deducted amount from Expert [Amount Transection]  *****************************/
      const deductToSeekexWallet=new TransactionHistory()
      deductToSeekexWallet.callId=callData?.id
      deductToSeekexWallet.description=remeningAmount+' deducted to Seekex Wallet for refund => RefundId-'+refund?.id
      deductToSeekexWallet.fromWallet=0
      deductToSeekexWallet.toWallet=seekexWallet?.id
      deductToSeekexWallet.isdebit=true
      deductToSeekexWallet.status=1
      deductToSeekexWallet.transactionAmount=remeningAmount
      deductToSeekexWallet.transactionType=10
      deductToSeekexWallet.transactionWalletId=seekexWallet?.id
      deductToSeekexWallet.transactionTo='Expert'
      deductToSeekexWallet.transactionAction='minus'
      deductToSeekexWallet.uid=uid
      deductToSeekexWallet.payout=false
      deductToSeekexWallet.useTransaction(trx)
      await deductToSeekexWallet.save()
      /***********************Update Wallet ****************************/
      seekexWallet.availableAmount=seekexWallet.availableAmount-remeningAmount
      seekexWallet.totalAmount=seekexWallet.totalAmount-remeningAmount
      await seekexWallet.save()
     
      /************************add amount to User [Amount Transection]  *****************************/
      const addToUserWallet=new TransactionHistory()
      addToUserWallet.callId=callData?.id
      addToUserWallet.description=(remeningAmount+gstAmount+commissionAmount)+' add to Seeker Wallet for refund => RefundId-'+refund?.id
      addToUserWallet.fromWallet=seekexWallet?.id
      addToUserWallet.toWallet=userWallet?.id
      addToUserWallet.isdebit=false
      addToUserWallet.status=1
      addToUserWallet.transactionAmount=(remeningAmount+gstAmount+commissionAmount)
      addToUserWallet.transactionType=10
      addToUserWallet.transactionWalletId=userWallet?.id
      addToUserWallet.transactionTo='User'
      addToUserWallet.transactionAction='plus'
      addToUserWallet.uid=uid
      addToUserWallet.payout=false
      addToUserWallet.useTransaction(trx)
      await addToUserWallet.save()
      /***********************Update Wallet ****************************/
      userWallet.availableAmount=userWallet.availableAmount+(remeningAmount+gstAmount+commissionAmount)
      userWallet.totalAmount=userWallet.totalAmount+(remeningAmount+gstAmount+commissionAmount)
      await userWallet.save()
  }

  async refundAmountByExpert(refund){
    const callData:any = await CallHistory.query().where({id:refund?.callId}).first()
    const uid = refund?.appliedBy
    const expertId = refund?.appliedFor
    const amount=refund?.amt
    var userWallet:any=await LedgerAccount.query().where({'uid':uid,name:'User'}).first()
    var expertWallet:any=await LedgerAccount.query().where({'uid':expertId,name:'User'}).first()
    const seekexWallet:any = await LedgerAccount.query().where({name:'Seekex'}).first()
    var sgstWallet:any=await LedgerAccount.query().where({name:'Sgst'}).first()
    var cgstWallet:any=await LedgerAccount.query().where({name:'Cgst'}).first()
    var igstWallet:any=await LedgerAccount.query().where({name:'Igst'}).first()
    var commissionWallet:any=await LedgerAccount.query().where({name:'Commission'}).first()
    
    // const IGST=parseFloat(Env.get('IGST_EXCLUSIVE') as string)
    // const NONIGST=parseFloat(Env.get('NON_IGST_EXCLUSIVE') as string)
    // const commission=parseFloat(Env.get('COMMISSION_AMOUNT') as string)
    const IGST=(callData?.gstPercentage/100)||await AppSetting.getValueByKey('gst')/100
    const commission=await AppSetting.getValueByKey('commission')/100
    const commissionAmount=parseFloat((amount*commission).toFixed(2))
    const gstAmount = parseFloat((amount-(amount/(100+(100*IGST)))*100).toFixed(2))
    const remeningAmount = amount-(commissionAmount+gstAmount)
    try {
      const trns_id= await Database.transaction(async (trx) => {
        if(callData?.isigst){
          var igstAmount=gstAmount
          const deductToIgstWallet=new TransactionHistory()
          deductToIgstWallet.callId=callData?.id
          deductToIgstWallet.description=igstAmount+' deducted to igst Wallet for refund => RefundId-'+refund?.id
          deductToIgstWallet.fromWallet=igstWallet?.id
          deductToIgstWallet.toWallet=igstWallet?.id
          deductToIgstWallet.isdebit=true
          deductToIgstWallet.status=1
          deductToIgstWallet.transactionAmount=igstAmount
          deductToIgstWallet.transactionType=10
          deductToIgstWallet.transactionWalletId=igstWallet?.id
          deductToIgstWallet.transactionTo='Igst'
          deductToIgstWallet.transactionAction='minus'
          deductToIgstWallet.uid=igstWallet?.uid
          deductToIgstWallet.payout=false
          deductToIgstWallet.useTransaction(trx)
          await deductToIgstWallet.save()
          /***********************Update Wallet ****************************/
          igstWallet.availableAmount=igstWallet.availableAmount-igstAmount
          igstWallet.totalAmount=igstWallet.totalAmount-igstAmount
          await igstWallet.save()
        }else{
          //var gstAmount=parseFloat((userCallingAmount*NONIGST).toFixed(2))
          const deductToCgstWallet=new TransactionHistory()
          deductToCgstWallet.callId=callData?.id
          deductToCgstWallet.description=gstAmount/2+' deducted to cgst Wallet for refund => RefundId-'+refund?.id
          deductToCgstWallet.fromWallet=cgstWallet?.id
          deductToCgstWallet.toWallet=cgstWallet?.id
          deductToCgstWallet.isdebit=true
          deductToCgstWallet.status=1
          deductToCgstWallet.transactionAmount=gstAmount/2
          deductToCgstWallet.transactionType=10
          deductToCgstWallet.transactionWalletId=cgstWallet?.id
          deductToCgstWallet.transactionTo='Igst'
          deductToCgstWallet.transactionAction='minus'
          deductToCgstWallet.uid=cgstWallet?.uid
          deductToCgstWallet.payout=false
          deductToCgstWallet.useTransaction(trx)
          await deductToCgstWallet.save()
          /***********************Update Wallet ****************************/
          cgstWallet.availableAmount=cgstWallet.availableAmount-(gstAmount/2)
          cgstWallet.totalAmount=cgstWallet.totalAmount-(gstAmount/2)
          await cgstWallet.save()
  
          const deductToSgstWallet=new TransactionHistory()
          deductToSgstWallet.callId=callData?.id
          deductToSgstWallet.description=gstAmount/2+' deducted to sgst Wallet for refund => RefundId-'+refund?.id
          deductToSgstWallet.fromWallet=sgstWallet?.id
          deductToSgstWallet.toWallet=sgstWallet?.id
          deductToSgstWallet.isdebit=true
          deductToSgstWallet.status=1
          deductToSgstWallet.transactionAmount=gstAmount/2
          deductToSgstWallet.transactionType=10
          deductToSgstWallet.transactionWalletId=sgstWallet?.id
          deductToSgstWallet.transactionTo='Igst'
          deductToSgstWallet.transactionAction='minus'
          deductToSgstWallet.uid=sgstWallet?.uid
          deductToSgstWallet.payout=false
          deductToSgstWallet.useTransaction(trx)
          await deductToSgstWallet.save()
          /***********************Update Wallet ****************************/
          sgstWallet.availableAmount=sgstWallet.availableAmount-(gstAmount/2)
          sgstWallet.totalAmount=sgstWallet.totalAmount-(gstAmount/2)
          await sgstWallet.save()
        }
        const deductToCommitionWallet=new TransactionHistory()
        deductToCommitionWallet.callId=callData?.id
        deductToCommitionWallet.description=commissionAmount+' deducted to commission Wallet for refund => RefundId-'+refund?.id
        deductToCommitionWallet.fromWallet=commissionWallet?.id
        deductToCommitionWallet.toWallet=commissionWallet?.id
        deductToCommitionWallet.isdebit=true
        deductToCommitionWallet.status=1
        deductToCommitionWallet.transactionAmount=commissionAmount
        deductToCommitionWallet.transactionType=10
        deductToCommitionWallet.transactionWalletId=commissionWallet?.id
        deductToCommitionWallet.transactionTo='Commission'
        deductToCommitionWallet.transactionAction='minus'
        deductToCommitionWallet.uid=commissionWallet?.uid
        deductToCommitionWallet.payout=false
        deductToCommitionWallet.useTransaction(trx)
          await deductToCommitionWallet.save()
          /***********************Update Wallet ****************************/
          commissionWallet.availableAmount=commissionWallet.availableAmount-commissionAmount
          commissionWallet.totalAmount=commissionWallet.totalAmount-commissionAmount
          await commissionWallet.save()
  
          /************************deducted amount from Expert [Amount Transection]  *****************************/
          const deductToExpertWallet=new TransactionHistory()
          deductToExpertWallet.callId=callData?.id
          deductToExpertWallet.description=remeningAmount+' deducted to Expert Wallet for refund => RefundId-'+refund?.id
          deductToExpertWallet.fromWallet=0
          deductToExpertWallet.toWallet=expertWallet?.id
          deductToExpertWallet.isdebit=true
          deductToExpertWallet.status=1
          deductToExpertWallet.transactionAmount=remeningAmount
          deductToExpertWallet.transactionType=10
          deductToExpertWallet.transactionWalletId=expertWallet?.id
          deductToExpertWallet.transactionTo='Expert'
          deductToExpertWallet.transactionAction='minus'
          deductToExpertWallet.uid=expertId
          deductToExpertWallet.payout=false
          deductToExpertWallet.useTransaction(trx)
          await deductToExpertWallet.save()
          /***********************Update Wallet ****************************/
          expertWallet.disputedAmount=expertWallet.disputedAmount-remeningAmount
          expertWallet.totalAmount=expertWallet.totalAmount-remeningAmount
          await expertWallet.save()
        
          /************************add amount to User [Amount Transection]  *****************************/
          const addToUserWallet=new TransactionHistory()
          addToUserWallet.callId=callData?.id
          addToUserWallet.description=(remeningAmount+gstAmount+commissionAmount)+' add to Seeker Wallet for refund => RefundId-'+refund?.id
          addToUserWallet.fromWallet=expertWallet?.id
          addToUserWallet.toWallet=userWallet?.id
          addToUserWallet.isdebit=false
          addToUserWallet.status=1
          addToUserWallet.transactionAmount=(remeningAmount+gstAmount+commissionAmount)
          addToUserWallet.transactionType=10
          addToUserWallet.transactionWalletId=userWallet?.id
          addToUserWallet.transactionTo='User'
          addToUserWallet.transactionAction='plus'
          addToUserWallet.uid=uid
          addToUserWallet.payout=false
          addToUserWallet.useTransaction(trx)
          await addToUserWallet.save()
          /***********************Update Wallet ****************************/
          userWallet.availableAmount=userWallet.availableAmount+(remeningAmount+gstAmount+commissionAmount)
          userWallet.totalAmount=userWallet.totalAmount+(remeningAmount+gstAmount+commissionAmount)
          await userWallet.save()
          return userWallet?.id
      })
      return {status:'success',statusCode:200,amount:amount,transaction_id:trns_id}
    } catch (error) {
      const results = perf.stop()
      await WebServiceLog.create({request:JSON.stringify(refund),response:JSON.stringify(error),service_name:'Refund Approved by Expert helper error',status:1,uid:uid,time_taken:results.time})
      return {status:'error',statusCode:error?.statusCode,msg:error?.error?.description}
    }
  }

  async referalAmountTranfer(userWallet,amount){
  
   //*********************User Transection***************************//
    const transectionData:any = new TransactionHistory()
    transectionData.description=amount+' referral amount tranfer to available amount in User Wallet'
    transectionData.fromWallet=userWallet?.id
    transectionData.toWallet=userWallet?.id
    transectionData.isdebit=false
    transectionData.razorPayPaymentId=''
    transectionData.bankId=0
    transectionData.status=1
    transectionData.transactionAmount=amount
    transectionData.transactionType=13
    transectionData.transactionWalletId=userWallet?.id
    transectionData.transactionTo='User'
    transectionData.transactionAction='plus'
    transectionData.uid=userWallet?.uid
    transectionData.payout=false
    await transectionData.save()
    userWallet.promoAmount=userWallet?.promoAmount-amount
    userWallet.availableAmount=userWallet?.availableAmount+amount
    return await userWallet.save()
    
    //*********************User Transection[End]***************************//
 }

}