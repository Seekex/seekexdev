import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import { schema, rules,validator } from '@ioc:Adonis/Core/Validator'
import Application from '@ioc:Adonis/Core/Application'
import Chat from 'App/Models/Chat'
import Database from '@ioc:Adonis/Lucid/Database'
import User from 'App/Models/User'
import { DateTime } from 'luxon'
import LoginHistory from 'App/Models/LoginHistory'
import NotificationHelper from 'App/Helpers/NotificationHelper'
const notificationHelper = new NotificationHelper() 
export default class ChatController {
  
  public async index({view,request,auth,response,session}:HttpContextContract){
    const page = request.input('page', 1)
    const s = request.input('s')
    const from =request.input('from')
    const to =request.input('to')
    const limit = 50
    const offset=(page-1)*limit

    var queryRaw='SELECT DISTINCT CASE WHEN sender_id > receiver_id THEN sender_id ELSE receiver_id END as sender_id, CASE WHEN sender_id > receiver_id THEN receiver_id ELSE sender_id END as receiver_id FROM chats C join users U ON C.sender_id=U.id OR C.receiver_id=U.id '
    var arg:any={}
      arg.limit=limit
      arg.offset=offset
     if(s){
      queryRaw+=' where U.name LIKE :s '
      const sd='%'+s+'%'
      arg.s=sd
     }
     if(from && to){
      var created_to = new Date(to)
      created_to.setUTCHours(0,0,0,0)
      var created_from = new Date(from)
      created_from.setUTCHours(23,59,0,0)
     // return {created_from,created_to}
     if(created_from.getTime()>created_to.getTime()){
      session.flash({error:'Invalid dates.'})
      return response.redirect('back')
     }
     arg.from=created_from
     arg.to=created_to
     if(s){
      queryRaw+=' and C.time_stamp between :from and :to '
     }else{
      queryRaw+=' where C.time_stamp between :from and :to '
     }
    }
     queryRaw+=' limit :limit offset :offset;'
     var rawChats = await Database.rawQuery(queryRaw,arg)

     var countRaw = "SELECT COUNT(DISTINCT CASE WHEN sender_id > receiver_id THEN sender_id ELSE receiver_id END, CASE WHEN sender_id > receiver_id THEN receiver_id ELSE sender_id END) as count FROM chats C join users U ON C.sender_id=U.id OR C.receiver_id=U.id "
     if(s){
        countRaw+=' where U.name LIKE ?; '
        const sd='%'+s+'%'
        var rawChatsCount = await Database.rawQuery(countRaw,[sd])
                       // .debug(true)
     }else{
        var rawChatsCount = await Database.rawQuery(countRaw)
     }
     
     const totalRec = rawChatsCount[0][0]?.count||1
     const totalPage = Math.ceil(totalRec/limit)
   // return  rawChats[0]

  //  const newd = await Chat.query().select().distinct().ca
  //   return newd
    const consvertitionsUsers:any = await Chat.query()
                                          .preload('receiver_user')
                                          .preload('sender_user')
                                          .has('receiver_user')
                                          .has('sender_user')
                                          .orderBy('created','desc')
                                          .paginate(page,limit)
    const allData = consvertitionsUsers?.rows
     consvertitionsUsers.baseUrl(request.url())
     consvertitionsUsers.queryString({'s':s,'from':from,'to':to})
   // return allData
    var chats = await Promise.all(rawChats[0].map(async(element:any)=>{
      const lastChat:any = await Chat.query()
                                .where({receiverId:element.receiver_id,senderId:element.sender_id})
                                .orWhere({receiverId:element.sender_id,senderId:element.receiver_id})
                                .orderBy('id','desc').first()
     element.timeStamp=DateTime.fromISO(new Date(lastChat?.timeStamp).toISOString(),{setZone:true}).setZone('Asia/Kolkata').toFormat('dd, LLL yyyy t')
      var count:any = await Chat.query().where({'senderId':element.sender_id,'receiverId':element.receiver_id,'status':1})
      .pojo<{ count: number}>()
      .count('id as count')
   //   newElement.unreadMsgCount==count[0]?.count
      element.receiver_user= await User.find(element.receiver_id)
      element.sender_user=await User.find(element.sender_id)
      var msg:any={}
      msg.chat=element
      msg.chat.unreadMsgCount=count[0]?.count
      msg.unreadMsgCount=count[0]?.count
      return msg
    }))
    //return chats
    return view.render('admin/chat/index',{totalPage,chats,consvertitionsUsers})
  }

  async getConversations({view,request,auth,params,response}:HttpContextContract){
    const senderId = params?.sid
    const reciverId = params?.rid
    const page=request.input('page', 1)
    const limit=50

    var allChats:any=[]
    const allChatsData:any = await Chat.query()
                              .preload('receiver_user')
                              .preload('sender_user')
                              .has('receiver_user')
                              .has('sender_user')
                              .where({receiverId:reciverId,senderId:senderId})
                              .orWhere({receiverId:senderId,senderId:reciverId})
                              .orderBy('id','desc')
                              .paginate(page,limit)
                              //.limit(limit)
      //allChats=allChats.serialize()
      allChats = allChatsData?.rows
      allChats = allChats.map((elm:any)=>{
        elm.timeStamp=DateTime.fromISO(new Date(elm.timeStamp).toISOString(),{setZone:true}).setZone('Asia/Kolkata').toFormat('dd, LLL yyyy t')
        return elm
      })
    
    // allChats.sort(function(a, b) {
    //   if (a.id < b.id) return -1
    // })
    const lastPage = allChatsData.lastPage
    if(request.ajax()){
      return response.status(200).json(allChats)
    }else{
      return view.render('admin/chat/view',{allChats,reciverId,senderId,lastPage})
    }
    
    return allChats
  }

  async create({view,request,auth,session,response}:HttpContextContract){
    var data=request.all()
    const fromUser=data.from||''
    const toUser=data.to||''
    if(fromUser && toUser && fromUser==toUser){
      session.flash({error:'Invalid data selected.'})
      return response.redirect('back')
    }
    const fromUserData= await User.query().where('id',fromUser).first()
    const toUserData= await User.query().where('id',toUser).first()
    var allChats:any=[]
    if(fromUser && toUser){
      const page=request.input('page', 1)
    const limit=50
    const allChatsData:any = await Chat.query()
                              .preload('receiver_user')
                              .preload('sender_user')
                              .has('receiver_user')
                              .has('sender_user')
                              .where({receiverId:toUser,senderId:fromUser})
                              .orWhere({receiverId:fromUser,senderId:toUser})
                              .orderBy('id','desc')
                              .paginate(page,limit)
                              //.limit(limit)
      //allChats=allChats.serialize()
      allChats = allChatsData?.rows
      allChats = allChats.map((elm:any)=>{
        elm.timeStamp=DateTime.fromISO(new Date(elm.timeStamp).toISOString(),{setZone:true}).setZone('Asia/Kolkata').toFormat('dd, LLL yyyy t')
        return elm
      })
      allChats.sort(function(a, b) {
        if (a.id < b.id) return -1
      })
    }
    return view.render('admin/chat/create',{from:fromUserData?.name||'',to:toUserData?.name||'',allChats:allChats,reciverId:toUser,senderId:fromUser})
  }

  async searchUserList({request,response}:HttpContextContract){
    const data=request.all()
    const name=data?.query
    const userList= await User.userListByIdOrName(name)
    return response.status(200).json({query:name,suggestions:userList})
  }

  async sendChat({response,auth,request}:HttpContextContract){
    const uid=auth.user?.id
    const data:any=request.all()
    const userTimeZone = auth.user?.timeZone||'UTC'
    var mediasize=0
    var msgType=14
    var allChats:any=[]
    if(data.message){
       try {
         var chatMsg:any
         if(data?.message ||data?.url){
           chatMsg = new Chat()
         //  chatMsg.url=uploadUrl?.Location||''
           chatMsg.media=data?.url?true:false
           chatMsg.message=data?.message
           chatMsg.url=data?.url
           chatMsg.receiverId=data?.receiverId
           chatMsg.senderId=data?.senderId
           chatMsg.size=mediasize||data?.size||0
           chatMsg.status=1
           chatMsg.type=data?.type||msgType
          //chatMsg.timeStamp=new Date()
          /*************************Notification Chat************************/           
            if(data?.message){
              var fromUser = await User.find(data?.senderId)
              var notifyUser= await LoginHistory.query().where("uid",data?.receiverId).orderBy("id","desc").first()
              var notifyData={fcm:notifyUser?.fcmToken||request.input('fcm',''),msg:data?.message,uid:data?.receiverId,chatMsg:chatMsg}
              var notify:any=await await notificationHelper.sendNotifictionForChat(notifyData,fromUser)
            }
          /*************************Notification Chat************************/ 
          await chatMsg.save()
         }else{
           chatMsg=''
         }
         if(data?.receiverId){
           allChats = await Chat.query()
                                .preload('receiver_user')
                                .preload('sender_user')
                                .has('receiver_user')
                                .has('sender_user')
                                .where({receiverId:data?.receiverId,senderId:data?.senderId})
                                .orWhere({receiverId:data?.senderId,senderId:data?.receiverId})
                                .orderBy('id','desc').limit(50)
            allChats = allChats.map((elm)=>{
              elm.timeStamp=DateTime.fromISO(new Date(elm.timeStamp).toISOString(),{setZone:true}).setZone(userTimeZone).toFormat('dd, LLL yyyy t')
              return elm
            })                              
            allChats.sort(function(a, b) {
              if (a.id < b.id) return -1
            })                       
         }
         return response.status(200).json(allChats)
       } catch (error) {
         return response.status(201).json({})
       }

    }else{
      allChats = await Chat.query()
                          .preload('receiver_user')
                          .preload('sender_user')
                          .has('receiver_user')
                          .has('sender_user')
                          .where({receiverId:data?.receiverId,senderId:data?.senderId||uid})
                          .orWhere({receiverId:data?.senderId||uid,senderId:data?.receiverId})
                          .orderBy('id','desc').limit(50)
      allChats = allChats.map((elm)=>{
        elm.timeStamp=DateTime.fromISO(new Date(elm.timeStamp).toISOString(),{setZone:true}).setZone(userTimeZone).toFormat('dd, LLL yyyy t')
        return elm
      })         
    allChats.sort(function(a, b) {
      if (a.id < b.id) return -1
    })                    
    return response.status(200).json(allChats)
    }
  }

  async export({response,session}:HttpContextContract){
    var xl = require('excel4node')
    var wb = new xl.Workbook({dateFormat: 'dd/mm/yyyy'})
    var ws = wb.addWorksheet('All Chats User')
    ws.cell(1, 1).string('Sender User')
    ws.cell(1, 2).string('Receiver User')
    ws.cell(1, 3).string('Unread messages')
    ws.cell(1, 4).string('Last Message')
    var queryRaw='SELECT DISTINCT CASE WHEN sender_id > receiver_id THEN sender_id ELSE receiver_id END as sender_id, CASE WHEN sender_id > receiver_id THEN receiver_id ELSE sender_id END as receiver_id FROM chats C join users U ON C.sender_id=U.id OR C.receiver_id=U.id '
    var rawChats = await Database.rawQuery(queryRaw,[])
    var chats = await Promise.all(rawChats[0].map(async(element:any)=>{
      const lastChat:any = await Chat.query()
                                .where({receiverId:element.receiver_id,senderId:element.sender_id})
                                .orWhere({receiverId:element.sender_id,senderId:element.receiver_id})
                                .orderBy('id','desc').first()
     element.timeStamp=DateTime.fromISO(new Date(lastChat?.timeStamp).toISOString(),{setZone:true}).setZone('Asia/Kolkata').toFormat('dd, LLL yyyy t')
      var count:any = await Chat.query().where({'senderId':element.sender_id,'receiverId':element.receiver_id,'status':1})
      .pojo<{ count: number}>()
      .count('id as count')
   //   newElement.unreadMsgCount==count[0]?.count
      element.receiver_user= await User.find(element.receiver_id)
      element.sender_user=await User.find(element.sender_id)
      var msg:any={}
      msg.chat=element
      msg.chat.unreadMsgCount=count[0]?.count
      msg.unreadMsgCount=count[0]?.count
      return msg
    }))
    for (const [indx,value] of Object.entries(chats)) {
      const row = (parseInt(indx)+2)
      const item:any=value
      //console.log(item)
      ws.cell(row, 1).string(item.chat.sender_user?item.chat.sender_user.name:'NA')
      ws.cell(row, 2).string(item.chat.receiver_user?item.chat.receiver_user.name:'NA')
      ws.cell(row, 3).number(item.unreadMsgCount)
      ws.cell(row, 4).string(item.chat.timeStamp)
    }
    const nf=`${new Date().getTime()}${'.xlsx'}`
    const file=Application.publicPath('files/')+nf
    const gFile = await new Promise( function(resolve,reject) {
                      wb.write(file, async function(err, stats) {
                        if (err) {
                          await reject(err)
                        } else {
                          await resolve(stats)
                        }
                      })
                  })
    if(gFile){
      return response.attachment(file,'chats_users.xlsx')
    }
    session.flash({error:'Internal error, please try again.'})
    return response.redirect('back')
  }

}
