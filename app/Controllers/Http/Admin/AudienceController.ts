import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import Event from '@ioc:Adonis/Core/Event'
import AppUserEvent from 'App/Models/AppUserEvent'
import User from 'App/Models/User'
const ext = (file)=>{
  return (file = file.substr(1 + file.lastIndexOf("/")).split('?')[0]).split('#')[0].substr(file.lastIndexOf("."))
}
import UploadHelper from 'App/Helpers/UploadHelper'
const uploadHelper = new UploadHelper()
import NotificationHelper from 'App/Helpers/NotificationHelper'
const notificationHelper = new NotificationHelper()
import SearchHelper from 'App/Helpers/SearchHelper'
const searchHelper = new SearchHelper()
import { string } from '@ioc:Adonis/Core/Helpers'
import FirebaseHelper from 'App/Helpers/FirebaseHelper'
const firebaseHelper = new FirebaseHelper()
import Application from '@ioc:Adonis/Core/Application'
import AudienceCampaign from 'App/Models/AudienceCampaign'
import UserVideo from 'App/Models/UserVideo'
import Post from 'App/Models/Post'

export default class AudienceController {

  public async index({view,request,auth}:HttpContextContract){
   // return await searchHelper.searchStrInKeywordsWithUserId('astro')

    return view.render('admin/audience/index')
  }

  public async allEvents({response}:HttpContextContract){
    const evnts=await AppUserEvent.eventsName()
    var filters:any=[]
    for await (const item of Object.entries(evnts)) {
      const evnt = {
        value: ''+item[0],
        name: string.capitalCase(item[1])
      }
      filters.push(evnt)
    }
    return response.json(filters)
  }

  public async allActions({response}:HttpContextContract){
    const evntsAct=await AppUserEvent.eventsActions()
    var acts:any=[]
    for await (const item of Object.entries(evntsAct)) {
      const evnt = {
        value: ''+item[0],
        name: string.capitalCase(item[1].name)
      }
      acts.push(evnt)
    }
    return response.status(200).json(acts)
  }

  public async actionData({params,response}:HttpContextContract){
    const actionType = params?.actionType
    if(actionType==1){
      //Videos
      const videos = await (await UserVideo.query().where('status',1)).map((item)=>{
        return {value:item?.id,name:'Video Id => '+item?.id}
      })
      return response.status(200).json(videos)
    }
    if(actionType==2){
      //Posts
      const posts = await (await Post.query().where('status',1)).map((item)=>{
        return {value:item?.id,name:'Post Id => '+item?.id}
      })
      return response.status(200).json(posts)
    }
    if(actionType==3){
      //Experts
      const experts = await (await User.query().where('status',1).where('expert',1)).map((item)=>{
        return {value:item?.id,name:'Expert => '+item?.name}
      })
      return response.status(200).json(experts)
    }
    return response.status(200).json([])
  }


  public async filterData({response,request,auth}:HttpContextContract){
    const filters=request.input('filters')
    const addMoney = request.input('addMoney')
    const addMoneyValue = request.input('addMoneyValue')||0
    const notifyTitle = request.input('notifyTitle')
    const message = request.input('message')
    const notifyAction = request.input('notifyAction')
    const notifyActionId = request.input('notifyActionId')
    const imageUrl = request.input('imageUrl')
    const coupon = request.input('coupon')
    const couponCode = request.input('couponCode')
    const showData = request.input('show')
    var errorMsg=''
   // return notifyAction
    const userQuery = User.query()
                      .select('id','name','email')
                      .preload('userFcm')
    for await (const item of filters) {
      var opt='<>'
      if(item?.eventValue=="1"){
        opt='>'
      }
      if(item?.eventValue=="0"){
        opt='<='
      }
      if(item?.eventId==1){
        if(item?.eventCondition=='OR'){
          userQuery.orWhereHas('userCalls',(q)=>{
            q.count('*')
          },opt,0)
        }else if(item?.eventCondition=='AND'){
          userQuery.andWhereHas('userCalls',(q)=>{
            q.count('*')
          },opt,0)
        }else{
          userQuery.whereHas('userCalls',(q)=>{
            q.count('*')
          },opt,0)
        }
      }
      if(item?.eventId==2){
        if(item?.eventCondition=='OR'){
          userQuery.orWhereHas('userMessages',(q)=>{
            q.count('*')
          },opt,0)
        }else if(item?.eventCondition=='AND'){
          userQuery.andWhereHas('userMessages',(q)=>{
            q.count('*')
          },opt,0)
        }else{
          userQuery.whereHas('userMessages',(q)=>{
            q.count('*')
          },opt,0)
        }
      }
      if(item?.eventId==5){
        if(item?.eventCondition=='OR'){
          userQuery.orWhereHas('userScheduleSetting',(q)=>{
            q.count('*')
          },opt,0).where('expert',1)
        }else if(item?.eventCondition=='AND'){
          userQuery.andWhereHas('userScheduleSetting',(q)=>{
            q.count('*')
          },opt,0).where('expert',1)
        }else{
          userQuery.whereHas('userScheduleSetting',(q)=>{
            q.count('*')
          },opt,0).where('expert',1)
        }
      }
      if(item?.eventId==6){
        if(item?.eventCondition=='OR'){
          userQuery.orWhere('regCompleteStep',opt,3).where('expert',1)
        }else if(item?.eventCondition=='AND'){
          userQuery.andWhere('regCompleteStep',opt,3).where('expert',1)
        }else{
          userQuery.where('regCompleteStep',opt,3).where('expert',1)
        }
      }
      if(item?.eventId==7){
        if(item?.eventCondition=='OR'){
          if(item?.eventValue==1){
            userQuery.orWhereNotNull('facebook')
          }
          if(item?.eventValue==0){
            userQuery.orWhereNull('facebook')
          }
        }else if(item?.eventCondition=='AND'){
          if(item?.eventValue==1){
            userQuery.andWhereNotNull('facebook')
          }
          if(item?.eventValue==0){
            userQuery.andWhereNull('facebook')
          }
        }else{
          if(item?.eventValue==1){
            userQuery.whereNotNull('facebook')
          }
          if(item?.eventValue==0){
            userQuery.whereNull('facebook')
          }
        }
      }
      if(item?.eventId==8){
        if(item?.eventCondition=='OR'){
          if(item?.eventValue==1){
            userQuery.orWhereNotNull('linkedin')
          }
          if(item?.eventValue==0){
            userQuery.orWhereNull('linkedin')
          }
        }else if(item?.eventCondition=='AND'){
          if(item?.eventValue==1){
            userQuery.andWhereNotNull('linkedin')
          }
          if(item?.eventValue==0){
            userQuery.andWhereNull('linkedin')
          }
        }else{
          if(item?.eventValue==1){
            userQuery.whereNotNull('linkedin')
          }
          if(item?.eventValue==0){
            userQuery.whereNull('linkedin')
          }
        }
      }
      if(item?.eventId==9){
        if(item?.eventCondition=='OR'){
          if(item?.eventValue==1){
            userQuery.orWhereNotNull('instagram')
          }
          if(item?.eventValue==0){
            userQuery.orWhereNull('instagram')
          }
        }else if(item?.eventCondition=='AND'){
          if(item?.eventValue==1){
            userQuery.andWhereNotNull('instagram')
          }
          if(item?.eventValue==0){
            userQuery.andWhereNull('instagram')
          }
        }else{
          if(item?.eventValue==1){
            userQuery.whereNotNull('instagram')
          }
          if(item?.eventValue==0){
            userQuery.whereNull('instagram')
          }
        }
      }
      if(item?.eventId==10){
        if(item?.eventCondition=='OR'){
          if(item?.eventValue==1){
            userQuery.orWhereNotNull('chanalUrl')
          }
          if(item?.eventValue==0){
            userQuery.orWhereNull('chanalUrl')
          }
        }else if(item?.eventCondition=='AND'){
          if(item?.eventValue==1){
            userQuery.andWhereNotNull('chanalUrl')
          }
          if(item?.eventValue==0){
            userQuery.andWhereNull('chanalUrl')
          }
        }else{
          if(item?.eventValue==1){
            userQuery.whereNotNull('chanalUrl')
          }
          if(item?.eventValue==0){
            userQuery.whereNull('chanalUrl')
          }
        }
      }
      if(item?.eventId==12){
        if(item?.eventCondition=='OR'){
          userQuery.orWhereHas('userPosts',(q)=>{
            q.count('*')
          },opt,0)
        }else if(item?.eventCondition=='AND'){
          userQuery.andWhereHas('userPosts',(q)=>{
            q.count('*')
          },opt,0)
        }else{
          userQuery.whereHas('userPosts',(q)=>{
            q.count('*')
          },opt,0)
        }
      }
      if(item?.eventId==13){
        if(item?.eventCondition=='OR'){
          userQuery.orWhereHas('callRequests',(q)=>{
            q.count('*')
          },opt,0)
        }else if(item?.eventCondition=='AND'){
          userQuery.andWhereHas('callRequests',(q)=>{
            q.count('*')
          },opt,0)
        }else{
          userQuery.whereHas('callRequests',(q)=>{
            q.count('*')
          },opt,0)
        }
      }
      if(item?.eventId==14){
        if(item?.eventCondition=='OR'){
          if(item?.eventValue==1){
            userQuery.orWhereNotNull('profilePic')
          }
          if(item?.eventValue==0){
            userQuery.orWhereNull('profilePic')
          }
        }else if(item?.eventCondition=='AND'){
          if(item?.eventValue==1){
            userQuery.andWhereNotNull('profilePic')
          }
          if(item?.eventValue==0){
            userQuery.andWhereNull('profilePic')
          }
        }else{
          if(item?.eventValue==1){
            userQuery.whereNotNull('profilePic')
          }
          if(item?.eventValue==0){
            userQuery.whereNull('profilePic')
          }
        }
      }
      if(item?.eventId==15){
        if(item?.eventCondition=='OR'){
          if(item?.eventValue==1){
            userQuery.orWhereNotNull('resume')
          }
          if(item?.eventValue==0){
            userQuery.orWhereNull('resume')
          }
        }else if(item?.eventCondition=='AND'){
          if(item?.eventValue==1){
            userQuery.andWhereNotNull('resume')
          }
          if(item?.eventValue==0){
            userQuery.andWhereNull('resume')
          }
        }else{
          if(item?.eventValue==1){
            userQuery.whereNotNull('resume')
          }
          if(item?.eventValue==0){
            userQuery.whereNull('resume')
          }
        }
      }
      if(item?.eventId==16){
        if(item?.eventCondition=='OR'){
          if(item?.eventValue==1){
            userQuery.orWhereNotNull('intro')
          }
          if(item?.eventValue==0){
            userQuery.orWhereNull('intro')
          }
        }else if(item?.eventCondition=='AND'){
          if(item?.eventValue==1){
            userQuery.andWhereNotNull('intro')
          }
          if(item?.eventValue==0){
            userQuery.andWhereNull('intro')
          }
        }else{
          if(item?.eventValue==1){
            userQuery.whereNotNull('intro')
          }
          if(item?.eventValue==0){
            userQuery.whereNull('intro')
          }
        }
      }
      if(item?.eventId==17){
        if(item?.eventCondition=='OR'){
          userQuery.orWhereHas('userPostLikes',(q)=>{
            q.count('id')
          },opt,0)
        }else if(item?.eventCondition=='AND'){
          userQuery.andWhereHas('userPostLikes',(q)=>{
            q.count('id')
          },opt,0)
        }else{
          userQuery.whereHas('userPostLikes',(q)=>{
            q.count('id')
          },opt,0)
        }
      }
      if(item?.eventId==18){
        if(item?.eventCondition=='OR'){
          userQuery.orWhereHas('userPostFollows',(q)=>{
            q.count('id')
          },opt,0)
        }else if(item?.eventCondition=='AND'){
          userQuery.andWhereHas('userPostFollows',(q)=>{
            q.count('id')
          },opt,0)
        }else{
          userQuery.whereHas('userPostFollows',(q)=>{
            q.count('id')
          },opt,0)
        }
      }
      if(item?.eventId==19){
        if(item?.eventCondition=='OR'){
          userQuery.orWhereHas('postComments',(q)=>{
            q.count('id')
          },opt,0)
        }else if(item?.eventCondition=='AND'){
          userQuery.andWhereHas('postComments',(q)=>{
            q.count('id')
          },opt,0)
        }else{
          userQuery.whereHas('postComments',(q)=>{
            q.count('id')
          },opt,0)
        }
      }
      if(item?.eventId==20){
        if(item.sKeyword==''){
          errorMsg='Please enter search keyword.'
        }else{
          const allUsers = await searchHelper.searchStrInKeywordsWithUserId(item?.sKeyword)
          //return allUsers
          if(allUsers.length>0){
            if(item?.eventCondition=='OR'){
              userQuery.orWhereIn('id',allUsers)
            }else if(item?.eventCondition=='AND'){
              userQuery.andWhereIn('id',allUsers)
            }else{
              userQuery.whereIn('id',allUsers)
            }
          }
        }
      }
    }
    if(errorMsg){
      return response.status(200).json({status:'error',msg:errorMsg})
    }
    const allUDatas =await userQuery
                      // .pojo<{ count: number}>()
                      // .count('id as count').first()
    // return allUDatas
     if(showData==1){
        const campaign = new AudienceCampaign()
        campaign.filtersData = JSON.stringify(filters)
        campaign.addAmount=addMoney
        campaign.amountValue=addMoneyValue
        campaign.coupon=coupon
        campaign.couponCode=couponCode
        campaign.notifyTitle=notifyTitle
        campaign.notifyMsg=message
        campaign.image=imageUrl
        campaign.action=notifyAction
        campaign.actionId=notifyActionId||0
        campaign.type=1
        campaign.totalUsers=allUDatas?.length||0
        campaign.status=0
        campaign.createdBy=auth?.user?.id||0
        await campaign.save()
        return response.status(200).json({status:'success',users:allUDatas})
     }                 
    var notifyData = {
                        title:notifyTitle,
                        message:message,
                        amount:addMoneyValue,
                        action:notifyAction,
                        actionId:notifyActionId,
                        addMoney:addMoney,
                        image:imageUrl,
                        coupon:coupon,
                        couponCode:couponCode,
                        campaignId:0
                      }              
    if(allUDatas.length>0){
      const campaign = new AudienceCampaign()
      campaign.filtersData = JSON.stringify(filters)
      campaign.addAmount=addMoney
      campaign.amountValue=addMoneyValue
      campaign.coupon=coupon
      campaign.couponCode=couponCode
      campaign.notifyTitle=notifyTitle
      campaign.notifyMsg=message
      campaign.image=imageUrl
      campaign.action=notifyAction
      campaign.actionId=notifyActionId||0
      campaign.type=2
      campaign.totalUsers=allUDatas?.length||0
      campaign.status=1
      campaign.createdBy=auth?.user?.id||0
      await campaign.save()
      notifyData.campaignId=campaign?.id
      var FirbaseUsersData = await allUDatas.map((u)=>{
       // const un = u.serialize()
        return {
          userId:u?.id,
          campaignId:campaign?.id,
          name:u?.name,
          email:u?.email,
          deviceId:u?.userFcm?.deviceId||'NA',
          fcm:u?.userFcm?.fcmToken||'NA',
          notificationStatus:0,
          sentAt:new Date(),
        }
      })
      firebaseHelper.saveCampaignUsers(FirbaseUsersData)
      Event.emit('new:bulkNotificationWithImg',{data:notifyData,devices:allUDatas})
      
      return response.status(200).json({status:'success',msg:'Notification sent successfuly to all users.'})
    }else{
      return response.status(200).json({status:'success',msg:'No user found for send notification.'})
    }
    
  }


  async uploadImage({request,response}: HttpContextContract){
    const image = request.file('image-0')
    if(image){
      const fileExtName=ext(image.clientName)
      const newFileName = `${new Date().getTime()}${fileExtName}`
      if(fileExtName=='.png' || fileExtName=='.jpg' ||fileExtName=='.jpeg'){
        await image.move(Application.publicPath('notification'),{
          name: newFileName,
        })
        const contentType=fileExtName=='.png'?'image/png':'image/jpeg'
        const uploadedFile=await uploadHelper.uploadFileToFolder(newFileName,contentType,'notification')
        if(uploadedFile){
          return response.status(200).json({status:'success',msg:'Image has been saved.',location:uploadedFile?.Location})
        }else{
          return response.status(200).json({status:'error',msg:'Image not saved, please try again',location:''})
        }
      }
    }
    return image
  }

  async campaignList({view,request,auth}:HttpContextContract){
    const page = request.input('page',1)
    const limit = 20
    var campaignQuery = AudienceCampaign.query()
    var campaigns = await campaignQuery.orderBy('id','desc').paginate(page,limit)
    campaigns.baseUrl(request.url()) 
   // return campaigns
    return view.render('admin/audience/campaign',{campaigns})
  }

  async campaignDetails({view,request,auth,params}:HttpContextContract){
    const id=params?.id
    const campaign = await AudienceCampaign.query().where('id',id).first()
   //const campaignNotificationData = await firebaseHelper.getUserNotificationStatusCounter(id)
    return view.render('admin/audience/campaign_view',{campaign})
  }

  async notificationsCount({response,params}:HttpContextContract){
    const id=params?.id
    const campaignNotificationData = await firebaseHelper.getUserNotificationStatusCounter(id)
    return response.status(200).json(campaignNotificationData)
  }
  
}
