import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import User from 'App/Models/User'
import UserVideo from 'App/Models/UserVideo'
import VideoComment from 'App/Models/VideoComment'
import WebServiceLog from 'App/Models/WebServiceLog'
import NotificationHelper from 'App/Helpers/NotificationHelper'
const notificationHelper = new NotificationHelper() 
import Event from '@ioc:Adonis/Core/Event'
import Application from '@ioc:Adonis/Core/Application'
import { DateTime } from 'luxon'
import LikeVideo from 'App/Models/LikeVideo'
export default class VideosController {

  async index({view,request,session,response}: HttpContextContract){
    const s = request.input('s')
    const from =request.input('from')
    const to =request.input('to')
    const type= request.input('type')
    const page = request.input('page', 1)
    const limit = 20
    const videoQuery =  UserVideo.query()
                          .preload('user')
                          .has('user')
                          .withCount('comments',(q)=>{
                            q.has('user')
                         })
                         .withCount('videoLikes')
                         .withCount('videoFollows')
                         .withCount('videoViews')
                   
    if(s){
      videoQuery.where(qu=>{
        qu.whereHas('user',(q)=>{
          q.where('name','LIKE',"%"+s+"%")
        }).orWhere('description','LIKE',"%"+s+"%")
      })
    }
    if(type){
      videoQuery.where('videoType',type)
    }
    if(from && to){
      var created_to = new Date(to)
      created_to.setUTCHours(0,0,0,0)
      var created_from = new Date(from)
      created_from.setUTCHours(23,59,0,0)
     // return {created_from,created_to}
     if(created_from.getTime()>created_to.getTime()){
      session.flash({error:'Invalid dates.'})
      return response.redirect('back')
     }
     videoQuery.where(qu=>{
      qu.whereBetween('created',[created_from,created_to])
      .orWhereBetween('fromDate',[created_from,created_to])
      .orWhereBetween('toDate',[created_from,created_to])
     })
    }

    const videos = await videoQuery
                          .orderBy('id','desc')
                          .paginate(page,limit)
      videos.baseUrl(request.url())
      videos.queryString({'s':s,'from':from,'to':to})
    return view.render('admin/video/index',{videos})
  }

  public async changeStatus({request}:HttpContextContract){
    const data=request.all()
		const video:any = await UserVideo.query().where({'id':data.id}).first()
		if(video.status==1){
			video.status=0
		}else{
			video.status=1
		}
		await video.save()
		return {status:'success'}
  }

  async view({view,params}: HttpContextContract){
    const id=params.id
    const video =   await UserVideo.query()
                    .preload('user')
                    .preload('comments',(q)=>{
                      q.preload('user')
                    })
                    .has('user')
                    .withCount('comments',(q)=>{
                      q.has('user')
                    })
                    .withCount('videoLikes')
                    .withCount('videoFollows')
                    .withCount('videoViews')
                    .withCount('userTagged')
                    .where({id:id}).first()
   // return video
    return view.render('admin/video/view',{video})
  }

  async postComment({ request,response,auth }: HttpContextContract){
    const data=request.all()
    //return data
    const uid:any=data?.uid
    const fromUser= await User.find(uid)
    var comment=new VideoComment()
    comment.comment=data.comment?data.comment:data.keyValue
    comment.userVideoId=data.postId||data.id
    comment.uid=uid
    comment.parentId=0
    try {
      //********************************************** */
     // await notificationHelper.sendNotificationForComment(comment,fromUser)
      //********************************************* */
      await comment.save()
      if(comment.userVideoId){ 
       // Event.emit('new:sendNotificationForCommentToFollowers',{data:comment,fromUser:fromUser})
      }
      var allComments=await VideoComment.query().preload('user').where('userVideoId',comment.userVideoId).limit(20).orderBy('id','desc')
      return response.status(200).json(allComments)    
    } catch (error) {
      await WebServiceLog.create({request:JSON.stringify(data),response:JSON.stringify(error),service_name:'Post Video Comment Service From Admin',status:1,uid:uid,time_taken:1})
      return response.status(200).json({
          "errors":error,
          "logout": false,
          "resMsg": "Post comment not saved",
          "status": false
      })
    }
  }

  async likeVideo({ request,response,auth }: HttpContextContract){
    const data = request.all()
    data.type=1
    const fromUser= await User.find(data?.uid)
    const saveData=new LikeVideo()
    saveData.uid=data?.uid
    saveData.userVideoId=data?.postId
    const exit:any = await LikeVideo.query().where({uid:data?.uid,userVideoId:data?.postId}).delete()
    try {
      if(exit==0){
        await saveData.save()
        /**************************Notifications********************************/
       //   await notificationHelper.sendNotificationForLikesFollow(saveData,fromUser,data?.type)
        /**************************Notifications********************************/
      }
      return response.status(200).json({
        "logout": false,
        "resMsg": "Successful",
        "status": true,
        "action":exit
      })
    } catch (error) {
      await WebServiceLog.create({request:JSON.stringify(data?.uid),response:JSON.stringify(error),service_name:'User Get Follow Like Service From Admin for video',status:1,uid:data?.uid,time_taken:0})
      return response.status(200).json({
        "errors":error,
        "resMsg": "Error Like Follow",
        "status": false,
        "logout": false
      })
    }  
  }

  async export({request,session,response}: HttpContextContract){
    const s = request.input('s')
    const from =request.input('from')
    const to =request.input('to')
    const type= request.input('type')

    const videoQuery =  UserVideo.query()
                          .preload('user')
                          .has('user')
                          .withCount('comments',(q)=>{
                            q.has('user')
                         })
                         .withCount('videoLikes')
                         .withCount('videoFollows')
                         .withCount('videoViews')
                   
    if(s){
      videoQuery.where(qu=>{
        qu.whereHas('user',(q)=>{
          q.where('name','LIKE',"%"+s+"%")
        }).orWhere('description','LIKE',"%"+s+"%")
      })
    }
    if(type){
      videoQuery.where('videoType',type)
    }
    if(from && to){
      var created_to = new Date(to)
      created_to.setUTCHours(0,0,0,0)
      var created_from = new Date(from)
      created_from.setUTCHours(23,59,0,0)
     // return {created_from,created_to}
     if(created_from.getTime()>created_to.getTime()){
      session.flash({error:'Invalid dates.'})
      return response.redirect('back')
     }
     videoQuery.where(qu=>{
      qu.whereBetween('created',[created_from,created_to])
      .orWhereBetween('fromDate',[created_from,created_to])
      .orWhereBetween('toDate',[created_from,created_to])
     })
    }

    const videos = await videoQuery.orderBy('id','desc')
    var xl = require('excel4node')
      var wb = new xl.Workbook({dateFormat: 'dd/mm/yyyy'})
      var ws = wb.addWorksheet('All Video Data')
      ws.cell(1, 1).string('Video Type')
      ws.cell(1, 2).string('Created By')
      ws.cell(1, 3).string('Video Url')
      ws.cell(1, 4).string('Public Id')
      ws.cell(1, 5).string('Video description')
      ws.cell(1, 6).string('Hash tags')
      ws.cell(1, 7).string('Views')
      ws.cell(1, 8).string('Likes')
      ws.cell(1, 9).string('Follows')
      ws.cell(1, 10).string('Comments')
      ws.cell(1, 11).string('Status')
      ws.cell(1, 12).string('Posted Date')
     

      for (const [indx,value] of Object.entries(videos)) {
        const row = (parseInt(indx)+2)
        const item:any=value
        ws.cell(row, 1).string(item?.videoTypeTxt)
        ws.cell(row, 2).string(item?.user.name)
        ws.cell(row, 3).string(item?.videoUrl||'NA')
        ws.cell(row, 4).string(item.publicId||"NA")
        ws.cell(row, 5).string(item?.description)
        ws.cell(row, 6).string(item?.hash_tags||'NA')
        ws.cell(row, 7).number(item?.$extras.videoViews_count||0)
        ws.cell(row, 8).number(item?.$extras.videoLikes_count||0)
        ws.cell(row, 9).number(item?.$extras.videoFollows_count||0)
        ws.cell(row, 10).number(item?.$extras.comments_count||0)
        ws.cell(row, 11).string(item?.status==0?'Blocked':'Active')
        const cdate = DateTime.fromISO(new Date(item.created).toISOString(),{setZone:true}).setZone('Asia/Kolkata').toFormat('dd, LLL yyyy t')
        ws.cell(row, 12).date(cdate)
      }

    const nf=`${new Date().getTime()}${'.xlsx'}`
    const file=Application.publicPath('files/')+nf
    const gFile = await new Promise( function(resolve,reject) {
                      wb.write(file, async function(err, stats) {
                        if (err) {
                          await reject(err)
                        } else {
                          await resolve(stats)
                        }
                      })
                  })
    if(gFile){
      return response.attachment(file,'video_data.xlsx')
    }
    session.flash({error:'Internal error, please try again.'})
    return response.redirect('back')
  }
}
