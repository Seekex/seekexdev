import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import Post from 'App/Models/Post'
import NotificationHelper from 'App/Helpers/NotificationHelper'
const notificationHelper = new NotificationHelper() 
import Event from '@ioc:Adonis/Core/Event'
import Comment from 'App/Models/Comment'
import WebServiceLog from 'App/Models/WebServiceLog'
import User from 'App/Models/User'
import LikePost from 'App/Models/LikePost'
import Application from '@ioc:Adonis/Core/Application'
import { DateTime } from 'luxon'
export default class PostsController {

  async index({view,request,response,session,params}:HttpContextContract){
    const userId = params.uid
    const page = request.input('page', 1)
    const limit = 20
    const postsQuery:any=Post.query().preload('user').has('user').withCount('reported')
    const s = request.input('s')
    const from =request.input('from')
    const to =request.input('to')
    if(userId){
      postsQuery.where('createdBy',userId)
    }
    if(s){
      postsQuery.where(qu=>{
        qu.whereHas('user',(q)=>{
          q.where('name','LIKE',"%"+s+"%")
        }).orWhere('description','LIKE',"%"+s+"%")
      }) 
    }

    if(from && to){
      var created_to = new Date(to)
      created_to.setUTCHours(0,0,0,0)
      var created_from = new Date(from)
      created_from.setUTCHours(23,59,0,0)
     // return {created_from,created_to}
     if(created_from.getTime()>created_to.getTime()){
      session.flash({error:'Invalid dates.'})
      return response.redirect('back')
     }
     postsQuery.where(qu=>{
      qu.whereBetween('created',[created_from,created_to])
     })
    }
    const posts = await postsQuery.orderBy('id','desc').paginate(page,limit)
    posts.baseUrl(request.url())
    posts.queryString({'s':s,'from':from,'to':to})
    return view.render('admin/post/index',{posts})
  }

  public async reportedPosts({view,request}:HttpContextContract){
    const page = request.input('page', 1)
    const limit = 20
    const posts:any=await Post.query().preload('user').has('user').has('reported', '>', 0).orderBy('id','desc').withCount('reported').paginate(page,limit)
    //return posts
    posts.baseUrl(request.url())
    return view.render('admin/post/reported',{posts})
  }

  
  async reportedPostDetail({view,params}){
    const id=params.id
    var post:any= await Post.query()
                      .preload('user')
                      .preload('medias')
                      .preload('comment',(query)=>{
                        query.preload('user').orderBy('id','desc').limit(20)
                      })
                      .preload('reported',(query)=>{
                        query.preload('reportedUser',(qu)=>{
                          qu.select('id','name')
                        })
                      })
                      .withCount('postLikes')
                      .where({'id':id}).first()
     // return post              
      return view.render('admin/post/reported_view',{post})           
  }

  async changePostStatus({request}:HttpContextContract){
    const data=request.all()
		const post:any = await Post.query().where({'id':data.id}).first()
		if(post.status==1){
			post.status=2
		}else{
			post.status=1
		}
		await post.save()
		return {status:'success'}
  }

  async postComment({ request,response,auth }: HttpContextContract){
    const data=request.all()
    //return data
    const uid:any=data?.uid
    const fromUser= await User.find(uid)
    var comment=new Comment()
    comment.comment=data.comment?data.comment:data.keyValue
    comment.postId=data.postId||data.id
    comment.uid=uid
    comment.likes=0
    try {
      //********************************************** */
      await notificationHelper.sendNotificationForComment(comment,fromUser)
      //********************************************* */
      await comment.save()
      if(comment.postId){ 
        Event.emit('new:sendNotificationForCommentToFollowers',{data:comment,fromUser:fromUser})
      }
      var allComments=await Comment.query().preload('user').where('postId',comment.postId).limit(20).orderBy('id','desc')
      return response.status(200).json(allComments)    
    } catch (error) {
      await WebServiceLog.create({request:JSON.stringify(data),response:JSON.stringify(error),service_name:'Post Comment Service From Admin',status:1,uid:uid,time_taken:1})
      return response.status(200).json({
          "errors":error,
          "logout": false,
          "resMsg": "Post comment not saved",
          "status": false
      })
    }
  }

  async likePost({ request,response,auth }: HttpContextContract){
    const data = request.all()
    data.type=1
    const fromUser= await User.find(data?.uid)
    const saveData=new LikePost()
    saveData.uid=data?.uid
    saveData.targetId=data?.postId
    const exit:any = await LikePost.query().where({uid:data?.uid,targetId:data?.postId}).delete()
    try {
      if(exit==0){
        await saveData.save()
        /**************************Notifications********************************/
          await notificationHelper.sendNotificationForLikesFollow(saveData,fromUser,data?.type)
        /**************************Notifications********************************/
      }
      return response.status(200).json({
        "logout": false,
        "resMsg": "Successful",
        "status": true,
        "action":exit
      })
    } catch (error) {
      await WebServiceLog.create({request:JSON.stringify(data?.uid),response:JSON.stringify(error),service_name:'User Get Follow Like Service From Admin',status:1,uid:data?.uid,time_taken:0})
      return response.status(200).json({
        "errors":error,
        "resMsg": "Error Like Follow",
        "status": false,
        "logout": false
      })
    }  
  }

  async export({session,response,request,params}:HttpContextContract){
    const userId = params.uid
    const postsQuery:any=Post.query().preload('user').has('user').withCount('reported')
    const s = request.input('s')
    const from =request.input('from')
    const to =request.input('to')
    if(userId){
      postsQuery.where('createdBy',userId)
    }
    if(s){
      postsQuery.where(qu=>{
        qu.whereHas('user',(q)=>{
          q.where('name','LIKE',"%"+s+"%")
        }).orWhere('description','LIKE',"%"+s+"%")
      }) 
    }

    if(from && to){
      var created_to = new Date(to)
      created_to.setUTCHours(0,0,0,0)
      var created_from = new Date(from)
      created_from.setUTCHours(23,59,0,0)
     // return {created_from,created_to}
     if(created_from.getTime()>created_to.getTime()){
      session.flash({error:'Invalid dates.'})
      return response.redirect('back')
     }
     postsQuery.where(qu=>{
      qu.whereBetween('created',[created_from,created_to])
     })
    }
    const posts = await postsQuery.orderBy('id','desc')

      var xl = require('excel4node')
      var wb = new xl.Workbook({dateFormat: 'dd/mm/yyyy'})
      var ws = wb.addWorksheet('All Post Data')
      ws.cell(1, 1).string('Post Contant')
      ws.cell(1, 2).string('Created By')
      ws.cell(1, 3).string('Hash tags')
      ws.cell(1, 4).string('Views')
      ws.cell(1, 5).string('Posted Date')
      ws.cell(1, 6).string('Block Status')
      ws.cell(1, 7).string('Total Reports')
      for (const [indx,value] of Object.entries(posts)) {
        const row = (parseInt(indx)+2)
        const item:any=value
        ws.cell(row, 1).string(item?.description)
        ws.cell(row, 2).string(item?.user.name)
        ws.cell(row, 3).string(item?.hash_tags||'NA')
        ws.cell(row, 4).number(item?.views||0)
        const cdate = DateTime.fromISO(new Date(item.created).toISOString(),{setZone:true}).setZone('Asia/Kolkata').toFormat('dd, LLL yyyy t')
        ws.cell(row, 5).date(cdate)
        ws.cell(row, 6).string(item?.status==2?'Blocked':'Active')
        ws.cell(row, 7).number(item?.$extras.reported_count||0)
      }

    const nf=`${new Date().getTime()}${'.xlsx'}`
    const file=Application.publicPath('files/')+nf
    const gFile = await new Promise( function(resolve,reject) {
                      wb.write(file, async function(err, stats) {
                        if (err) {
                          await reject(err)
                        } else {
                          await resolve(stats)
                        }
                      })
                  })
    if(gFile){
      return response.attachment(file,'post_data.xlsx')
    }
    session.flash({error:'Internal error, please try again.'})
    return response.redirect('back')
  }
}