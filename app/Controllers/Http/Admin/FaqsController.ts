import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import Faq from 'App/Models/Faq'
export default class FaqsController {

  public async index({auth,view,request}: HttpContextContract){
    const uid=auth.user?.id
    const page = request.input('page', 1)
    const limit = 20
    const faqs:any= await Faq.query().paginate(page,limit)                      
    faqs.baseUrl(request.url()) 
    return view.render('admin/faq/index',{faqs})
  }

  public async view({auth,view,request,params,response}: HttpContextContract){
    const uid=auth.user?.id
    const id=params.id
    const page:any= await Faq.query().where({id:id}).first()
    const type= await Faq.getType()
    const faqFor= await Faq.getFaqFor()
    const subSections= await Faq.getSubSection()
    if(page){
      return view.render('admin/faq/view',{page,type,faqFor,subSections})
    }
    return response.redirect('/admin')
  }
  public async add({auth,view,response}: HttpContextContract){
    const uid=auth.user?.id
    const type= await Faq.getType()
    const faqFor= await Faq.getFaqFor()
    const subSections= await Faq.getSubSection()
    return view.render('admin/faq/add',{type,faqFor,subSections})
  }

  public async changeStatus({request,params}){
    const id=params.id||request.input('id')
    const page:any= await Faq.query().where({id:id}).first()
    page.status=!page.status
    await page.save()
  }
  
  public async save({auth,request,response,session}: HttpContextContract){
    const data = request.all()
    if(data?.id){
      var page:any = await Faq.find(data?.id)
      page.question = data?.question
      page.answere =  data?.answere
      page.type = data?.type
      page.faqFor =  data?.faqFor
      page.section=data?.section
      page.status=1
    }else{
      var page:any = new Faq()
      page.question = data?.question
      page.answere =  data?.answere
      page.type = data?.type
      page.faqFor =  data?.faqFor
      page.section=data?.section
      page.status=1
    }
   // return emailTmp
    try {
      await page.save()
      session.flash({
        "successResMsg": "Data successfuly save",
        "status": 'success'
      })
      return response.redirect('back')
      // return response.status(200).json({
      //   "resMsg": "Data successfuly save",
      //   "status": 'success',
      // })
    } catch (error) {
      session.flash({
        "errorResMsg": "Data not save",
        "status": 'error'
      })
      return response.redirect('back')
      // return response.status(200).json({
      //   "resMsg": "Data not save",
      //   "status": 'error',
      // })
    }
  }
}
