import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import AppSetting from 'App/Models/AppSetting'
import Env from '@ioc:Adonis/Core/Env'
import CountryState from 'App/Models/CountryState'
import NewsletterSubscriber from 'App/Models/NewsletterSubscriber'
import ContactData from 'App/Models/ContactData'
import Application from '@ioc:Adonis/Core/Application'
import { DateTime } from 'luxon'
import SendNewsletter from 'App/Models/SendNewsletter'
import Event from '@ioc:Adonis/Core/Event'
export default class SettingsController {
  public async settings({view}: HttpContextContract){
    const allStates= await (await CountryState.query().where({parentId:91}))
                                                      .map((elm)=>{
                                                        const nelm={id:elm?.id,name:elm?.name}
                                                        return nelm
                                                      })                                           
    const setting=await AppSetting.all()
    var settingData={}
    for (const value of setting) {
      settingData[value.fieldKey]=value.value
    }
    return view.render('admin/settings',{settingData,allStates})
  }

  public async saveSettings({request,response,session}: HttpContextContract){
    const data= request.except(['_csrf'])
    for (const [key, value] of Object.entries(data)) {
      const existkey= await AppSetting.query().where({fieldKey:key}).first()
      if(existkey){
        existkey.value=value
        await existkey.save()
      }else{
        const newData=new AppSetting()
        newData.fieldKey=key
        newData.value=value
        newData.isactive=true
        newData.type=1
       await newData.save()
      }
    }
   // Env.set('TELEPHONY_CHARGE',"2")
    session.flash({success:'Your data has been saved.'})
    return response.redirect('back')
  }

  public async newsletterSubscribers({view,request,session,response}: HttpContextContract){
    const page = request.input('page', 1)
    const limit = 20
    const s = request.input('s')
    const from =request.input('from')
    const to =request.input('to')
    const nlQuery = NewsletterSubscriber.query()
    if(s){
			nlQuery.where('email','LIKE',"%"+s+"%")
		}
    if(from && to){
			var created_to = new Date(to)
			created_to.setUTCHours(0,0,0,0)
			var created_from = new Date(from)
			created_from.setUTCHours(23,59,0,0)
			// return {created_from,created_to}
			if(created_from.getTime()>created_to.getTime()){
			session.flash({error:'Invalid dates.'})
			return response.redirect('back')
			}
			nlQuery.where(qu=>{
			qu.whereBetween('created',[created_from,created_to])
			})
		}
    const datas = await nlQuery.orderBy('id','desc').paginate(page,limit)
    datas.baseUrl(request.url())
    return view.render('admin/newsletter',{datas})
  }

  public async contactData({view,request}: HttpContextContract){
    const page = request.input('page', 1)
    const limit = 20
    const s = request.input('s')
    const from =request.input('from')
    const to =request.input('to')
    const fQuery = ContactData.query()
    if(s){
			fQuery.where('email','LIKE',"%"+s+"%").orWhere('name','LIKE',"%"+s+"%").orWhere('subject','LIKE',"%"+s+"%")
		}
    const datas = await fQuery.orderBy('id','desc').paginate(page,limit)
    datas.baseUrl(request.url())

    return view.render('admin/contact_data',{datas})
  }

  public async newsletterExport({request,session,response}: HttpContextContract){
    const page = request.input('page', 1)
    const limit = 20
    const s = request.input('s')
    const from =request.input('from')
    const to =request.input('to')
    const nlQuery = NewsletterSubscriber.query()
    if(s){
			nlQuery.where('email','LIKE',"%"+s+"%")
		}
    if(from && to){
			var created_to = new Date(to)
			created_to.setUTCHours(0,0,0,0)
			var created_from = new Date(from)
			created_from.setUTCHours(23,59,0,0)
			// return {created_from,created_to}
			if(created_from.getTime()>created_to.getTime()){
			session.flash({error:'Invalid dates.'})
			return response.redirect('back')
			}
			nlQuery.where(qu=>{
			qu.whereBetween('created',[created_from,created_to])
			})
		}
    const datas = await nlQuery.orderBy('id','desc')
    var xl = require('excel4node')
    var wb = new xl.Workbook({dateFormat: 'dd/mm/yyyy'})
    var ws = wb.addWorksheet('Subscribers')
		ws.cell(1, 1).string('Email')
		ws.cell(1, 2).string('Subscribe Date')
    for (const [indx,value] of Object.entries(datas)) {
      const row = (parseInt(indx)+2)
      const item:any=value
      ws.cell(row, 1).string(item?.email)
      const cdate = DateTime.fromISO(new Date(item.created).toISOString(),{setZone:true}).setZone('Asia/Kolkata').toFormat('dd, LLL yyyy t')
      ws.cell(row, 2).date(cdate)
    }
    const nf=`${new Date().getTime()}${'.xlsx'}`
    const file=Application.publicPath('files/')+nf
    const gFile = await new Promise( function(resolve,reject) {
                      wb.write(file, async function(err, stats) {
                        if (err) {
                          await reject(err)
                        } else {
                          await resolve(stats)
                        }
                      })
                  })
    if(gFile){
      return response.attachment(file,'subscribers.xlsx')
    }
    session.flash({error:'Internal error, please try again.'})
    return response.redirect('back')
  }

  public async formDataExport({request,session,response}: HttpContextContract){
    const s = request.input('s')
    const from =request.input('from')
    const to =request.input('to')
    const fQuery = ContactData.query()
    if(s){
			fQuery.where('email','LIKE',"%"+s+"%").orWhere('name','LIKE',"%"+s+"%").orWhere('subject','LIKE',"%"+s+"%")
		}
    const datas = await fQuery.orderBy('id','desc')
    var xl = require('excel4node')
    var wb = new xl.Workbook({dateFormat: 'dd/mm/yyyy'})
    var ws = wb.addWorksheet('Form Data')
		ws.cell(1, 1).string('Name')
		ws.cell(1, 2).string('Email')
    ws.cell(1, 3).string('Subject')
    ws.cell(1, 4).string('Message')
    ws.cell(1, 5).string('Created')
    for (const [indx,value] of Object.entries(datas)) {
      const row = (parseInt(indx)+2)
      const item:any=value
      ws.cell(row, 1).string(item?.name)
      ws.cell(row, 2).string(item?.email)
      ws.cell(row, 3).string(item?.subject)
      ws.cell(row, 4).string(item?.message)
      const cdate = DateTime.fromISO(new Date(item.created).toISOString(),{setZone:true}).setZone('Asia/Kolkata').toFormat('dd, LLL yyyy t')
      ws.cell(row, 5).date(cdate)
    }
    const nf=`${new Date().getTime()}${'.xlsx'}`
    const file=Application.publicPath('files/')+nf
    const gFile = await new Promise( function(resolve,reject) {
                      wb.write(file, async function(err, stats) {
                        if (err) {
                          await reject(err)
                        } else {
                          await resolve(stats)
                        }
                      })
                  })
    if(gFile){
      return response.attachment(file,'form_data.xlsx')
    }
    session.flash({error:'Internal error, please try again.'})
    return response.redirect('back')
  }

  public async sendNewsletter({view,request}: HttpContextContract){

    return view.render('admin/send_newsletter',{})
  }

  public async sendToSubscribers({request,session,response}: HttpContextContract){
    const data=request.all()
    const s = request.input('s')
    const from =request.input('from')
    const to =request.input('to')
    const nlQuery = NewsletterSubscriber.query().where('status',1)
    if(s){
			nlQuery.where('email','LIKE',"%"+s+"%")
		}
    if(from && to){
			var created_to = new Date(to)
			created_to.setUTCHours(0,0,0,0)
			var created_from = new Date(from)
			created_from.setUTCHours(23,59,0,0)
			// return {created_from,created_to}
			if(created_from.getTime()>created_to.getTime()){
			session.flash({error:'Invalid dates.'})
			return response.redirect('back')
			}
			nlQuery.where(qu=>{
			qu.whereBetween('created',[created_from,created_to])
			})
		}
    const emailDatas = await (await nlQuery.orderBy('id','desc')).map(elm=>{
      return elm?.email
    })
    if(emailDatas.length>0){
      const newsLetter = new SendNewsletter()
      newsLetter.title = data?.title
      newsLetter.content =data?.content
      newsLetter.emailData = JSON.stringify(emailDatas)
      try {
        const saveData = await newsLetter.save()
        Event.emit('new:sendNewsletter',{data:saveData,emails:emailDatas})
        session.flash({success:'Newsletter successfuly sent.'})
			  return response.redirect('back')
      } catch (error) {
        session.flash({error:'Newsletter not sent.'})
			  return response.redirect('back')
      }
    }
    session.flash({error:'Newsletter not sent.'})
		return response.redirect('back')
  }
}
