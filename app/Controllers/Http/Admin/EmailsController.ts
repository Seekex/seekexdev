 import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
 import EmailContent from 'App/Models/EmailContent'
export default class EmailsController {

  public async index({auth,view,request}: HttpContextContract){
    const uid=auth.user?.id
    const page = request.input('page', 1)
    const limit = 20
    const emails:any= await EmailContent.query().paginate(page,limit)                      
    emails.baseUrl(request.url()) 
    return view.render('admin/email/index',{emails})
  }

  public async view({auth,view,request,params,response}: HttpContextContract){
    const uid=auth.user?.id
    const id=params.id
    const email:any= await EmailContent.query().where({id:id}).first()
    if(email){
      return view.render('admin/email/view',{email})
    }
    return response.redirect('/admin')
  }
  public async add({auth,view,response}: HttpContextContract){
    const uid=auth.user?.id
    return view.render('admin/email/add')
    
  }

  public async changeStatus({request,params}){
    const id=params.id||request.input('id')
    const email:any= await EmailContent.query().where({id:id}).first()
    email.status=!email.status
    await email.save()
  }
  
  public async save({auth,request,response,session}: HttpContextContract){
    const data = request.all()
    if(data?.id){
      var emailTmp:any = await EmailContent.find(data?.id)
      emailTmp.name = data?.name
     // emailTmp.slug= emailTmp?.slug.replace(/[^\w\s]/gi, '').replace(/\s/g, "-").toLowerCase()||data?.name.replace(/[^\w\s]/gi, '').replace(/\s/g, "-").toLowerCase()
      emailTmp.subject = data?.subject
      emailTmp.fromEmail=data?.fromEmail
      emailTmp.content=data?.content
      emailTmp.status=1
      emailTmp.createdBy=auth.user?.id
    }else{
      var emailTmp:any = new EmailContent()
      emailTmp.name = data?.name
      emailTmp.slug= data.slug?data?.slug.replace(/[^\w\s]/gi, '').replace(/\s/g, "-").toLowerCase():data?.name.replace(/[^\w\s]/gi, '').replace(/\s/g, "-").toLowerCase()
      emailTmp.subject = data?.subject
      emailTmp.fromEmail=data?.fromEmail
      emailTmp.content=data?.content
      emailTmp.status=1
      emailTmp.createdBy=auth.user?.id
    }
   // return emailTmp
    try {
      await emailTmp.save()
      session.flash({
        "successResMsg": "Data successfuly save",
        "status": 'success'
      })
      return response.redirect('back')
      // return response.status(200).json({
      //   "resMsg": "Data successfuly save",
      //   "status": 'success',
      // })
    } catch (error) {
      session.flash({
        "errorResMsg": "Data not save",
        "status": 'error'
      })
      return response.redirect('back')
      // return response.status(200).json({
      //   "resMsg": "Data not save",
      //   "status": 'error',
      // })
    }
  }
}
