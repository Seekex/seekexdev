import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import BlogMedia from 'App/Models/BlogMedia'
import Blog from 'App/Models/Blog'
const ext = (file)=>{
  return (file = file.substr(1 + file.lastIndexOf("/")).split('?')[0]).split('#')[0].substr(file.lastIndexOf("."))
}
import UploadHelper from 'App/Helpers/UploadHelper'
const uploadHelper = new UploadHelper()
import Application from '@ioc:Adonis/Core/Application'

export default class BlogController {

  public async index({auth,view,request}: HttpContextContract){
    const uid=auth.user?.id
    const page = request.input('page', 1)
    const limit = 20
    const blogs:any= await Blog.query().orderBy('id','desc').paginate(page,limit)                      
    blogs.baseUrl(request.url()) 
    return view.render('admin/blog/index',{blogs})
  }

  public async view({auth,view,request,params,response}: HttpContextContract){
    const uid=auth.user?.id
    const id=params.id
    const page:any= await Blog.query().where({id:id}).first()
    if(page){
      return view.render('admin/blog/view',{page})
    }
    return response.redirect('/admin')
  }
  public async add({auth,view,response}: HttpContextContract){
    const uid=auth.user?.id
    return view.render('admin/blog/add')
  }

  public async changeStatus({request,params}){
    const id=params.id||request.input('id')
    const page:any= await Blog.query().where({id:id}).first()
    page.status=!page.status
    await page.save()
  }
  
  public async save({auth,request,response,session}: HttpContextContract){
    const data = request.all()
    if(data?.id){
      var blog:any = await Blog.find(data?.id)
      blog.title = data?.title
      blog.slug =  data.slug?data?.slug.replace(/\s/g, "-").toLowerCase():blog.slug
      blog.content=data?.content
      blog.seoTitle=data?.seo_title
      blog.seoKeywords=data?.seo_keywords
      blog.seoDescription=data?.seo_description
      blog.status=1
      blog.createdBy=auth.user?.id
    }else{
      var blog:any = new Blog()
      blog.title = data?.title
      // blog.slug= data?.slug?data?.slug.replace(/[^\w\s]/gi, '').replace(/\s/g, "-").toLowerCase():data?.title.replace(/[^\w\s]/gi, '').replace(/\s/g, "-").toLowerCase()
      blog.content=data?.content
      blog.seoTitle=data?.seo_title
      blog.seoKeywords=data?.seo_keywords
      blog.seoDescription=data?.seo_description
      blog.status=1
      blog.createdBy=auth.user?.id
    }
   // return emailTmp
    try {
      const saveblog = await blog.save()
      session.flash({
        "successResMsg": "Data successfuly save",
        "status": 'success'
      })

      return response.redirect().toRoute('Admin/BlogController.view',{id:saveblog?.id})
      // return response.status(200).json({
      //   "resMsg": "Data successfuly save",
      //   "status": 'success',
      // })
    } catch (error) {
      //return error
      session.flash({
        "errorResMsg": "Data not save",
        "status": 'error'
      })
      return response.redirect('back')
      // return response.status(200).json({
      //   "resMsg": "Data not save",
      //   "status": 'error',
      // })
    }
  }

  async uploadedImages({request,response}: HttpContextContract){
    const allImages=await (await BlogMedia.query()).map((elm,indx)=>{
      return {title:'Image-'+(indx+1),value:elm?.url}
    })
    return response.status(200).json(allImages)
  }

  async imageUpload({request,response}: HttpContextContract){
    const blogImg = request.file('file')
    if(blogImg){
      const fileExtName=ext(blogImg.clientName)
      const newFileName = `${new Date().getTime()}${fileExtName}`
      if(fileExtName=='.png' || fileExtName=='.jpg' ||fileExtName=='.jpeg'){
        await blogImg.move(Application.publicPath('blog'),{
          name: newFileName,
        })
        const contentType=fileExtName=='.png'?'image/png':'image/jpeg'
        const uploadedFile=await uploadHelper.uploadFileToFolder(newFileName,contentType,'blog')
        if(uploadedFile){
          const newBlogImg=new BlogMedia()
          newBlogImg.url=uploadedFile?.Location
          await newBlogImg.save()
          return response.status(200).json({status:'success',msg:'Image has been saved.',location:uploadedFile?.Location})
        }else{
          return response.status(200).json({status:'error',msg:'Image not saved, please try again',location:''})
        }
      }
    }
    return response.status(200).json({status:'error',msg:'Image not saved, please try again',location:''})
  }
}
