import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import Feedback from 'App/Models/Feedback'
const AWS = require('aws-sdk');
//const END_POINT_URL=Env.get('AWS_EndpointUrl') as string
import Env from '@ioc:Adonis/Core/Env'
import MasterGodown from 'App/Models/MasterGodown';
const KEY=Env.get('AWS_AccessKey') as string
const SECRET=Env.get('AWS_SecretKey') as string
AWS.config.update({region: 'us-east-1'});
import TaskHelper from 'App/Helpers/TaskHelper'
import Organization from 'App/Models/Organization';
const taskHelper = new TaskHelper()
import Application from '@ioc:Adonis/Core/Application'
import UploadHelper from "App/Helpers/UploadHelper"
const uploadHelper:any=new UploadHelper()
import CommonHelper from 'App/Helpers/CommonHelper'
import { DataExchange } from 'aws-sdk';
const  commonHelper = new CommonHelper()
import Route from '@ioc:Adonis/Core/Route'
const cw = new AWS.CloudWatch({
  apiVersion: '2010-08-01',
  accessKeyId: KEY,
  secretAccessKey: SECRET
})
import { DateTime } from 'luxon'
const http = require('http')
const fs = require('fs')
const ext = (file)=>{
  return (file = file.substr(1 + file.lastIndexOf("/")).split('?')[0]).split('#')[0].substr(file.lastIndexOf("."))
}
export default class DashboardsController {


  public async dashboard({auth,view}: HttpContextContract){
    const user=auth?.user
    var params = {
      Dimensions: [
        {
          Name: 'LogGroupName', /* required */
        },
      ],
      MetricName: 'IncomingLogEvents',
      Namespace: 'AWS/Logs'
    }
    
    // cw.listMetrics(params, function(err, data) {
    //   if (err) {
    //     console.log("Error =>", err);
    //   } else {
    //     console.log("Metrics", JSON.stringify(data.Metrics));
    //   }
    // })
    return view.render('admin/dashboard')
  }

  public async feedBacks({auth,view,request}: HttpContextContract){
    const user=auth?.user
    const page = request.input('page', 1)
    const limit = 20
    const feedbacks = await Feedback.query().preload('user').paginate(page,limit)
    feedbacks.baseUrl(request.url())
    return view.render('admin/feedback',{feedbacks})
  }


  
  public async masterIndex({auth,view,request}: HttpContextContract){
    const page = request.input('page', 1)
    const limit = 20
    const type = request.input('type')
    const s = request.input('s')
    const query = MasterGodown.query().preload('user').preload('parent')
    if(type>0){
      query.where('masterType',type)
    }
    if(s){
      query.where('name','LIKE',"%"+s+"%")
    }
    const datas = await query.orderBy('id','desc').paginate(page,limit)
    //return datas
    const parentTagData = await MasterGodown.query().preload('user').where('parentId',0)
    datas.baseUrl(request.url())
    datas.queryString({'type':type})
    const masterTypes= MasterGodown.allTypes()
    return view.render('admin/master_godown',{datas,masterTypes,parentTagData})
  }

  public async addNewTag({auth,view,request,response}: HttpContextContract){
    const uid:any=auth.user?.id
    const data=request.all()
    const file = request.file('image')
    //return {data,file}
    if(!data.tag){
      return response.status(200).json({type:'error',msg:'Please enter tag'})
    }
    const newMasterGodown = new MasterGodown()
    newMasterGodown.masterType=data?.type||data?.type
    newMasterGodown.name=data?.tag.trim()
    newMasterGodown.createdBy=uid
    newMasterGodown.used=0
    newMasterGodown.parentId=data?.parentId||0
    const newMdata = await newMasterGodown.save()
    if(file){
      const fileName=`${new Date().getTime()}${ext(file.clientName)}`
      const fileExtName=ext(file.clientName)
     // return {fileExtName,fileName}
      if(fileExtName=='.png' || fileExtName=='.jpg' ||fileExtName=='.jpeg'){
        await file.move(Application.publicPath('appimages'),{
          name: fileName,
        })
       const uploadedFile= await commonHelper.localImageCropAndUpload(fileName,fileExtName,newMdata)
      }else{
        return response.status(200).json({type:'error',msg:'Invalid file type'})
      }
     }else{
       
     }
    return response.status(200).json({type:'success'})
  }

  public async editTag({auth,view,request,response}: HttpContextContract){
    const uid:any=auth.user?.id
    const data=request.all()
    const file = request.file('image')
    const id=data?.id
    if(!data.tag){
      return response.status(200).json({type:'error',msg:'Please enter tag'})
    }
    if(id){
      const masterGodown:any = await MasterGodown.find(id)
        masterGodown.masterType=data?.type||data?.type
        masterGodown.name=data?.tag.trim()
        masterGodown.parentId=data?.parentId||0
        const newMdata = await masterGodown.save()
        if(file){
          const fileName=`${new Date().getTime()}${ext(file.clientName)}`
          const fileExtName=ext(file.clientName)
         // return {fileExtName,fileName}
          if(fileExtName=='.png' || fileExtName=='.jpg' ||fileExtName=='.jpeg'){
            await file.move(Application.publicPath('appimages'),{
              name: fileName,
            })
           const uploadedFile = await commonHelper.localImageCropAndUpload(fileName,fileExtName,newMdata)
          }else{
            return response.status(200).json({type:'error',msg:'Invalid file type'})
          }
         }else{
           
         }
        return response.status(200).json({type:'success'})
    }
    
    return data
  }

  async getParentTag({request}:HttpContextContract){
    const type=request.input('type')
    const parentTagData = await MasterGodown.query().preload('user').where({'parentId':0,masterType:type})
    var out='<option value="0" >Select Parent</option>'
    if(parentTagData){
      parentTagData.forEach(element => {
        out+='<option value="'+element?.id+'" >'+element?.name+'</option>'
      });
    }
    return out
  }
  async changeTagStatus({request}:HttpContextContract){
    const data=request.all()
		const masterData:any = await MasterGodown.query().where({'id':data?.id}).first()
		if(masterData.isVerified==1){
			masterData.isVerified=0
		}else{
			masterData.isVerified=1
		}
    if(!masterData.used){
      masterData.used=1
    }
    if(!masterData.parentId){
      masterData.parentId=0
    }

    if(!masterData?.imageUrl && masterData?.masterType==5){
      await taskHelper.freeImageApiForInterestById(masterData?.id)
    }
		await masterData.save()
		return {status:'success'}
  }


  public async orgIndex({auth,view,request}: HttpContextContract){
    const page = request.input('page', 1)
    const limit = 20
    const datas = await Organization.query().preload('user').preload('parent').orderBy('id','desc').paginate(page,limit)
    const parentDatas=await Organization.query().where({parentId:0})
    datas.baseUrl(request.url())
    return view.render('admin/orgnization',{datas,parentDatas})
  }

  public async addNewOrg({auth,view,request,response}: HttpContextContract){
    const uid:any=auth.user?.id
    const data=request.all()
    const file = request.file('image')
    if(!data?.org){
      return response.status(200).json({type:'error',msg:'Please enter Orgnization name'})
    }
    const newOrg:any = new Organization()
    newOrg.name=data?.org||''
    newOrg.parentId=data?.parentId||0
    newOrg.type=data?.type||1
    newOrg.created_by=uid
    newOrg.used=0
    await newOrg.save()
    if(file){
      const fileName=`${new Date().getTime()}${ext(file.clientName)}`
      const fileExtName=ext(file.clientName)
     // return {fileExtName,fileName}
      if(fileExtName=='.png' || fileExtName=='.jpg' ||fileExtName=='.jpeg'){
        await file.move(Application.publicPath('appimages'),{
          name: fileName,
        })
       const uploadedFile= await commonHelper.localImageCropAndUpload(fileName,fileExtName,newOrg)
      }else{
        return response.status(200).json({type:'error',msg:'Invalid file type'})
      }
     }
    return response.status(200).json({type:'success'})
  }

  public async editOrg({auth,view,request,response}: HttpContextContract){
    const uid:any=auth.user?.id
    const data=request.all()
    const file = request.file('image')
    if(!data?.org){
      return response.status(200).json({type:'error',msg:'Please enter Orgnization name'})
    }
    const newOrg:any = await Organization.find(data?.id)
    newOrg.name=data?.org||''
    newOrg.parentId=data?.parentId||0
    newOrg.type=data?.type||1
    newOrg.created_by=uid
    newOrg.used=0
    await newOrg.save()
    if(file){
      const fileName=`${new Date().getTime()}${ext(file.clientName)}`
      const fileExtName=ext(file.clientName)
     // return {fileExtName,fileName}
      if(fileExtName=='.png' || fileExtName=='.jpg' ||fileExtName=='.jpeg'){
        await file.move(Application.publicPath('appimages'),{
          name: fileName,
        })
       const uploadedFile= await commonHelper.localImageCropAndUpload(fileName,fileExtName,newOrg)
      }else{
        return response.status(200).json({type:'error',msg:'Invalid file type'})
      }
     }
    return response.status(200).json({type:'success'})
  }


  async changeOrgStatus({request}:HttpContextContract){
    const data=request.all()
		const orgData:any = await Organization.query().where({'id':data?.id}).first()
		if(orgData.isVerified==1){
			orgData.isVerified=0
		}else{
			orgData.isVerified=1
		}
    if(!orgData.parentId){
      orgData.parentId=0
    }
		await orgData.save()
		return {status:'success'}
  }

  async tagExport({request,params,response,session}:HttpContextContract){
    const data= request.all()
    const allTags = MasterGodown.query().preload('user').preload('parent')
    if(data?.type>0){
      allTags.where('masterType',data?.type)
    }
    const allTagsDatas:any= await allTags.orderBy('id','desc')
    //return allTagsDatas
    if(allTagsDatas){
      var xl = require('excel4node')
      var wb = new xl.Workbook({dateFormat: 'd/m/yyyy hh:mm:ss'})
      var ws = wb.addWorksheet('All Tags')
      ws.cell(1, 1).string('ID')
      ws.cell(1, 2).string('Name')
      ws.cell(1, 3).string('Image_Url')
      ws.cell(1, 4).string('Type')
      ws.cell(1, 5).string('Parent')
      ws.cell(1, 6).string('Verify_Status')
      ws.cell(1, 7).string('Created_By')
      ws.cell(1, 8).string('Created_Date')
      for (const [indx,value] of Object.entries(allTagsDatas)) {
        const row = (parseInt(indx)+2)
        const item:any=value
        //console.log(item)
        ws.cell(row, 1).number(item?.id)
        ws.cell(row, 2).string(item?.name)
        ws.cell(row, 3).string(item?.imageUrl?item?.imageUrl:'NA')
        ws.cell(row, 4).string(item?.tagType)
        ws.cell(row, 5).string(item.parent?item.parent.name:'NA')
        ws.cell(row, 6).bool(item.isVerified?true:false)
        ws.cell(row, 7).string(item.user?item.user.name:'Admin')
        //const cdate= item.created?new Date(item.created).toISOString():'NA'
        const cdate = DateTime.fromISO(new Date(item.created).toISOString(),{setZone:true}).setZone('Asia/Kolkata').toFormat('dd, LLL yyyy t')
        ws.cell(row, 8).date(cdate)
      }
      const nf=`${new Date().getTime()}${'.xlsx'}`
      const file=Application.publicPath('files/')+nf
      const gFile = await new Promise( function(resolve,reject) {
                      wb.write(file, async function(err, stats) {
                        if (err) {
                          await reject(err)
                        } else {
                          await resolve(stats)
                        }
                      })
                  })
      if(gFile){
        return response.attachment(file,nf)
      }
      session.flash({error:'Internal error, please try again.'})
      return response.redirect('back')
    }else{
      session.flash({error:'Internal error, please try again.'})
      return response.redirect('back')
    }
  }

  async importTagData({request,params,response,session}:HttpContextContract){
    const data=request.all()
    const file = request.file('file')
    if(!file){
      session.flash({error:'Please select a file.'})
      return response.redirect('back')
    }
    const Xlsx = require('xlsx')
    const newFileName=`${new Date().getTime()}.${file.extname}`
    let dir     = Application.publicPath('files/imports/')
    await file.move(dir,{
      name: newFileName,
    })
    if(file.hasErrors){
      session.flash({error:'file.errors'})
      return response.redirect('back')
  }else{
    let uploadesFile= Application.publicPath('files/imports/') + newFileName
    var workbook = await Xlsx.readFile(uploadesFile)
    var first_sheet_name = workbook.SheetNames[0];
    var worksheet = workbook.Sheets[first_sheet_name];
    var sheetdata=Xlsx.utils.sheet_to_json(worksheet, {raw:true})
    if(sheetdata.length>0){
      for await (const item of sheetdata) {
        var imgUrl
        if(item?.Image_Url!='NA'){
          const url:any= await commonHelper.imageCropAndUploadByUrl(item?.Image_Url)
          imgUrl=url?.Location
        }
        if(item?.ID){
          const tag:any= await MasterGodown.find(item?.ID)
          tag.imageUrl = imgUrl?imgUrl:tag.imageUrl?tag.imageUrl:''
          tag.parentId= await MasterGodown.getIDByName(item?.Parent)
          tag.name=item?.Name
          tag.masterType=MasterGodown.findType(item?.Type)
          tag.isVerified=item?.Verify_Status
          tag.createdBy=0
          await tag.save()
        }else{
          const tag:any = new MasterGodown()
          tag.imageUrl = imgUrl
          tag.parentId= await MasterGodown.getIDByName(item?.Parent)
          tag.name=item?.Name
          tag.masterType=MasterGodown.findType(item?.Type)
          tag.isVerified=item?.Verify_Status
          tag.createdBy=0
          await tag.save()
        }
      }
    }
    session.flash({success:'Data has been saved'})
    return response.redirect('back')
    return sheetdata
  }
    return file
  }
}
