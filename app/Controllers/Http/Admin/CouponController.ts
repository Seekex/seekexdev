import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import Refund from 'App/Models/Refund'
import CallHistory from 'App/Models/CallHistory'
import RefundHistory from 'App/Models/RefundHistory'
import User from 'App/Models/User'
import Event from '@ioc:Adonis/Core/Event'
import WebServiceLog from 'App/Models/WebServiceLog'
const perf = require('execution-time')()
import LedgerHelper from 'App/Helpers/LedgerHelper'
const Ledger = new LedgerHelper()
import Application from '@ioc:Adonis/Core/Application'
import { DateTime } from 'luxon'
import CouponCode from 'App/Models/CouponCode'
import { schema,rules } from '@ioc:Adonis/Core/Validator'
export default class CouponController {

  public async index({view,request,session,response}: HttpContextContract){
    const s = request.input('s')
    const from =request.input('from')
    const to =request.input('to')
    const type= request.input('type')
    const page = request.input('page', 1)
    const limit = 20
    const couponQuery =  CouponCode.query()
                          .preload('user')
                          .whereHas('user',(q)=>{
                            q.where('regCompleted',1)
                          })
                          
    if(s){
      couponQuery.where(qu=>{
        qu.whereHas('user',(q)=>{
          q.where('name','LIKE',"%"+s+"%")
        }).orWhere('code','LIKE',"%"+s+"%")
      })
    }
    if(type){
      couponQuery.where('couponType',type)
    }
    if(from && to){
      var created_to = new Date(to)
      created_to.setUTCHours(0,0,0,0)
      var created_from = new Date(from)
      created_from.setUTCHours(23,59,0,0)
     // return {created_from,created_to}
     if(created_from.getTime()>created_to.getTime()){
      session.flash({error:'Invalid dates.'})
      return response.redirect('back')
     }
     couponQuery.where(qu=>{
      qu.whereBetween('created',[created_from,created_to])
      .orWhereBetween('fromDate',[created_from,created_to])
      .orWhereBetween('toDate',[created_from,created_to])
     })
    }

    const coupons = await couponQuery
                          //.where({status:1})
                          .orderBy('id','desc')
                          .paginate(page,limit)
    coupons.baseUrl(request.url())
    coupons.queryString({'s':s,'from':from,'to':to})
    // return await CouponCode.createUserTypeCoupon('LZT04E',10)
    return view.render('admin/coupon/index',{coupons})
  }

  public async add({view,params,request,response,auth,session}: HttpContextContract){
    if (request.method()=='POST'){
      const data = request.all()
      const newCoupponSchema = schema.create({
        code: schema.string({}, [
          rules.unique({ table: 'coupon_codes', column: 'code' })
        ])
      })
      try {
        const payload = await request.validate({ 
          schema: newCoupponSchema,
          messages: {
            'code.unique': 'This coupon is already in use.'
          }
        })  
      } catch (error) {
        var errMsg=''
        if(error.messages){
        Object.entries(error.messages).forEach((value:any) => {
          errMsg+=value[1][0].message+'</br>'
        })
        }
        session.flash({errors:errMsg,data:data})
        return response.redirect('back')
      }
      if(data?.fromDate && data?.toDate){
        var created_to = new Date(data?.toDate)
        created_to.setUTCHours(0,0,0,0)
        var created_from = new Date(data?.fromDate)
        created_from.setUTCHours(23,59,0,0)
        if(created_from.getTime()>created_to.getTime()){
          session.flash({errors:'Invalid dates,please correct dates.',data:data})
          return response.redirect('back')
        }
      }
      const coupon:any = new CouponCode()
      coupon.code=data?.code
      coupon.amount=data?.amount
      coupon.maxUses=data?.maxUses
      coupon.fromDate=data?.fromDate
      coupon.toDate=data?.toDate
      coupon.couponType=2
      coupon.status=1
      coupon.uid=auth.user?.id
      try {
        await coupon.save()
        session.flash({success:'Coupon successfuly created'})
        return response.redirect('back')
      } catch (error) {
        session.flash({error:'Internal error, please try again',data:data})
        return response.redirect('back')
      }
    }
    const newCoupon = await CouponCode.newCode()
    return view.render('admin/coupon/add',{newCoupon})
  }

  public async couponHistory({view,request,session,response,params}: HttpContextContract){
      const code = params.code
      const coupon = await CouponCode.query()
                                      .preload('user')
                                      .preload('usedCoupon')
                                      .withCount('usedCoupon',(q)=>{
                                        q.whereNotColumn('uid','=','refer_by')
                                      })
                                      .where('code',code).first()
      //return coupon                                
      return view.render('admin/coupon/view',{coupon})
  }

  public async changeStatus({request}:HttpContextContract){
    const data=request.all()
		const coupon:any = await CouponCode.query().where({'id':data.id}).first()
		if(coupon.status==1){
			coupon.status=0
		}else{
			coupon.status=1
		}
		await coupon.save()
		return {status:'success'}
  }

  async couponExport({session,response,request,params}:HttpContextContract){
    const s = request.input('s')
    const from =request.input('from')
    const to =request.input('to')
    const type= request.input('type')
    const couponQuery =  CouponCode.query()
                          .preload('user')
                          .whereHas('user',(q)=>{
                            q.where('regCompleted',1)
                          })
                          
    if(s){
      couponQuery.where(qu=>{
        qu.whereHas('user',(q)=>{
          q.where('name','LIKE',"%"+s+"%")
        }).orWhere('code','LIKE',"%"+s+"%")
      })
    }
    if(type){
      couponQuery.where('couponType',type)
    }
    if(from && to){
      var created_to = new Date(to)
      created_to.setUTCHours(0,0,0,0)
      var created_from = new Date(from)
      created_from.setUTCHours(23,59,0,0)
     // return {created_from,created_to}
     if(created_from.getTime()>created_to.getTime()){
      session.flash({error:'Invalid dates.'})
      return response.redirect('back')
     }
     couponQuery.where(qu=>{
      qu.whereBetween('created',[created_from,created_to])
      .orWhereBetween('fromDate',[created_from,created_to])
      .orWhereBetween('toDate',[created_from,created_to])
     })
    }
    const coupons = await couponQuery
                          .orderBy('id','desc')

      var xl = require('excel4node')
      var wb = new xl.Workbook({dateFormat: 'dd/mm/yyyy'})
      var ws = wb.addWorksheet('All Coupon Data')
      ws.cell(1, 1).string('Coupon Code')
      ws.cell(1, 2).string('Created By')
      ws.cell(1, 3).string('Amout')
      ws.cell(1, 4).string('Max Uses')
      ws.cell(1, 5).string('Valid From')
      ws.cell(1, 6).string('Valid To')
      ws.cell(1, 7).string('Coupon Type')
      ws.cell(1, 8).string('Status')
      ws.cell(1, 9).string('Created')

      for (const [indx,value] of Object.entries(coupons)) {
        const row = (parseInt(indx)+2)
        const item:any=value
        ws.cell(row, 1).string(''+item.code)
        ws.cell(row, 2).string(item?.user.name)
        ws.cell(row, 3).number(item?.amount||0)
        ws.cell(row, 4).number(item?.maxUses||0)
        const fDate = DateTime.fromISO(new Date(item.fromDate).toISOString(),{setZone:true}).setZone('Asia/Kolkata').toFormat('dd, LLL yyyy t')
        ws.cell(row, 5).date(fDate)
        const tDate = DateTime.fromISO(new Date(item.toDate).toISOString(),{setZone:true}).setZone('Asia/Kolkata').toFormat('dd, LLL yyyy t')
        ws.cell(row, 6).date(tDate)
        ws.cell(row, 7).string(item.couponCreatedType)
        const status=item.status==1?'Active':'Inactive'
        ws.cell(row, 8).string(status)
        const cdate = DateTime.fromISO(new Date(item.created).toISOString(),{setZone:true}).setZone('Asia/Kolkata').toFormat('dd, LLL yyyy t')
        ws.cell(row, 9).date(cdate)
      }
  
    const nf=`${new Date().getTime()}${'.xlsx'}`
    const file=Application.publicPath('files/')+nf
    const gFile = await new Promise( function(resolve,reject) {
                      wb.write(file, async function(err, stats) {
                        if (err) {
                          await reject(err)
                        } else {
                          await resolve(stats)
                        }
                      })
                  })
    if(gFile){
      return response.attachment(file,'coupon_data.xlsx')
    }
    session.flash({error:'Internal error, please try again.'})
    return response.redirect('back')
  }

  async getSystemCoupons({request,response}:HttpContextContract){
    const data=request.all()
    const code=data?.query
      const coupons =  await CouponCode.getSystemTypeCoupons(code)
      return response.status(200).json({query:code,suggestions:coupons})
  }

}