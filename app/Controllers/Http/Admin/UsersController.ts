import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import LedgerAccount from 'App/Models/LedgerAccount'
import TransactionHistory from 'App/Models/TransactionHistory'
import User from 'App/Models/User'
import UserApprovalHistory from 'App/Models/UserApprovalHistory'
const transTypes = {0:'NA',1:'User/Expert',2:'Razorpay',3:'Seekex',4:'Sgst',5:'Cgst',6:'Igst',7:'Commission',8:'Exotel',9:'Promotion',10:'Refund'}
const perf = require('execution-time')()
import Database from '@ioc:Adonis/Lucid/Database'
import WebServiceLog from 'App/Models/WebServiceLog'
import NotificationHelper from 'App/Helpers/NotificationHelper'
const notificationHelper = new NotificationHelper()
import CommonHelper from 'App/Helpers/CommonHelper'
import Event from '@ioc:Adonis/Core/Event'
import CallHistory from 'App/Models/CallHistory'
const commonHelper = new CommonHelper()
import { DateTime } from 'luxon'
import MasterGodown from 'App/Models/MasterGodown'
import CountryState from 'App/Models/CountryState'
import Application from '@ioc:Adonis/Core/Application'
import UserCountryState from 'App/Models/UserCountryState'
import UserMasterGodown from 'App/Models/UserMasterGodown'
import WithoutRegUser from 'App/Models/WithoutRegUser'
import Organization from 'App/Models/Organization'
import Post from 'App/Models/Post'
import YoutubeVideo from 'App/Models/YoutubeVideo'
import EmailHelper from 'App/Helpers/EmailHelper'
const emailHelper = new EmailHelper()
import SearchHelper from 'App/Helpers/SearchHelper'
import ReferralHistory from 'App/Models/ReferralHistory'
const searchHelper = new SearchHelper()
import LedgerHelper from 'App/Helpers/LedgerHelper'
const ledgerHelper = new LedgerHelper()

export default class UsersController {

  public async index({view,request}:HttpContextContract){
    const page = request.input('page', 1)
    const limit = 20
    const s = request.input('s')
    const userQuery = User.query()
    if(s){
      userQuery.where(q=>{
        q.where('name','LIKE',"%"+s+"%").orWhere('email','LIKE',"%"+s+"%").orWhere('mobile','LIKE',"%"+s+"%")
      })
    }
    const users=await userQuery.whereNot({'user_role':'admin',expert:1}).where({requested:0}).orderBy('id','desc').paginate(page,limit)
    users.baseUrl(request.url())
    return view.render('admin/user/index',{users})
  }
  public async expertIndex({view,request}:HttpContextContract){
    const page = request.input('page', 1)
    const limit = 20
    const s = request.input('s')
    const userQuery = User.query()
    if(s){
      userQuery.where(q=>{
        q.where('name','LIKE',"%"+s+"%").orWhere('email','LIKE',"%"+s+"%").orWhere('mobile','LIKE',"%"+s+"%")
      })
    }
    const users=await userQuery.whereNot({'user_role':'admin'}).where({expert:1,verified:1}).orderBy('id','desc').paginate(page,limit)
    users.baseUrl(request.url())
    return view.render('admin/user/index',{users})
  }

  public async pendingExpert({view,request}:HttpContextContract){
    const page = request.input('page', 1)
    const limit = 20
    const users=await User.query().where({requested:1,expert:1,verified:0}).whereNot({'user_role':'admin'}).orderBy('updated','desc').paginate(page,limit)
    users.baseUrl(request.url())
    return view.render('admin/user/index',{users})
  }

  async userDetail({view,params}){
    const id=params.id
    var user:any= await User.query()
                      // .preload('accomplishmentsList',(query)=>{
                      //   query.preload('organization')
                      // })
                      // .preload('certificatesList',(query)=>{
                      //   query.preload('organization')
                      // })
                      // .preload('projectList',(query)=>{
                      //   query.preload('organization')
                      // })
                      .preload('accomplishmentsList')
                      .preload('certificatesList')
                      .preload('projectList')
                      .preload('country')
                      .preload('state')
                      .preload('highestEducation')
                      .preload('collegeUniversity')
                      .preload('occupation')
                      .preload('currentOrg')
                      .preload('skillsList')
                      .preload('intExpertiseList')
                      .where({'id':id}).first()
   // return user
    const postCount = await Post.query().where('createdBy',id)
                    .pojo<{ count: number}>()
                    .count('id as count').first()
    const callCount = await CallHistory.query().where((query)=>{
                      //query.where("duration",'>',1).whereNotNull('duration')
                    }).where((query)=>{
                      query.where({'receiver':id}).orWhere({'initiater':id})
                    })
                    .pojo<{ count: number}>()
                    .count('* as count').first()
    var videos:any
    if(id){
      videos = await YoutubeVideo.query().where({'uid':id})
    }else{
      videos=[]
    }
    return view.render('admin/user/view',{user,postCount:postCount?.count,callCount:callCount?.count,videos:videos})
  }

  async userEdit({view,params}:HttpContextContract){
    const id=params.id
    var user:any= await User.query()
                      // .preload('accomplishmentsList')
                      // .preload('certificatesList')
                      // .preload('projectList')
                      .preload('country')
                      .preload('state')
                      .preload('highestEducation')
                      .preload('collegeUniversity')
                      .preload('occupation')
                      .preload('currentOrg')
                      .preload('skillsList')
                      .preload('intExpertiseList')
                      .where({'id':id}).first()
      user=user.serialize()
      // return user
      user.skillsList=await(user.skillsList).map(elm=>{
        return elm?.name
      })
      user.intExpertiseList=await(user.intExpertiseList).map(elm=>{
        return elm?.name
      })            
    const countryList = await CountryState.getCountriesList()
   //return countryList
    return view.render('admin/user/edit',{user,countryList})
  }

  async userEditPassword({view,params}:HttpContextContract){
    const id=params.id
    var user:any= await User.query()
                      .where({'id':id}).first()
    user=user.serialize()
    return view.render('admin/user/edit_password',{user})
  }

  async userSave({request,params,session,response}:HttpContextContract){
    const id = params.id
    const data = request.all()
    //return data
    var user:any= await User.query().where({'id':id}).first()
    if(user){
      user.name = data?.name
      user.dob = data?.dob
      const age=data?.dob?new Date(Date.now()-new Date(data?.dob).getTime()).getUTCFullYear()-1970:0
      user.age=age
      // if(data?.password){
      //   user.password = data?.password
      // }
      user.audioCallRate=data?.audioCallRate||0
      user.videoCallRate=data?.videoCallRate||0
      user.audioCallRatePerSec=await commonHelper.showCallAmount(data?.audioCallRate,false,'VOIP')||0
      user.videoCallRatePerSec=await commonHelper.showCallAmount(data?.videoCallRate,true,'VOIP')||0
      user.facebook=data?.facebook
      user.instagram=data?.instagram
      user.linkedin=data?.linkedin
      user.chanalUrl=data?.chanalUrl
      user.aboutMe=data?.aboutMe
      var masterGodowns:any=[]
      if(data?.occupation){
        const Occupation:any = await MasterGodown.getIDByName(data?.occupation)
        if(Occupation){
          await MasterGodown.query().where('id',Occupation).increment('used',1)
          masterGodowns.push({masterGodownId:Occupation,masterType:1,masterFor:user?.id})
        }
      }
      if(data?.currentOrg){
          const existOrg:any = await Organization.getIDByName(data?.currentOrg)
          if(existOrg){
            await Organization.query().where('id',existOrg).increment('used',1)
            masterGodowns.push({masterGodownId:existOrg,masterType:7,masterFor:user?.id})
          }
      }
      if(data?.highestEducation){
        const Highest_Education:any = await MasterGodown.getIDByName(data?.highestEducation)
        if(Highest_Education){
          await MasterGodown.query().where('id',Highest_Education).increment('used',1)
          masterGodowns.push({masterGodownId:Highest_Education,masterType:4,masterFor:user?.id})
        }
      }
      if(data?.collegeUniversity){
        const existOrg:any = await Organization.getIDByName(data?.collegeUniversity)
        if(existOrg){
          await Organization.query().where('id',existOrg).increment('used',1)
          masterGodowns.push({masterGodownId:existOrg,masterType:9,masterFor:user?.id})
        }
      }
      if(data?.skills){
        const skillsData = data?.skills.split(',')
        for await (const item of skillsData) {
          const existTag= await MasterGodown.getIDByName(item)
          if(existTag){
            await MasterGodown.query().where('id',existTag).increment('used',1)
            masterGodowns.push({masterGodownId:existTag,masterType:3,masterFor:user?.id})
          }else{
            const newMasterGodown:any= new MasterGodown();
              newMasterGodown.imageUrl=''
              newMasterGodown.masterType=3
              newMasterGodown.name=item.trim()
              newMasterGodown.createdBy=user?.id
              newMasterGodown.used=1
            var newMdata = await newMasterGodown.save()
            masterGodowns.push({masterGodownId:newMdata?.id,masterType:3,masterFor:user?.id})
          }
        }
      }
      if(data?.expertises){
        const expertisesData = data?.expertises.split(',')
        for await (const item of expertisesData) {
          const existTag= await MasterGodown.getIDByName(item)
          if(existTag){
            await MasterGodown.query().where('id',existTag).increment('used',1)
            masterGodowns.push({masterGodownId:existTag,masterType:5,masterFor:user?.id})
          }else{
            const newMasterGodown:any= new MasterGodown();
              newMasterGodown.imageUrl=''
              newMasterGodown.masterType=5
              newMasterGodown.name=item.trim()
              newMasterGodown.createdBy=user?.id
              newMasterGodown.used=1
            var newMdata = await newMasterGodown.save()
            masterGodowns.push({masterGodownId:newMdata?.id,masterType:5,masterFor:user?.id})
          }
        }
      }
      try {
        await user.save()
        if(masterGodowns.length>0){
          await UserMasterGodown.query().where({'masterFor':user?.id}).delete()
          await UserMasterGodown.createMany(masterGodowns)
        }
        // if(data?.password){
        //   await emailHelper.sendUserPasswordEmail({email:user?.email,name:user?.name,password:data?.password})
        // }
        session.flash({success:'User has been saved.'})
        return response.redirect('/admin/expert-list')
      } catch (error) {
        session.flash({error:'User has been not saved, please try again.'})
        return response.redirect('/admin/expert-list')
      }
    }
    session.flash({error:'User has been not saved, please try again.'})
    return response.redirect('/admin/expert-list')
   // return user
  }
 async savePassword({request,params,session,response}:HttpContextContract){
    const id = params.id
    const data = request.all()
    //return data
    var user:any= await User.query().where({'id':id}).first()
    if(user){
        if(data?.password){
          user.password = data?.password
        }
        if(data?.phone_password){
          user.password = data?.phone_password
        }
      try {
        await user.save()
        if(data?.password){
          await emailHelper.sendUserPasswordEmail({email:user?.email,name:user?.name,password:data?.password})
        }
        if(data?.phone_password){
          await commonHelper.sendPasswordOnMobile(user?.mobile,data?.phone_password)
        }
        session.flash({success:'User password has been changed.'})
        return response.redirect('/admin/expert-list')
      } catch (error) {
        session.flash({error:'User password has been not changed, please try again.'})
        return response.redirect('/admin/expert-list')
      }
    }
    session.flash({error:'User password has been not changed, please try again.'})
    return response.redirect('/admin/expert-list')
   // return user
  }
  async changeUserStatus({request}:HttpContextContract){
    const data=request.all()
		const user:any = await User.query().where({'id':data.id}).first()
		if(user.status==1){
			user.status=0
		}else{
			user.status=1
		}
		await user.save()
		return {status:'success'}
  }

  async sendVerifyEmail({params}){
    const id=params.id
    const user:any=await User.find(id)
    if(user?.email){
      const otp=await commonHelper.sendEmailOtp(user?.email);
      return {status:'success',msg:'A varification email send to user'}
    }else{
      return {status:'error',msg:'An error, please try again.'}
    }
  }

  async sendVerifySms({params}){
    const id=params.id

    return {status:'success'}
  }

  async approveExpert({request,auth}:HttpContextContract){
    const data=request.all()
    const expert:any = await User.query().where({'id':data.id}).first()
    expert.verified=data.status
    expert.expert=1
    const app_history:any=new UserApprovalHistory()
    app_history.uid=data.id
    app_history.approveBy=auth.user?.id
    app_history.comment=data.comment
    app_history.status=data.status==1?'Approved':data.status==2?'Unapproved':'Other Action'
    try {
      await expert.save()
      await app_history.save()
      searchHelper.addData({
        name:expert?.name,
        title:expert?.name,
        type:'expert',
        dataId:expert?.id,
        userId:expert?.id
      })
      if(data.status==1){
        var notifyData={uid:expert?.id}
        await notificationHelper.sendNotificationForApprovedExpert(notifyData)
        const allData={
          expertName:expert?.name,
          expertEmail:expert?.email
        }
        Event.emit('new:expertApprovalEmail',allData)
      }
      return {status:'success'}
    } catch (error) {
      return {status:'error',error:error}
    }
  }
  async visibleExpert({request,auth}:HttpContextContract){
    const data=request.all()
    const expert:any = await User.query().where({'id':data.id}).first()
    if(expert.isVisible==0){
      expert.isVisible=1
    }else{
      expert.isVisible=0
    }
    const app_history:any=new UserApprovalHistory()
    app_history.uid=data.id
    app_history.approveBy=auth.user?.id
    app_history.comment='Mark expert visible'
    app_history.status='Make expert visible'
    try {
      await expert.save()
      await app_history.save()
      return {status:'success'}
    } catch (error) {
      return {status:'error',error:error}
    }
  }


  async featuredExpert({request}:HttpContextContract){
    const data=request.all()
    const expert:any = await User.query().where({'id':data.id}).first()
    if(expert.featured==1){
      expert.featured=false
    }else{
      expert.featured=true
    }
    try {
      await expert.save()
      return {status:'success'}
    } catch (error) {
      return {status:'error',error:error}
    }
  }


  async reportedUsers({view,request}:HttpContextContract){
    const page = request.input('page', 1)
    const limit = 20
    const users:any=await User.query().preload('reported').has('reported', '>', 0).withCount('reported').orderBy('id','desc').paginate(page,limit)
    users.baseUrl(request.url())
   // const posts:any=await Post.query().preload('user').has('reported', '>', 0).orderBy('id','desc').withCount('reported').paginate(page,limit)
    return view.render('admin/user/reported',{users})
  }

  async reportedUserDetail({view,params}){
    const id=params.id
    var user:any= await User.query()
                      .preload('reported',(query)=>{
                        query.preload('reportedUser',(qu)=>{
                          qu.select('id','name')
                        })
                      })
                      .where({'id':id}).first()
       // return user              
      return view.render('admin/user/reported_view',{user})                
  }

  async userWallet({view,params,response,request}:HttpContextContract){
    const id=params.id
    const page = request.input('page', 1)
    const limit = 20
    var userWallet:any=  await LedgerAccount.query().preload('user').where({'uid':id,name:'User'}).first()
    if(userWallet){
      var transections:any= await TransactionHistory.query().where({toWallet:userWallet?.id,uid:id}).orWhere({fromWallet:userWallet?.id,uid:id}).orderBy('id','desc').paginate(page,limit) //,transactionTo:'User'

      //const transections = await TransactionHistory.query().where({uid:id,transactionTo:'User'}).orderBy('created','desc').paginate(page,limit)
      transections.baseUrl(request.url())
      //return transections
      userWallet=userWallet.serialize()
      return view.render('admin/user/wallet',{userWallet,transections})
    }
    return response.redirect('back')
     
  }

  async addAmountInWallet({params,response,request}:HttpContextContract){
    perf.start()
    const uid=params.id
    const data = request.all()
    //return Number(parseFloat(data.amount))
    if(!Number(parseFloat(data.amount))){
      return response.status(201).json({status:'error',msg:'Not a valid amount'})
    }
    if(!data.description.trim()){
      data.description=data.amount+' Added by admin for promotion.'
    }
    var userWallet:any=  await LedgerAccount.query().where({'uid':uid,name:'User'}).first()
   // return userWallet

    const promotionWallet:any = await LedgerAccount.query().where({name:'Promotion'}).first()
    try {
      const trns_id= await Database.transaction(async (trx) => {
        const addToUserWallet=new TransactionHistory()
        addToUserWallet.callId=0
        addToUserWallet.description=data.amount + ' Add to user wallet ' + data.description
        addToUserWallet.fromWallet=promotionWallet?.id
        addToUserWallet.toWallet=userWallet?.id
        addToUserWallet.isdebit=false
        addToUserWallet.status=1
        addToUserWallet.transactionAmount=data.amount
        addToUserWallet.transactionType=9
        addToUserWallet.transactionWalletId=userWallet?.id
        addToUserWallet.transactionTo='User'
        addToUserWallet.transactionAction='plus'
        addToUserWallet.uid=uid
        addToUserWallet.payout=false
        addToUserWallet.useTransaction(trx)
        await addToUserWallet.save()
        userWallet.promoAmount=userWallet.promoAmount+parseFloat(data.amount)
        userWallet.totalAmount=userWallet.totalAmount+parseFloat(data.amount)
        userWallet.useTransaction(trx)
        await userWallet.save()

        const deductedToPromotionWallet=new TransactionHistory()
        deductedToPromotionWallet.callId=0
        deductedToPromotionWallet.description=data.amount+' Amount deducted by admin and add to user for promotion userId=> '+uid
        deductedToPromotionWallet.fromWallet=promotionWallet?.id
        deductedToPromotionWallet.toWallet=userWallet?.id
        deductedToPromotionWallet.isdebit=true
        deductedToPromotionWallet.status=1
        deductedToPromotionWallet.transactionAmount=data.amount
        deductedToPromotionWallet.transactionType=9
        deductedToPromotionWallet.transactionWalletId=promotionWallet?.id
        deductedToPromotionWallet.transactionTo='User'
        deductedToPromotionWallet.transactionAction='minus'
        deductedToPromotionWallet.uid=promotionWallet?.uid
        deductedToPromotionWallet.payout=false
        deductedToPromotionWallet.useTransaction(trx)
        await deductedToPromotionWallet.save()

        promotionWallet.availableAmount=promotionWallet.availableAmount-parseFloat(data.amount)
        promotionWallet.totalAmount=promotionWallet.totalAmount-parseFloat(data.amount)
        promotionWallet.useTransaction(trx)
        await promotionWallet.save()
        return addToUserWallet?.id
      })
      return response.status(200).json({status:'success',msg:'Amount successfuly added',trns_id:trns_id})
    } catch (error) {
      const results = perf.stop()
      await WebServiceLog.create({request:JSON.stringify(data),response:JSON.stringify(error),service_name:'In Admin Add money in user wallet',status:2,uid:uid,time_taken:results.time})
      return response.status(201).json({status:'error',msg:'Internal error, amount not added'})
    }
  }

  async callHistory({params,response,request,view}:HttpContextContract){
    perf.start()
    const uid=params.id
    const data = request.all()
    const page = data?.page||1
		 const limit = 20
     const userTimeZone='Asia/Kolkata'
    var historyQuery =  CallHistory.query()
				  //.select('callTime','duration','initiater','receiver','requestId','video','id','callingType')
				  .preload('expert').preload('user').has('user').has('expert')
        historyQuery.where((query)=>{
          query.where({"receiver":uid}).orWhere({'initiater':uid})
        })
        
			 var callsHistory:any= await historyQuery.orderBy('id','desc').paginate(page,limit)
       var newcallsHistoryData:any
			 callsHistory.baseUrl(request.url())
			 if(callsHistory){
        newcallsHistoryData = callsHistory?.rows
			 }
			 newcallsHistoryData = await Promise.all(newcallsHistoryData.map(async(call)=>{
														var dt:any={}
														call.duration = Math.floor(call?.duration||0)
														// const timeZone=DateTime.local(call.callTime).setZone(userTimeZone).toFormat('dd, LLL yyyy t')
														// const localTime = DateTime.local(call.callTime)
													//	call.callTime=timeZone
														call.callTime=DateTime.fromISO(new Date(call.callTime).toISOString(),{setZone:true}).setZone(userTimeZone).toFormat('dd, LLL yyyy t')
													//	dt.callHistory=call
														dt.callHistory={
															"callTime":call?.callTime,
															"duration": await commonHelper.durationFormat(call?.duration),
															"initiater": call?.initiater,
															"receiver": call?.receiver,
															"requestId": call?.requestId,
															"video": call?.video?true:false,
															"id": call?.id,
															"callingType": call?.callingType,
															"name": call?.name,
															"profileImage": call?.profileImage,
															"expertCallBackTime": call?.expertCallBackTime,
														}
                            dt.call=call

														if(call.duration==0 || call.duration==null){
															dt.type=3
														}else if(call.initiater==uid){
															dt.type=1
														}else if(call.receiver==uid){
															dt.type=2
														}

														if(call?.initiater==uid){
															dt.name= call?.name||''
															dt.profilePic=call?.profileImage||call?.profilePic||''
															dt.uid=call?.receiver
															dt.verified=call?.expert?.verified?true:false
															const rat = await commonHelper.callRating(call?.id)
															dt.rating=parseFloat((rat?.rating).toFixed(1))||0
															dt.ratingCount=rat?.count||0
														}else{
															dt.name= call?.user.name||''
															dt.profilePic=call?.user?.profileImage||call?.user?.profilePic||''
															dt.uid=call?.initiater
															dt.verified=call?.user?.verified?true:false
															const rat = await commonHelper.callRating(call?.id)
															dt.rating=parseFloat((rat?.rating).toFixed(1))||0
															dt.ratingCount=rat?.count||0
														}
														
														if(call?.callingType=='Exotel'){
															dt.isExotel=true
														}else{
															dt.isExotel=false
														}
														delete dt.callHistory['user']
														delete dt.callHistory.expert
														return dt
												}))
    return view.render('admin/user/call_history',{callsHistory,newcallsHistoryData})
  }

  async expertSample({response,session}:HttpContextContract){
    var xl = require('excel4node')
    var wb = new xl.Workbook({dateFormat: 'dd/mm/yyyy'})
    var ws = wb.addWorksheet('Create Expert')
    var ws1 = wb.addWorksheet('Lists')
    const occupations = await MasterGodown.getOccupations()
    const skills = await MasterGodown.getSkills()
    const expertise = await MasterGodown.getExpertises()
    const educations = await MasterGodown.getEducations()
    const countries = await CountryState.getCountries()
    const companies = await Organization.getCompanies()
    const colleges = await Organization.getColleges()
    const genders=['Male','Female','Other']
    //return {occupations,skills,expertise,educations,countries}
  //  return {companies,colleges}

    for (const [indx,value] of Object.entries(occupations)) {
      //A
      const row = (parseInt(indx)+1)
      const item:any=value
      ws1.cell(row, 1).string(item)
    }
    for (const [indx,value] of Object.entries(skills)) {
      //B
      const row = (parseInt(indx)+1)
      const item:any=value
      ws1.cell(row, 2).string(item)
    }
    for (const [indx,value] of Object.entries(expertise)) {
      //C
      const row = (parseInt(indx)+1)
      const item:any=value
      ws1.cell(row, 3).string(item)
    }
    for (const [indx,value] of Object.entries(educations)) {
      //D
      const row = (parseInt(indx)+1)
      const item:any=value
      ws1.cell(row, 4).string(item)
    }
    for (const [indx,value] of Object.entries(countries)) {
      //E
      const row = (parseInt(indx)+1)
      const item:any=value
      ws1.cell(row, 5).string(item)
    }
    for (const [indx,value] of Object.entries(genders)) {
      //F
      const row = (parseInt(indx)+1)
      const item:any=value
      ws1.cell(row, 6).string(item)
    }
    
    for (const [indx,value] of Object.entries(companies)) {
      //G
      const row = (parseInt(indx)+1)
      const item:any=value
      ws1.cell(row, 7).string(item)
    }
    for (const [indx,value] of Object.entries(colleges)) {
      //H
      const row = (parseInt(indx)+1)
      const item:any=value
      ws1.cell(row, 8).string(item)
    }

    ws.cell(1, 1).string('Name')
    ws.cell(1, 2).string('DOB')
    ws.cell(1, 3).string('Gender')
    ws.cell(1, 4).string('Email')
    ws.cell(1, 5).string('Phone')
    ws.cell(1, 6).string('Occupation')
    ws.cell(1, 7).string('Current_Company')
    ws.cell(1, 8).string('Skill_1')
    ws.cell(1, 9).string('Skill_2')
    ws.cell(1, 10).string('Skill_3')
    ws.cell(1, 11).string('Expertise_1')
    ws.cell(1, 12).string('Expertise_2')
    ws.cell(1, 13).string('Expertise_3')
    ws.cell(1, 14).string('Highest_Education')
    ws.cell(1, 15).string('College_University')
    ws.cell(1, 16).string('Profile_Image')
    ws.cell(1, 17).string('About_Me')
    ws.cell(1, 18).string('Country')
    ws.cell(1, 19).string('Audio_Callrate')
    ws.cell(1, 20).string('Video_Callrate')

    ws.cell(2, 1).string('')
    const cdate = DateTime.fromISO(new Date('01/01/2001').toISOString(),{setZone:true}).setZone('Asia/Kolkata').toFormat('dd, LLL yyyy ')
    ws.cell(2, 2).date(cdate).
    ws.addDataValidation({
      type: 'list',
      allowBlank: true,
      prompt: 'Choose from dropdown',
      error: 'Invalid choice was chosen',
      showDropDown: true,
      sqref:'C2:C1000',
      formulas: ['=Lists!$F$1:$F$'+genders?.length]
    })
   // ws.cell(2, 3).string('')
    ws.cell(2, 4).string('')
    ws.cell(2, 5).string('')

    ws.addDataValidation({
      type: 'list',
      allowBlank: true,
      prompt: 'Choose from dropdown',
      error: 'Invalid choice was chosen',
      showDropDown: true,
      sqref:'F2:F1000',
      formulas: ['=Lists!$A$1:$A$'+occupations?.length]
    })
    ws.addDataValidation({
      type: 'list',
      allowBlank: true,
      prompt: 'Choose from dropdown',
      error: 'Invalid choice was chosen',
      showDropDown: true,
      sqref:'G2:G1000',
      formulas: ['=Lists!$G$1:$G$'+companies?.length]
    })
    //Skills
    ws.addDataValidation({
      type: 'list',
      allowBlank: true,
      prompt: 'Choose from dropdown',
      error: 'Invalid choice was chosen',
      showDropDown: true,
      sqref:'H2:H1000',
      formulas: ['=Lists!$B$1:$B$'+skills?.length]
    })
    ws.addDataValidation({
      type: 'list',
      allowBlank: true,
      prompt: 'Choose from dropdown',
      error: 'Invalid choice was chosen',
      showDropDown: true,
      sqref:'I2:I1000',
      formulas: ['=Lists!$B$1:$B$'+skills?.length]
    })
    ws.addDataValidation({
      type: 'list',
      allowBlank: true,
      prompt: 'Choose from dropdown',
      error: 'Invalid choice was chosen',
      showDropDown: true,
      sqref:'J2:J1000',
      formulas: ['=Lists!$B$1:$B$'+skills?.length]
    })
    //Expertise
    ws.addDataValidation({
      type: 'list',
      allowBlank: true,
      prompt: 'Choose from dropdown',
      error: 'Invalid choice was chosen',
      showDropDown: true,
      sqref:'K2:K1000',
      formulas: ['=Lists!$C$1:$C$'+expertise?.length]
    })
    ws.addDataValidation({
      type: 'list',
      allowBlank: true,
      prompt: 'Choose from dropdown',
      error: 'Invalid choice was chosen',
      showDropDown: true,
      sqref:'L2:L1000',
      formulas: ['=Lists!$C$1:$C$'+expertise?.length]
    })
    ws.addDataValidation({
      type: 'list',
      allowBlank: true,
      prompt: 'Choose from dropdown',
      error: 'Invalid choice was chosen',
      showDropDown: true,
      sqref:'M2:M1000',
      formulas: ['=Lists!$C$1:$C$'+expertise?.length]
    })
    //Education
    ws.addDataValidation({
      type: 'list',
      allowBlank: true,
      prompt: 'Choose from dropdown',
      error: 'Invalid choice was chosen',
      showDropDown: true,
      sqref:'N2:N1000',
      formulas: ['=Lists!$D$1:$D$'+educations?.length]
    })
    ws.addDataValidation({
      type: 'list',
      allowBlank: true,
      prompt: 'Choose from dropdown',
      error: 'Invalid choice was chosen',
      showDropDown: true,
      sqref:'O2:O1000',
      formulas: ['=Lists!$H$1:$H$'+colleges?.length]
    })
    ws.cell(2, 16).string('')
    ws.cell(2, 17).string('')
    ws.addDataValidation({
      type: 'list',
      allowBlank: true,
      prompt: 'Choose from dropdown',
      error: 'Invalid choice was chosen',
      showDropDown: true,
      sqref:'R2:R1000',
      formulas: ['=Lists!$E$1:$E$'+countries?.length]
    })
    ws.cell(2, 19).number(0.0)
    ws.cell(2, 20).number(0.0)

    const nf=`${new Date().getTime()}${'.xlsx'}`
    const file=Application.publicPath('files/')+nf
    const gFile = await new Promise( function(resolve,reject) {
                      wb.write(file, async function(err, stats) {
                        if (err) {
                          await reject(err)
                        } else {
                          await resolve(stats)
                        }
                      })
                  })
    if(gFile){
      return response.attachment(file,'expert_sample.xlsx')
    }
    session.flash({error:'Internal error, please try again.'})
    return response.redirect('back')
    //ws.cell(1, 16).string('State')
  }

  async expertImport({request,response,session}:HttpContextContract){
    const data=request.all()
    const file = request.file('file')
    if(!file){
      session.flash({error:'Please select a file.'})
      return response.redirect('back')
    }
    const Xlsx = require('xlsx')
    const newFileName=`${new Date().getTime()}.${file.extname}`
    let dir     = Application.publicPath('files/imports/')
    await file.move(dir,{
      name: newFileName,
    })
    if(file.hasErrors){
      session.flash({error:'file.errors'})
      return response.redirect('back')
    }else{
      let uploadesFile= Application.publicPath('files/imports/') + newFileName
      var workbook = await Xlsx.readFile(uploadesFile,{cellDates: true})
      var first_sheet_name = workbook.SheetNames[0];
      var worksheet = workbook.Sheets[first_sheet_name];
      var sheetdata=Xlsx.utils.sheet_to_json(worksheet, {raw:true})
     // return sheetdata
     var allUser:any=[]
      if(sheetdata.length>0){
        for await (const item of sheetdata) {
          const existUser = await User.query().where('email',item?.Email).first()
          if(existUser){
           // var regData:any=existUser
           session.flash({error:'This user already saved => '+item?.Email})
            continue
          }else{
            var regData:any=new User()
          }
          var reg = /^[0-9]{1,10}$/;
          var checking = reg.test(item?.Phone)
          if(!checking){
            session.flash({error:'Phone no is not valid => '+item?.Phone})
            continue
          }
          var profilePic
          if(item?.Image_Url!='NA'){
            const url:any= await commonHelper.imageCropAndUploadByUrl(item?.Profile_Image)
            profilePic=url?.Location
          }else{
            const url:any= await commonHelper.genTextImageForProfileNew(item?.Name,regData)
            profilePic=url?.Location
          }
          const ndate = new Date(item.DOB)
          ndate.setDate(ndate.getDate()+1)
          const cdate = DateTime.fromISO(ndate.toISOString(),{setZone:true}).setZone('Asia/Kolkata').toFormat('yyyy-LL-dd')
          const age=item.DOB?new Date(Date.now()-new Date(item.DOB).getTime()).getUTCFullYear()-1970:0
          regData.email=item?.Email||''
          regData.mobile=item?.Phone
          regData.password="seekex123"
          regData.age=age
          regData.dob=cdate
          regData.name=item?.Name
          regData.aboutMe=item?.About_Me||''
          regData.genderId=item?.Gender=='Male'?1:item?.Gender=='Female'?2:3
          regData.profilePic=profilePic||''
          regData.mobileverified=true
          regData.emailverified=true
          regData.expert=1
          regData.requested=1
          regData.verified=1
          regData.regCompleted=true
          regData.mobileOtp=''
          regData.emailOtp=''
          regData.followers=0
          regData.following=0
          regData.conversations=0
          regData.views=0
          regData.timeZone=data.timeZone||'Asia/Kolkata'
          regData.exotelCallOption=1
          regData.voipCallOption=0
          regData.audioCallRate=item?.Audio_Callrate||0
          regData.videoCallRate=item?.Video_Callrate||0
          regData.audioCallRatePerSec=await commonHelper.showCallAmount(item?.Audio_Callrate,false,'VOIP')||0
          regData.videoCallRatePerSec=await commonHelper.showCallAmount(item?.Video_Callrate,true,'VOIP')||0
          regData.regCompleteStep=4
          const user = await regData.save()
          if(item?.Country){
            const cId= await CountryState.getCountryIdByName(item.Country)
            const userCountery:any={countryStateId:cId,countryStateParentId:0,uid:user?.id}
            await UserCountryState.query().where({'uid':user?.id}).delete()
            await UserCountryState.create(userCountery)
          }
          var masterGodowns:any=[]
          if(item?.Occupation){
            const Occupation = await MasterGodown.getIDByName(item?.Occupation)
            if(Occupation){
              await MasterGodown.query().where('id',Occupation).increment('used',1)
              masterGodowns.push({masterGodownId:Occupation,masterType:1,masterFor:user?.id})
            }
          }
          if(item?.Current_Company){
              const existOrg:any = await Organization.getIDByName(item?.Current_Company)
              if(existOrg){
                await Organization.query().where('id',existOrg).increment('used',1)
                masterGodowns.push({masterGodownId:existOrg,masterType:7,masterFor:user?.id})
              }
          }

          if(item?.Skill_1){
            const Skill_1 = await MasterGodown.getIDByName(item?.Skill_1)
            if(Skill_1){
              await MasterGodown.query().where('id',Skill_1).increment('used',1)
              masterGodowns.push({masterGodownId:Skill_1,masterType:3,masterFor:user?.id})
            }
          }
          if(item?.Skill_2){
            const Skill_2 = await MasterGodown.getIDByName(item?.Skill_2)
            if(Skill_2){
              await MasterGodown.query().where('id',Skill_2).increment('used',1)
              masterGodowns.push({masterGodownId:Skill_2,masterType:3,masterFor:user?.id})
            }
          }
          if(item?.Skill_3){
            const Skill_3 = await MasterGodown.getIDByName(item?.Skill_3)
            if(Skill_3){
              await MasterGodown.query().where('id',Skill_3).increment('used',1)
              masterGodowns.push({masterGodownId:Skill_3,masterType:3,masterFor:user?.id})
            }
          }
          if(item?.Expertise_1){
            const Expertise_1 = await MasterGodown.getIDByName(item?.Expertise_1)
            if(Expertise_1){
              await MasterGodown.query().where('id',Expertise_1).increment('used',1)
              masterGodowns.push({masterGodownId:Expertise_1,masterType:5,masterFor:user?.id})
            }
          }
          if(item?.Expertise_2){
            const Expertise_2 = await MasterGodown.getIDByName(item?.Expertise_2)
            if(Expertise_2){
              await MasterGodown.query().where('id',Expertise_2).increment('used',1)
              masterGodowns.push({masterGodownId:Expertise_2,masterType:5,masterFor:user?.id})
            }
          }
          if(item?.Expertise_3){
            const Expertise_3 = await MasterGodown.getIDByName(item?.Expertise_3)
            if(Expertise_3){
              await MasterGodown.query().where('id',Expertise_3).increment('used',1)
              masterGodowns.push({masterGodownId:Expertise_3,masterType:5,masterFor:user?.id})
            }
          }

          if(item?.Highest_Education){
            const Highest_Education = await MasterGodown.getIDByName(item?.Highest_Education)
            if(Highest_Education){
              await MasterGodown.query().where('id',Highest_Education).increment('used',1)
              masterGodowns.push({masterGodownId:Highest_Education,masterType:4,masterFor:user?.id})
            }
          }
          if(item?.College_University){
            const existOrg:any = await Organization.getIDByName(item?.College_University)
            if(existOrg){
              await Organization.query().where('id',existOrg).increment('used',1)
              masterGodowns.push({masterGodownId:existOrg,masterType:9,masterFor:user?.id})
            }
        }
          if(masterGodowns.length>0){
            await UserMasterGodown.query().where({'masterFor':user?.id}).delete()
            await UserMasterGodown.createMany(masterGodowns)
          }
        }
        session.flash({success:'Experts has been saved.'})
        return response.redirect('back')
       // return {allUser,sheetdata}
      }
      session.flash({error:'No any valid data.'})
      return response.redirect('back')
    }
  }

  async unregistered({request,response,view}:HttpContextContract){
    const page = request.input('page', 1)
    const limit = 50
    const users=await WithoutRegUser.query().where({status:false}).orderBy('id','desc').paginate(page,limit)
    users.baseUrl(request.url())
    return view.render('admin/user/unregistered',{users})
  }

  async notificationSend({request,response}:HttpContextContract){
    const data = request.all()
    const type=data?.type
    
    if(type=='all'){
      const alldevices=await (await WithoutRegUser.query().where({status:false})).map((dev)=>{
        return dev?.fcm
      })
      if(alldevices.length>0){
        Event.emit('new:bulkNotification',{msg:data?.msg,alldevices:alldevices})
      }
      return response.status(200).json({
        "status": 'success',
        "msg":'Notification sent to all devices'
      })
    }else{
      if(data?.ids.length>0){
        const alldevices=await WithoutRegUser.query().whereIn('id',data?.ids)
        return alldevices
      }else{
        return response.status(200).json({
          "status": 'error',
          "msg":'No any device(s) selected.'
        })
      } 
    }
    return data
  }
  
  async profile({params,response,request,view,auth}:HttpContextContract){
    var user:any= await User.query()
    .preload('country')
    .preload('state')
    .preload('highestEducation')
    .preload('collegeUniversity')
    .preload('occupation')
    .preload('currentOrg')
    .preload('skillsList')
    .preload('intExpertiseList')
    .where({'id':auth?.user?.id}).first()
    user=user.serialize()
    return view.render('admin/user/profile',{user})
  }

  async profileEdit({request,session,response,auth}:HttpContextContract){
    const id = auth?.user?.id
    const data = request.all()
    var user:any= await User.query().where({'id':id}).first()
    if(user){
      user.name = data?.name
      user.dob = data?.dob
      const age=data?.dob?new Date(Date.now()-new Date(data?.dob).getTime()).getUTCFullYear()-1970:0
      user.age=age
      if(data?.password){
        user.password = data?.password
      }
      user.facebook=data?.facebook
      user.instagram=data?.instagram
      user.linkedin=data?.linkedin
      user.chanalUrl=data?.chanalUrl
      user.aboutMe=data?.aboutMe
      try {
        await user.save()
        session.flash({success:'User has been saved.'})
        return response.redirect().back()
      } catch (error) {
        session.flash({error:'User has been not saved, please try again.'})
        return response.redirect().back()
      }
    }
    session.flash({error:'User has been not saved, please try again.'})
    return response.redirect().back()
  }

  async pendingExport({response,session}:HttpContextContract){
    var xl = require('excel4node')
    var wb = new xl.Workbook({dateFormat: 'dd/mm/yyyy'})
    var ws = wb.addWorksheet('Pending Expert')
    ws.cell(1, 1).string('Name')
    ws.cell(1, 2).string('Email')
    ws.cell(1, 3).string('Phone')
    ws.cell(1, 4).string('DOB')
    ws.cell(1, 5).string('Account Status')
    ws.cell(1, 6).string('Registration Date')
    ws.cell(1, 7).string('Status')
    const users=await User.query().where({requested:1,expert:1,verified:0}).whereNot({'user_role':'admin'}).orderBy('updated','desc')

    for (const [indx,value] of Object.entries(users)) {
      const row = (parseInt(indx)+2)
      const item:any=value
      //console.log(item)
      ws.cell(row, 1).string(item?.name)
      ws.cell(row, 2).string(item?.email)
      ws.cell(row, 3).string(item?.mobile)
      const dob = DateTime.fromISO(new Date(item.dob).toISOString(),{setZone:true}).setZone('Asia/Kolkata').toFormat('dd, LLL yyyy')
      ws.cell(row, 4).date(dob)
      ws.cell(row, 5).string(item.verified?'Verified':'Unverified')
      const cdate = DateTime.fromISO(new Date(item.created).toISOString(),{setZone:true}).setZone('Asia/Kolkata').toFormat('dd, LLL yyyy t')
      ws.cell(row, 6).date(cdate)
      ws.cell(row, 7).string(item.status==1?'Active':'Inactive')
    }

    const nf=`${new Date().getTime()}${'.xlsx'}`
    const file=Application.publicPath('files/')+nf
    const gFile = await new Promise( function(resolve,reject) {
                      wb.write(file, async function(err, stats) {
                        if (err) {
                          await reject(err)
                        } else {
                          await resolve(stats)
                        }
                      })
                  })
    if(gFile){
      return response.attachment(file,'pending_expert.xlsx')
    }
    session.flash({error:'Internal error, please try again.'})
    return response.redirect('back')
    //ws.cell(1, 16).string('State')
  }

  async userExport({response,session}:HttpContextContract){
    var xl = require('excel4node')
    var wb = new xl.Workbook({dateFormat: 'dd/mm/yyyy'})
    var ws = wb.addWorksheet('All Users')
    ws.cell(1, 1).string('Name')
    ws.cell(1, 2).string('Email')
    ws.cell(1, 3).string('Phone')
    ws.cell(1, 4).string('DOB')
    ws.cell(1, 5).string('Account Status')
    ws.cell(1, 6).string('Registration Date')
    ws.cell(1, 7).string('Status')
    const users=await User.query().whereNot({'user_role':'admin',expert:1}).where({requested:0}).orderBy('updated','desc')

    for (const [indx,value] of Object.entries(users)) {
      const row = (parseInt(indx)+2)
      const item:any=value
      //console.log(item)
      ws.cell(row, 1).string(item?.name)
      ws.cell(row, 2).string(item?.email)
      ws.cell(row, 3).string(item?.mobile)
      const dob = DateTime.fromISO(new Date(item.dob).toISOString(),{setZone:true}).setZone('Asia/Kolkata').toFormat('dd, LLL yyyy')
      ws.cell(row, 4).date(dob)
      ws.cell(row, 5).string(item.verified?'Verified':'Unverified')
      const cdate = DateTime.fromISO(new Date(item.created).toISOString(),{setZone:true}).setZone('Asia/Kolkata').toFormat('dd, LLL yyyy t')
      ws.cell(row, 6).date(cdate)
      ws.cell(row, 7).string(item.status==1?'Active':'Inactive')
    }

    const nf=`${new Date().getTime()}${'.xlsx'}`
    const file=Application.publicPath('files/')+nf
    const gFile = await new Promise( function(resolve,reject) {
                      wb.write(file, async function(err, stats) {
                        if (err) {
                          await reject(err)
                        } else {
                          await resolve(stats)
                        }
                      })
                  })
    if(gFile){
      return response.attachment(file,'all_users.xlsx')
    }
    session.flash({error:'Internal error, please try again.'})
    return response.redirect('back')
  }

  async expertExport({response,session}:HttpContextContract){
    var xl = require('excel4node')
    var wb = new xl.Workbook({dateFormat: 'dd/mm/yyyy'})
    var ws = wb.addWorksheet('All Experts')
    ws.cell(1, 1).string('Name')
    ws.cell(1, 2).string('Email')
    ws.cell(1, 3).string('Phone')
    ws.cell(1, 4).string('DOB')
    ws.cell(1, 5).string('Account Status')
    ws.cell(1, 6).string('Registration Date')
    ws.cell(1, 7).string('Status')
    ws.cell(1, 8).string('Visible Status')
    ws.cell(1, 9).string('Featured Status')
    const users=await User.query().where({expert:1,verified:1}).whereNot({'user_role':'admin'}).orderBy('updated','desc')

    for (const [indx,value] of Object.entries(users)) {
      const row = (parseInt(indx)+2)
      const item:any=value
      //console.log(item)
      ws.cell(row, 1).string(item?.name)
      ws.cell(row, 2).string(item?.email)
      ws.cell(row, 3).string(item?.mobile)
      const dob = DateTime.fromISO(new Date(item.dob).toISOString(),{setZone:true}).setZone('Asia/Kolkata').toFormat('dd, LLL yyyy')
      ws.cell(row, 4).date(dob)
      ws.cell(row, 5).string(item.verified?'Verified':'Unverified')
      const cdate = DateTime.fromISO(new Date(item.created).toISOString(),{setZone:true}).setZone('Asia/Kolkata').toFormat('dd, LLL yyyy t')
      ws.cell(row, 6).date(cdate)
      ws.cell(row, 7).string(item.status==1?'Active':'Inactive')
      ws.cell(row, 8).string(item.isVisible==1?'Visible':'Unvisible')
      ws.cell(row, 9).string(item.featured==1?'Yes':'No')
    }

    const nf=`${new Date().getTime()}${'.xlsx'}`
    const file=Application.publicPath('files/')+nf
    const gFile = await new Promise( function(resolve,reject) {
                      wb.write(file, async function(err, stats) {
                        if (err) {
                          await reject(err)
                        } else {
                          await resolve(stats)
                        }
                      })
                  })
    if(gFile){
      return response.attachment(file,'experts.xlsx')
    }
    session.flash({error:'Internal error, please try again.'})
    return response.redirect('back')
  }

  public async referralCode({params,response,request,view,auth}:HttpContextContract){
    const userId = params?.id
    const user:any = await User.find(userId)
    const referralHistoryTotal = await ReferralHistory.query()
                              .where('referralCode',user?.referralCode)
                             // .where('status',1)
                              .whereColumn('uid','=','refer_by')
                              .pojo<{ totalAmount: number,count:number}>()
                              .sum('amount as totalAmount')
                              .count('id as count').first()
    const referralHistoryTotalEarned = await ReferralHistory.query()
                              .where('referralCode',user?.referralCode)
                              .where('status',1)
                              .whereColumn('uid','=','refer_by')
                              .pojo<{ totalAmount: number,count:number}>()
                              .sum('amount as totalAmount')
                              .count('id as count').first()                          

    const referralHistories = await ReferralHistory.query()
                              .preload('usedUser')
                              .where('referralCode',user?.referralCode)
                              .whereColumn('uid','=','refer_by')
   // return referralHistoryTotalEarned //min_referral_transfer
    return view.render('admin/user/referral_view',{user,referralHistories,referralHistoryTotal,referralHistoryTotalEarned})
  }

  public async checkTransferReferralAmount({params,response,request,view,auth}:HttpContextContract){
      const userId = params?.id
      try {
        const user:any = await User.find(userId)
        const referralHistoryTotalEarned = await ReferralHistory.query()
                                                .where('referralCode',user?.referralCode)
                                                .where('status',1)
                                                .whereColumn('uid','=','refer_by')
                                                .pojo<{ totalAmount: number,count:number}>()
                                                .sum('amount as totalAmount')
                                                .count('id as count').first()
        const userWallet = await LedgerAccount.query().where({name:'User',uid:userId}).first()
        return response.status(200).json({status:'success',data:{referralHistoryTotalEarned,userWallet,user}})
      } catch (error) {
        return response.status(200).json({status:'error',msg:'Internal error, please try again'})
      }
  }
  
  public async transferReferralAmount({params,response,request,view,auth}:HttpContextContract){
      const userId = params?.id
      try {
        const user:any = await User.find(userId)
        const referralHistoryTotalEarned = await ReferralHistory.query()
                                                .where('referralCode',user?.referralCode)
                                                .where('status',1)
                                                .whereColumn('uid','=','refer_by')
                                                .pojo<{ totalAmount: number,count:number}>()
                                                .sum('amount as totalAmount')
                                                .count('id as count').first()
        const userWallet = await LedgerAccount.query().where({name:'User',uid:userId}).first()
        const userPromoAmount:any = userWallet?.promoAmount
        const refAmount:any = referralHistoryTotalEarned?.totalAmount
        if(refAmount>userPromoAmount || userPromoAmount==0){
          return response.status(200).json({status:'error',msg:'This transfer is not allow'})
        }else{
          const promoTransfer = await ledgerHelper.referalAmountTranfer(userWallet,refAmount)
          if(promoTransfer){
            await ReferralHistory.query()
                                  .where('referralCode',user?.referralCode)
                                  .where('status',1)
                                  .whereColumn('uid','=','refer_by')
                                  .update({status:2})
          }
          return response.status(200).json({status:'success',msg:'Amount successfuly tranfered.'})
        }
        //return response.status(200).json({status:'success',data:{referralHistoryTotalEarned,userWallet,user}})
      } catch (error) {
        return response.status(200).json({status:'error',msg:'Internal error, please try again'})
      }
  }
}
