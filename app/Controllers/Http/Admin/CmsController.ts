 import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
 import CmsPage from 'App/Models/CmsPage'
export default class CmsController {

  public async index({auth,view,request}: HttpContextContract){
    const uid=auth.user?.id
    const page = request.input('page', 1)
    const limit = 20
    const pages:any= await CmsPage.query().paginate(page,limit)                      
    pages.baseUrl(request.url()) 
    return view.render('admin/cms/index',{pages})
  }

  public async view({auth,view,request,params,response}: HttpContextContract){
    const uid=auth.user?.id
    const id=params.id
    const page:any= await CmsPage.query().where({id:id}).first()
    if(page){
      return view.render('admin/cms/view',{page})
    }
    return response.redirect('/admin')
  }
  public async add({auth,view,response}: HttpContextContract){
    const uid=auth.user?.id
    return view.render('admin/cms/add')
  }

  public async changeStatus({request,params}){
    const id=params.id||request.input('id')
    const page:any= await CmsPage.query().where({id:id}).first()
    page.status=!page.status
    await page.save()
  }
  
  public async save({auth,request,response,session}: HttpContextContract){
    const data = request.all()
    if(data?.id){
      var page:any = await CmsPage.find(data?.id)
      page.name = data?.name
      page.slug =  data.slug?data?.slug.replace(/\s/g, "-").toLowerCase():page.slug
      page.content=data?.content
      page.status=1
      page.createdBy=auth.user?.id
    }else{
      var page:any = new CmsPage()
      page.name = data?.name
      page.slug= data.slug?data?.slug.replace(/[^\w\s]/gi, '').replace(/\s/g, "-").toLowerCase():data?.name.replace(/[^\w\s]/gi, '').replace(/\s/g, "-").toLowerCase()
      page.content=data?.content
      page.status=1
      page.createdBy=auth.user?.id
    }
   // return emailTmp
    try {
      await page.save()
      session.flash({
        "successResMsg": "Data successfuly save",
        "status": 'success'
      })
      return response.redirect('back')
      // return response.status(200).json({
      //   "resMsg": "Data successfuly save",
      //   "status": 'success',
      // })
    } catch (error) {
      session.flash({
        "errorResMsg": "Data not save",
        "status": 'error'
      })
      return response.redirect('back')
      // return response.status(200).json({
      //   "resMsg": "Data not save",
      //   "status": 'error',
      // })
    }
  }
}
