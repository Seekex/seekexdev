import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import { schema, rules,validator } from '@ioc:Adonis/Core/Validator'
import { DateTime } from 'luxon'
import AclGroup from 'App/Models/AclGroup'
import Agent from 'App/Models/Agent'
import User from 'App/Models/User'
import Application from '@ioc:Adonis/Core/Application'
export default class AgentsController {
  
  public async index({view,request,auth}:HttpContextContract){
    const page = request.input('page', 1)
    const limit = 20
    const agents:any = await User.query().orderBy('id','desc').whereNotIn('user_role',['user','admin']).paginate(page,limit)                     
    agents.baseUrl(request.url())
    var userList:any=[]
    for await (const item of agents.rows) {
      var newitem = item.serialize()
      const group:any = await AclGroup.getGroup(parseInt(item?.userRole))
      newitem.userRole = group?.name
      userList.push(newitem)
    }
    //return userList
    return view.render('admin/agent/index',{agents,userList})
  }

  public async add({auth,view,response}: HttpContextContract){
    const uid=auth.user?.id
    const groups= await AclGroup.getGroupList()
    return view.render('admin/agent/add',{groups})
  }

  public async save({auth,request,response,session}: HttpContextContract){
    const data = request.all()
    if(data?.id){
      var validationSchema = schema.create({
        name:schema.string({ trim: true }),
        email: schema.string({ trim: true }, [
          rules.email(),
          rules.unique({ table: 'users', column: 'email',whereNot:{id:data?.id} }),
        ]),
        mobile: schema.string({ trim: true }, [
          rules.unique({ table: 'users', column: 'mobile',whereNot:{id:data?.id} }),
        ]),
      })
    }else{
      var validationSchema = schema.create({
        name:schema.string({ trim: true }),
        email: schema.string({ trim: true }, [
          rules.email(),
          rules.unique({ table: 'users', column: 'email' }),
        ]),
        mobile: schema.string({ trim: true }, [
          rules.unique({ table: 'users', column: 'mobile' }),
        ]),
      })
    }
      try {
        await validator.validate({
        schema: validationSchema,
        messages: {
          'email.email': 'Invalid email',
          'email.unique': 'The email is already in use',
          'mobile.unique': 'The mobile is already in use',
        },
        data:data
      })
    } catch (error) {
      var errMsg=''
      //return Object.entries(error.messages)
      if(error.messages){
        Object.entries(error.messages).forEach((value:any) => {
          errMsg+=value[1][0]+'</br>'
        })
      }
      session.flash({
        "errorResMsg": errMsg,
        "status": 'error',
        email:data.email,
        name:data.name,
        mobile:data.mobile
      })
      return response.redirect('back')
    }
    if(data?.id){
      var agent:any = await User.find(data?.id)
      agent.name = data?.name
      agent.agentGroup = data?.group
      agent.email =  data.email
      agent.mobile=data?.mobile
      if(data?.password){
        agent.password=data?.password
      }
      agent.status=1
    }else{
      var agent:any = new User()
      agent.name = data?.name
      agent.userRole = data?.group
      agent.email =  data.email
      agent.mobile=data?.mobile
      if(data?.password){
        agent.password=data?.password
      }
      agent.status=1
    }
   // return emailTmp
    try {
      await agent.save()
      session.flash({
        "successResMsg": "Data successfuly save",
        "status": 'success'
      })
      return response.redirect('/admin/agents')
    } catch (error) {
      session.flash({
        "errorResMsg": "Data not save",
        "status": 'error'
      })
      return response.redirect('back')
    }
  }

  public async view({auth,view,request,params,response}: HttpContextContract){
    const uid=auth.user?.id
    const id=params.id
    const agent:any= await User.query().where({id:id}).first()
    const groups= await AclGroup.getGroupList()
    if(agent){
      return view.render('admin/agent/view',{agent,groups})
    }
    return response.redirect('admin/agents')
  }


  public async changeStatus({request,params}){
    const id=params.id||request.input('id')
    const agent:any= await User.query().where({id:id}).first()
    agent.status=!agent.status
    await agent.save()
    return 'success'
  }

  public async export({response,session}:HttpContextContract){
    var xl = require('excel4node')
    var wb = new xl.Workbook({dateFormat: 'dd/mm/yyyy'})
    var ws = wb.addWorksheet('All Agents')
    ws.cell(1, 1).string('Name')
    ws.cell(1, 2).string('Email')
    ws.cell(1, 3).string('Phone')
    ws.cell(1, 4).string('Group')
    ws.cell(1, 5).string('Status')
    ws.cell(1, 6).string('Created')

    const agents:any = await User.query().orderBy('id','desc').whereNotIn('user_role',['user','admin'])
    for await (const [indx,value] of Object.entries(agents)) {
      const row = (parseInt(indx)+2)
      const item:any=value
      //console.log(item)
      ws.cell(row, 1).string(item?.name)
      ws.cell(row, 2).string(item?.email)
      ws.cell(row, 3).string(item?.mobile)
      const group:any = await AclGroup.getGroup(parseInt(item?.userRole))
      ws.cell(row, 4).string(group?.name||'NA')
      ws.cell(row, 5).string(item.status==1?'Active':'Inactive')
      const cdate = DateTime.fromISO(new Date(item.created).toISOString(),{setZone:true}).setZone('Asia/Kolkata').toFormat('dd, LLL yyyy t')
      ws.cell(row, 6).date(cdate)
    }
    const nf=`${new Date().getTime()}${'.xlsx'}`
    const file=Application.publicPath('files/')+nf
    const gFile = await new Promise( function(resolve,reject) {
                      wb.write(file, async function(err, stats) {
                        if (err) {
                          await reject(err)
                        } else {
                          await resolve(stats)
                        }
                      })
                  })
    if(gFile){
      return response.attachment(file,'agents.xlsx')
    }
    session.flash({error:'Internal error, please try again.'})
    return response.redirect('back')
  }

}
