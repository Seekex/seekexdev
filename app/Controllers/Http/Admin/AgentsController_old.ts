import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import { schema, rules,validator } from '@ioc:Adonis/Core/Validator'
import Agent from 'App/Models/Agent'

export default class AgentsController {
  
  public async index({view,request,auth}:HttpContextContract){
    const page = request.input('page', 1)
    const limit = 20
    const agents = await Agent.query().orderBy('id','desc').paginate(page,limit)                      
    agents.baseUrl(request.url()) 
    return view.render('admin/agent/index',{agents})
  }

  public async add({auth,view,response}: HttpContextContract){
    const uid=auth.user?.id
    const groups= await Agent.allGroups()
    return view.render('admin/agent/add',{groups})
  }

  public async save({auth,request,response,session}: HttpContextContract){
    const data = request.all()
    if(data?.id){
      var validationSchema = schema.create({
        name:schema.string({ trim: true }),
        email: schema.string({ trim: true }, [
          rules.email(),
          rules.unique({ table: 'agents', column: 'email',whereNot:{id:data?.id} }),
        ]),
        mobile: schema.string({ trim: true }, [
          rules.unique({ table: 'agents', column: 'mobile',whereNot:{id:data?.id} }),
        ]),
      })
    }else{
      var validationSchema = schema.create({
        name:schema.string({ trim: true }),
        email: schema.string({ trim: true }, [
          rules.email(),
          rules.unique({ table: 'agents', column: 'email' }),
        ]),
        mobile: schema.string({ trim: true }, [
          rules.unique({ table: 'agents', column: 'mobile' }),
        ]),
      })
    }
      try {
        await validator.validate({
        schema: validationSchema,
        messages: {
          'email.email': 'Invalid email',
          'email.unique': 'The email is already in use',
          'mobile.unique': 'The mobile is already in use',
        },
        data:data
      })
    } catch (error) {
      var errMsg=''
      //return Object.entries(error.messages)
      if(error.messages){
        Object.entries(error.messages).forEach((value:any) => {
          errMsg+=value[1][0]+'</br>'
        })
      }
      session.flash({
        "errorResMsg": errMsg,
        "status": 'error',
        email:data.email,
        name:data.name,
        mobile:data.mobile
      })
      return response.redirect('back')
    }
    if(data?.id){
      var agent:any = await Agent.find(data?.id)
      agent.name = data?.name
      agent.agentGroup = data?.group
      agent.email =  data.email
      agent.mobile=data?.mobile
      if(data?.password){
        agent.password=data?.password
      }
      agent.status=1
    }else{
      var agent:any = new Agent()
      agent.name = data?.name
      agent.agentGroup = data?.group
      agent.email =  data.email
      agent.mobile=data?.mobile
      if(data?.password){
        agent.password=data?.password
      }
      agent.status=1
    }
   // return emailTmp
    try {
      await agent.save()
      session.flash({
        "successResMsg": "Data successfuly save",
        "status": 'success'
      })
      return response.redirect('/admin/agents')
    } catch (error) {
      session.flash({
        "errorResMsg": "Data not save",
        "status": 'error'
      })
      return response.redirect('back')
    }
  }

  public async view({auth,view,request,params,response}: HttpContextContract){
    const uid=auth.user?.id
    const id=params.id
    const agent:any= await Agent.query().where({id:id}).first()
    const groups= await Agent.allGroups()
    if(agent){
      return view.render('admin/agent/view',{agent,groups})
    }
    return response.redirect('/agent')
  }


  public async changeStatus({request,params}){
    const id=params.id||request.input('id')
    const agent:any= await Agent.query().where({id:id}).first()
    agent.status=!agent.status
    await agent.save()
  }

}
