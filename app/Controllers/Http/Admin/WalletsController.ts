import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import Database from '@ioc:Adonis/Lucid/Database'
import LedgerAccount from 'App/Models/LedgerAccount'
import TransactionHistory from 'App/Models/TransactionHistory'
import WebServiceLog from 'App/Models/WebServiceLog'
//import Feedback from 'App/Models/Feedback'
const perf = require('execution-time')()

export default class WalletsController {


  public async index({auth,view}: HttpContextContract){
    const user=auth?.user
    const seekexWallets= await LedgerAccount.query().whereNot({name:'User'})
    return view.render('admin/wallet/index',{seekexWallets})
  }

  public async details({view,params,response,request}:HttpContextContract){
    const id=params.id
    const page = request.input('page', 1)
    const limit = 20
    var userWallet:any= await LedgerAccount.find(id)
    userWallet=userWallet.serialize()
    if(userWallet){
      const transections = await TransactionHistory.query().where({transactionWalletId:id}).orderBy('created','desc').paginate(page,limit)
      transections.baseUrl(request.url())
      //return transections
      return view.render('admin/wallet/view',{userWallet,transections})
    }
    return response.redirect('back')
    
  }

  async addAmountInWallet({params,response,request,auth}:HttpContextContract){
    perf.start()
    const id=params.id
    const uid=auth?.user?.id
    const data = request.all()
    //return Number(parseFloat(data.amount))
    if(!Number(parseFloat(data.amount))){
      return response.status(201).json({status:'error',msg:'Not a valid amount'})
    }
    if(!data.description.trim()){
      data.description=data.amount+' Amount Added by admin.'
    }
    var userWallet:any=  await LedgerAccount.query().where({'id':id}).first()
   // return userWallet
    try {
      const trns_id= await Database.transaction(async (trx) => {
        const addToUserWallet:any=new TransactionHistory()
        addToUserWallet.callId=0
        addToUserWallet.description=data.description
        addToUserWallet.fromWallet=userWallet?.id
        addToUserWallet.toWallet=userWallet?.id
        addToUserWallet.isdebit=false
        addToUserWallet.status=1
        addToUserWallet.transactionAmount=data.amount
        addToUserWallet.transactionType=9
        addToUserWallet.transactionWalletId=userWallet?.id
        addToUserWallet.transactionTo='User'
        addToUserWallet.transactionAction='plus'
        addToUserWallet.uid=uid
        addToUserWallet.payout=false
        addToUserWallet.useTransaction(trx)
        await addToUserWallet.save()
       // return userWallet.availableAmount+parseFloat(data.amount)
        userWallet.promoAmount = userWallet.promoAmount+parseFloat(data.amount)
        userWallet.totalAmount = userWallet.totalAmount+parseFloat(data.amount)
        userWallet.useTransaction(trx)
        await userWallet.save()
        return addToUserWallet?.id
       ///return userWallet.availableAmount
      })
     /// return trns_id
      return response.status(200).json({status:'success',msg:'Amount successfuly added',trns_id:trns_id})
    } catch (error) {
      const results = perf.stop()
      await WebServiceLog.create({request:JSON.stringify(data),response:JSON.stringify(error),service_name:'In Admin Add money in seekex wallet',status:2,uid:uid,time_taken:results.time})
      return response.status(201).json({status:'error',msg:'Internal error, amount not added'})
    }
  }
}
