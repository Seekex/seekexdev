import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import Request from 'App/Models/Request'
import WebServiceLog from 'App/Models/WebServiceLog'
import LedgerHelper from 'App/Helpers/LedgerHelper'
const Ledger = new LedgerHelper()
import Application from '@ioc:Adonis/Core/Application'
import { DateTime } from 'luxon'
export default class AppointmentsController {


  public async index({auth,view,request,session,response}: HttpContextContract){
    const uid=auth.user?.id
    const page = request.input('page', 1)
    const limit = 20
    const s = request.input('s')
    const from =request.input('from')
    const to =request.input('to')
    const appointmentQuery = Request.query()
                            .preload('user')
                            .preload('expert')
                            .preload('slots')
    if(s){
      appointmentQuery.where(qu=>{
        qu.whereHas('user',(q)=>{
          q.where('name','LIKE',"%"+s+"%")
        }).orWhereHas('expert',(q)=>{
          q.where('name','LIKE',"%"+s+"%")
        })
      })
    }
    if(from && to){
      var created_to = new Date(to)
      created_to.setUTCHours(0,0,0,0)
      var created_from = new Date(from)
      created_from.setUTCHours(23,59,0,0)
      if(created_from.getTime()>created_to.getTime()){
        session.flash({error:'Invalid dates.'})
        return response.redirect('back')
      }
      appointmentQuery.where(qu=>{
        qu.whereBetween('created',[created_from,created_to])
      })
    }  
    const appointments:any= await appointmentQuery
                            .orderBy('id',"desc")
                            .paginate(page,limit)                      
    appointments.baseUrl(request.url())
    return view.render('admin/appointment/index',{appointments})
  }


  public async view({auth,view,params}){
    const uid=auth.user?.id
    const appId= params.id
    const appointment:any= await Request.query()
                            .preload('user')
                            .preload('expert')
                            .preload('slots',(query)=>{
                              query.preload("schedule")
                           })
                            .where("id",appId).first()
    //return appointment                        
    return view.render('admin/appointment/view',{appointment})
  }

  public async cancel({auth,request,response}: HttpContextContract){
    const uid=auth.user?.id
    const appId=request.input('id')
    const comment=request.input('comment')
    const appointment:any= await Request.query().where("id",appId).first()
    appointment.comment=comment
    appointment.canceledBy=uid
    appointment.status=3
    appointment.canceledDate=new Date()
    try {
      await appointment.save()
      await Ledger.releaseAmountForRequest(appointment)
      return response.status(200).json({status:"success",msg:'Appointment successfuly canceled.'})
    } catch (error) {
      await WebServiceLog.create({request:JSON.stringify(request.all()),response:JSON.stringify(error),service_name:'Appointment cancel by admin',status:1,uid:uid,time_taken:0})
      return response.status(401).json({status:"error",msg:'Internal error, please try again.'})
    }
  } 

  async export({session,response,request}:HttpContextContract){
    const s = request.input('s')
    const from =request.input('from')
    const to =request.input('to')
    const appointmentQuery = Request.query()
                            .preload('user')
                            .preload('expert')
                            .preload('slots')
    if(s){
      appointmentQuery.where(qu=>{
        qu.whereHas('user',(q)=>{
          q.where('name','LIKE',"%"+s+"%")
        }).orWhereHas('expert',(q)=>{
          q.where('name','LIKE',"%"+s+"%")
        })
      })
    }
    if(from && to){
      var created_to = new Date(to)
      created_to.setUTCHours(0,0,0,0)
      var created_from = new Date(from)
      created_from.setUTCHours(23,59,0,0)
      if(created_from.getTime()>created_to.getTime()){
        session.flash({error:'Invalid dates.'})
        return response.redirect('back')
      }
      appointmentQuery.where(qu=>{
        qu.whereBetween('created',[created_from,created_to])
      })
    }  
    const appointments:any= await appointmentQuery.orderBy('id',"desc")
    var xl = require('excel4node')
      var wb = new xl.Workbook({dateFormat: 'dd/mm/yyyy'})
      var ws = wb.addWorksheet('All Appointments Data')
      ws.cell(1, 1).string('User Name')
      ws.cell(1, 2).string('Expert Name')
      ws.cell(1, 3).string('Booking Date')
      ws.cell(1, 4).string('Booking Day')
      ws.cell(1, 5).string('Estimated Duration')
      ws.cell(1, 6).string('Estimated Cost')
      ws.cell(1, 7).string('Call Rate')
      ws.cell(1, 8).string('Call Type')
      ws.cell(1, 9).string('Status')
      ws.cell(1, 10).string('Created')
      for (const [indx,value] of Object.entries(appointments)) {
        const row = (parseInt(indx)+2)
        const item:any=value
        ws.cell(row, 1).string(item.user.name||'NA')
        ws.cell(row, 2).string(item.expert.name||'NA')
       // const bookingDate  = DateTime.fromISO(new Date(item.bookingDate).toISOString(),{setZone:true}).setZone('Asia/Kolkata').toFormat('dd, LLL yyyy t')
        ws.cell(row, 3).string(''+item.bookingDate)
        ws.cell(row, 4).string(''+item.days)
        ws.cell(row, 5).string(item.estimatedCall)
        ws.cell(row, 6).string(''+item.estimatedCost)
        ws.cell(row, 7).string(item.rate+'/Sec')
        const type = item.videocall?'Video':'Audio'
        ws.cell(row, 8).string(type)
        const status=item.reqStatus
        ws.cell(row, 9).string(status)
        const cdate = DateTime.fromISO(new Date(item.created).toISOString(),{setZone:true}).setZone('Asia/Kolkata').toFormat('dd, LLL yyyy t')
        ws.cell(row, 10).date(cdate)
      }
  
    const nf=`${new Date().getTime()}${'.xlsx'}`
    const file=Application.publicPath('files/')+nf
    const gFile = await new Promise( function(resolve,reject) {
                      wb.write(file, async function(err, stats) {
                        if (err) {
                          await reject(err)
                        } else {
                          await resolve(stats)
                        }
                      })
                  })
    if(gFile){
      return response.attachment(file,'appointments_data.xlsx')
    }
    session.flash({error:'Internal error, please try again.'})
    return response.redirect('back')
  }
}