import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import { schema, rules,validator } from '@ioc:Adonis/Core/Validator'
import Chat from 'App/Models/Chat'
import Database from '@ioc:Adonis/Lucid/Database'
import User from 'App/Models/User'
import { DateTime } from 'luxon'
import CommonHelper from 'App/Helpers/CommonHelper'
import Event from '@ioc:Adonis/Core/Event'
import CallHistory from 'App/Models/CallHistory'
import Invoice from 'App/Models/Invoice'
import Application from '@ioc:Adonis/Core/Application'
const commonHelper = new CommonHelper()
export default class CallsController {
  
  public async index({view,request,auth,session,response}:HttpContextContract){

    const data = request.all()
    const page = data?.page||1
    const limit = 20
    const userTimeZone='Asia/Kolkata'
    const s = request.input('s')
    const from =request.input('from')
    const to =request.input('to')
		const duration = request.input('duration')
    var historyQuery =  CallHistory.query()
				                  .preload('expert')
                          .preload('user')
                          .preload('refund')
                          .has('user')
                          .has('expert')
        if(s){
          historyQuery.where(qu=>{
            qu.whereHas('user',(q)=>{
              q.where('name','LIKE',"%"+s+"%")
            }).orWhereHas('expert',(q)=>{
              q.where('name','LIKE',"%"+s+"%")
            })
          }) 
        }
				if(duration && duration==1){
					historyQuery.where('duration',0)
				}
				if(duration && duration==2){
					historyQuery.where('duration','>',0)
				}

        if(from && to){
          var created_to = new Date(to)
          created_to.setUTCHours(0,0,0,0)
          var created_from = new Date(from)
          created_from.setUTCHours(23,59,0,0)
         // return {created_from,created_to}
         if(created_from.getTime()>created_to.getTime()){
          session.flash({error:'Invalid dates.'})
          return response.redirect('back')
         }
         historyQuery.where(qu=>{
          qu.whereBetween('callTime',[created_from,created_to])
         })
        }
        
			 var callsHistory:any= await historyQuery.orderBy('id','desc').paginate(page,limit)
       var newcallsHistoryData:any
			 callsHistory.baseUrl(request.url())
       callsHistory.queryString({'s':s,'from':from,'to':to})
			 if(callsHistory){
        newcallsHistoryData = callsHistory?.rows
			 }
			 newcallsHistoryData = await Promise.all(newcallsHistoryData.map(async(call)=>{
														var dt:any={}
														call.duration = Math.floor(call?.duration||0)
														call.callTime=DateTime.fromISO(new Date(call.callTime).toISOString(),{setZone:true}).setZone(userTimeZone).toFormat('dd, LLL yyyy t')
														dt.callHistory={
															"callTime":call?.callTime,
															"duration": await commonHelper.durationFormat(call?.duration),
															"initiater": call?.initiater,
															"receiver": call?.receiver,
															"requestId": call?.requestId,
															"video": call?.video?true:false,
															"id": call?.id,
															"callingType": call?.callingType,
															"name": call?.name,
															"profileImage": call?.profileImage,
															"expertCallBackTime": call?.expertCallBackTime,
														}
                            dt.call=call
                            dt.invoice=await Invoice.query().where('callId',call?.id).first()
														if(call.duration==0 || call.duration==null){
															dt.type=3
														}
														const rat = await commonHelper.callRating(call?.id)
                            dt.rating=parseFloat((rat?.rating).toFixed(1))||0
                            dt.ratingCount=rat?.count||0
														if(call?.callingType=='Exotel'){
															dt.isExotel=true
														}else{
															dt.isExotel=false
														}
														delete dt.callHistory['user']
														delete dt.callHistory.expert
														return dt
												}))
     // return {newcallsHistoryData}                
    return view.render('admin/call/index',{callsHistory,newcallsHistoryData})
  }

	public async export({session,response,request}:HttpContextContract){
		const s = request.input('s')
    const from =request.input('from')
    const to =request.input('to')

		var xl = require('excel4node')
    var wb = new xl.Workbook({dateFormat: 'dd/mm/yyyy'})
    var ws = wb.addWorksheet('All Calls Data')
		ws.cell(1, 1).string('Call ID')
		ws.cell(1, 2).string('Initiater User')
    ws.cell(1, 3).string('Receiver User')
    ws.cell(1, 4).string('Call Type')
    ws.cell(1, 5).string('Call Time')
		ws.cell(1, 6).string('Duration')
		ws.cell(1, 7).string('Estimated Duration')
		ws.cell(1, 8).string('Call Amount')
		ws.cell(1, 9).string('Call Recordings')
		var historyQuery =  CallHistory.query()
				                  .preload('expert')
                          .preload('user')
                          .preload('refund')
                          .has('user')
                          .has('expert')
		if(s){
			historyQuery.where(qu=>{
				qu.whereHas('user',(q)=>{
					q.where('name','LIKE',"%"+s+"%")
				}).orWhereHas('expert',(q)=>{
					q.where('name','LIKE',"%"+s+"%")
				})
			}) 
		}

		if(from && to){
			var created_to = new Date(to)
			created_to.setUTCHours(0,0,0,0)
			var created_from = new Date(from)
			created_from.setUTCHours(23,59,0,0)
			// return {created_from,created_to}
			if(created_from.getTime()>created_to.getTime()){
			session.flash({error:'Invalid dates.'})
			return response.redirect('back')
			}
			historyQuery.where(qu=>{
			qu.whereBetween('callTime',[created_from,created_to])
			})
		}											
		var callsHistory:any= await historyQuery.orderBy('id','desc')
		var newcallsHistoryData:any
		const userTimeZone='Asia/Kolkata'
		newcallsHistoryData = await Promise.all(callsHistory.map(async(call)=>{
						var dt:any={}
						call.duration = Math.floor(call?.duration||0)
						call.callTime=DateTime.fromISO(new Date(call.callTime).toISOString(),{setZone:true}).setZone(userTimeZone).toFormat('dd, LLL yyyy t')
						dt.callHistory={
							"callTime":call?.callTime,
							"duration": await commonHelper.durationFormat(call?.duration),
							"initiater": call?.initiater,
							"receiver": call?.receiver,
							"requestId": call?.requestId,
							"video": call?.video?true:false,
							"id": call?.id,
							"callingType": call?.callingType,
							"name": call?.name,
							"profileImage": call?.profileImage,
							"expertCallBackTime": call?.expertCallBackTime,
						}
						dt.call=call
						dt.invoice=await Invoice.query().where('callId',call?.id).first()
						if(call.duration==0 || call.duration==null){
							dt.type=3
						}
						const rat = await commonHelper.callRating(call?.id)
						dt.rating=parseFloat((rat?.rating).toFixed(1))||0
						dt.ratingCount=rat?.count||0
						if(call?.callingType=='Exotel'){
							dt.isExotel=true
						}else{
							dt.isExotel=false
						}
						delete dt.callHistory['user']
						delete dt.callHistory.expert
						return dt
			}))
			for (const [indx,value] of Object.entries(newcallsHistoryData)) {
				const row = (parseInt(indx)+2)
				const item:any=value
				ws.cell(row, 1).number(item?.callHistory.id)
				ws.cell(row, 2).string(item?.call.user.name)
				ws.cell(row, 3).string(item?.call.expert.name)
				ws.cell(row, 4).string(item?.callHistory.video?'Video':'Audio')
				ws.cell(row, 5).string(item?.callHistory.callTime)
				ws.cell(row, 6).string(item?.callHistory.duration)
				ws.cell(row, 7).string(item?.call.estimatedDuration?item?.call.estimatedDuration+' sec':'NA')
				ws.cell(row, 8).string(item.invoice?item.invoice.totalAmount:'0.00')
				ws.cell(row, 9).string(item?.call.recordingUrl?item?.call.recordingUrl:'NA')
			}

		const nf=`${new Date().getTime()}${'.xlsx'}`
    const file=Application.publicPath('files/')+nf
    const gFile = await new Promise( function(resolve,reject) {
                      wb.write(file, async function(err, stats) {
                        if (err) {
                          await reject(err)
                        } else {
                          await resolve(stats)
                        }
                      })
                  })
    if(gFile){
      return response.attachment(file,'calls_data.xlsx')
    }
    session.flash({error:'Internal error, please try again.'})
    return response.redirect('back')
	}
}
