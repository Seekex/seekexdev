import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import Refund from 'App/Models/Refund'
import CallHistory from 'App/Models/CallHistory'
import RefundHistory from 'App/Models/RefundHistory'
import User from 'App/Models/User'
import Event from '@ioc:Adonis/Core/Event'
import WebServiceLog from 'App/Models/WebServiceLog'
const perf = require('execution-time')()
import LedgerHelper from 'App/Helpers/LedgerHelper'
const Ledger = new LedgerHelper()
import Application from '@ioc:Adonis/Core/Application'
import { DateTime } from 'luxon'
export default class RefundController {

  public async index({view,request,session,response}: HttpContextContract){
    const s = request.input('s')
    const from =request.input('from')
    const to =request.input('to')
    const page = request.input('page', 1)
    const limit = 20
    const refundsQuery =  Refund.query()
                          .preload('callDetail')
                          .preload('appliedByUser')
                          .preload('appliedForUser')
                          .has('appliedByUser')
                          .has('appliedForUser')
                          .has('callDetail')
    if(s){
      refundsQuery.where(qu=>{
        qu.whereHas('appliedByUser',(q)=>{
          q.where('name','LIKE',"%"+s+"%")
        }).orWhereHas('appliedForUser',(q)=>{
          q.where('name','LIKE',"%"+s+"%")
        })
      })
    }
    if(from && to){
      var created_to = new Date(to)
      created_to.setUTCHours(0,0,0,0)
      var created_from = new Date(from)
      created_from.setUTCHours(23,59,0,0)
     // return {created_from,created_to}
     if(created_from.getTime()>created_to.getTime()){
      session.flash({error:'Invalid dates.'})
      return response.redirect('back')
     }
     refundsQuery.where(qu=>{
      qu.whereBetween('created',[created_from,created_to])
     })
    }

    const refunds = await refundsQuery
                          .where({status:1})
                          .orderBy('id','desc')
                          .paginate(page,limit)

    return view.render('admin/refund/index',{refunds})
  }

  public async refunds({view,request,session,response}: HttpContextContract){
    const s = request.input('s')
    const from =request.input('from')
    const to =request.input('to')
    const page = request.input('page', 1)
    const limit = 20
    const refundsQuery =  Refund.query()
                          .preload('callDetail')
                          .preload('appliedByUser')
                          .preload('appliedForUser')
                          .has('appliedByUser')
                          .has('appliedForUser')
                          .has('callDetail')
    if(s){
      refundsQuery.where(qu=>{
        qu.whereHas('appliedByUser',(q)=>{
          q.where('name','LIKE',"%"+s+"%")
        }).orWhereHas('appliedForUser',(q)=>{
          q.where('name','LIKE',"%"+s+"%")
        })
      })
    }
    if(from && to){
      var created_to = new Date(to)
      created_to.setUTCHours(0,0,0,0)
      var created_from = new Date(from)
      created_from.setUTCHours(23,59,0,0)
     // return {created_from,created_to}
     if(created_from.getTime()>created_to.getTime()){
      session.flash({error:'Invalid dates.'})
      return response.redirect('back')
     }
     refundsQuery.where(qu=>{
      qu.whereBetween('created',[created_from,created_to])
     })
    }
    const refunds = await refundsQuery
                          .whereNot({status:1})
                          .orderBy('id','desc')
                          .paginate(page,limit)
    return view.render('admin/refund/index',{refunds})
  }

  public async details({view,params,request}: HttpContextContract){
    const id=params.id
    const page = request.input('page', 1)
    const limit = 20
    const refund:any=await Refund.query()
                  .preload('callDetail')
                  .preload('appliedByUser')
                  .preload('appliedForUser')
                  .has('appliedByUser').has('appliedForUser').has('callDetail')
                  .where({id:id}).first()
    var callHistories=await CallHistory.query().preload('refund').where({initiater:refund.appliedBy}).whereNot({id:refund.callId}).where('duration','>',0).whereNotNull('duration').orderBy('id','desc').paginate(page,limit)
    var refundHistories = await Refund.query()
                          .preload('callDetail')
                          .preload('appliedByUser')
                          .preload('appliedForUser').has('appliedByUser').has('appliedForUser')
                          .whereNot({id:id}).orderBy('id','desc').paginate(page,limit)
    var refundHistoryAction = await RefundHistory.query().preload('sent_user').where({refundId:refund.id}).orderBy('id','desc')
    
    return view.render('admin/refund/view',{refund,callHistories,refundHistories,refundHistoryAction})
  }

  public async sendMsg({request,response,auth}:HttpContextContract){
    perf.start()
    const data = request.all()
    if(data.emailMsg.trim() === "" && data.smsMsg.trim() === ""){
      return response.status(201).json({status:'error',msg:'Please enter email/sms message.'})
    }
    var sendTo = await User.query().where({id:data.id}).first()
    if(sendTo){
      if(data.emailMsg.trim()){
        var sendData:any={to:sendTo.email,subject:'Refund Email',message:data.emailMsg}
        try {
          await RefundHistory.create({refundId:data.refundId,approveBy:auth.user?.id,comment:data.emailMsg,status:'',type:'Email',sendTo:data.id})
          await Event.emit('new:email', sendData)
        } catch (error) {
          const results = perf.stop()
          await WebServiceLog.create({request:JSON.stringify(data),response:JSON.stringify(error),service_name:'Admin Refund Send Email',status:1,uid:auth.user?.id,time_taken:results.time})
          return response.status(201).json({status:'error',msg:'Internal Email Error'})
        }
      }
      if(data.smsMsg.trim()){
        try {
          await RefundHistory.create({refundId:data.refundId,approveBy:auth.user?.id,comment:data.smsMsg,status:'',type:'SMS',sendTo:data.id})

        } catch (error) {
          const results = perf.stop()
          await WebServiceLog.create({request:JSON.stringify(data),response:JSON.stringify(error),service_name:'Admin Refund Send SMS',status:2,uid:auth.user?.id,time_taken:results.time})
          return response.status(201).json({status:'error',msg:'Internal SMS Error'})
        }
      }
    }
    return response.status(200).json({status:'success',msg:'Message sent'})
  }

  public async callTO({request,response,auth}:HttpContextContract){
    const uid = auth.user?.id
    const data = request.all()
    
  }

  public async approveRefund({request,response,auth}:HttpContextContract){
    perf.start()
    const uid = auth.user?.id
    const data = request.all()
    const refund:any=await Refund.query().where({id:data.id,status:1}).first()
    var refundHistory:any=new RefundHistory()

    if(data.status==1){
      refund.status=2
      refundHistory.status='Approved'
      refundHistory.Type='Borne by Expert'
      /*******************************************Code for amount transection*******************************************/
          await Ledger.refundAmountFromExpert(refund)
      /*******************************************Code for amount transection[End]*************************************/
    }else if(data.status==2){
      refund.status=2
      refundHistory.status='Approved'
      refundHistory.Type='Borne by Seekex'
      /*******************************************Code for amount transection*******************************************/
      await Ledger.refundAmountFromSeekex(refund)
      /*******************************************Code for amount transection[End]*************************************/
    }else if(data.status==3){
      refund.status=3
      refundHistory.status='Rejected'
      refundHistory.Type='NA'
      const seeker = await User.find(refund?.appliedBy)
      const expert = await User.find(refund?.appliedFor)
      const allData={
        refundId:refund?.id,
        seekerName:seeker?.name,
        seekerEmail:seeker?.email,
        expertName:expert?.name,
        refundPolicy:''
      }
       Event.emit('new:refundRejectEmail',allData)
    }
    //refund.reason=data.comment
    refundHistory.comment=data.comment
    refundHistory.refundId=data.id
    try {
      await refund.save()
      await refundHistory.save()
      return response.status(200).json({status:'success',msg:'Refund approved'})
    } catch (error) {
      const results = perf.stop()
      await WebServiceLog.create({request:JSON.stringify(data),response:JSON.stringify(error),service_name:'Admin Refund Action Error',status:2,uid:auth.user?.id,time_taken:results.time})
      return response.status(201).json({status:'error',msg:'Internal error'})
    }
  }

  public async approvePartialRefund({request,response,auth}:HttpContextContract){
    perf.start()
    const uid = auth.user?.id
    const data = request.all()
    const refund:any=await Refund.query().where({id:data.id,status:1}).first()

    if(!Number(parseFloat(data.amount))){
      return response.status(200).json({status:'error',msg:'Not a valid amount'})
    }else if(parseFloat(data.amount) < 1){
      return response.status(200).json({status:'error',msg:'Not a valid amount'})
    }
    var amountCal
    if(data.value=='value'){
      if(parseFloat(data.amount)>refund.amt){
        return response.status(200).json({status:'error',msg:'This amount not allow for refund.'})
      }
      amountCal = parseFloat(data.amount)
    }else if(data.value=='percentage'){
       amountCal=(refund.amt*parseFloat(data.amount))/100
      console.log(amountCal)
      if(amountCal>refund.amt){
        return response.status(200).json({status:'error',msg:'This amount not allow for refund.'})
      }
    }
    var refundHistory:any=new RefundHistory()
    if(data.status==1){
      refund.status=2
      refundHistory.status='Approved'
      refundHistory.Type='Borne by Expert'
      /*******************************************Code for amount transection*******************************************/
        await Ledger.refundPartialAmountFromExpert(refund,amountCal)
      /*******************************************Code for amount transection[End]*************************************/
    }else if(data.status==2){
      refund.status=2
      refundHistory.status='Approved'
      refundHistory.Type='Borne by Seekex'
      /*******************************************Code for amount transection*******************************************/
      await Ledger.refundPartialAmountFromSeekex(refund,amountCal)
      /*******************************************Code for amount transection[End]*************************************/
    }
    //refund.reason=data.comment
    refund.amt=amountCal
    refundHistory.comment=data.comment +'=>'+amountCal
    refundHistory.refundId=data.id
    try {
      await refund.save()
      await refundHistory.save()
      return response.status(200).json({status:'success',msg:'Refund approved'})
    } catch (error) {
      const results = perf.stop()
      await WebServiceLog.create({request:JSON.stringify(data),response:JSON.stringify(error),service_name:'Admin Partial Refund Action Error',status:2,uid:auth.user?.id,time_taken:results.time})
      return response.status(201).json({status:'error',msg:'Internal error'})
    }
  }

  async refundExport({session,response,request,params}:HttpContextContract){
    const s = request.input('s')
    const from =request.input('from')
    const to =request.input('to')
    const refundsQuery =  Refund.query()
                          .preload('callDetail')
                          .preload('appliedByUser')
                          .preload('appliedForUser')
                          .has('appliedByUser')
                          .has('appliedForUser')
                          .has('callDetail')
    if(s){
      refundsQuery.where(qu=>{
        qu.whereHas('appliedByUser',(q)=>{
          q.where('name','LIKE',"%"+s+"%")
        }).orWhereHas('appliedForUser',(q)=>{
          q.where('name','LIKE',"%"+s+"%")
        })
      })
    }
    if(from && to){
      var created_to = new Date(to)
      created_to.setUTCHours(0,0,0,0)
      var created_from = new Date(from)
      created_from.setUTCHours(23,59,0,0)
     // return {created_from,created_to}
     if(created_from.getTime()>created_to.getTime()){
      session.flash({error:'Invalid dates.'})
      return response.redirect('back')
     }
     refundsQuery.where(qu=>{
      qu.whereBetween('created',[created_from,created_to])
     })
    }

    const refunds = await refundsQuery
                          .whereNot({status:1})
                          .orderBy('id','desc')

      var xl = require('excel4node')
      var wb = new xl.Workbook({dateFormat: 'dd/mm/yyyy'})
      var ws = wb.addWorksheet('All Refund Data')
      ws.cell(1, 1).string('Call Id')
      ws.cell(1, 2).string('Applied User')
      ws.cell(1, 3).string('Expert')
      ws.cell(1, 4).string('Amout')
      ws.cell(1, 5).string('Expert Refund Amout')
      ws.cell(1, 6).string('Call Duration')
      ws.cell(1, 7).string('Reason')
      ws.cell(1, 8).string('Applied Date')
      ws.cell(1, 9).string('Status')

      for (const [indx,value] of Object.entries(refunds)) {
        const row = (parseInt(indx)+2)
        const item:any=value
        ws.cell(row, 1).string(''+item?.callId)
        ws.cell(row, 2).string(item?.appliedByUser.name)
        ws.cell(row, 3).string(item?.appliedForUser.name)
        ws.cell(row, 4).number(item?.amt||0)
        ws.cell(row, 5).number(item?.expertRefundAmount||0)
        ws.cell(row, 6).number(item.callDetail.duration||0)
        ws.cell(row, 7).string(item.reason?item.reason:'NA')
        const cdate = DateTime.fromISO(new Date(item.created).toISOString(),{setZone:true}).setZone('Asia/Kolkata').toFormat('dd, LLL yyyy t')
        ws.cell(row, 8).date(cdate)
        const status=item.status==1?'Applied':item.status==2?'Approved':item.status==3?'Rejected':'NA'
        ws.cell(row, 9).string(status)
      }
  
    const nf=`${new Date().getTime()}${'.xlsx'}`
    const file=Application.publicPath('files/')+nf
    const gFile = await new Promise( function(resolve,reject) {
                      wb.write(file, async function(err, stats) {
                        if (err) {
                          await reject(err)
                        } else {
                          await resolve(stats)
                        }
                      })
                  })
    if(gFile){
      return response.attachment(file,'refund_data.xlsx')
    }
    session.flash({error:'Internal error, please try again.'})
    return response.redirect('back')
  }

  async pendingRefundExport({session,response,request,params}:HttpContextContract){
    const s = request.input('s')
    const from =request.input('from')
    const to =request.input('to')
    const refundsQuery =  Refund.query()
                          .preload('callDetail')
                          .preload('appliedByUser')
                          .preload('appliedForUser')
                          .has('appliedByUser')
                          .has('appliedForUser')
                          .has('callDetail')
    if(s){
      refundsQuery.where(qu=>{
        qu.whereHas('appliedByUser',(q)=>{
          q.where('name','LIKE',"%"+s+"%")
        }).orWhereHas('appliedForUser',(q)=>{
          q.where('name','LIKE',"%"+s+"%")
        })
      })
    }
    if(from && to){
      var created_to = new Date(to)
      created_to.setUTCHours(0,0,0,0)
      var created_from = new Date(from)
      created_from.setUTCHours(23,59,0,0)
     // return {created_from,created_to}
     if(created_from.getTime()>created_to.getTime()){
      session.flash({error:'Invalid dates.'})
      return response.redirect('back')
     }
     refundsQuery.where(qu=>{
      qu.whereBetween('created',[created_from,created_to])
     })
    }

    const refunds = await refundsQuery
                          .where({status:1})
                          .orderBy('id','desc')
      var xl = require('excel4node')
      var wb = new xl.Workbook({dateFormat: 'dd/mm/yyyy'})
      var ws = wb.addWorksheet('All Pending Refund Data')
      ws.cell(1, 1).string('Call Id')
      ws.cell(1, 2).string('Applied User')
      ws.cell(1, 3).string('Expert')
      ws.cell(1, 4).string('Amout')
      ws.cell(1, 5).string('Expert Refund Amout')
      ws.cell(1, 6).string('Call Duration')
      ws.cell(1, 7).string('Reason')
      ws.cell(1, 8).string('Applied Date')
      ws.cell(1, 9).string('Status')

      for (const [indx,value] of Object.entries(refunds)) {
        const row = (parseInt(indx)+2)
        const item:any=value
        ws.cell(row, 1).string(''+item?.callId)
        ws.cell(row, 2).string(item?.appliedByUser.name)
        ws.cell(row, 3).string(item?.appliedForUser.name)
        ws.cell(row, 4).number(item?.amt||0)
        ws.cell(row, 5).number(item?.expertRefundAmount||0)
        ws.cell(row, 6).number(item.callDetail.duration||0)
        ws.cell(row, 7).string(item.reason?item.reason:'NA')
        const cdate = DateTime.fromISO(new Date(item.created).toISOString(),{setZone:true}).setZone('Asia/Kolkata').toFormat('dd, LLL yyyy t')
        ws.cell(row, 8).date(cdate)
        const status=item.status==1?'Applied':item.status==2?'Approved':item.status==3?'Rejected':'NA'
        ws.cell(row, 9).string(status)
      }
  
    const nf=`${new Date().getTime()}${'.xlsx'}`
    const file=Application.publicPath('files/')+nf
    const gFile = await new Promise( function(resolve,reject) {
                      wb.write(file, async function(err, stats) {
                        if (err) {
                          await reject(err)
                        } else {
                          await resolve(stats)
                        }
                      })
                  })
    if(gFile){
      return response.attachment(file,'pending_refund_data.xlsx')
    }
    session.flash({error:'Internal error, please try again.'})
    return response.redirect('back')
  }

}
