import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import { schema, rules,validator } from '@ioc:Adonis/Core/Validator'
import Agent from 'App/Models/Agent'
import Route from '@ioc:Adonis/Core/Route'
import AclList from 'App/Models/AclList'
import AclGroup from 'App/Models/AclGroup'
import GroupAcl from 'App/Models/GroupAcl'


export default class AgentsController {

  public async syncSystem({response,request,auth,session}:HttpContextContract){
    const allRoutes:any = await Route.toJSON().root
    const regex = new RegExp('Admin/\\b', 'g')
    let adminRoutes = allRoutes.filter((e)=> {
      const h = e.handler.toString()
      return h.match(regex)
    })
    if(adminRoutes.length>0){
      var aclList:any=[]
      for await (const item of adminRoutes) {
        const handle = item.handler.split('/')[1].split('.')
        const existAcl = await AclList.query().where({controller:handle[0],action:handle[1]}).first()
        var listData:any={}
        if(existAcl){
          listData=existAcl
        }else{
          listData = new AclList()
        }
        listData.controller=handle[0]
        listData.action=handle[1]
        listData.url=item?.pattern
        listData.name=item?.name
        listData.method=item?.methods[item?.methods.length-1]
        await listData.save()
      }
     // await AclList.aclListInFile()
      session.flash({success:'System Sync Done.'})
      return response.redirect('back')
    }
    session.flash({error:'System Sync Not Done.'})
    return response.redirect('back')
  }
  
  public async index({view,request,auth}:HttpContextContract){
    const page = request.input('page', 1)
    const limit = 20
    const groups = await AclGroup.query().withCount('acls').orderBy('id','desc').paginate(page,limit)
   // const acls = await AclGroup.getGroupAcls(auth.user?.userRole)
    groups.baseUrl(request.url()) 
    return view.render('admin/acl/index',{groups})
  }

  public async create({view,request,auth}:HttpContextContract){
    const aclList = await AclList.getList()
    //console.log('New List',aclList)
    return await view.render('admin/acl/add',{aclList})
  }

  public async saveGroup({view,request,auth,session,response}:HttpContextContract){
    const data=request.all()
    const groupName = data?.name||''
    const actions = data?.action||{}
    const checkGroup= await AclGroup.query().where('name',groupName).first()
    if(checkGroup){
      session.flash({error:'This group is already exist.'})
      return response.redirect('back')
    }
    const group = new AclGroup()
    group.name=groupName
    if(actions){
      const savedGroup = await group.save()
      const actList = await actions.map(elm=>{
        const newelm:any={}
        newelm.acl_list_id=elm
        newelm.acl_group_id=savedGroup?.id
        return newelm
      })
     //
     // await group.related('acls').createMany(actList)
      await GroupAcl.createMany(actList)
      //return actList
      session.flash({success:'This group is saved.'})
      return response.redirect('back')
    }
    //
    return actions
  }

  public async view({view,request,auth,session,response,params}:HttpContextContract){
    const uid=auth.user?.id
    const id=params?.id
    const group = await AclGroup.getGroup(id)
    const aclList = await AclGroup.getAclListForGroup(id)
    //return aclList
    return await view.render('admin/acl/view',{group,aclList})
  }

  public async editGroup({view,request,auth,session,response,params}:HttpContextContract){
    const data=request.all()
    const groupName = data?.name||''
    const actions = data?.action||{}
    const id=params?.id
    const group:any = await AclGroup.find(id)
          group.name=groupName
    if(actions){
      const actList = await actions.map(elm=>{
        const newelm:any={}
        newelm.acl_list_id=elm
        newelm.acl_group_id=group?.id
        return newelm
      })
      await group.save()
      await GroupAcl.query().where('acl_group_id',group?.id).delete()
      await GroupAcl.createMany(actList)
      session.flash({success:'This group is updated.'})
      return response.redirect('back')
    }
    //
    return actions
  }

  public async aclList({view,request,auth,session,response,params}:HttpContextContract){
    const aclList = await AclList.getList()
    return await view.render('admin/acl/list',{aclList})
  }

  public async changeAclName({request,auth,session,response}:HttpContextContract){
    const data = request.all()
    const id=data?.pk
    const acl:any = await AclList.find(id)
    acl.displayName=data?.value
    await acl.save()
    return 'success'
  }

}
