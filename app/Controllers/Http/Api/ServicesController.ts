import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import User from "App/Models/User"
import UserToken from "App/Models/UserToken"
import ResumeHelper from "App/Helpers/ResumeHelper"
const resumeHelper:any=new ResumeHelper()
import WebServiceLog from 'App/Models/WebServiceLog'
import Invoice from 'App/Models/Invoice'
import EmailHelper from "App/Helpers/EmailHelper"
import Application from '@ioc:Adonis/Core/Application'
import Env from '@ioc:Adonis/Core/Env'
const emailHelper:any=new EmailHelper()
const new_request = require('request')
const fs = require('fs')
let pdf = require("html-pdf")
const perf = require('execution-time')()
import LedgerHelper from 'App/Helpers/LedgerHelper'
const Ledger = new LedgerHelper()
import FirebaseHelper from 'App/Helpers/FirebaseHelper'
const firebaseHelper=new FirebaseHelper()
const ext = (url)=>{
  return (url = url.substr(1 + url.lastIndexOf("/")).split('?')[0]).split('#')[0].substr(url.lastIndexOf("."))
}
const fileName = (url)=>{
  return (url = url.substr(1 + url.lastIndexOf("/")).split('?')[0])
}

const download = async (uri, filename, callback)=>{
  await new_request.head(uri, async (err, res, body)=>{
    await new_request(uri).pipe(fs.createWriteStream(filename)).on('close', callback)
  })
}
const getrequest = require('request')
import NotificationHelper from 'App/Helpers/NotificationHelper'
const notificationHelper = new NotificationHelper()
import Request from 'App/Models/Request'
import Post from 'App/Models/Post'
import TransactionHistory from 'App/Models/TransactionHistory'
import Chat from 'App/Models/Chat'
import CallHistory from 'App/Models/CallHistory'
import AppNotification from 'App/Models/AppNotification'
import Refund from 'App/Models/Refund'
import ExpertSchedule from 'App/Models/ExpertSchedule'
import { DateTime,Duration } from 'luxon'
const expertSchedules = async(ids)=>{
  return ids
  //var schs:any= await ExpertSchedule.query().whereIn('id',ids).orderBy('id','asc')
  //  return schs
}
import SoxCommand from "sox-audio"

import CommonHelper from 'App/Helpers/CommonHelper'
const commonHelper = new CommonHelper()
import SearchHelper from 'App/Helpers/SearchHelper'
const searchHelper = new SearchHelper()

import TwilioHelper from 'App/Helpers/TwilioHelper'
import AppSetting from 'App/Models/AppSetting'
import LedgerAccount from 'App/Models/LedgerAccount'
import Event from '@ioc:Adonis/Core/Event'
import LikePost from 'App/Models/LikePost'
import FollowPost from 'App/Models/FollowPost'
import BlockList from 'App/Models/BlockList'
import FollowUser from 'App/Models/FollowUser'
import CmsPage from 'App/Models/CmsPage'
import Faq from 'App/Models/Faq'
import LoginHistory from 'App/Models/LoginHistory'
import Comment from 'App/Models/Comment'
const twilioHelper = new TwilioHelper()
import WhatsappHelper from 'App/Helpers/WhatsappHelper'
const whatsappHelper = new WhatsappHelper()
import TaskHelper from 'App/Helpers/TaskHelper'
const taskHelper = new TaskHelper()
import BookedSlot from 'App/Models/BookedSlot'
import MasterGodown from 'App/Models/MasterGodown'
import ZoomHelper from 'App/Helpers/ZoomHelper'
const zoomHelper = new ZoomHelper()

import Database from '@ioc:Adonis/Lucid/Database'
const date_suffix=(dt)=>
{
  return dt+(dt % 10 == 1 && dt != 11 ? 'st' : (dt % 10 == 2 && dt != 12 ? 'nd' : (dt % 10 == 3 && dt != 13 ? 'rd' : 'th')))
}
const monthNames = ["Jan", "Feb", "March", "April", "May", "June",
  "July", "Aug", "Sept", "Oct", "Nov", "Dec"
];

var calculateInterest = function (total,ratePercent, roundToPlaces) {
  var interestRate = ((ratePercent/100) - 1)
  return (total * interestRate).toFixed(roundToPlaces)
}
import UploadHelper from "App/Helpers/UploadHelper"
import AppUserEvent from 'App/Models/AppUserEvent'
const uploader:any=new UploadHelper()

export default class ServicesController {

  async getAppVersion({response}:HttpContextContract){
    const versionCode= await AppSetting.getValueByKey('version_code')
    const versionName=await AppSetting.getValueByKey('version_name')
    const skipable=await AppSetting.getValueByKey('skipable')
    return response.status(200).json({
      "versionCode": versionCode,
      "versionName":'version '+versionName,
      "skipable": skipable==1?true:false
    })
  }
  
  async readResume({auth,response}){
    const uid:any=auth.user?.id
    const user=await User.find(uid)
    let resumeFile=user?.resume
   //https://seekex.s3.ap-south-1.amazonaws.com/resume/1585664707790.pdf
   //https://seekex.s3.ap-south-1.amazonaws.com/resume/Resume.doc
   // let resumeFile='./public/Resume.doc'
   // let resumeFile='https://seekex.s3.ap-south-1.amazonaws.com/resume/Resume.doc'
    if(resumeFile){
      let fileExt=ext(resumeFile)
      let fileNme=fileName(resumeFile)
      try {
        await download(resumeFile, './public/'+fileNme, async function(){})
        var parseFile= await resumeHelper.parseResume(fileNme,fileExt)
        if(parseFile){
          console.log('file data',parseFile)
          return response.status(200).json({
            "logout": true,
            "resMsg": "Done",
            "status": true
          })
        }
      } catch (error) {
        console.log(error)
      }
    }
  }

  async updateUserToken({auth,response,request}:HttpContextContract){
    const uid:any=auth.user?.id
    const data=request.all()
    var userToken=await UserToken.query().where({user_id:uid,device_id:data.deviceId}).first()
    if(userToken){
      userToken.userToken=data.userToken
      userToken.fcmToken=data.fcmToken
      try {
        await userToken.save()
        return response.status(200).json({
          "data":userToken,
          "logout": true,
          "resMsg": "Done",
          "status": true
        })
      } catch (error) {
        await WebServiceLog.create({request:JSON.stringify(data),response:JSON.stringify(error),service_name:'User Token Service',status:1,uid:uid,time_taken:0})
      return response.status(201).json({
        "errors":error,
        "resMsg": "Error",
        "status": false,
        "logout": false
      })
      }
    }else{
      var newuserToken=new UserToken()
      newuserToken.userToken=data.userToken
      newuserToken.fcmToken=data.fcmToken
      newuserToken.userId=uid
      newuserToken.deviceId=data.deviceId
      try {
        await newuserToken.save()
        return response.status(200).json({
          "data":newuserToken,
          "logout": true,
          "resMsg": "Done",
          "status": true
        })
      } catch (error) {
        await WebServiceLog.create({request:JSON.stringify(data),response:JSON.stringify(error),service_name:'User Token Service',status:1,uid:uid,time_taken:0})
        return response.status(201).json({
          "errors":error,
          "resMsg": "Error",
          "status": false,
          "logout": false
        })
      }
    }
  }

  async invoiceRequest({auth,response,request,view}:HttpContextContract){
    perf.start()
    const uid:any=auth.user?.id
    const email=auth.user?.email
    const data=request.all()
    const invoiceData = await Invoice.find(data.invoice)
    const user = await User.find(uid)
    var tempFile = view.render('invoices/invoice',{invoiceData,user})
    let options = {
      "height": "11.25in",
      "width": "8.5in",
      "header": {
          "height": "20mm"
      },
      "footer": {
          "height": "20mm",
      },
    }
    const fileName = "invoice"+invoiceData?.id+".pdf"
    pdf.create(tempFile, options).toFile(Application.publicPath(fileName),async function (err,data) {
      if (err) {
        const res = perf.stop()
        await WebServiceLog.create({request:JSON.stringify(data),response:JSON.stringify(err),service_name:'Invoice email Service',status:1,uid:uid,time_taken:res.time})
        fs.unlink(Application.publicPath(fileName))
        return false
      } else {
        const sendMsg='<p>Hi Mr./Ms.'+auth.user?.name+'</p>,<p>As per your request, please find attached the GST invoice id INV-'+invoiceData?.id+' for call id '+invoiceData?.callId+'.For any discrepancy in this invoice, please reach out to us at accounts@seekex.in.</p><p>Thanks and Regards,</br>Team Seekex</br>Democratising Knowledge.</p>'

        var sendData={to:email,subject:'Invoice '+invoiceData?.id+' for call id '+invoiceData?.callId,message:sendMsg}
        await emailHelper.sendEmailInvoice(sendData,fileName)
        fs.unlink(Application.publicPath(fileName))
        console.log("File Sent successfully")
        return true
      }
    })
    return response.status(200).json({
      "logout": false,
      "resMsg": "Success",
      "status": true
    })
  }


  async cancelAppointmentRequest({auth,response,request}:HttpContextContract){
    perf.start()
    const uid:any=auth.user?.id
    const appId=request.input('requestId')
    const comment=request.input('reasontext')
    const appointment:any= await Request.query().where("id",appId).first()
    appointment.comment=comment
    appointment.canceledBy=uid
    appointment.status=3
    appointment.canceledDate=new Date()
    try {
        //*********************Notifications**********************/
        var notifyUserId
        if(uid==appointment?.requestedBy){
          notifyUserId=appointment?.requestedBy
        }else{
          notifyUserId=appointment?.toUid
        }
        var fromUser = await User.find(uid)
        var notifyUser= await LoginHistory.query().where("uid",notifyUserId).orderBy("id","desc").first()
        var notifyData={fcm:notifyUser?.fcmToken||request.input('fcm',''),date:appointment?.bookingDate,id:appointment?.id}
        var notify:any=await notificationHelper.sendNotifictionForCancelRequest(notifyData,fromUser)

        //*********************Notifications**********************/
      await appointment.save()
      await Ledger.releaseAmountForRequest(appointment)
      return response.status(200).json({
          "logout": false,
          "resMsg": "Appointment successfuly canceled.",
          "status": true
        })
    } catch (error) {
      const exeTime = perf.stop()
      await WebServiceLog.create({request:JSON.stringify(request.all()),response:JSON.stringify(error),service_name:'Appointment cancel service error',status:1,uid:uid,time_taken:exeTime.time})
      return response.status(201).json({
        "logout": false,
        "resMsg": "Internal error, please try again.",
        "status": false
      })
    }
  }

  async changeRequestStatus({auth,response,request}:HttpContextContract){
    perf.start()
    const uid:any=auth.user?.id
    const appId=request.input('requestId')||request.input('id')
    const comment=request.input('text')
    const status =request.input('status')
    const appointment:any= await Request.query().where("id",appId).first()
    appointment.comment=comment
    appointment.status=status
    if(uid==appointment?.requestedBy && status==2){
      return response.status(200).json({
        "logout": false,
        "resMsg": "Sorry! you can't take action on this call request.",
        "status": false
      })
    }
    if(status==2){
      var expert = await User.find(appointment?.toUid)
      var notifyUser= await LoginHistory.query().where("uid",appointment?.requestedBy).orderBy("id","desc").first()
      var notifyData={fcm:notifyUser?.fcmToken||request.input('fcm',''),id:appointment?.id,toUid:appointment?.requestedBy}
      var notify:any=await notificationHelper.sendNotifictionForAcceptRequest(notifyData,expert)
    }
    if(status==4){
      appointment.canceledBy=uid
      appointment.canceledDate=new Date()
      await Ledger.releaseAmountForRequest(appointment)
    }
    try {
      await appointment.save()
      if(status==4){
        await BookedSlot.query().where("requestId",appId).update({status:3})
      }
      return response.status(200).json({
          "logout": false,
          "resMsg": "Appointment status changed successfuly.",
          "status": true
        })
    } catch (error) {
      const exeTime = perf.stop()
      await WebServiceLog.create({request:JSON.stringify(request.all()),response:JSON.stringify(error),service_name:'Appointment cancel service error',status:1,uid:uid,time_taken:exeTime.time})
      return response.status(201).json({
        "logout": false,
        "resMsg": "Internal error, please try again.",
        "status": false
      })
    }
  }


/* Search Types
  //SEARCH_TYPE_BOTH = 1
  SEARCH_TYPE_POST = 2
  SEARCH_TYPE_USER = 3
  SEARCH_TYPE_REQUEST = 4
  SEARCH_TYPE_TRANSACTION = 5
  SEARCH_TYPE_CHAT = 6
  SEARCH_TYPE_CALL_HISTORY = 7
  SEARCH_TYPE_NOTIFICATION = 8
  SEARCH_TYPE_RATING = 9
  SEARCH_TYPE_REFUNDS = 10 
*/
  async searchData({auth,response,request}:HttpContextContract){
    perf.start()
    const authorization =  request.header('authorization')
    var uid
    if(authorization && authorization !='Bearer'){
      try {
        await auth.use('api').authenticate()
      } catch (error) {
        return response.status(200).json({
          "errors":error,
          "resMsg": "An Error",
          "status": false,
          "logout": true
        })
      }
      uid = auth.use('api')?.user?.id
    }
    await WebServiceLog.create({request:JSON.stringify(request.all()),response:JSON.stringify({}),service_name:'Search service data init',status:1,uid:uid,time_taken:0})
    const data=request.all()
    const keys=data?.keys||''
    const keyType=data?.keysType||''
    const type=data?.type||1
    var searchData:any
    var newData:any=[]
    var searchDataprofile:any
   // return keys
    var page:any = request.input('page', 1)
    if(page=='null' || page==null ||page=='NULL' || page==0){
      page=1
    }
    var lastPage
    const limit = 20
    const from_date=data?.from_date
    if(data?.to_date){
      const date=new Date(data?.to_date)
      var to_date=new Date(date.setDate(date.getDate() + 1)).toISOString().slice(0,10)
    }else{
      var to_date=''
    }
    const created=data?.created
    var created_to:any,created_from:any
    if(created==1){
        created_to = new Date()
        created_to.setUTCHours(0,0,0,0)
        created_from = new Date()
        created_from.setUTCHours(23,59,0,0)
    }
    if(created==2){
      var curr = new Date()
      var first = curr.getDate() - curr.getDay()
      var last = first + 6
      created_to = new Date(curr.setDate(first))
      created_to.setUTCHours(0,0,0,0)
      created_from = new Date(curr.setDate(last))
      created_from.setUTCHours(23,59,0,0)
    }
    if(created==3){
      var date = new Date()
      created_to = new Date(date.getFullYear(), date.getMonth(), 2)
      created_to.setUTCHours(0,0,0,0)
      created_from = new Date(date.getFullYear(), date.getMonth() + 1, 1)
      created_from.setUTCHours(23,59,0,0)
    }
    if(created==4){
      var date = new Date()
      created_to = new Date(date.getFullYear(), 0, 2)
      created_to.setUTCHours(0,0,0,0)
      created_from = new Date(date.getFullYear(), 12, 1)
      created_from.setUTCHours(23,59,0,0)
    }

    await WebServiceLog.create({request:JSON.stringify(request.all()),response:JSON.stringify(searchData),service_name:'Search service data start',status:1,uid:uid,time_taken:0})
    await searchHelper.search(keys,type,uid)
      if(type==2){ //post
        const post_search_by=data?.search_by
        const search_by_occu=data?.search_by_occu

        var postQuery = Post.query().preload('user',query=>{
          query.preload('highestEducation')
              .preload('occupation')
        }).preload('userTagged',query=>{
          query.preload('highestEducation')
              .preload('occupation')
          // .preload('accomplishmentsList')
          // .preload('certificatesList')
          // .preload('projectList')
          // .preload('country')
          // .preload('state')
          // .preload('skillsList')
          // .preload('intExpertiseList')

         }).preload('medias',(query)=>{
            query.select('url').where('media_type',5)
        })

        if(keys){
          postQuery.where((query)=>{
            query.where('description', 'LIKE', "%"+keys+"%").orWhere('hash_tags', 'LIKE', "%"+keys+"%")
            query.orWhereHas('user',query=>{
              query.where('name', 'LIKE', "%"+keys+"%")
            })
          })
          // postQuery.orWhere((query)=>{
          //   query.whereHas('user',query=>{
          //     query.where('name', 'LIKE', "%"+keys+"%")
          //   })
          //})
        }
        postQuery.where((query)=>{
          if(post_search_by==1){
            query.whereHas('user',query=>{
              query.where('expert',1)
            })
          }else if(post_search_by==2){
            query.whereHas('user',query=>{
              query.where('expert',0)
            })
          }
          if(search_by_occu){
            query.whereHas('user',query=>{
              query.whereHas('occupation',ocquery=>{
                ocquery.where('masterGodownId',search_by_occu).where('masterType', 1)
              })
            })
          }
        })
        postQuery.where((query)=>{
          if(from_date && to_date){
            query.whereBetween('created',[from_date,to_date])
          }
          if(created_to && created_from){
            query.whereBetween('created',[created_to,created_from])
          }
        })
        postQuery.whereHas('user',(q)=>{
          q.where('status',1)
        }).where('status',1)
        searchData = await postQuery.orderBy('id','desc').paginate(page,limit)
        lastPage = searchData.lastPage
        searchData=searchData.rows
        var newpostData:any=[]
        for (const element of searchData) {
          if(uid){
            const UserLike = await LikePost.query().where({targetId:element.id,uid:uid}).first()
            if(UserLike){
              element.likestatus=true
            }else{
              element.likestatus=false
            }
            const UserFollow = await FollowPost.query().where({targetId:element.id,uid:uid}).first()
            if(UserFollow){
              element.followstatus=true
            }else{
              element.followstatus=false
            }
          }else{
            element.likestatus=false
            element.followstatus=false
          }
           
          var newelement:any=element.serialize()
          if(element.medias.length>0){
            var mdImg:any=[]
            newelement.medias.forEach(elm => {
              mdImg.push(elm?.url)
            })
            newelement.medias=mdImg
          }
          if(newelement.userTagged.length>0){
            for await (const userProf of newelement?.userTagged) {
              userProf.profile = await User.find(userProf?.id)
            }
          }
          newelement.viewBy=[]

          newpostData.push({"postDTO":newelement})
        }
        searchData={
          "searchModels":newpostData,
          "logout": false,
          "resMsg": "Success",
          "status": true,
          "resultCount":newpostData.length||1,
          "currentPage":page,
          "totalPage":lastPage,
          "prevPage":page>1?page-1:null,
          "nextPage":page<lastPage?page+1:null
        }
      }
      if(type==3){ // expert
        const is_featured=data?.is_featured
        const is_verified=data?.is_verified
        const call_price_from=data?.call_price_from
        const call_price_to=data?.call_price_to
        const response_rate =data?.response_rate
        const sex_type=data?.sex_type
        const age_group=data?.age_group
        const feedback_score=data?.feedback_score
        const isfree=data?.isfree
        //return feedback_score
    var profiles = User.query()
          .preload('highestEducation')
          .preload('occupation')
          .preload('skillsList')
          .preload('currentOrg')
          .preload('collegeUniversity')
        profiles.where('expert',1).apply((scopes) => scopes.visible())
        
        profiles.where((query)=>{
            if(keys && keyType=='profile'){
              query.where('name', 'LIKE', "%"+keys+"%")
              query.orWhere('about_me', 'LIKE', "%"+keys+"%")
                    .orWhere('resume_text', 'LIKE', "%"+keys+"%")
            }else if(keys){
              query.where('name', 'LIKE', "%"+keys+"%")
              query.orWhere('about_me', 'LIKE', "%"+keys+"%")
                    .orWhere('resume_text', 'LIKE', "%"+keys+"%")
            }
            if(keys && keyType=='education'){
              query.whereHas('highestEducation',(query)=>{
                query.whereHas('master',(qu)=>{
                  qu.where('name', 'LIKE', "%"+keys+"%")
                })
              })
            }
            if(keys && keyType=='occupation'){
              query.whereHas('occupation',(query)=>{
                query.whereHas('master',(qu)=>{
                  qu.where('name', 'LIKE', "%"+keys+"%")
                })
              })
            }
            if(keys && keyType=='skills'){
              query.whereHas('skillsList',(query)=>{
                  query.whereHas('master',(qu)=>{
                    qu.where('name', 'LIKE', "%"+keys+"%")
                  })
                })
            }
            if(keys && keyType=='hashtags'){
              query.whereHas('hashtags',(query)=>{
                query.whereHas('master',(qu)=>{
                  qu.where('name', 'LIKE', "%"+keys+"%")
                })
              })
            }
            if(keys && keyType=='all'){
                query.orWhereHas('highestEducation',(query)=>{
                      query.whereHas('master',(qu)=>{
                        qu.where('name', 'LIKE', "%"+keys+"%")
                      })
                    })
                    .orWhereHas('occupation',(query)=>{
                      query.whereHas('master',(qu)=>{
                        qu.where('name', 'LIKE', "%"+keys+"%")
                      })
                    })
                    .orWhereHas('skillsList',(query)=>{
                      query.whereHas('master',(qu)=>{
                        qu.where('name', 'LIKE', "%"+keys+"%")
                      })
                    })
                    .orWhereHas('hashtags',(query)=>{
                      query.whereHas('master',(qu)=>{
                        qu.where('name', 'LIKE', "%"+keys+"%")
                      })
                    })
            }
          }).where((query)=>{
            
            if(feedback_score){
              if(feedback_score==1){
                query.whereHas('feedbacks',(query)=>{
                  query.avg('user_rating')
                },'>',4)
              }
              if(feedback_score==2){
                query.whereHas('feedbacks',(query)=>{
                  query.avg('user_rating')
                },'>=',3)
                .andWhereHas('feedbacks',(query)=>{
                  query.avg('user_rating')
                },'<=',4)
              }
              if(feedback_score==3){
                query.whereHas('feedbacks',(query)=>{
                  query.avg('user_rating')
                },'<',3).orDoesntHave('feedbacks')
              }
            }
            if(is_featured){
              query.where('featured',is_featured)
            }
            if(is_verified){
              query.where('verified',is_verified)
            }
            if(sex_type){
              query.where('genderId',sex_type)
            }
            if(age_group){
              if(age_group==5){
                query.where('age','>',50)
              }
            }
            if(isfree==1){
             // query.where('audioCallRate','=',0).orWhere('videoCallRate','=',0)
             query.where('verified',0)
            }
            if(isfree==2){
              //query.where('audioCallRate','>',0).orWhere('videoCallRate','>',0)
              query.where('verified',1)
            }
            
            if(response_rate){
              if(response_rate==1){
                query.where('callResponseRate','>',90)
              }
              if(response_rate==2){
                query.whereBetween('callResponseRate',[75,90])
              }
              if(response_rate==3){
                query.where('callResponseRate','<',75)
              }
            }

            if(age_group){
              if(age_group==1){
                query.whereBetween('age',[10,20])
              }
              if(age_group==2){
                query.whereBetween('age',[21,30])
              }
              if(age_group==3){
                query.whereBetween('age',[31,40])
              }
              if(age_group==4){
                query.whereBetween('age',[42,50])
              }
            }

            if(call_price_from && call_price_to){
              query.whereBetween('audioCallRatePerSec',[call_price_from,call_price_to]).orWhereBetween('videoCallRatePerSec',[call_price_from,call_price_to])
            }
            if(created_to && created_from){
              query.whereBetween('created',[created_to,created_from])
            }
          })
          // profiles.where((query)=>{
          //   query.where('audioCallRate','>',0).orWhere('videoCallRate','>',0)
          // })
          if(uid){
            profiles.where((query)=>{
              query.where('status',1).whereNot({id:uid})
            })
          }else{
            profiles.where((query)=>{
              query.where('status',1)
            })
          }
          

        searchData=await profiles
                      // .apply((scopes) => scopes.rate())
                      // .apply((scopes) => scopes.active())
                      .orderBy('id','desc').paginate(page,limit)
        lastPage = searchData.lastPage
        searchData=searchData.rows
        searchData = await Promise.all(searchData.map(async(elm:any)=>{
          if(elm.audioCallRate>0){
            elm.audioCallRate= await commonHelper.showCallAmount(elm?.audioCallRate||0,false,'VOIP')
          }
          if(elm.videoCallRate>0){
            elm.videoCallRate=await commonHelper.showCallAmount(elm?.videoCallRate||0,true,'VOIP')
          }
          var newElm:any = elm.serialize()
          var profileData = elm.serialize()
          newElm.profile =  profileData

          if(!newElm.highestEducation){
            newElm.highestEducation={name:'',parentId:0,masterType:0,imageUrl:'',used:0}
          }
          if(!newElm.occupation){
            newElm.occupation={name:'',parentId:0,masterType:0,imageUrl:'',used:0}
          }
          if(!newElm.rating){
            newElm.rating=0
          }
          if(!newElm.ratingCount){
            newElm.ratingCount=0
          }
          if(uid){
            newElm.block= await BlockList.query().where({byUid:newElm?.id,toUid:uid}).first()?true:false
            newElm.blocked= await BlockList.query().where({byUid:uid,toUid:newElm?.id}).first()?true:false
            newElm.followstatus = await FollowUser.query().where({uid:uid,targetId:newElm?.id}).first()?true:false
          }else{
            newElm.block= false
            newElm.blocked= false
            newElm.followstatus = false
          }

          if(newElm.profile?.hasOwnProperty('highestEducation')){
            delete newElm.profile['highestEducation']
          }
          if(newElm.profile?.hasOwnProperty('occupation')){
            delete newElm.profile['occupation']
          }
          if(newElm.profile?.hasOwnProperty('currentOrg')){
            delete newElm.profile['currentOrg']
          }
          if(newElm.profile?.hasOwnProperty('collegeUniversity')){
            delete newElm.profile['collegeUniversity']
          }
          newElm.genderId=parseInt(newElm.genderId)
          /** Rating And Responce Count */
            const rat= await commonHelper.userRating(newElm?.id)
            //newElm.conversations=await commonHelper.userConversations(newElm?.id)||0
            newElm.rating=rat?.rating
            newElm.ratingCount=rat?.count||0
          /** Rating And Responce Count */
          return {userProfileDTO:newElm}

        }))

        searchData={
          "searchModels":searchData,
          "logout": false,
          "resMsg": "Success",
          "status": true,
          "currentPage":page,
          "totalPage":lastPage,
          "prevPage":page>1?page-1:null,
          "nextPage":page<lastPage?page+1:null
        }
      }

      if(type==4 && uid){ //Request
        const from_amount=parseFloat(data?.from_amount)||parseFloat(data?.amount_from)
        const to_amount=parseFloat(data?.to_amount)||parseFloat(data?.amount_to)
        const call_type=data?.call_type
        const reqStatus=data?.status
        const call_duration_from=data?.call_duration_from
        const call_duration_to=data?.call_duration_to

        var requestQuery= Request.query()
                      //.preload('expert')
                      .preload('medias').preload('slots')
                      .where((query)=>{
                        query.where("requestedBy",uid)
                        .orWhere("toUid",uid)
                      })
        
        requestQuery.where((query)=>{
          if(keys){
            query
            .where('description', 'LIKE', "%"+keys+"%")
            .orWhere('estimatedCall', 'LIKE', "%"+keys+"%")
            .orWhere('estimatedCost', 'LIKE', "%"+keys+"%")
            .orWhereHas('expert',(query)=>{
              query.where('name', 'LIKE', "%"+keys+"%")
            })
          }
        })
        requestQuery.where((query)=>{
          if(call_type){
            if(call_type==1){
              query.where('videocall',0)
              query.where('callingType','VOIP')
            }
            if(call_type==2){
              query.where('videocall',1)
              query.where('callingType','VOIP')
            }
            if(call_type==3){
              query.where('videocall',0)
              query.where('callingType','Exotel')
            }
          }
          if(reqStatus){
            if(reqStatus==3){
              query.whereIn("status",[3,4])
            }else{
              query.where("status",reqStatus)
            }
          }
          if(call_duration_from && call_duration_to){
            query.whereBetween('estimatedCall',[call_duration_from,call_duration_to])
          }
          if(from_amount && to_amount){
            query.whereBetween('estimatedCost',[from_amount,to_amount])
          }
          if(from_date && to_date){
            query.whereBetween('created',[from_date,to_date])
          }
          if(created_to && created_from){
            query.whereBetween('created',[created_to,created_from])
          }
        })

        searchData = await requestQuery.orderBy('id','desc').paginate(page,limit)
        var resdata:any=[]
        if(searchData){
          const userTimeZone = auth.user?.timeZone||'UTC'
          for (const relement of searchData) {
            var schData:any=[]
            var element:any=relement
            if(element.slots){
                  var expertSchIds:any=[]
                  element.slots.forEach((elm:any)=>{
                    expertSchIds.push(elm.slotsId)
                  })
                  element.expertSchedules=await ExpertSchedule.query().whereIn('id',expertSchIds).orderBy('id','asc')
                }
            element.bookingDate=new Date(element.bookingDate)
            element.bookingDate=date_suffix(element.bookingDate.getDate())+' '+monthNames[element.bookingDate.getMonth()]
            if(element.requestedBy==uid){
              var userData={profile:await User.query().where({id:element.toUid}).first()}
              element.userProfile=userData
            }else{
              var userData={profile:await User.query().where({id:element.requestedBy}).first()}
              element.userProfile=userData
            }
            var newelement:any=element.serialize()
            if(element.medias.length>0){
              var mdImg:any=[]
              newelement.medias.forEach(elm => {
                mdImg.push(elm?.url)
              })
              newelement.medias=mdImg
              resdata.push({requestDTOS:newelement})
            }else{
              resdata.push({requestDTOS:element})
            }
          }
          await firebaseHelper.changeUserStatus(uid,1)
        }
        lastPage = searchData.lastPage
        searchData=searchData.rows

        searchData={
          "searchModels":resdata,
          "logout": false,
          "resMsg": "Success",
          "status": true,
          "currentPage":page,
          "totalPage":lastPage,
          "prevPage":page>1?page-1:null,
          "nextPage":page<lastPage?page+1:null
        }
      }
      if(type==5 && uid){ //Transections
        const from_amount=data?.from_amount||data?.amount_from
        const to_amount=data?.to_amount||data?.amount_to
        const tranType=data?.stype
        var transectionQuery = TransactionHistory.query().where('uid',uid)
        transectionQuery.where((query)=>{
          if(keys){
            query.where('transactionAmount', 'LIKE', "%"+keys+"%")
                  .orWhere('description', 'LIKE', "%"+keys+"%")
          }
        })
        transectionQuery.where((query)=>{
          if(tranType){
            if(tranType==1){
              query.where('isdebit',1)
            }
            if(tranType==2){
              query.where('isdebit',0)
            }
          }
          if(from_date && to_date){
            query.whereBetween('created',[from_date,to_date])
          }
          if(created_to && created_from){
            query.whereBetween('created',[created_to,created_from])
          }
          if(from_amount && to_amount){
            query.whereBetween('transactionAmount',[from_amount,to_amount])
          }
        })

        searchData = await transectionQuery.orderBy('id','desc').paginate(page,limit)
        lastPage = searchData.lastPage
        searchData=searchData.rows

        for (const element of searchData) {
          newData.push({"transactionHistories":element})
        }
        searchData={
          "searchModels":newData,
          "logout": true,
          "resMsg": "Success",
          "status": true,
          "currentPage":page,
          "totalPage":lastPage,
          "prevPage":page>1?page-1:null,
          "nextPage":page<lastPage?page+1:null
        }
      }
      if(type==6 && uid){ //Chats
        var chatQuery = Chat.query().where(query=>{
              query.where('senderId',uid).orWhere('receiverId',uid)
          })
        if(keys){
          chatQuery.where('message', 'LIKE', "%"+keys+"%")
        }

        searchData = await chatQuery.orderBy('id','desc').paginate(page,limit)
        lastPage = searchData.lastPage
        searchData=searchData.rows

        for (const element of searchData) {
          var newelm={chat:element}
          newData.push({"chats":newelm})
        }
        
        searchData={
          "searchModels":newData,
          "logout": true,
          "resMsg": "Success",
          "status": true,
          "currentPage":page,
          "totalPage":lastPage,
          "prevPage":page>1?page-1:null,
          "nextPage":page<lastPage?page+1:null
        }
      }
      if(type==7 && uid){ //Call History
        const is_recording=data?.is_recording
        const is_refunded=data?.is_refunded
        const call_duration_from=data?.call_duration_from*60
        const call_duration_to=data?.call_duration_to*60
        const call_type=data?.call_type
        const call_rating=data?.call_rating
        const call_price_from=data?.call_price_from
        const call_price_to=data?.call_price_to
        const userTimeZone = auth.user?.timeZone||'UTC'
        var callHistQuery = CallHistory.query()
                              .select('callTime','duration','initiater','receiver','requestId','video','id','callingType')
                              .preload('expert').preload('user').preload('refund')
                           
        if(is_refunded){
          if(is_refunded==1){
            callHistQuery.has('refund')
          }
          if(is_refunded==2){
            callHistQuery.doesntHave('refund')
          }
        }                  
        if(keys){
          callHistQuery.where((query)=>{
            query.whereHas('expert',(query)=>{
              query.where('name', 'LIKE', "%"+keys+"%")
            })
          })
        }
        callHistQuery.where((query)=>{
          query.where("initiater",uid).orWhere("receiver",uid)
        })
        callHistQuery.where((query)=>{
            if(call_rating){
                if(call_rating==1){
                  query.whereHas('callRating',(query)=>{
                      query.where('userRating','>',4)
                  })
              }
            if(call_rating==2){
              query.whereHas('callRating',(query)=>{
                query.whereBetween('userRating',[3,4])
              })
            }
            if(call_rating==3){
              query.whereHas('callRating',(query)=>{
                query.where('userRating','<',3)
              })
            }
           }
          if(call_type){
            if(call_type==1){
              query.where('video',0).where('callingType','VOIP')
            }
            if(call_type==2){
              query.where('video',1).where('callingType','VOIP')
            }
            if(call_type==3){
              query.where('video',0).where('callingType','Exotel')
            }
          }
          if(is_recording){
            if(is_recording==1){
              query.whereNotNull('recordingUrl').orWhereNotNull('recordingUrlNew')
            }
            if(is_recording==2){
              query.whereNull('recordingUrl').andWhereNull('recordingUrlNew')
            }
          }
          // if(call_rating){
          //   if(call_rating==1){
          //     query.where('initiaterRating','>',4).orWhere('receiverRating','>',4)
          //   }
          //   if(call_rating==2){
          //     query.whereBetween('initiaterRating',[3,4]).orWhereBetween('receiverRating',[3,4])
          //   }
          //   if(call_rating==3){
          //     query.where('initiaterRating','<',3).orWhere('receiverRating','<',3)
          //   }
          // }
          if(from_date && to_date){
            query.whereBetween('created',[from_date,to_date])
          }
          if(created_to && created_from){
            query.whereBetween('created',[created_to,created_from])
          }
          if(call_duration_from && call_duration_to){
            query.whereBetween('duration',[call_duration_from,call_duration_to])
          }
          if(call_price_from && call_price_to){
            query.whereBetween('callAmount',[call_price_from,call_price_to])
          }
        })

        searchData = await callHistQuery.orderBy('id','desc').paginate(page,limit)
        lastPage = searchData.lastPage
        searchData=searchData.rows


        // searchData = searchData.map(elm=>{
        //   var newelm={callHistory:elm}
        //   return {callHistoryResData:newelm}
        // })
        searchData = await Promise.all(searchData.map(async(call)=>{
          var dt:any={}
          call.duration = Math.floor(call.duration)
          call.callTime=DateTime.fromISO(new Date(call.callTime).toISOString(),{setZone:true}).setZone(userTimeZone).toFormat('dd, LLL yyyy t')
          dt.callHistory={
            "callTime":call?.callTime,
            "duration": call?.duration,
            "initiater": call?.initiater,
            "receiver": call?.receiver,
            "requestId": call?.requestId,
            "video": call?.video?true:false,
            "id": call?.id,
            "callingType": call?.callingType,
            "name": call?.name,
            "profileImage": call?.profileImage,
            "expertCallBackTime": call?.expertCallBackTime,
          }
          if(call.duration==0 || call.duration==null){
            dt.type=3
          }else if(call.initiater==uid){
            dt.type=1
          }else if(call.receiver==uid){
            dt.type=2
          }
          if(call?.initiater==uid){
            dt.name= call?.name||''
            dt.profilePic=call?.profileImage||call?.profilePic||''
            dt.uid=call?.receiver
            dt.verified=call?.expert.verified?true:false
            const rat = await commonHelper.callRating(call?.id)
            dt.rating=parseFloat((rat?.rating).toFixed(1))||0
            dt.ratingCount=rat?.count||0
          }else{
            dt.name= call?.user.name||''
            dt.profilePic=call?.user.profileImage||call?.user.profilePic||''
            dt.uid=call?.initiater
            dt.verified=call?.user.verified?true:false
            const rat = await commonHelper.callRating(call?.id)
            dt.rating=parseFloat((rat?.rating).toFixed(1))||0
            dt.ratingCount=rat?.count||0
          }
          
          if(call?.callingType=='Exotel'){
            dt.isExotel=true
          }else{
            dt.isExotel=false
          }
          delete dt.callHistory['user']
          delete dt.callHistory.expert
          return {callHistoryResData:dt}
      }))
        searchData={
          "searchModels":searchData,
          "logout": true,
          "resMsg": "Success",
          "status": true,
          "currentPage":page,
          "totalPage":lastPage,
          "prevPage":page>1?page-1:null,
          "nextPage":page<lastPage?page+1:null
        }
      }

      if(type==8 && uid){ //Notification
        const notiType=data?.stype
        var notificationQuery = AppNotification.query().where({uid:uid})
        if(keys){
          notificationQuery.where((query)=>{
            query.where('title', 'LIKE', "%"+keys+"%")
                  .orWhere('message', 'LIKE', "%"+keys+"%")
          })
        }
        if(notiType){
          notificationQuery.where('type',notiType)
        }
        if(from_date && to_date){
          notificationQuery.whereBetween('created',[from_date,to_date])
        }
        if(created_to && created_from){
          notificationQuery.whereBetween('created',[created_to,created_from])
        }
        searchData = await notificationQuery.orderBy('created','desc').paginate(page,limit)
        lastPage = searchData.lastPage
        searchData=searchData.rows

        for (const element of searchData) {
          newData.push({"appNotifications":element})
        }
        searchData={
          "searchModels":newData,
          "logout": true,
          "resMsg": "Success",
          "status": true,
          "currentPage":page,
          "totalPage":lastPage,
          "prevPage":page>1?page-1:null,
          "nextPage":page<lastPage?page+1:null
        }
      }
      
      // if(type==9){
      //   var ratingQuery
      // }
      
      if(type==10 && uid){ //Refund
        const refType=data?.stype
        const refStatus=data?.status
        const from_amount=data?.from_amount
        const to_amount=data?.to_amount
        const call_type=data?.call_type

        var refundQuery = Refund.query().preload('callDetail').where((query)=>{
                                      query.where('appliedBy',uid).orWhere('appliedFor',uid)
                                    })

        if(keys){
          refundQuery.where((query)=>{
                query.where('reason', 'LIKE', "%"+keys+"%").orWhere('callId', 'LIKE', "%"+keys+"%")
              })
        }
        refundQuery.where((query)=>{
          if(call_type){
            if(call_type==1){
              query.whereHas('callDetail',query=>{
                query.where('video',0).where('callingType','Twilio')
              })
            }
            if(call_type==2){
              query.whereHas('callDetail',query=>{
                query.where('video',1).where('callingType','Twilio')
              })
            }
            if(call_type==3){
              query.whereHas('callDetail',query=>{
                query.where('video',0).where('callingType','Exotel')
              })
            }
          }
        })
        refundQuery.where((query)=>{
          if(refType){
            query.where('type',refType)
          }
          if(refStatus){
            query.where('status',refStatus)
          }
          if(from_date && to_date){
            query.whereBetween('created',[from_date,to_date])
          }
          if(created_to && created_from){
            query.whereBetween('created',[created_to,created_from])
          }
          if(from_amount && to_amount){
            query.whereBetween('amt',[from_amount,to_amount])
          }
        })

        searchData = await refundQuery
                      .debug(true)
                      .orderBy('id','desc').paginate(page,limit)
        lastPage = searchData.lastPage
        searchData=searchData.rows

        for (const element of searchData) {
          newData.push({"refundDTO":element})
        }
        searchData={
          "searchModels":newData,
          "logout": true,
          "resMsg": "Success",
          "status": true,
          "currentPage":page,
          "totalPage":lastPage,
          "prevPage":page>1?page-1:null,
          "nextPage":page<lastPage?page+1:null
        }

      }

      if(searchData){
        const exeTime = perf.stop()
        await WebServiceLog.create({request:JSON.stringify(request.all()),response:JSON.stringify(searchData),service_name:'Search service data',status:1,uid:uid,time_taken:exeTime.time})
        return response.status(200).json(searchData)
      }else{
        return response.status(200).json({
          "searchModels":[],
          "logout": false,
          "resMsg": "No data",
          "status": false,
          "currentPage":page||0,
          "totalPage":lastPage||0,
          "prevPage":null,
          "nextPage":null
        })
      }
  }


  async searchSuggestion({auth,response,request}:HttpContextContract){
    // const uid:any=auth.user?.id
    // const data=request.all()
    // const keys=data?.keys
    // const type=data?.type
  //  if(type==3 && keys){
      // var suggetionOut={
      //   profile:keys+' search in profile',
      //   occupation:keys+' search in occupation',
      //   education:keys+' search in education',
      //   skills:keys+' search in skills',
      //   hashtags:keys+' search in hash tags',
      //   all:keys+' search in all'
      // }
      var suggetionOut=[
        {keyType:'profile',value:'Profile'},
        {keyType:'occupation',value:'Occupation'},
        {keyType:'education',value:'Education'},
        {keyType:'skills',value:'Skills'},
        {keyType:'hashtags',value:'Hashtags'},
        {keyType:'all',value:'All'}
      ]
      return response.status(200).json(suggetionOut)
   // }
    //return response.status(201).json({})
  }

  async getTimeZone({response}){
    var newElm:any=[]  
    JSON.parse(await fs.readFileSync('public/timezones.json'))
                .forEach((elm)=>{
                  elm.utc.forEach(element => {
                    newElm.push({value:element})
                  })
                })
    return response.status(200).json(newElm)
  }

  async getReferralCode({auth,response,request}:HttpContextContract){
    const uid:any=auth.user?.id
    const amount= await AppSetting.query().where({fieldKey:'user_ref_amount_all'}).first()
    return response.status(200).json({
      "data": {
        "cerditsId": uid,
        "code": auth.user?.referralCode,
        "id": 0,
        "referBy": 0,
        "referTo": 0,
        "status": true,
        "value": amount?.value||0
      },
      "logout": false,
      "resMsg": "string",
      "status": true
    })
  }

  async getCms({response,params}){
    const slug=params.slug
    const pageData:any = await CmsPage.query().where({slug:slug}).first()
    
    return response.status(200).json({
      "data":pageData?pageData.content:'',
      "logout": false,
      "resMsg": "all faqs",
      "status": true
    })
  }

  async checkApi({auth,response,request}:HttpContextContract){
    var https = require('https')
    const getMP3Duration = require('get-mp3-duration')
    const axios = require('axios')
    perf.start()
    const uid:any=auth.user?.id
    const data=request.all()
    const key=Env.get('EXOTEL_KEY') as string
    const token=Env.get('EXOTEL_TOKEN') as string
    const sid=Env.get('EXOTEL_SID') as string
    const CallSid='a340c98fc4ffa5ef5735a3746b161534'
    const allMData= await MasterGodown.all()
    var allD:any=[]
    // for await (const item of allMData) {
    //   if(item?.imageUrl){
    //     const d = await commonHelper.imageCropAndUpload(item?.imageUrl,item)
    //     allD.push(d)
    //   }
    // }
    //await CallHistory.finishCall(9)
   // return await CallHistory.query().where('id',9).update({room_sid:'RM5c3d06dce4d3fb7d8c8c6c1bcd8a6b6e'})
    return twilioHelper.retryUpdateCallDetails()
    //return commonHelper.checkMobileDevice(request.header('user-agent'))
    return commonHelper.bookingRequestSlotValidate(data)

    //return zoomHelper.createUserToken(1,1,1)
    return commonHelper.userConversations(1)
    // return allD
   // return await emailHelper.sendExpertApprovalEmail(auth?.user)
      // const Wallet:any=await LedgerAccount.query().where({name:'User',uid:1}).first()
      // Wallet.totalAmount=Wallet.totalAmount+1
      // await Wallet.save()
   // return taskHelper.approveUnActionRefund()
   //var str='2021-03-18'
   
  //  var v = DateTime.local().setZone('Asia/Kolkata')
  //  var j=new Date()
  //  return v.hour
    //return commonHelper.genTextImage('PRADEEP','')
    var command = SoxCommand()
    var fileName1='./public/1617692356049.mp3'
    var outFile='./public/recordings/test_file.mp3'
	command.input(fileName1)
  command.output(outFile)
  command.outputFileType('mp3').outputSampleRate(1600)
  command.on('prepare', function(args) {
    console.log('Preparing sox command with args ' + args.join(' '));
  });
   
  command.on('start', function(commandLine) {
    console.log('Spawned sox with command ' + commandLine);
  });
   
  command.on('progress', function(progress) {
    console.log('Processing progress: ', progress);
  });
   
  command.on('error', function(err, stdout, stderr) {
    console.log('Cannot process audio: ' + err.message);
    console.log('Sox Command Stdout: ', stdout);
    console.log('Sox Command Stderr: ', stderr)
  });
   
  command.on('end', async function() {
    console.log('Sox command succeeded!');
    var uploadFile= await uploader.uploadRecording('test_file.mp3','recordings',uid)
    console.log('Upload File',uploadFile)
  });
  
  const outcmd=command.run()
  return outcmd
  //  return {v,j}
  //   console.log(uid)
  //  return commonHelper.referralAmount('NP7J5E',244)
    return 
    return commonHelper.sendMobileOtp('918882204776')

    //return await firebaseHelper.changeUserStatus(uid,1)
   // return commonHelper.userRating(1)
   // return n

    const call = await CallHistory.find(3)
    return Ledger.calculateAndTransferAmountForCall(call)
   //const apiUrl ='https://'+key+':'+token+'@api.exotel.in/v1/Accounts/'+sid+'/Calls/203ce679638357ae2dc3a55718df1533.json?details=true'
    // const apiUrl ='https://'+key+':'+token+'@api.exotel.in/v1/Accounts/'+sid+'/Calls/'+CallSid+'.json?details=true'
    // const res = await axios.get(apiUrl)
    // return res?.data?.Call?.Details?.ConversationDuration
    //const call = await CallHistory.find(3)
    // return await twilioHelper.updateRoomDetails(call,'RMf46bdbb1ef7d37ab3b801a56ff8502d2',call?.receiver)
    // getrequest
      //     .get('https://s3-ap-southeast-1.amazonaws.com/exotelrecordings/seekextechnologies1/821941257319b59f1775098ae8b11532.mp3')
      //     .on('error', function(err) {
      //       console.log('error',err)
      //     })
      //     .pipe(fs.createWriteStream('./public/recordings/exotel/2.mp3'))
      //     .on('close',function(err, res, body){
      //       console.log('res',res)
      //     })
      //var call = await CallHistory.find(1889)
     // await commonHelper.downloadExotelFileAndUploadOnSeekex('https://s3-ap-southeast-1.amazonaws.com/exotelrecordings/seekextechnologies1/821941257319b59f1775098ae8b11532.mp3',call)

    // await download('https://s3-ap-southeast-1.amazonaws.com/exotelrecordings/seekextechnologies1/821941257319b59f1775098ae8b11532.mp3','./public/recordings/exotel/821941257319b59f1775098ae8b11532.mp3',async ()=>{
      
    //  // var uploadFile= await uploadHelper.uploadRecordingExotel(fileNme,'recordings',call?.uid)
    //   const buffer = await fs.readFileSync('./public/recordings/exotel/821941257319b59f1775098ae8b11532.mp3')
    //   const duration = getMP3Duration(buffer)
    //   var fduration=Math.round(duration/1000)
    //   console.log('res',fduration)
    // })
     return 'done';
   // return await notificationHelper.busyUserNotification({expertId:38})
    //return await notificationHelper.sendNotificationForCommentToFollowers(data,uid)
    //return await taskHelper.freeImageApiForInterest('Art')
    // const comment:any=new Comment()
    // comment.comment=data.comment?data.comment:data.keyValue
    // comment.postId=data.postId||data.id
    // comment.uid=uid
    // comment.likes=0
    //csi-YJA5QciOZ9W_VnSbsa:APA91bFiXB6Hxa0tK4E3oe8zfsJcDqjh65z7Pn1yUvVmEb7Q6NzxcX50TQ9c4BSG4srkAd6NHZPXYlTusZkHiFFx97ionKGaWoSQgfWjHrpegTWLcZWlsAXxoVdXRlQs_lpU4vKsOfcl
   //  const fileN = 'https://s3-ap-southeast-1.amazonaws.com/exotelrecordings/aboutme1/177f552a65cc864e79c136cb4ef1152g.mp3'

    //  let buff = new Buffer.from(0)
    //  const r = getrequest
    //         .get(fileN)
    //         .on('data', chunk => {
    //             buff = Buffer.concat([buff, chunk]);
    //             if (buff.length > 10) {
    //                 r.abort();
    //                 // deal with your buff
    //             }
    //         })
    // const call = await CallHistory.find(1)
    // const set= await AppSetting.getValueByKey('exotel_charge')
    // return set
    // return await Ledger.calculateAndTransferAmountForCall(call)

    //return await Ledger.calculateAndTransferAmountForCall(call)
     
    //  const fileContents=getrequest.get(fileN)
    //  const getMP3Duration = require('get-mp3-duration')
    //  const fileNme = fileName(fileN)
    //  return await download(fileN, './public/'+fileNme, async function(){
    //     const buffer = await fs.readFileSync('./public/'+fileNme)
    //     const duration = getMP3Duration(buffer) 
    //       console.log(Math.round(17.6))
    //       return duration
    //       //fs.unlink(Application.publicPath(fileNme))
    //   })
      // const buffer = await fs.readFileSync('./public/b2826d0f5362c63ada4a89b210051523.mp3')
      // const duration = getMP3Duration(buffer) 
      //   console.log(duration/1000)
 //duration
    // const fileContent = fs.readFileSync('https://s3-ap-southeast-1.amazonaws.com/exotelrecordings/seekextechnologies1/b2826d0f5362c63ada4a89b210051523.mp3')
    // mp3Duration(fileN, function (err, duration) {
    //   if (err) return console.log(err.message);
    //   console.log('Your file is ' + duration + ' seconds long');
    // });
    // https.get(fileN, response => {
    //   response.pipe(file);
    // });
    // return
   // return fileContent

   // return await whatsappHelper.sendMsg(comment)
    
    // const data=request.all()
   // return await notificationHelper.sendNotificationForCommentToFollowers(comment)
    //return exeTime.time
   // return new Date().getTimezoneOffset()/60
   // return twilioHelper.createUserToken(uid,'test-room')
      //console.log("start",new Date())
      // for(let i = 0; i < 1000000000; i++){
      // }
      // for(let i = 0; i < 1000000000; i++){
      // } 
      //   console.log("end",new Date())

      // console.log('start',new Date());
      // const promiseOne = new Promise((res, rej) => {
      //   console.log('first')
      //     for(let i = 0; i < 100000000; i++){

      //     }
      //     console.log('first end')   
      // })
      // const promiseTwo =new Promise((res, rej) => {
      //   console.log('second')
      //     for(let i = 0; i < 100000000; i++){
            
      //     }
      //   console.log('second end')  
      // })
      // Promise.all(([promiseOne, promiseTwo]))
      // console.log('end', new Date())
     // const exeTime = perf.stop()

     // console.log('taken time',exeTime.time)
    //  const profile:any=await User.find(1)
    //   Event.emit('new:regEmail',profile)
    //const uid= Math.floor((Math.random() * 100) + 1)
    //await firebaseHelper.userStatus(uid)
  //   return await firebaseHelper.deleteCallRooms()
  //  return  uid
   // return await firebaseHelper.oflineUserStatus()
   // return await firebaseHelper.oflineUserStatusByCall()
    // var answer = 56.6-((56.6/118)*100)
    // return answer
   //var call = await CallHistory.find(36)

   //return await Ledger.calculateAndTransferAmountForCall(call)

    

  //   let file='https://seekex.s3.amazonaws.com/temp/1603695848006.wav'
  //  // const fileNme = fileName(file)
  //   // await download(file, './public/'+fileNme, async function(){
  //   //   console.log('success')
  //   // })
  //   // await download(file, './public/'+fileNme, async function(){
  //   //   console.log('success')
  //   // })
  //   return 'Success'
  //   //return  Math.ceil(await commonHelper.estimatedDuration(uid,1,false))

  //   return (Math.round(0 * 100) / 100).toFixed(2);
    
    
  }

  async checkNotify({auth,response,request}:HttpContextContract){
      const data=request.all()
      var notifyUser= 'jj'//await LoginHistory.query().where("uid",data?.uid).orderBy("id","desc").first()
      const info:any={}
      const payload:any={}
      if(notifyUser){
        info.title=data?.title||'Test Notify'
        info.body=data?.title||'This is a test notification'
        info.payload=payload
        info.type=data?.type||1
        try {
          const status =  await firebaseHelper.sendNotifictionNew(info,'f3sRMWk1Q_2jq4oIuOiaEq:APA91bFr3GKdklg7c-Ua4lLDtkNH4zjlbgcghmfbjsVL2rZrPVoToD8GodasGav4k4ANjlaCJ98-PpLHHrlNDQZQl91P4w0vZsQtvD2VinkdleA00wJ_sMCsJpEM7mR71clFzIE6S1-8')
          //dzyDLvR8QLe2MXv903gMj1:APA91bFxj-vacf7uXnN-D18SMt-rTXKRldpCAQXTAXjBSnO_0EQZYBJu4oDzlSRBIZOr-rL0VVNrapJQSNI_vIphbavQ2P3pYQ-kC8pIjY0IJHzQOjsMMAEvL3S9thQ7Kd1PdBlq2tvs
          return response.status(200).json(status)
          
        } catch (error) {
          return response.status(200).json(error)
        }
      }
      return response.status(200).json({'msg':'Not Send'})
  }

  async checkNotifyForCall({auth,response,request}:HttpContextContract){
    perf.start()
    const data=request.all()
    const payload:any={}
    const info:any={}
   
    const fromUser:any= await User.find(data?.uid)
    var notifyUser= await LoginHistory.query().where("uid",data?.uid).orderBy("id","desc").first()
    const fcm = notifyUser?.fcmToken
    payload.callDto=data
    payload.activity="com.seekx.webrtcSdk.call.CallActivity"
    payload.profile=fromUser?fromUser:{}
    info.title='Incoming Call Notification'
    info.body = 'Incoming Call Notification From '+fromUser?.name
    info.payload=JSON.stringify(payload)
    info.type='2'
    info.room_id=data?.id+'_room'
    info.sound ="default"
    // const notifId= await this.insertNotification(info,fromUser?.id,NOTIFICATION_TYPE_CALL)
    // info.notification_id=''+notifId
    info.priority='high'
    try {
      const status=await firebaseHelper.sendNotifictionNew(info,fcm)
      return response.status(200).json(status)
    } catch (error) {
      return response.status(200).json(error)
    }
  }

  async saveUserEvent({response,request,auth}:HttpContextContract){
    perf.start()
    const uid:any=auth.user?.id
    const data = request.all()
    const existData = await AppUserEvent.query().where({event_id:data?.event_id,userId:uid}).first()
    var saveEventData:any
    if(existData){
      existData.eventName=data?.event_name
      existData.status=data?.status
      saveEventData=existData
    }else{
      const newEvent = new AppUserEvent()
      newEvent.eventId=data?.event_id||0
      newEvent.eventName=data?.event_name||''
      newEvent.status=data?.status||false
      newEvent.userId=uid
      saveEventData=newEvent
    }
    try {
      await saveEventData.save()
      const userAllEvents = await AppUserEvent.query().where({userId:uid})
      return response.status(200).json({
        "events":userAllEvents,
        "logout": false,
        "resMsg": "Event data saved",
        "status": true
      })
    } catch (error) {
      const exeTime = perf.stop()
      await WebServiceLog.create({request:JSON.stringify(request.all()),response:JSON.stringify(error),service_name:'User Event data service error',status:1,uid:uid,time_taken:exeTime.time})
      return response.status(201).json({
        "logout": false,
        "resMsg": "Internal error, please try again.",
        "status": false
      })
    }
  }


  async zoomToken({response,request}:HttpContextContract){
    const data=request.all()
    const uid=data?.uid
    const meetingId=data?.meetingId
    const role=data?.role||0
    const token = await zoomHelper.createUserToken()
    //return token
    return response.status(200).json(token)
  }

}
