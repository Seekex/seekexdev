import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import WebServiceLog from 'App/Models/WebServiceLog'

const circleNO= {'DL':'08047094435'}
const defaultNo="08047094435"
const outgoingNo="+918047094435"


export default class CrmController{

  async connectToAgent({response,request }:HttpContextContract){
		const indata=request.get()
		const CallFrom = indata.CallFrom
			await WebServiceLog.create({request:JSON.stringify(indata),response:JSON.stringify(CallFrom),service_name:'Call Center Api connectToAgent',status:1,uid:0,time_taken:0})
			return response.status(200).header('Content-type', 'application/json').json({})
		}
	
}
