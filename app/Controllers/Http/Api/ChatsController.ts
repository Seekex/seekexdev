import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import Application from '@ioc:Adonis/Core/Application'
import Chat from "App/Models/Chat"
import WebServiceLog from "App/Models/WebServiceLog"
import AppNotification from "App/Models/AppNotification"
import UploadHelper from "App/Helpers/UploadHelper"
const uploader:any=new UploadHelper()
import FirebaseHelper from 'App/Helpers/FirebaseHelper'
const firebaseHelper=new FirebaseHelper()
import Database from '@ioc:Adonis/Lucid/Database'
import User from 'App/Models/User'
import { DateTime } from 'luxon'
import NotificationHelper from 'App/Helpers/NotificationHelper'
import LoginHistory from 'App/Models/LoginHistory'
const notificationHelper = new NotificationHelper() 
export default class ChatsController {

  async getInboxUser({response,auth,request }:HttpContextContract){
    const uid:any=auth.user?.id
    const data=request.all()
    const userTimeZone = auth.user?.timeZone||'UTC'
   // return auth.user
    try {
      // var chats = await (await Chat.query().select('id','senderId','media','message','receiverId','size','status','type','url','timeStamp')
      //           .preload("sender_user")
      //           .preload("receiver_user")
      //           .where((query)=>{
      //             query.where('senderId',uid).orWhere('receiverId',uid)
      //           })
      //           .whereNotNull('senderId').whereNotNull('receiverId').groupBy('senderId').groupBy('receiverId').orderBy('senderId','desc').orderBy('id','desc'))
      //           .map((element:any)=>{
      //               var msg:any={}
      //               var senderUser=element.sender_user
      //               var reciver =element.receiver_user
      //               msg.chat=element
      //               if(element.receiverId==uid){
      //                 msg.userProfileDTO={"profile":senderUser}
      //               }else{
      //                 msg.userProfileDTO={"profile":reciver}
      //               }
      //               return msg
      //             })
          const queryRaw="SELECT c.*  FROM chats c JOIN ( SELECT MAX(id) as id, CASE WHEN `receiver_id` = ? THEN sender_id WHEN `sender_id` = ? THEN receiver_id END AS users FROM chats GROUP BY CASE WHEN `receiver_id` = ? THEN sender_id WHEN `sender_id` = ? THEN receiver_id END HAVING users IS NOT NULL ) msg ON c.id = msg.id order by c.id desc;"

       var rawChats = await Database.rawQuery(queryRaw,[uid,uid,uid,uid])
       var chats = await Promise.all(rawChats[0].map(async(element:any)=>{
                      element.senderId=element.sender_id
                      element.receiverId=element.receiver_id
                     // const localTime = DateTime.local(element.time_stamp)
                     element.timeStamp=DateTime.fromISO(new Date(element.time_stamp).toISOString(),{setZone:true}).setZone(userTimeZone).toFormat('dd, LLL yyyy t')
                      var count:any = await Chat.query().where({'senderId':element.sender_id,'receiverId':uid,'status':1}).pojo<{ total: number }>().count('id as total')
                   //   newElement.unreadMsgCount==count[0]?.count
                      var msg:any={}
                     
                      msg.chat=element
                      msg.chat.unreadMsgCount=count[0]?.total
                      msg.unreadMsgCount=count[0]?.total
                      if(element.receiverId==uid){
                        var senderUser=await User.find(element.sender_id)
                        msg.userProfileDTO={"profile":senderUser}
                      }else{
                        var reciver =await User.find(element.receiver_id)
                        msg.userProfileDTO={"profile":reciver}
                      }
                      return msg
       }))

     // return response.status(200).json(chats)
      return response.status(200).json({
          "data":chats,
          "logout": true,
          "resMsg": "Get inbox users",
          "status": true
      })
    } catch (error) {
      await WebServiceLog.create({request:JSON.stringify(data),response:JSON.stringify(error),service_name:'Get Inbox User Service',status:1,uid:uid,time_taken:1})
      return response.status(201).json({
          "errors":error,
          "logout": false,
          "resMsg": "Get inbox users error",
          "status": false
      })
    }
  }

  async sendChat({response,auth,request}:HttpContextContract){
    const uid=auth.user?.id
    const data:any=request.all()
    const chat=data?.chat
    const userTimeZone = auth.user?.timeZone||'UTC'
    const mediaFile:any=request.file('media')
    var uploadUrl:any=''
    var mediasize=0
    var msgType=14
    var allChats:any=[]
    if(chat){
      if(mediaFile){
        const fileName=`${new Date().getTime()}.${mediaFile.extname}`
         await mediaFile.move(Application.tmpPath('uploads'),{
           name: fileName,
         })
         var uploadFolder=mediaFile.type=='image'?'chat/image':mediaFile.type=='application'?'chat/file':mediaFile.type=='video'?'chat/video':mediaFile.type=='audio'?'chat/audio':'chat'
         uploadUrl= await uploader.uploadChatFile(fileName,mediaFile.type,uid,5,0,uploadFolder)
         mediasize= await uploader.getFileSize(Application.tmpPath('uploads/'+fileName))
         msgType = mediaFile.type=='image'?10:mediaFile.type=='application'?13:mediaFile.type=='video'?12:mediaFile.type=='audio'?11:14
       }
       
       try {
         var chatMsg:any
         if(chat?.message ||chat?.url){
           chatMsg = new Chat()
         //  chatMsg.url=uploadUrl?.Location||''
           chatMsg.media=chat?.url?true:false
           chatMsg.message=chat?.message
           chatMsg.url=chat?.url
           chatMsg.receiverId=chat?.receiverId
           chatMsg.senderId=uid
           chatMsg.size=mediasize||chat?.size
           chatMsg.status=1
           chatMsg.type=chat?.type||msgType
          //chatMsg.timeStamp=new Date()
          /*************************Notification Chat************************/
            
            if(chat?.message){
              var fromUser = await User.find(uid)
              var notifyUser= await LoginHistory.query().where("uid",chat?.receiverId).orderBy("id","desc").first()
              var notifyData={fcm:notifyUser?.fcmToken||request.input('fcm',''),msg:chat?.message,uid:chat?.receiverId,chatMsg:chatMsg}
              var notify:any=await await notificationHelper.sendNotifictionForChat(notifyData,fromUser)
            }
          /*************************Notification Chat************************/ 
          await chatMsg.save()
         }else{
           chatMsg=''
         }
         if(chat?.receiverId){
           allChats = await Chat.query().where({receiverId:chat?.receiverId,senderId:uid})
                                        .orWhere({receiverId:uid,senderId:chat?.receiverId})
                                        .orderBy('id','desc').limit(50)
            allChats = allChats.map((elm)=>{
              elm.timeStamp=DateTime.fromISO(new Date(elm.timeStamp).toISOString(),{setZone:true}).setZone(userTimeZone).toFormat('dd, LLL yyyy t')
              return elm
            })                              
            allChats.sort(function(a, b) {
              if (a.id < b.id) return -1
            })                       
         }
         return response.status(200).json({
           "block": false,
           "blocked": false,
           "chats": allChats,
           "logout": true,
           "resMsg": "Message sent",
           "status": true
         })
       } catch (error) {
         await WebServiceLog.create({request:JSON.stringify(data),response:JSON.stringify(error),service_name:'Chat Message Service error',status:1,uid:uid,time_taken:0})
         return response.status(201).json({
             "errors":error,
             "logout": false,
             "resMsg": "Chat message sent failed",
             "status": false
         })
       }

    }else{
      allChats = await Chat.query().where({receiverId:data?.receiverId,senderId:data?.senderId||uid})
                                   .orWhere({receiverId:data?.senderId||uid,senderId:data?.receiverId})
                                   .orderBy('id','desc').limit(50)
      allChats = allChats.map((elm)=>{
        elm.timeStamp=DateTime.fromISO(new Date(elm.timeStamp).toISOString(),{setZone:true}).setZone(userTimeZone).toFormat('dd, LLL yyyy t')
        return elm
      })         
    allChats.sort(function(a, b) {
      if (a.id < b.id) return -1
    })  
    await firebaseHelper.changeUserStatus(uid,1)                      
      return response.status(200).json({
        "block": false,
        "blocked": false,
        "chats": allChats,
        "logout": false,
        "resMsg": "Message sent",
        "status": true
      })
    }
  }

  async changeMsgStatus({response,auth,request}:HttpContextContract){
    const uid=auth.user?.id
    const data:any=request.all()
    const status=data.status
    var notifId=data.id
     try {
       if(notifId){
          const notification:any=await AppNotification.find(notifId)
          notification.status=true
          await notification.save()
       }
       await Chat.query().whereIn('id', data?.ids).update({ status:status})
       return response.status(200).send(
        {
          "logout": false,
          "resMsg": "Success",
          "status": true
        }
       )
     }catch (error) {
      await WebServiceLog.create({request:JSON.stringify(data),response:JSON.stringify(error),service_name:'change Msg Status Service',status:1,uid:uid,time_taken:0})
      return response.status(201).json({
          "logout": false,
          "resMsg": "Change Msg Status failed",
          "status": false
      })
     }
  }
}
