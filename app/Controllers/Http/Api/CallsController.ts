import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import CallHistory from 'App/Models/CallHistory'
import WebServiceLog from 'App/Models/WebServiceLog'
import PendingRating from 'App/Models/PendingRating'
import UserRating from 'App/Models/UserRating'
import UserBusyStatus from 'App/Models/UserBusyStatus'
import PendingRecording from 'App/Models/PendingRecording'
import Invoice from 'App/Models/Invoice'
import AppSetting from "App/Models/AppSetting"
import SoxCommand from "sox-audio"
//import Application from '@ioc:Adonis/Core/Application'
//var fs = require('fs');
import Env from '@ioc:Adonis/Core/Env'
const key=Env.get('EXOTEL_KEY') as string
const token=Env.get('EXOTEL_TOKEN') as string
const sid=Env.get('EXOTEL_SID') as string
//const formUrlEncoded = x =>Object.keys(x).reduce((p, c) => p + `&${c}=${encodeURIComponent(x[c])}`, '')
const axios = require('axios')
import UploadHelper from "App/Helpers/UploadHelper"
const uploader:any=new UploadHelper()
import Application from '@ioc:Adonis/Core/Application'
import Event from '@ioc:Adonis/Core/Event'
import Request from 'App/Models/Request'
import LedgerAccount from 'App/Models/LedgerAccount'
import User from 'App/Models/User'
import OnlineOfflineStatus from 'App/Models/OnlineOfflineStatus'
import BookedSlot from 'App/Models/BookedSlot'
import ExpertSchedule from 'App/Models/ExpertSchedule'
import SeekerCallMap from 'App/Models/SeekerCallMap'
import NotificationHelper from 'App/Helpers/NotificationHelper'
const notificationHelper = new NotificationHelper()
import TwilioHelper from 'App/Helpers/TwilioHelper'
const twilioHelper = new TwilioHelper()
import FirebaseHelper from 'App/Helpers/FirebaseHelper'
const firebaseHelper = new FirebaseHelper()
import CommonHelper from 'App/Helpers/CommonHelper'
const  commonHelper = new CommonHelper()
import { DateTime } from 'luxon'
// const fetch = require('node-fetch')
// var NodeRequest = require('request')
const isToday = (someDate) => {
  const today = new Date()
  return someDate.getDate() == today.getDate() &&
    someDate.getMonth() == today.getMonth() &&
    someDate.getFullYear() == today.getFullYear()
}

const getMinutes=(str)=>{
	var time:any = str.split(':')
	return time[0]*60+time[1]*1
}
const getMinutesNow=()=>{
	var timeNow = new Date()
	return timeNow.getHours()*60+timeNow.getMinutes()
}
const isTime = (stime,endTime)=>{
	var now = getMinutesNow()
	var start = getMinutes(stime)
	var end = getMinutes(endTime)
	if (start > end) end += getMinutes('24:00')
	if ((now >= start) && (now < end)) {
		return true
	}else{
		return false
	}
}
const circleNO= {'DL':'08047094435'}
const defaultNo="08047094435"
const outgoingNo="+918047094435"
// const circleNO= {'DL':'011-412-19457'}
// const defaultNo="01141219457"
// const outgoingNo="+911141219457"

import LedgerHelper from "App/Helpers/LedgerHelper"
const Ledger:any=new LedgerHelper()
import Refund from 'App/Models/Refund'
import AppNotification from 'App/Models/AppNotification'

const perf = require('execution-time')()
const request = require('request')
const fs = require('fs')
const ext = (url)=>{
  return (url = url.substr(1 + url.lastIndexOf("/")).split('?')[0]).split('#')[0].substr(url.lastIndexOf("."))
}
const fileName = (url)=>{
  return (url = url.substr(1 + url.lastIndexOf("/")).split('?')[0])
}

const download = function(url, filename, callback){
  request.head(url, function(err, res, body){
    console.log('content-type:', res.headers['content-type']);
    console.log('content-length:', res.headers['content-length']);
    request(url).pipe(fs.createWriteStream(filename)).on('close', callback);
  })
}
import ZoomHelper from 'App/Helpers/ZoomHelper'
const zoomHelper = new ZoomHelper()
export default class CallsController{

  async connectCall({response,request }:HttpContextContract){
		const indata=request.get()
		const CallFrom = indata.CallFrom
		var callDetail:any = await SeekerCallMap.query().where({fromNo:CallFrom,status:0}).first()
		await WebServiceLog.create({request:JSON.stringify(indata),response:JSON.stringify({}),service_name:'Exotel Call Service on connect start data',status:1,uid:0,time_taken:0})
		if(callDetail){
			callDetail.flowId=indata.flow_id
			await callDetail.save()
			var re =/^(\+91-|\+91)/
			var expertNo = callDetail?.toNo
			//return re.test(expertNo)
			if(!re.test(expertNo)){
				expertNo = '+91'+expertNo.replace(/^0+/, '')
			}

			var data={
				"fetch_after_attempt":false,
				"destination": {
					"numbers":[expertNo]
				},
			 "outgoing_phone_number":outgoingNo,
			 "record": true,
			 "recording_channels":"dual",
			 "max_ringing_duration":45,
			 "max_conversation_duration":callDetail?.estimatedDuration>3600?3600:callDetail?.estimatedDuration>0?callDetail?.estimatedDuration:3600||60,
			 "music_on_hold": {
					"type":"operator_tone"
			 },
			 "start_call_playback": {
					"type":"text",
					"value":"Please wait while we connect to an expert. Your call will be recorded for quality and training purposes."
				}
			}
			//await WebServiceLog.create({request:JSON.stringify(data),response:JSON.stringify("data"),service_name:'Exotel Call Service on connect',status:1,uid:0,time_taken:0})
			return response.status(200).header('Content-type', 'application/json').json(data)
		}else{
			var data2={
				"fetch_after_attempt":false,
				"destination": {
					"numbers":[]
				},
			 "outgoing_phone_number":outgoingNo,
			 "record": true,
			 "recording_channels":"dual",
			 "max_ringing_duration":45,
			 "max_conversation_duration":60,
			 "music_on_hold": {
					"type":"operator_tone"
			 },
			 "start_call_playback": {
					"type":"text",
					"value":"Sorry! We are not able to connect, please try again"
				}
			}
			await WebServiceLog.create({request:JSON.stringify(data2),response:JSON.stringify("data2"),service_name:'Exotel Call Service on connect',status:1,uid:0,time_taken:0})
			return response.status(200).header('Content-type', 'application/json').json(data2)
		}
	}

	async getExpertNo({ request,response,auth}: HttpContextContract){
		perf.start()
		const uid:any=auth.user?.id
		const data=request.all()
		const expertId=data.expertId||data?.receiver
		const userData:any= await User.query().where({id:uid}).first()
		const expertData:any= await User.query().where({id:expertId}).first()
		const wallet:any = await LedgerAccount.query().where({uid:uid,name:'User'}).first()
		const walletAmount=wallet.availableAmount>0?wallet?.availableAmount:0
		const callRate=expertData.audioCallRate||0
		var receiverExpert:any = await User.find(data?.receiver)
		await WebServiceLog.create({request:JSON.stringify(data),response:JSON.stringify({}),service_name:'Exotel call Data Api',status:1,uid:0,time_taken:0})
		if(data?.requestId){
			const currentDate=new Date().toISOString().split('T')[0]
			var callrequest = await Request.query().preload('slots',function(query){
						query.preload('schedule')
					})
				.where({'requestedBy':data?.initiater,'toUid':data?.receiver,status:2,id:data?.requestId})
				.whereRaw("DATE(booking_date) =?",[currentDate]).first()
			if(callrequest){
				var requestStatus= await commonHelper.bookingRequestSlotValidate(callrequest)
				if(!requestStatus){
					return response.status(200).json({
						"callDTOS":{},
						"logout":false,
						"resMsg":'This request not valid for call this time',
						"status": false
					})
				}
			}else{
				return response.status(200).json({
					"callDTOS":{},
					"logout":false,
					"resMsg":'This request not valid for call now',
					"status": false
				})
			}
		}
		var callConnectionStatus= await commonHelper.checkUserCallStatusWithExpert(data,'Exotel')
			if(callConnectionStatus?.status){
				return response.status(200).json({
					"callDTOS":{},
					"logout":false,
					"requestList":[],
					"resMsg": callConnectionStatus?.msg,
					"status": false
				})
			}
			
		var callHistoryData:any = new CallHistory()
		callHistoryData.callTime=new Date()
		callHistoryData.initiater=uid
		callHistoryData.receiver=expertId
		callHistoryData.requestId=data?.requestId
		callHistoryData.isigst=await commonHelper.igstCgst(uid)
		//callHistoryData.rate=callRate
		var callDur
		if(data?.requestId){
			const req= await Request.find(data?.requestId)
			callHistoryData.rate=req?.rate||0
			callDur = await commonHelper.estimatedCallDuration(uid,data,'Exotel')//VOIP/Exotel
			callHistoryData.estimatedDuration=callDur>3600?3600:Math.floor(callDur)
		}else{
		if(receiverExpert?.verified){
			callDur = await commonHelper.estimatedCallDuration(uid,data,'Exotel')//VOIP/Exotel
			callHistoryData.rate=await commonHelper.showCallAmount(callRate,false,'Exotel')
			callHistoryData.estimatedDuration=callDur>3600?3600:Math.floor(callDur)
		}else{
			const exotelCallOpt=await AppSetting.getValueByKey('exotel_call')
			if(!exotelCallOpt){
				return response.status(200).json({
					"resMsg": "Exotel call option not for this expert",
					"status": false,
					"logout": false
				})
			}
			callDur=3600
			callHistoryData.rate=0
			callHistoryData.estimatedDuration=3600
		}
	}
		
		callHistoryData.otherCharge = await AppSetting.getValueByKey('exotel_charge')
		const gst = await AppSetting.getValueByKey('gst')
		const commission = await AppSetting.getValueByKey('commission')
		if(callHistoryData?.isigst){
			callHistoryData.gstPercentage=gst||0
		}else{
			callHistoryData.gstPercentage=gst||0
		}
		callHistoryData.commissionPercentage=commission ||0
		callHistoryData.video=false
		callHistoryData.callingType='Exotel'
		await callHistoryData.save()
		if(callHistoryData?.requestId){
			await WebServiceLog.create({request:JSON.stringify(callHistoryData),response:JSON.stringify(callHistoryData?.requestId),service_name:'Request Call Count increment with exotel call',status:1,uid:uid,time_taken:0})
			await Request.query().where('id',callHistoryData?.requestId).increment('call_count',1)
		}
		var re =/^(\0)/
		if(!re.test(userData?.mobile)){
			userData.mobile = '0'+userData?.mobile
		}
		if(!re.test(expertData?.mobile)){
			expertData.mobile = '0'+expertData?.mobile
		}
			/**************************Check expert mobile Circle***************************************/
		const url = "https://"+key+":"+token+"@api.exotel.in/v1/Accounts/"+sid+"/Numbers/"+expertData?.mobile+'.json'
		var callingNO=''
		var circle=''
		try {
			const res = await axios.get(url)
			circle = res?.data?.Numbers?.Circle
			callingNO=circleNO[circle]
		} catch (error) {
			const tTime = perf.stop()
			await WebServiceLog.create({request:JSON.stringify(data),response:JSON.stringify(error),service_name:'Check Expert mobile Circle Service',status:1,uid:0,time_taken:tTime?.time})
			return response.status(200).json({
				"resMsg": "Exotel api error",
				"status": false,
				"logout": false
			})
		}
	/**************************Check expert mobile Circle***************************************/
	
		try {
		await SeekerCallMap.query().where({fromNo:userData?.mobile}).whereNot({toNo:expertData?.mobile}).update({status:1})
		const callMapexist:any = await SeekerCallMap.query().where({fromNo:userData?.mobile,status:0}).first()
		if(!callMapexist){
			var savedata:any= {
				fromNo:userData?.mobile,
				toNo:expertData?.mobile,
				status:0,
				estimatedDuration:Math.floor(callDur)||60,
				callId:callHistoryData?.id,
				exotelNo:callingNO||defaultNo,
				exotelNoCircle:circle
			}
			await SeekerCallMap.create(savedata)
		}else{
			callMapexist.callId=callHistoryData?.id
			callMapexist.exotelNo=callingNO||defaultNo
			callMapexist.exotelNoCircle=circle
			callMapexist.estimatedDuration=Math.floor(callDur)
			await callMapexist.save()
		}

			if(callingNO){
				return response.status(200).json({
					"callingNO":callingNO||defaultNo,
					"resMsg": "Exotel api responce",
					"status": true,
					"logout": false
				})
			}else{
				return response.status(200).json({
					"callingNO":defaultNo,
					"resMsg": "Exotel Api responce",
					"status": true,
					"logout": false
				})
			}
		} catch (error) {
			const tTime = perf.stop()
			await WebServiceLog.create({request:JSON.stringify(data),response:JSON.stringify(error),service_name:'Save Expert mobile Circle data and other data Service',status:1,uid:0,time_taken:tTime?.time})
			return response.status(200).json({
				"resMsg": "Internal api error",
				"status": false,
				"logout": false
			})
		} 
	
	}

	async callResponse({response,request }:HttpContextContract){
		const data = request.get()
		const CallFrom =data?.CallFrom
		const CallSid=data?.CallSid
		var callDetail:any = await SeekerCallMap.query().where({fromNo:CallFrom,status:0}).first()
		await WebServiceLog.create({request:JSON.stringify(data),response:JSON.stringify(callDetail),service_name:'Exotel Call Service  on call end',status:1,uid:0,time_taken:0})
		if(callDetail?.callId){
			
				// const apiUrl ='https://'+key+':'+token+'@api.exotel.in/v1/Accounts/'+sid+'/Calls/'+CallSid+'.json?details=true'
				// const res = await axios.get(apiUrl)
				// await WebServiceLog.create({request:JSON.stringify(res),response:JSON.stringify(data),service_name:'Exotel Call Service  with call details',status:1,uid:0,time_taken:0})
				// const duration=res?.data?.Call?.Details?.ConversationDuration||0
				const callHistoryData:any = await CallHistory.query().where({id:callDetail.callId}).first()
				callHistoryData.startTime=data.StartTime
				callHistoryData.endTime=data.EndTime
				callHistoryData.duration=data?.Legs[0]?.OnCallDuration||data?.DialCallDuration
				
				callHistoryData.callDuration=data.DialCallDuration
				callHistoryData.recordingUrlNew=data.RecordingUrl
				callHistoryData.callResponse='Expert picked the call.'
				callHistoryData.apiResponse=JSON.stringify(data)
				await callHistoryData.save()
				await commonHelper.downloadExotelFileAndUploadOnSeekex(data.RecordingUrl,callHistoryData)
				//await commonHelper.updateExotelDuration(callHistoryData,CallSid)
				callDetail.status=1
				callDetail.message='Expert picked the call.'
				await callDetail.save()
				var receiverExpert:any = await User.find(callHistoryData?.receiver)
				var pRating= await PendingRating.query().where('callId',callHistoryData?.id).first()
				if(!pRating){
					await PendingRating.create({callId:callHistoryData?.id,skip:0,status:0,uid:callHistoryData?.initiater})
				}
				if(receiverExpert?.verified){
					const trans=await Ledger.calculateAndTransferAmountForCall(callHistoryData)
					await WebServiceLog.create({request:JSON.stringify(callHistoryData),response:JSON.stringify(trans),service_name:'Exotel call Api Amount transfer',status:1,uid:0,time_taken:0})
				}else{
					if(callHistoryData?.requestId && callHistoryData?.rate==0){
						await Request.completeRequest(callHistoryData?.requestId)
					}
					await WebServiceLog.create({request:JSON.stringify(callHistoryData),response:JSON.stringify(receiverExpert),service_name:'Exotel call Api no Amount transfer for not verify expert',status:1,uid:0,time_taken:0})
				}
			await WebServiceLog.create({request:JSON.stringify(data),response:JSON.stringify(callHistoryData),service_name:'Exotel Call Service  on end',status:1,uid:callHistoryData?.initiater,time_taken:0})
		}
		var outData={}
		return response.status(200).header('Content-type', 'application/json').json(outData)
	}

	async callResponseNoAnswer({response,request }:HttpContextContract){
	
		const data = request.get()
		const CallFrom =data.CallFrom
		var callDetail:any = await SeekerCallMap.query().where({fromNo:CallFrom,status:0}).first()
		if(callDetail?.callId){
				const callHistoryData:any = await CallHistory.query().where({id:callDetail.callId}).first()
				callHistoryData.startTime=data.StartTime
				callHistoryData.endTime=data.EndTime
				callHistoryData.duration=0
				callHistoryData.recordingUrl=data.RecordingUrl
				callHistoryData.callResponse='No answer for this call.'
				callHistoryData.apiResponse=JSON.stringify(data)
				await callHistoryData.save()
				callDetail.status=1
				callDetail.message='No answer for this call.'
				await callDetail.save()
		}

		var outData={}
		return response.status(200).header('Content-type', 'application/json').json(outData)
	}

  async callResponseNoDial({response,request }:HttpContextContract){
		const data = request.get()
		const CallFrom =data.CallFrom
		var callDetail:any = await SeekerCallMap.query().where({fromNo:CallFrom,status:0}).first()
		if(callDetail?.callId){
				const callHistoryData:any = await CallHistory.query().where({id:callDetail.callId}).first()
				callHistoryData.startTime=data.StartTime
				callHistoryData.endTime=data.EndTime
				callHistoryData.duration=0
				callHistoryData.recordingUrl=''
				callHistoryData.callResponse='Number not dial buy system.'
				callHistoryData.apiResponse=JSON.stringify(data)
				await callHistoryData.save()
				callDetail.status=1
				callDetail.message='Number not dial buy system.'
				await callDetail.save()
		}

		var outData={}
		return response.status(200).header('Content-type', 'application/json').json(outData)
	}

	async call({request,response,auth }: HttpContextContract){
		perf.start()
		const uid:any=auth.user?.id
		await WebServiceLog.create({request:JSON.stringify(request.all()),response:JSON.stringify({}),service_name:'VOIP Call Service initiated',status:1,uid:uid,time_taken:0})
		const data = request.all()
		var usertwilioToken,room
		//var notify:any=false
		if(data?.requestId){
			const currentDate=new Date().toISOString().split('T')[0]
			var callrequest = await Request.query().preload('slots',function(query){
						query.preload('schedule')
					})
				.where({'requestedBy':data?.initiater,'toUid':data?.receiver,status:2,id:data?.requestId})
				.whereRaw("DATE(booking_date) =?",[currentDate]).first()
			if(callrequest){
				var requestStatus= await commonHelper.bookingRequestSlotValidate(callrequest)
				if(!requestStatus){
					return response.status(200).json({
						"callDTOS":{},
						"logout":false,
						"resMsg":'This request not valid for call this time',
						"status": false
					})
				}
			}else{
				
				return response.status(200).json({
					"callDTOS":{},
					"logout":false,
					"resMsg":'This request not valid for call now',
					"status": false
				})
			}
		}
		if(uid==data?.initiater){
			var callConnectionStatus= await commonHelper.checkUserCallStatusWithExpert(data,'VOIP')
			if(callConnectionStatus?.status){
				return response.status(200).json({
					"callDTOS":{},
					"logout":false,
					"requestList":[],
					"resMsg": callConnectionStatus?.msg,
					"status": false
				})
			}
			/**************************Save call Data***************************/
			const expertData:any= await User.query().where({id:data?.receiver}).first()
			const callRate=(data.video?expertData.videoCallRate:expertData.audioCallRate)||0
			var callHistoryData:any = new CallHistory()
			callHistoryData.requestId=data?.requestId
			callHistoryData.callTime=new Date()
			callHistoryData.initiater=uid
			callHistoryData.receiver=data.receiver
			callHistoryData.video=data?.video
			callHistoryData.isigst=await commonHelper.igstCgst(uid)
			if(data?.requestId){
				const req= await Request.find(data?.requestId)
				callHistoryData.rate=req?.rate||0
			}else{
				if(expertData?.verified ){
					callHistoryData.rate=await commonHelper.showCallAmount(callRate,callHistoryData?.video,'VOIP')||0
				}else{
					callHistoryData.rate=0
				}
			}
			if(data?.video){
				callHistoryData.otherCharge = await AppSetting.getValueByKey('voip_video_charge')
			}else{
				callHistoryData.otherCharge = await AppSetting.getValueByKey('voip_audio_charge')
			}
			const gst = await AppSetting.getValueByKey('gst')
			const commission = await AppSetting.getValueByKey('commission')
			if(callHistoryData?.isigst){
				callHistoryData.gstPercentage=gst||0
			}else{
				callHistoryData.gstPercentage=gst||0
			}
			callHistoryData.commissionPercentage=commission ||0
			callHistoryData.callingType='VOIP'
			if(expertData?.verified){
				const callDur=await commonHelper.estimatedCallDuration(uid,data,'VOIP')||0
				callHistoryData.estimatedDuration=callDur>3600?3600:callDur
			}else{
				callHistoryData.estimatedDuration=3600
			}

			await callHistoryData.save()
			if(callHistoryData?.requestId){
				await WebServiceLog.create({request:JSON.stringify(callHistoryData),response:JSON.stringify(callHistoryData?.requestId),service_name:'Request Call Count increment',status:1,uid:uid,time_taken:0})
				await Request.query().where('id',callHistoryData?.requestId).increment('call_count',1)
			}
			callHistoryData = await CallHistory.query().preload('expert').where({id:callHistoryData?.id}).first()
			callHistoryData= callHistoryData.serialize()
			if(callHistoryData?.hasOwnProperty('expert')){
				delete callHistoryData['expert']
			}
			 room = callHistoryData?.id+'_room'
			/*******************************************************************/
			usertwilioToken = await twilioHelper.createUserToken(uid,room)
			const receiverToken =  await twilioHelper.createUserToken(callHistoryData?.receiver,room)
			callHistoryData.receiverToken=receiverToken
			callHistoryData.initiaterToken=usertwilioToken
			callHistoryData.roomId=room
			callHistoryData.waitingTime=45
			callHistoryData.startCall=new Date().getTime()
			///******************************Notification**********************************//
				var notify:any=await notificationHelper.sendNotifictionForCall(callHistoryData,auth?.user)
			///******************************Notification**********************************//

			///******************************Fire Store Data save **********************************//

		 		await firebaseHelper.newCall(callHistoryData,notify)
			///******************************Fire Store Data save **********************************//
			const tTime = perf.stop()
			await WebServiceLog.create({request:JSON.stringify(callHistoryData),response:JSON.stringify(notify),service_name:'Call Service Calling by initiater',status:1,uid:uid,time_taken:tTime.time})
		}else{
			var callHistoryData:any = await CallHistory.query().preload('expert').where({id:data?.id}).first()
			callHistoryData= callHistoryData.serialize()
			callHistoryData.waitingTime=45
			if(callHistoryData?.hasOwnProperty('expert')){
				delete callHistoryData['expert']
			}
			const tTime = perf.stop()
			await WebServiceLog.create({request:JSON.stringify(callHistoryData),response:JSON.stringify(notify),service_name:'Call Service Calling by reciever',status:1,uid:uid,time_taken:tTime.time})
		}

		return response.status(200).json({
			"callDTOS":callHistoryData,
			"twilioToken":usertwilioToken,
			"roomID":room,
			"logout":false,
			"resMsg": "twillo call",
			"status": true,
			"notify":notify||false
		})

	}

	async getCallHistory({ request,response,auth }: HttpContextContract){
		const uid:any=auth.user?.id
		 const data=request.all()
		 const type = data.type
		 const userTimeZone = auth.user?.timeZone||'Asia/Kolkata'
		 const page = data?.page||1
		 const limit = 40
		  
		 try {
			 var historyQuery =  CallHistory.query()
					.select('callTime','duration','initiater','receiver','requestId','video','id','callingType')
				.preload('expert').preload('user').has('user').has('expert')
			 
			 if(data?.requestId && type==4){
					historyQuery.where({'requestId':data.requestId})
			 }

			historyQuery.where((query)=>{
				query.where({"receiver":uid}).orWhere({'initiater':uid})
			})
			historyQuery.where((query)=>{
				query.whereNotNull('startTime')
			})

			//  if(data.type==1){
			// 		historyQuery.where({'initiater':uid})
			//  }
			//  if(data.type==2){
			// 	historyQuery.where({"receiver":uid}).where("duration",'>',1)
			// }
			// if(data.type==3){
			// 	historyQuery.where({"receiver":uid}).where((query)=>{
			// 		query.where("duration",'<',1).orWhereNull('duration')
			// 	})
			// }
			 var callsHistory:any= await historyQuery.orderBy('id','desc').paginate(page,limit)
			 var lastPage = callsHistory?.lastPage
			 if(callsHistory){
					callsHistory = callsHistory?.rows
			 }
			 callsHistory = await Promise.all(callsHistory.map(async(call)=>{
														var dt:any={}
														call.duration = Math.floor(call?.duration||0)
														// const timeZone=DateTime.local(call.callTime).setZone(userTimeZone).toFormat('dd, LLL yyyy t')
														// const localTime = DateTime.local(call.callTime)
													//	call.callTime=timeZone
														call.callTime=DateTime.fromISO(new Date(call.callTime).toISOString(),{setZone:true}).setZone(userTimeZone).toFormat('dd, LLL yyyy t')
													//	dt.callHistory=call
														dt.callHistory={
															"callTime":call?.callTime,
															"duration": await commonHelper.durationFormat(call?.duration),
															"initiater": call?.initiater,
															"receiver": call?.receiver,
															"requestId": call?.requestId,
															"video": call?.video?true:false,
															"id": call?.id,
															"callingType": call?.callingType,
															"name": call?.name,
															"profileImage": call?.profileImage,
															"expertCallBackTime": call?.expertCallBackTime,
														}

														if((call?.duration==0 && call?.receiver==uid) || (call.duration==null && call?.receiver==uid)){
															dt.type=3
														}else if(call.initiater==uid){
															dt.type=1
														}else if(call.receiver==uid){
															dt.type=2
														}

														if(call?.initiater==uid){
															dt.name= call?.name||''
															dt.profilePic=call?.profileImage||call?.profilePic||''
															dt.uid=call?.receiver
															dt.verified=call?.expert?.verified?true:false
															const rat = await commonHelper.callRating(call?.id)
															dt.rating=parseFloat((rat?.rating).toFixed(1))||0
															dt.ratingCount=rat?.count||0
														}else{
															dt.name= call?.user.name||''
															dt.profilePic=call?.user?.profileImage||call?.user?.profilePic||''
															dt.uid=call?.initiater
															dt.verified=call?.user?.verified?true:false
															const rat = await commonHelper.callRating(call?.id)
															dt.rating=parseFloat((rat?.rating).toFixed(1))||0
															dt.ratingCount=rat?.count||0
														}
														
														if(call?.callingType=='Exotel'){
															dt.isExotel=true
														}else{
															dt.isExotel=false
														}
														delete dt.callHistory['user']
														delete dt.callHistory.expert
														return dt
												}))

			await WebServiceLog.create({request:JSON.stringify(data),response:JSON.stringify(callsHistory),service_name:'User Conversation Service success',status:1,uid:uid,time_taken:1})								
				return response.status(200).json({
					"data":callsHistory,
					"logout": false,
					"resMsg": "All Conversation",
					"status": true,
					"currentPage":page,
          "totalPage":lastPage,
          "prevPage":page>1?page-1:null,
          "nextPage":page<lastPage?page+1:null
				})
		 } catch (error) {
			await WebServiceLog.create({request:JSON.stringify(data),response:JSON.stringify(error),service_name:'User Conversation Service',status:1,uid:uid,time_taken:1})
      return response.status(200).json({
          "errors":error,
          "logout": false,
          "resMsg": "Get user call conversation",
          "status": false
      })
		 }
	}
	
	async getConversation({ request,response,auth }: HttpContextContract){
		const uid:any=auth.user?.id
		 const data=request.all()
		 const type = data.type
		 try {
			 var callsHistory= await(await CallHistory.query()
													 .select('callTime','duration','initiater','receiver','requestId','video')
													 .preload('expert').where('initiater',uid).limit(20)).map((call)=>{
														var dt:any={}
														dt.callHistory=call
														dt.type=type
														dt.uid=uid
														return dt
													})
				return response.status(200).json({
					"data":callsHistory,
					"logout": true,
					"resMsg": "All Conversation",
					"status": true
				})
		 } catch (error) {
			await WebServiceLog.create({request:JSON.stringify(data),response:JSON.stringify(error),service_name:'User Conversation Service',status:1,uid:uid,time_taken:1})
      return response.status(201).json({
          "errors":error,
          "logout": false,
          "resMsg": "Get user call conversation",
          "status": false
      })
		 }
	}

	async getPendingRating({ request,response,auth }: HttpContextContract){
		const uid:any=auth.user?.id
		const data=request.all()
		try {
			var penddingRating:any
			if(data?.callId){
				penddingRating = await PendingRating.query()
																			// .preload('user',(query)=>{
																			// 		query.select('name','expert','profilePic')
																			// })
																			.where({'uid':uid,status:0,skip:0,callId:data?.callId}).first()

				if(penddingRating){
					if(penddingRating?.callId){
						const call=await CallHistory.find(penddingRating?.callId)
						const exp:any= await User.find(call?.receiver)
						penddingRating.name=exp?.name
						penddingRating.pic=exp?.profilePic
						penddingRating.uid=call?.receiver
					}
					penddingRating.userType='Expert'
				}
			}else{
				penddingRating = await PendingRating.query()
																// .preload('user',(query)=>{
																// 	query.select('name','expert','profilePic')
																// })
																.where({'uid':uid,status:0,skip:0}).first()

				if(penddingRating){
					if(penddingRating?.callId){
						const call=await CallHistory.find(penddingRating?.callId)
						const exp:any= await User.find(call?.receiver)
						penddingRating.name=exp?.name
						penddingRating.pic=exp?.profilePic
						penddingRating.uid=call?.receiver
					}
					penddingRating.userType='Expert'
				}
			}
			//const userAllUnreadNotifications=await AppNotification.query().where('uid',uid).whereNot('type',2).whereNot('type',4).whereNot('type',1)
			// .pojo<{ count: number}>()
			// .count('id as count')
			await WebServiceLog.create({request:JSON.stringify(data),response:JSON.stringify(penddingRating),service_name:'Get Pending Rating Service Data',status:1,uid:uid,time_taken:0})
			if(penddingRating){
				return response.status(200).json({
					"data":penddingRating,
					"logout": false,
					"resMsg": "Get Pending Rating",
					"status": true,
					"uid":uid
				//	"unreadNotificationCount":userAllUnreadNotifications[0]?userAllUnreadNotifications[0].count:2
				//	"unreadNotificationCount":2
				})
			}else{
				return response.status(200).json({
					"data":{},
					"logout": false,
					"resMsg": "No Pending Rating",
					"status": false,
					"uid":uid
				//	"unreadNotificationCount":userAllUnreadNotifications[0]?userAllUnreadNotifications[0].count:2
				///	"unreadNotificationCount":2
				})
			}
		} catch (error) {
			await WebServiceLog.create({request:JSON.stringify(data),response:JSON.stringify(error),service_name:'Get Pending Rating Service error',status:1,uid:uid,time_taken:0})
      return response.status(200).json({
				  "data":{},
          "errors":error,
          "logout": false,
          "resMsg": "Get Pending Rating",
          "status": false
      })
		}
	}

	async giveRating({ request,response,auth }: HttpContextContract){
		const uid:any=auth.user?.id
		const data=request.all()
		try {
			const existCall= await UserRating.query().where({callId:data?.callId}).first()
			var userRating:any
			if(existCall){
				existCall.callId=data.callId
				existCall.callFeedBack=data.callFeedBack
				existCall.callRating=data.callRating
				existCall.givenBy=data.givenBy
				existCall.uid=data?.uid
				existCall.userFeedBack=data.userFeedBack
				existCall.userRating=data.userRating
				existCall.video=data.video
				await existCall.save()
			}else{
				userRating = new UserRating()
				userRating.callId=data.callId
				userRating.callFeedBack=data.callFeedBack
				userRating.callRating=data.callRating
				userRating.givenBy=data.givenBy
				userRating.uid=data?.uid
				userRating.userFeedBack=data.userFeedBack
				userRating.userRating=data.userRating
				userRating.video=data.video
				await userRating.save()
			}
			await PendingRating.query().where({'callId':data?.callId}).update({'status':1})
			await notificationHelper.feedbackRating(userRating,auth.user)
			await WebServiceLog.create({request:JSON.stringify(data),response:JSON.stringify(userRating),service_name:'Get Rating Service Success',status:1,uid:uid,time_taken:0})
			return response.status(200).json({
				"logout": true,
				"resMsg": "Call Rating Done",
				"status": true
			})
			
		} catch (error) {
			await WebServiceLog.create({request:JSON.stringify(data),response:JSON.stringify(error),service_name:'Get Rating Service Error',status:1,uid:uid,time_taken:0})
      return response.status(201).json({
          "errors":error,
          "logout": false,
          "resMsg": "Call Rating",
          "status": false
      })
		}
	}

	async getRating({ request,response,auth }: HttpContextContract){
		const uid:any=auth.user?.id
		const data=request.all()
		try {
			var userRatings=await(await UserRating.query().preload('user',(query)=>{
												query.select('name','expert','profilePic')
											}).where({uid:uid})).map((element)=>{
												var dR:any={}
												dR=element
												dR.name=element.user.name
												dR.profilePic=element.user.profilePic
												return dR
											})
				return response.status(200).json({
					"data":userRatings,
					"logout": false,
					"resMsg": "Get Call Rating",
					"status": true
				})						
			
		} catch (error) {
			await WebServiceLog.create({request:JSON.stringify(data),response:JSON.stringify(error),service_name:'Get Rating Service',status:1,uid:uid,time_taken:0})
      return response.status(201).json({
          "errors":error,
          "logout": false,
          "resMsg": "Get Call Rating",
          "status": false
      })
		}
	}

	async skipRating({ request,response,auth }: HttpContextContract){
		const uid:any=auth.user?.id
		const data=request.all()
		var callRating:any = await PendingRating.query().where({callId:data?.callId,uid:uid}).first()
		if(callRating){
			try {
				callRating.skip=1
				callRating.save()
				return response.status(200).json({
					"logout": false,
					"resMsg": "skip Rating",
					"status": true
				})	
			} catch (error) {
				await WebServiceLog.create({request:JSON.stringify(data),response:JSON.stringify(error),service_name:'Skip Rating Service',status:1,uid:uid,time_taken:0})
				return response.status(200).json({
						"logout": false,
						"resMsg": "skip Rating error",
						"status": false
				})
			}
		}else{
			await WebServiceLog.create({request:JSON.stringify(data),response:JSON.stringify('NO data fount'),service_name:'Skip Rating Service',status:1,uid:uid,time_taken:0})
      return response.status(200).json({
          "logout": false,
          "resMsg": "No Pending Rating",
          "status": false
      })
		}
	}

	async updateBusyStatus({response,auth }){
		const uid:any=auth.user?.id
		try {
			var status= await UserBusyStatus.query().where({uid:uid}).first()
			if(status){
				status.delete()
			}else{
				await UserBusyStatus.create({uid:uid})
			}
			return response.status(200).json({
				"logout": true,
				"resMsg": "User Busy Status Updated",
				"status": true
			})
		} catch (error) {
			await WebServiceLog.create({request:JSON.stringify(uid),response:JSON.stringify(error),service_name:'Update Busy Status Service',status:1,uid:uid,time_taken:0})
      return response.status(201).json({
					"errors":error,
          "logout": false,
          "resMsg": "Update Busy Status failed",
          "status": false
      })
		}
	}

	async getCallDetails({ request,response,auth }: HttpContextContract){
		const uid:any=auth.user?.id
		const data=request.all()
		const callId=data?.callId
		try {
			var callInvoice:any= await Invoice.query().where('callId',callId).first()
			var callHistory:any = await CallHistory.query()
														.preload('user',(query)=>{
															query.select('name','profilePic')
														}).preload('expert',(query)=>{
															query.select('name','profilePic')
														})
														.where({id:callId}).first() //,initiater:uid						
					callHistory= callHistory.serialize()
				//	return callInvoice
					if(callHistory.duration==0 ){ //|| callHistory.rate==0
						return response.status(200).json({
							"logout": false,
							"resMsg": "No details for this call.",
							"status": false
						})
					}
					if(callHistory?.initiater==uid){
						callHistory.name=callHistory.expert?.name
						callHistory.profilePic=callHistory.expert.profilePic
					}else{
						callHistory.name=callHistory.user?.name
						callHistory.profilePic=callHistory.user.profilePic
					}
					callHistory.duration=callHistory?.duration
					callHistory.callId=data?.callId	
					callHistory.saleAmt=callInvoice?.salesAmount||0
					callHistory.totalAmt=callInvoice?.totalAmount||0
					callHistory.gstCommission=callInvoice?.gstCommissionAount||0
					callHistory.gstAmt=callInvoice?.gstAmount||0
					callHistory.finalPayout=callInvoice?.finalAmount||0
					callHistory.commission=callInvoice?.commissionAmount||0
					callHistory.otherChargeValue=callInvoice?.otherChargeValue||0
					if(callHistory?.igst){
						callHistory.gstPercentage=callHistory.gstPercentage/2
					}
					const refund = await Refund.query().where('callId',callId).first()
					if(refund){
						callHistory.refundapplied=true
					}else{
						callHistory.refundapplied=false
					}
					await WebServiceLog.create({request:JSON.stringify(data),response:JSON.stringify(callHistory),service_name:'Get call details Service',status:1,uid:uid,time_taken:0})
					return response.status(200).json({
						"data":callHistory,
						"logout": true,
						"resMsg": "Success",
						"status": true
					})
		} catch (error) {
			await WebServiceLog.create({request:JSON.stringify(data),response:JSON.stringify(error),service_name:'Get call details Service error',status:1,uid:uid,time_taken:0})
      return response.status(201).json({
					"errors":error,
          "logout": false,
          "resMsg": "Get call details failed",
          "status": false
      })
		}
	}

	async uploadCallRecordingS({ request,response,auth }: HttpContextContract){
		const uid:any=auth.user?.id
		const data=request.all()
		const callId=data?.callId
		await WebServiceLog.create({request:JSON.stringify(data),response:JSON.stringify('File upload start'),service_name:'Call recording upload and mearg Service start',status:1,uid:uid,time_taken:0})
		var call:any = await CallHistory.query().where({id:callId}).first()
		var pr:any= await PendingRecording.query().where({callId:callId,status:0}).first()
		
		if(!pr){
			pr = new PendingRecording()
			pr.callId=callId
		}
		if(call?.initiater==uid){
			pr.initiaterUrl=data?.tempUrl||data?.url
			pr.status=0
		}
		if(call?.receiver==uid){
			pr.receiverUrl=data?.tempUrl||data?.url
			pr.status=0
		}

	//	return pr
		await pr.save()

		await WebServiceLog.create({request:JSON.stringify(pr),response:JSON.stringify('File upload saved'),service_name:'Call recording upload save files service.',status:1,uid:uid,time_taken:0})
		var prUp:any= await PendingRecording.query().where({callId:callId,status:0}).first()
		if(prUp?.initiaterUrl && prUp?.receiverUrl){
		//	const callfileName=`${new Date().getTime()}.wav`
			const callfileName=`${new Date().getTime()}.mp3`
			var tempFile1=fileName(prUp?.initiaterUrl)
			var tempFile2=fileName(prUp?.receiverUrl)

			// await download(prUp?.initiaterUrl, './public/'+tempFile1, async function(){})
			// await download(prUp?.receiverUrl, './public/'+tempFile2, async function(){
			// 	console.log('success')
			// })

			//return prUp
			var command = SoxCommand()
			// var fileName1=prUp?.initiaterUrl
			// var fileName2=prUp?.receiverUrl
			var fileName1='./tmp/uploads/'+tempFile1
			var fileName2='./tmp/uploads/'+tempFile2
			command.input(fileName1).inputSampleRate(16000)
			//.inputEncoding('signed').inputBits(16).inputFileType('raw')
			command.input(fileName2).inputSampleRate(16000)
			//.inputEncoding('signed').inputBits(16).inputFileType('raw')
			
			command.output('./public/recordings/'+callfileName)
		//command.concat()
			command.combine('merge')
			command.on('error', async function(err, stdout, stderr) {
				console.log('Cannot process audio: ' + err.message)
				console.log('Sox Command Stdout: ', stdout)
				console.log('Sox Command Stderr: ', stderr)
				await WebServiceLog.create({request:JSON.stringify(uid),response:JSON.stringify(err.message +' :=>'+stdout+':=>'+stderr),service_name:'Call recording merge Service failed',status:1,uid:uid,time_taken:0})
				return response.status(201).json({
						"errors":err.message,
						"logout": false,
						"resMsg": "Call recording merge Service failed",
						"status": false
				})
			});
			command.on('end', async function() {
				console.log('Sox command succeeded!');
				try {
					var uploadFile= await uploader.uploadRecording(callfileName,'recordings',uid)
					if(uploadFile?.Location){
						call.recordingUrl=uploadFile?.Location||''
						try {
							await call.save()
							pr.status=1
							await pr.save()
							return response.status(200).json({
								"logout": false,
								"resMsg": "Call recording uploaded",
								"status": true
							})
						} catch (error) {
							await WebServiceLog.create({request:JSON.stringify(uid),response:JSON.stringify(error),service_name:'Call recording upload and seve Service failed',status:1,uid:uid,time_taken:0})
							return response.status(200).json({
									"errors":error,
									"logout": false,
									"resMsg": "Call recording upload and save Service failed",
									"status": true
							})
						}
					}
				} catch (error) {
					await WebServiceLog.create({request:JSON.stringify(uid),response:JSON.stringify(error),service_name:'Call recording upload Service failed',status:1,uid:uid,time_taken:0})
					return response.status(200).json({
							"errors":error,
							'path':Application.publicPath(),
							"logout": false,
							"resMsg": "Call recording upload Service failed",
							"status": false
					})
				}
			//	await WebServiceLog.create({request:JSON.stringify(uid),response:JSON.stringify('File merge succeeded!'),service_name:'Call recording merge Service success',status:1,uid:uid,time_taken:0})
			})
			try {
				command.outputFileType('mp3')
				//.outputSampleRate(1600).outputEncoding('signed').outputBits(16)
				await command.run()
				return response.status(200).json({
					"logout": false,
					"resMsg": "Call recording uploaded",
					"status": true
				})
				//return command
			} catch (error) {
				await WebServiceLog.create({request:JSON.stringify(uid),response:JSON.stringify(error),service_name:'Call recording merge Service failed',status:1,uid:uid,time_taken:0})
				return response.status(200).json({
						"errors":error,
						'path':Application.publicPath(),
						"logout": false,
						"resMsg": "Call recording merge and upload Service failed",
						"status": false
				})
			}
		}else{

			if(prUp?.status==1){
				return response.status(200).json({
					"logout": false,
					"resMsg": "No pending recording for merge Service",
					"status": true
				})
			}
			if(!prUp?.initiaterUrl){
				return response.status(200).json({
					"logout": false,
					"resMsg": "User recording not found for merging",
					"status": true
				})
			}
			if(!prUp?.receiverUrl){
				return response.status(200).json({
					"logout": false,
					"resMsg": "Expert recording not found for merging",
					"status": true
				})
			}
			return response.status(200).json({
				"logout": false,
				"resMsg": "No pending recording for merge Service",
				"status": true
			})
		}
	}

	async viewRequest({ request,response,auth }: HttpContextContract){
    const uid:any=auth.user?.id
		const data=request.all()
		const ids=data?.ids
		const fcm=data?.fcm
		if(data.type==3){
			var req:any= await Request.find(ids[0])
			req.view=true
			try {
				await req.save()
				if(fcm){
					// var msg={title:'Test',body:'View request message',type:7}
					// Event.emit('new:notification', {msg:msg,fcm:fcm})
				}
				return response.status(200).json({
          "logout": true,
          "resMsg": "Service Success",
          "status": true
      	})
			} catch (error) {
				await WebServiceLog.create({request:JSON.stringify(uid),response:JSON.stringify(error),service_name:'Service failed',status:1,uid:uid,time_taken:0})
				return response.status(201).json({
					"errors":error,
          "logout": false,
          "resMsg": "Service failed",
          "status": false
      	})
			}
		}
	}
	

	async twilioCallEnded({request,response}: HttpContextContract){
		response.header('Content-Type','application/x-www-urlencoded')
		const data=request.all()
	//	const roomName= data?.RoomName
		if(data.StatusCallbackEvent=='room-created'){
			const room=data.RoomName.split('_')
			if(room[0]){
				var callHistoryData:any = await CallHistory.query().where({id:room[0]}).first()
				callHistoryData.apiResponse=JSON.stringify(data)
				callHistoryData.startTime=new Date(data?.Timestamp)
				callHistoryData.roomSid=data?.RoomSid
				try {
					await callHistoryData.save()
					await WebServiceLog.create({request:JSON.stringify(data),response:JSON.stringify(callHistoryData),service_name:'Twilio call created Api on success',status:1,uid:0,time_taken:0})
				} catch (error) {
					await WebServiceLog.create({request:JSON.stringify(data),response:JSON.stringify(error),service_name:'Twilio call created Api on error',status:1,uid:0,time_taken:0})
				}
			}
		}
		if(data?.RoomStatus=='completed'){
			const room=data.RoomName.split('_')
			const RoomSid=data?.RoomSid
			if(room[0]){
				var callHistoryData:any = await CallHistory.query().where({id:room[0]}).first()
				callHistoryData.callDuration=data.RoomDuration
				callHistoryData.apiResponse=JSON.stringify(data)
				callHistoryData.endTime=new Date(data?.Timestamp)
				callHistoryData.roomSid=RoomSid
				try {
					await callHistoryData.save()
					const callDuration = await twilioHelper.updateRoomDetails(callHistoryData,RoomSid,callHistoryData?.receiver)
					await CallHistory.finishCall(callHistoryData?.id)
					// if(callDuration>0){
					// 	callHistoryData.duration=callDuration
					// }
					await WebServiceLog.create({request:JSON.stringify(data),response:JSON.stringify(callHistoryData),service_name:'Twilio call end Api on success with call duration =>'+callDuration,status:1,uid:0,time_taken:0})
				} catch (error) {
					await WebServiceLog.create({request:JSON.stringify(data),response:JSON.stringify(error),service_name:'Twilio call end Api on error',status:1,uid:0,time_taken:0})
				}
				var invoice = await Invoice.query().where({callId:callHistoryData?.id}).first()
				if(invoice){
					await WebServiceLog.create({request:JSON.stringify(callHistoryData),response:JSON.stringify(invoice),service_name:'Twilio Invoice already updated',status:1,uid:0,time_taken:0})
				}else{

					// var pRating= await PendingRating.query().where('callId',callHistoryData?.id).first()
					// if(!pRating && data?.duration>0){
					// 	await PendingRating.create({callId:callHistoryData?.id,skip:0,status:0,uid:callHistoryData?.initiater})
					// }
					// var receiverExpert:any = await User.find(callHistoryData?.receiver)
					// if(receiverExpert?.verified){

					// 	var callHistoryData:any = await CallHistory.query().where({id:room[0]}).first()
					// 	await WebServiceLog.create({request:JSON.stringify(callHistoryData),response:JSON.stringify({}),service_name:'Twilio call Api Amount transfer for call Id =>'+callHistoryData?.id+' to =>'+receiverExpert?.receiver,status:1,uid:receiverExpert?.receiver,time_taken:0})
					// 	if(callHistoryData?.duration>0){
					// 	//	const trans=await Ledger.calculateAndTransferAmountForCall(callHistoryData)
					// 	}else{
					// 		await WebServiceLog.create({request:JSON.stringify(callHistoryData),response:JSON.stringify({error:'call duration invalid'}),service_name:'Twilio call Api Amount transfer for call Id =>'+callHistoryData?.id+' to =>'+receiverExpert?.receiver,status:1,uid:receiverExpert?.receiver,time_taken:0})
					// 	}

					// }else{
					// 	var callHistoryData:any = await CallHistory.query().where({id:room[0]}).first()
					// 	await WebServiceLog.create({request:JSON.stringify(callHistoryData),response:JSON.stringify(receiverExpert),service_name:'Twilio call Api Amount not transfer => expert not verified',status:1,uid:0,time_taken:0})
					// }
				}
			}
		}
		await WebServiceLog.create({request:JSON.stringify(data),response:JSON.stringify(data?.RoomName),service_name:'Twilio call Api Data on every call event',status:1,uid:0,time_taken:0})
		return
		// return response.status(200).json({
		// 	"logout": false,
		// 	"resMsg": "Call ended",
		// 	"status": true,
		// 	"room":roomName
		// })
	}

	async callDuration({request,response,auth}: HttpContextContract){
		return false

		const uid:any=auth.user?.id
		const data=request.all()
		await WebServiceLog.create({request:JSON.stringify(data),response:JSON.stringify({}),service_name:'Call end manual Api on success init',status:1,uid:uid,time_taken:0})
		const room=data?.roomId.split('_')
	//var duration=data?.duration.split(':')
		//const totalDuration=data?.duration
		
		if(room[0]){
			// if(duration.length==2){
			// //	var totalDuration:any=(parseInt(duration[0])*60)+parseInt(duration[1])
			// }else if(duration.length==3){
			// //	var totalDuration:any=(parseInt(duration[0])*60*60)+(parseInt(duration[1])*60)+parseInt(duration[2])
			// }
			var callHistoryData:any = await CallHistory.query().where({id:room[0]}).first()
			var invoice = await Invoice.query().where({callId:callHistoryData?.id}).first()
			if(invoice){
				await WebServiceLog.create({request:JSON.stringify(callHistoryData),response:JSON.stringify(invoice),service_name:'Invoice already updated',status:1,uid:uid,time_taken:0})
				return response.status(200).json({
					"logout": false,
					"resMsg": "Invoice already updated",
					"status": true
				})
			}
			if(uid==callHistoryData?.initiater){
				await WebServiceLog.create({request:JSON.stringify(data),response:JSON.stringify(callHistoryData),service_name:'Same User',status:1,uid:uid,time_taken:0})
				return response.status(200).json({
					"logout": false,
					"resMsg": "This user Not for this api",
					"status": true
				})
			}
			callHistoryData.duration = data?.duration
			try {
				await callHistoryData.save()
				var pRating= await PendingRating.query().where('callId',callHistoryData?.id).first()
					if(!pRating && data?.duration>0){
						await PendingRating.create({callId:callHistoryData?.id,skip:0,status:0,uid:callHistoryData?.initiater})
					}
					var receiverExpert:any = await User.find(callHistoryData?.receiver)
					if(receiverExpert?.verified){
						const trans=await Ledger.calculateAndTransferAmountForCall(callHistoryData)
						await WebServiceLog.create({request:JSON.stringify(callHistoryData),response:JSON.stringify(trans),service_name:'Manual call Api Amount transfer for call Id =>'+callHistoryData?.id+' to =>'+receiverExpert?.receiver,status:1,uid:receiverExpert?.receiver,time_taken:0})
					}else{
						await WebServiceLog.create({request:JSON.stringify(callHistoryData),response:JSON.stringify(receiverExpert),service_name:'Manual call Api Amount not transfer => expert not verified',status:1,uid:0,time_taken:0})
					}
				await WebServiceLog.create({request:JSON.stringify(data),response:JSON.stringify(callHistoryData),service_name:'Call end manual Api on success',status:1,uid:uid,time_taken:0})
				return response.status(200).json({
					"logout": false,
					"resMsg": "Call ended",
					"status": true
				})
			} catch (error) {
				await WebServiceLog.create({request:JSON.stringify(data),response:JSON.stringify(error),service_name:'Call end manual Api on error',status:1,uid:uid,time_taken:0})
				return response.status(200).json({
					"logout": false,
					"resMsg": "Call ended",
					"status": false
				})
			}
		}
	}

	/// This function for upload call recording from user end (Both streaming)
	async uploadCallRec({ request,response,auth }: HttpContextContract){
	//	return request.all()
		const uid:any=auth.user?.id
		const data=request.all()
		const callId=data?.callId||0
		var url=data?.url||'' //data?.url||
		const size=data?.size||0
		var call:any = await CallHistory.query().where({id:callId}).first()

		//const allMedai=request.file('file')
		// if(allMedai){
		// 	await WebServiceLog.create({request:JSON.stringify({data,allMedai}),response:JSON.stringify({}),service_name:'Call Recording Upload Service file upload',status:1,uid:uid,time_taken:1})
		// 	//const fileName=`${new Date().getTime()+allMedai.size+uid}.${allMedai.extname}`
		// 	const fileName=`${new Date().getTime()+allMedai.size+uid}.${allMedai.extname=='amr'||allMedai.extname=='AMR'?'mp3':allMedai.extname}`

		// 		await allMedai.move(Application.publicPath('recordings'),{
		// 			name: fileName,
		// 		})
		// 	 url = await uploader.uploadRecording(fileName,'recordings',uid)
		// }

		if(call){
			if(uid==call?.initiater){
				call.recordingUrlNew=url
				call.recordingNewSize=size
			}else{
				call.recordingUrl=url
				call.recordingSize=size
			}
			// if(!call.recordingUrl){
			// 	call.recordingUrl=url
			// }
			try {
				await call.save()
				await WebServiceLog.create({request:JSON.stringify(data),response:JSON.stringify(call),service_name:'Call New recording url save success',status:1,uid:uid,time_taken:0})
				return response.status(200).json({
					"logout": false,
					"resMsg": "Call recording saved",
					"status": true
				})
			} catch (error) {
				await WebServiceLog.create({request:JSON.stringify(data),response:JSON.stringify(error),service_name:'Call New recording url save failed',status:1,uid:uid,time_taken:0})
				return response.status(200).json({
						"errors":error,
						'path':Application.publicPath(),
						"logout": false,
						"resMsg": "Call recording save failed",
						"status": false
				})
			}
		}else{
			await WebServiceLog.create({request:JSON.stringify(data),response:JSON.stringify({}),service_name:'Call New recording save failed, call data not founded',status:1,uid:uid,time_taken:0})
			return response.status(200).json({
				'path':Application.publicPath(),
				"logout": false,
				"resMsg": "Call New recording save failed",
				"status": false
			})
		}
	}

	async createZoomMeeting({ request,response,auth }: HttpContextContract){
		const uid:any=auth.user?.id
		const data=request.all()
		const meetConfig = {
      "topic": data?.topic||"TOPIC 1",
      "type": data?.type||"2",
      "duration": data?.duration||"30",
      "start_time": new Date(),
      "timezone": "Asia/Kolkata",
      "password": "123456",
      "agenda": data?.agenda||"AGENDA"
      }
		return zoomHelper.createMeeting(meetConfig)
	}
	
	async zoomCall({request,response,auth }: HttpContextContract){
		perf.start()
		const uid:any=auth.user?.id
		await WebServiceLog.create({request:JSON.stringify(request.all()),response:JSON.stringify({}),service_name:'Zoom Call Service initiated',status:1,uid:uid,time_taken:0})
		const data = request.all()
		if(data?.requestId){
			const currentDate=new Date().toISOString().split('T')[0]
			var callrequest = await Request.query().preload('slots',function(query){
						query.preload('schedule')
					})
				.where({'requestedBy':data?.initiater,'toUid':data?.receiver,status:2,id:data?.requestId})
				.whereRaw("DATE(booking_date) =?",[currentDate]).first()
			if(callrequest){
				var requestStatus= await commonHelper.bookingRequestSlotValidate(callrequest)
				if(!requestStatus){
					return response.status(200).json({
						"callDTOS":{},
						"logout":false,
						"resMsg":'This request not valid for call this time',
						"status": false
					})
				}
			}else{
				
				return response.status(200).json({
					"callDTOS":{},
					"logout":false,
					"resMsg":'This request not valid for call now',
					"status": false
				})
			}
		}
		if(uid==data?.initiater){
			var callConnectionStatus= await commonHelper.checkUserCallStatusWithExpert(data,'VOIP')
			if(callConnectionStatus?.status){
				return response.status(200).json({
					"callDTOS":{},
					"logout":false,
					"requestList":[],
					"resMsg": callConnectionStatus?.msg,
					"status": false
				})
			}
			/**************************Save call Data***************************/
			const expertData:any= await User.query().where({id:data?.receiver}).first()
			const callRate=(data.video?expertData.videoCallRate:expertData.audioCallRate)||0
			var callHistoryData:any = new CallHistory()
			callHistoryData.requestId=data?.requestId
			callHistoryData.callTime=new Date()
			callHistoryData.initiater=uid
			callHistoryData.receiver=data.receiver
			callHistoryData.video=data?.video
			callHistoryData.isigst=await commonHelper.igstCgst(uid)
			callHistoryData.startTime=new Date()
			if(data?.requestId){
				const req= await Request.find(data?.requestId)
				callHistoryData.rate=req?.rate||0
			}else{
				if(expertData?.verified ){
					callHistoryData.rate=await commonHelper.showCallAmount(callRate,callHistoryData?.video,'VOIP')||0
				}else{
					callHistoryData.rate=0
				}
			}
			if(data?.video){
				callHistoryData.otherCharge = await AppSetting.getValueByKey('voip_video_charge')
			}else{
				callHistoryData.otherCharge = await AppSetting.getValueByKey('voip_audio_charge')
			}
			const gst = await AppSetting.getValueByKey('gst')
			const commission = await AppSetting.getValueByKey('commission')
			if(callHistoryData?.isigst){
				callHistoryData.gstPercentage=gst||0
			}else{
				callHistoryData.gstPercentage=gst||0
			}
			callHistoryData.commissionPercentage=commission ||0
			callHistoryData.callingType='VOIP'
			if(expertData?.verified){
				const callDur=await commonHelper.estimatedCallDuration(uid,data,'VOIP')||0
				callHistoryData.estimatedDuration=callDur>3600?3600:callDur
			}else{
				callHistoryData.estimatedDuration=3600
			}

			await callHistoryData.save()
			// if(callHistoryData?.requestId){
			// 	await WebServiceLog.create({request:JSON.stringify(callHistoryData),response:JSON.stringify(callHistoryData?.requestId),service_name:'Request Call Count increment',status:1,uid:uid,time_taken:0})
			// 	await Request.query().where('id',callHistoryData?.requestId).increment('call_count',1)
			// }
			callHistoryData = await CallHistory.query().preload('expert').where({id:callHistoryData?.id}).first()
			callHistoryData= callHistoryData.serialize()
			if(callHistoryData?.hasOwnProperty('expert')){
				delete callHistoryData['expert']
			}
			// room = callHistoryData?.id+'_room'
			/*******************************************************************/

			callHistoryData.token=data?.token||''
			callHistoryData.sessionName=data?.sessionName||''
			callHistoryData.waitingTime=45
			callHistoryData.startCall=new Date().getTime()
			///******************************Notification**********************************//
				var notify:any=await notificationHelper.sendNotifictionForCall(callHistoryData,auth?.user)
			///******************************Notification**********************************//

			///******************************Fire Store Data save **********************************//
		 	//	await firebaseHelper.newCall(callHistoryData,notify)
			///******************************Fire Store Data save **********************************//

			const tTime = perf.stop()
			await WebServiceLog.create({request:JSON.stringify(callHistoryData),response:JSON.stringify(notify),service_name:'Zoom Call Service Calling by initiater',status:1,uid:uid,time_taken:tTime.time})
		}else{
			var callHistoryData:any = await CallHistory.query().preload('expert').where({id:data?.id}).first()
			callHistoryData= callHistoryData.serialize()
			callHistoryData.waitingTime=45
			callHistoryData.token=data?.token||''
			callHistoryData.sessionName=data?.sessionName||''
			callHistoryData.startCall=new Date().getTime()
			
			if(callHistoryData?.hasOwnProperty('expert')){
				delete callHistoryData['expert']
			}
			const tTime = perf.stop()
			await WebServiceLog.create({request:JSON.stringify(callHistoryData),response:JSON.stringify(notify),service_name:'Zoom Call Service Calling by reciever',status:1,uid:uid,time_taken:tTime.time})
		}

		return response.status(200).json({
			"callDTOS":callHistoryData,
			"logout":false,
			"resMsg": "zoom call",
			"status": true,
			"notify":notify||false
		})
	}

	async zoomCallEnd({request,response,auth }: HttpContextContract){
		const uid:any=auth.user?.id
		const data=request.all()
		await WebServiceLog.create({request:JSON.stringify(data),response:JSON.stringify({}),service_name:'zoom call end Api on success init',status:1,uid:uid,time_taken:0})
		const firebaseDocId=data?.docId
		const fireBaseDate:any = await firebaseHelper.getSessionDocData(firebaseDocId)
		const callId=data?.callId||fireBaseDate?.callId
		if(callId){
			var callHistoryData:any = await CallHistory.query().where({id:callId}).first()
			var invoice = await Invoice.query().where({callId:callHistoryData?.id}).first()
			if(invoice){
				await WebServiceLog.create({request:JSON.stringify(callHistoryData),response:JSON.stringify(invoice),service_name:'Invoice already updated',status:1,uid:uid,time_taken:0})
				return response.status(200).json({
					"logout": false,
					"resMsg": "Invoice already updated",
					"status": true
				})
			}
			if(uid==callHistoryData?.initiater){
				await WebServiceLog.create({request:JSON.stringify(data),response:JSON.stringify(callHistoryData),service_name:'Same User',status:1,uid:uid,time_taken:0})
				return response.status(200).json({
					"logout": false,
					"resMsg": "This user Not for this api",
					"status": true
				})
			}
			callHistoryData.duration = fireBaseDate?.duration||0
			callHistoryData.endTime=new Date()
			callHistoryData.roomSid=fireBaseDate?.session
			try {
				await callHistoryData.save()
				var pRating= await PendingRating.query().where('callId',callHistoryData?.id).first()
					if(!pRating && callHistoryData?.duration>0){
						const pRat = await PendingRating.create({
							callId:callHistoryData?.id,
							skip:0,
							status:0,
							uid:callHistoryData?.initiater
						})
						const pRatn = await PendingRating.query()
															.where({uid:callHistoryData?.initiater,status:0,skip:0,callId:callHistoryData?.id})
															.first()
						if(pRatn){
							const exp:any= await User.find(callHistoryData?.receiver)
							pRatn.name=exp?.name
							pRatn.pic=exp?.profilePic
							pRatn.uid=callHistoryData?.receiver
							pRatn.userType='Expert'
						}
						await notificationHelper.sendRatingPopupNotification(pRatn,pRat?.uid)
					}
					var receiverExpert:any = await User.find(callHistoryData?.receiver)
					if(receiverExpert?.verified){
						const trans=await Ledger.calculateAndTransferAmountForCall(callHistoryData)
						await WebServiceLog.create({request:JSON.stringify(callHistoryData),response:JSON.stringify(trans),service_name:'Zoom call Api Amount transfer for call Id =>'+callHistoryData?.id+' to =>'+receiverExpert?.receiver,status:1,uid:receiverExpert?.receiver,time_taken:0})
					}else{
						await WebServiceLog.create({request:JSON.stringify(callHistoryData),response:JSON.stringify(receiverExpert),service_name:'Zoom call Api Amount not transfer => expert not verified',status:1,uid:0,time_taken:0})
					}
				await WebServiceLog.create({request:JSON.stringify(data),response:JSON.stringify(callHistoryData),service_name:'Zoom Call end Api on success',status:1,uid:uid,time_taken:0})
				return response.status(200).json({
					"logout": false,
					"resMsg": "Call ended api success",
					"status": true
				})
			} catch (error) {
				await WebServiceLog.create({request:JSON.stringify(data),response:JSON.stringify(error),service_name:'Zoom Call end Api on error',status:1,uid:uid,time_taken:0})
				return response.status(200).json({
					"logout": false,
					"resMsg": "Call ended api error",
					"status": false
				})
			}
		}

	}
	
}
