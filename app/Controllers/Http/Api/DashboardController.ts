 import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
 import Application from '@ioc:Adonis/Core/Application'
import UploadHelper from "App/Helpers/UploadHelper"
const uploader:any=new UploadHelper()
import Faq from "App/Models/Faq"
import WebServiceLog from "App/Models/WebServiceLog"
import Feedback from "App/Models/Feedback"
import TempMedia from 'App/Models/TempMedia'
import Post from 'App/Models/Post'
import Comment from 'App/Models/Comment'
import Chat from 'App/Models/Chat'
import MasterGodown from 'App/Models/MasterGodown'
import CountryState from 'App/Models/CountryState'
import Organization from 'App/Models/Organization'
import User from 'App/Models/User'
import LedgerAccount from 'App/Models/LedgerAccount'
import CallHistory from 'App/Models/CallHistory'
import UserAdvanceDetail from 'App/Models/UserAdvanceDetail'
import Request from 'App/Models/Request'
import UserRating from 'App/Models/UserRating'

const ext = (url)=>{
  return (url = url.substr(1 + url.lastIndexOf("/")).split('?')[0]).split('#')[0].substr(url.lastIndexOf("."))
}
const nativeRound=(num)=>{
  var n = num
  const dc= n % 1
  var v = Math.floor(n)
  if(dc>=0.5){
    n=v+0.5
  }else{
    n=v
  }
  return n
}
import SoxCommand from "sox-audio"
import Refund from 'App/Models/Refund'

export default class DashboardController {

  async getFaq({response,auth }){
    const uid:any=auth.user?.id
    try {
      var faqs=await Faq.all()
      return response.status(200).json({
        "faqList":faqs,
        "logout": true,
        "resMsg": "all faqs",
        "status": true
      })   
    } catch (error) {
      await WebServiceLog.create({request:JSON.stringify({}),response:JSON.stringify(error),service_name:'Get Faqs Service',status:1,uid:uid,time_taken:1})
      return response.status(201).json({
          "errors":error,
          "logout": false,
          "resMsg": "get faqs",
          "status": false
      })
    }
  }

  

  async submitFeedback({ request,response,auth }: HttpContextContract){
    const uid:any=auth.user?.id
    const data=request.all()
    var feedback:any = await Feedback.query().where({uid:uid}).first()
    //return response.json(feedback)
    if(!feedback){
      feedback = new Feedback()
      feedback.uid=uid
    }
    feedback.description=data?.description
    //return response.json(feedback)
    try {
      await feedback.save()
      return response.status(200).json({
        "logout": true,
        "resMsg": "Feedback submited",
        "status": true
      })
    } catch (error) {
      await WebServiceLog.create({request:JSON.stringify(uid),response:JSON.stringify(error),service_name:'Submit Feedback',status:1,uid:uid,time_taken:1})
      return response.status(201).json({
          "errors":error,
          "logout": false,
          "resMsg": "Feedback not submited",
          "status": false
      })
    }
  }

  async uploadMultipleFile({ request,response,auth }: HttpContextContract){
    const uid:any=auth.user?.id
   // const data=request.all()
    const allMedai=request.files('file')
   // return allMedai
    var allMediaFileUrl:any=[]
    if(allMedai){
      
      for (const ufile of allMedai) {
        const fileName=`${new Date().getTime()+ufile.size+uid}.${ufile.extname}`
        await ufile.move(Application.tmpPath('uploads'),{
          name: fileName,
        })
        var url = await uploader.uploadFileToTemp(fileName,ufile.type,'temp',uid)
       // console.log(url)
        allMediaFileUrl.push(url?.Location)
        const tempMedia=new TempMedia()
        tempMedia.url=url?.Location
        tempMedia.save()
      }
      // const fileName=`${new Date().getTime()+allMedai.size}.${allMedai.extname}`
      //   await allMedai.move(Application.tmpPath('uploads'),{
      //     name: fileName,
      //   })
      // var url = await uploader.uploadFileToTemp(fileName,allMedai.type,'temp',uid)
      // allMediaFileUrl.push(url?.Location)
      // const tempMedia=new TempMedia()
      // tempMedia.url=url?.Location
      // tempMedia.save()
     // return response.json(allMediaFileUrl)
    }
    if(allMediaFileUrl.length>0){
      return response.status(200).json({
        "appMedia":allMediaFileUrl,
        "logout": true,
        "resMsg": "files upload success",
        "status": true
      })
    }else{
      return response.status(201).json({
        "logout": false,
        "resMsg": "files upload failed",
        "status": false
    })
    }
  }

  async uploadTempFile({ request,response,auth }: HttpContextContract){
    const uid:any=auth.user?.id||0
    const data=request.all()
    var url
    const allMedai=request.file('file')
    await WebServiceLog.create({request:JSON.stringify(allMedai),response:JSON.stringify(data),service_name:'Upload Temp file service',status:1,uid:uid,time_taken:0})
    if(allMedai){
     //const fileName=`${new Date().getTime()+allMedai.size+uid}.${allMedai.extname}`
        const fileName=`${new Date().getTime()+allMedai.size+uid}.${allMedai.extname=='amr'||allMedai.extname=='AMR'||allMedai.extname=='3gp'?'mp3':allMedai.extname}`
        await allMedai.move(Application.tmpPath('uploads'),{
          name: fileName,
        })

        url = await uploader.uploadFileToTemp(fileName,allMedai.type,'temp',uid)
        const tempMedia=new TempMedia()
        tempMedia.url=url?.Location||''       
        try {
          await tempMedia.save()
        } catch (error) {
          return response.status(200).json({
            "errors":error,
            "logout": false,
            "resMsg": "No file uploaded in temp",
            "status": false
          })
        }
    }
    if(url?.Location){
      return response.status(200).json({
        "appMedia":url,
        "logout": false,
        "resMsg": url?.Location,
        "status": true
      })
    }else{
      return response.status(200).json({
        "logout": false,
        "resMsg": "No file uploaded in temp",
        "status": false
      })
    }
  }

  async delete({ request,response,auth }: HttpContextContract){
    const uid:any=auth.user?.id
    const data=request.all()
    const type = data.type
    var deleteData:any
    if(type==1){
      deleteData = await Post.find(data.id)
    }
    if(type==2){
      deleteData= await Comment.find(data.id)
    }
    if(type==3){
      deleteData= await Chat.find(data.id)
    }
    if(type==4){
      deleteData= await Request.find(data.id)
    }
    if(type==5){
      deleteData= await UserAdvanceDetail.find(data.id)
    }
    try {
      deleteData.delete()
      return response.status(200).json({
        "logout": true,
        "resMsg": "Success",
        "status": true
      })
    } catch (error) {
      await WebServiceLog.create({request:JSON.stringify(data),response:JSON.stringify(error),service_name:'Delete Service',status:1,uid:uid,time_taken:1})
      return response.status(201).json({
          "errors":error,
          "logout": false,
          "resMsg": "Data not deleted",
          "status": false
      })
    }
  }

  async deleteFile({ request,response,auth }: HttpContextContract){
    const uid:any=auth.user?.id
    const data=request.all()
    try {

    } catch (error) {
      await WebServiceLog.create({request:JSON.stringify(data),response:JSON.stringify(error),service_name:'Delete file Service',status:1,uid:uid,time_taken:1})
      return response.status(201).json({
          "errors":error,
          "logout": false,
          "resMsg": "file not deleted",
          "status": false
      })
  }
}

  async getDashboardData({response,auth }){
    const uid:any=auth.user?.id
    try {
      var userWallet:any=await LedgerAccount.query().where({'uid':uid,name:'User'}).first()

      var dialed:any = await CallHistory.query().where({'initiater':uid})
      .pojo<{ dialed: number}>()
      .count('id as dialed')
      var missed:any = await CallHistory.query().where({"receiver":uid}).where((query)=>{
                                              query.where("duration",'<',1).orWhereNull('duration')
                                            })
                                            .pojo<{ missed: number}>()
                                            .count('id as missed')
      var received:any = await CallHistory.query().where({"receiver":uid}).where("duration",'>',1)
      .pojo<{ received: number}>()
      .count('id as received')
      var duration:any = await CallHistory.query().where({"receiver":uid}).orWhere({'initiater':uid}).where("duration",'>',1)
      .pojo<{ duration: number}>()
      .sum('duration as duration')
      const userRatAuidoCall:any= await UserRating.query().whereHas("callHistory",(query)=>{
        query.where('video',0)
      }).where('uid',uid)
      .pojo<{ rating: number}>()
      .avg('user_rating as rating')
      const userRatVideoCall:any= await UserRating.query().whereHas("callHistory",(query)=>{
        query.where('video',1)
      }).where('uid',uid)
      .pojo<{ rating: number}>()
      .avg('user_rating as rating')

      const userRatAllCall:any= await UserRating.query().where('uid',uid)
      .pojo<{ rating: number}>()
      .avg('user_rating as rating')
      .pojo<{ count: number}>()
      .count('id as count')
      const audioCallRating=nativeRound(userRatAuidoCall[0].rating)
      const overallRating=nativeRound(userRatAllCall[0].rating)
      const ratingCount=userRatAllCall[0].count
      const videoCallRating=nativeRound(userRatVideoCall[0].rating)
      
      const refundAmountCal= await Refund.query().where({'appliedFor':uid,status:2})
      .pojo<{ amount: number}>()
      .sum('amt as amount').first()
      // return {audioCallRating,overallRating,videoCallRating,ratingCount}
      // return {userRatAuidoCall,userRatVideoCall}

      userWallet=userWallet.serialize()
     // return duration
      return response.status(200).json({
        "calling": {
          "dialed": dialed[0]?.dialed,
          "duration":duration[0]?.duration>0?parseFloat((Math.round(duration[0]?.duration * 100) / 100).toFixed(2)):parseFloat((Math.round(0 * 100) / 100).toFixed(2)),
          "missed": missed[0]?.missed,
          "received": received[0]?.received
        },
        "earning": {
          "available": parseFloat(userWallet?.availableAmount.toFixed(2)),
          "hold": parseFloat(userWallet?.holdAmount.toFixed(2)),
          "refunded": refundAmountCal?.amount||0
        },
        "rating": {
          "audioCallRating": audioCallRating||0,
          "overallRating": overallRating||0,
          "ratingCount": ratingCount||0,
          "videoCallRating": videoCallRating||0
        },
        'uid':uid,
        "logout": true,
        "resMsg": "User Dashboard data",
        "status": true
      })
    } catch (error) {
      await WebServiceLog.create({request:JSON.stringify({}),response:JSON.stringify(error),service_name:'Dashboard data Service',status:1,uid:uid,time_taken:1})
      return response.status(201).json({
          "errors":error,
          "logout": false,
          "resMsg": "No data found",
          "status": false
      })
    }
  }

  async getMasterData({ request,response,auth }: HttpContextContract){
    const uid:any=auth.user?.id
    const data=request.all()
    if(data.masterType==8){
      var countryQuery = CountryState.query()
      if(data.parentId){
        countryQuery.where('parent_id',data.parentId)
     }else{
      //countryQuery.where('parent_id',0)
     }
     if(data.keys){
      var keys=data.keys.toLowerCase()
      countryQuery.where('name', 'LIKE', "%"+keys+"%")
    }
    try {
      var countryData = await (await countryQuery)
                          .map((ele:any)=>{
                            ele.master_type=data.masterType
                            ele.created_by=0
                            ele.image_url=''
                            ele.created=''
                            return ele
                          })
      return response.status(200).json({
        "data":countryData,
        "logout": true,
        "resMsg": "Master data",
        "status": true
      })
    } catch (error) {
      await WebServiceLog.create({request:JSON.stringify({}),response:JSON.stringify(error),service_name:'Get master data Service',status:1,uid:uid,time_taken:1})
       return response.status(201).json({
           "errors":error,
           "logout": false,
           "resMsg": "No data found",
           "status": false
       })
    }
    }else if(data.masterType==7){
      var orgQuery = Organization.query()
      
    if(data.keys){
      var keys=data.keys.toLowerCase()
      orgQuery.where('name', 'LIKE', "%"+keys+"%")
    }
    orgQuery.where({'parentId':0,"type":1})
    try {
      var orgData = await (await orgQuery.apply((scopes) => scopes.isVerified()))
                          .map((ele:any)=>{
                            ele.master_type=data.masterType
                            ele.created_by=0
                            ele.image_url=''
                            ele.parent_id=0
                            return ele
                          })
      return response.status(200).json({
        "data":orgData,
        "logout": true,
        "resMsg": "Master data",
        "status": true
      })
    } catch (error) {
      await WebServiceLog.create({request:JSON.stringify({}),response:JSON.stringify(error),service_name:'Get master data Service',status:1,uid:uid,time_taken:1})
       return response.status(201).json({
           "errors":error,
           "logout": false,
           "resMsg": "No data found",
           "status": false
       })
      }
    }else if(data.masterType==9){
      var orgQuery = Organization.query()
      
      if(data.keys){
        var keys=data.keys.toLowerCase()
        orgQuery.where('name', 'LIKE', "%"+keys+"%")
      }
      orgQuery.where({'parentId':0,"type":2})
      try {
        var orgData = await (await orgQuery.apply((scopes) => scopes.isVerified()))
                            .map((ele:any)=>{
                              ele.master_type=data?.masterType
                              ele.created_by=0
                              ele.image_url=''
                              ele.parent_id=0
                              return ele
                            })
        return response.status(200).json({
          "data":orgData,
          "logout": true,
          "resMsg": "Master data",
          "status": true
        })
      } catch (error) {
        await WebServiceLog.create({request:JSON.stringify({}),response:JSON.stringify(error),service_name:'Get master data Service',status:1,uid:uid,time_taken:1})
         return response.status(201).json({
             "errors":error,
             "logout": false,
             "resMsg": "No data found",
             "status": false
         })
        }
    }
    else{
      try {
        var masterQuery = MasterGodown.query()
        if(data.masterType){
           masterQuery.where('masterType',data.masterType)
        }
        if(data.parentId){
           masterQuery.where('parentId',data.parentId)
        }else{
          masterQuery.where('parentId',0)
        }
        if(data.keys){
          var keys=data.keys.toLowerCase()
          if(data.masterType==6){
            keys= keys.replace(/#/g,'')
          }
          //return keys
          masterQuery.where('name', 'LIKE', "%"+keys+"%")
        }
        var masterData = await masterQuery.apply((scopes) => scopes.isVerified()).orderBy('used','desc')
        return response.status(200).json({
          "data":masterData,
         "logout": true,
         "resMsg": "Master data",
         "status": true
        })
     } catch (error) {
       await WebServiceLog.create({request:JSON.stringify({}),response:JSON.stringify(error),service_name:'Get master data Service',status:1,uid:uid,time_taken:1})
       return response.status(201).json({
           "errors":error,
           "logout": false,
           "resMsg": "No data found",
           "status": false
       })
     }
    }
    
  }
async getSkillsList({response}){
    var masterDataQuery = MasterGodown.query().where('master_type',5)

    var masterData = await (await masterDataQuery).map(elm=>{
      return elm
      return {"value":elm?.name,"text":elm?.name}
    })
    return response.status(200).json(masterData)
  }

  async getSkills({response}){
    var masterData = await MasterGodown.query().where('master_type',5).limit(6)
    return response.status(200).json({
      "data":masterData,
     "logout": false,
     "resMsg": "Master data",
     "status": true
    })
  }

  async uploadRecFile({ request,response,auth }: HttpContextContract){
    const uid:any=auth.user?.id||0
    const data=request.all()
    var url
    const allMedai=request.file('file')
    await WebServiceLog.create({request:JSON.stringify(allMedai),response:JSON.stringify(data),service_name:'Upload Recording file service',status:1,uid:uid,time_taken:0})
    if(allMedai){
      //const fileName=`${new Date().getTime()+allMedai.size+uid}.${allMedai.extname}`
         const fileName=`${new Date().getTime()+allMedai.size+uid}.${allMedai.extname=='amr'||allMedai.extname=='AMR'?'mp3':allMedai.extname}`
         await allMedai.move(Application.publicPath('recordings'),{
           name: fileName,
         })

          const convetFile = "seekexRec_"+fileName
          var command = SoxCommand()
          var inFileName='./public/recordings/'+fileName
          var outFile='./public/recordings/'+convetFile
          command.input(inFileName)
          command.output(outFile)
          command.outputFileType('mp3').outputSampleRate(1600)

          command.on('start', function(commandLine) {
            console.log('Spawned sox with command ' + commandLine);
          })
          
          // command.on('progress', function(progress) {
          //   console.log('Processing progress: ', progress);
          // })
          
          // command.on('error', function(err, stdout, stderr) {
          //   console.log('Cannot process audio: ' + err.message);
          //   console.log('Sox Command Stdout: ', stdout);
          //   console.log('Sox Command Stderr: ', stderr)
          // })
          
          command.on('end', async function() {
            console.log('Sox command succeeded!');
            var uploadFile= await uploader.uploadRecording(convetFile,'recordings',uid)
            console.log('Upload File',uploadFile)
          })
          command.run()

          url = await uploader.uploadRecording(fileName,allMedai.type,uid)

     }

     if(url?.Location){
       return response.status(200).json({
         "appMedia":url,
         "logout": false,
         "resMsg": url?.Location,
         "status": true
       })
     }else{
       return response.status(200).json({
         "logout": false,
         "resMsg": "No file uploaded in recordings",
         "status": false
       })
     }
  }
}
