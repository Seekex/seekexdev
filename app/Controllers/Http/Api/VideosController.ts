import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import Application from '@ioc:Adonis/Core/Application'
import UserVideo from 'App/Models/UserVideo'
const perf = require('execution-time')()
import CommonHelper from 'App/Helpers/CommonHelper'
const  commonHelper = new CommonHelper()
import VideoTaggedUser from 'App/Models/VideoTaggedUser'
import LikeVideoComment from 'App/Models/LikeVideoComment'
import LikeVideo from 'App/Models/LikeVideo'
import FollowVideo from 'App/Models/FollowVideo'
import WebServiceLog from 'App/Models/WebServiceLog'
import VideoComment from 'App/Models/VideoComment'
import NotificationHelper from 'App/Helpers/NotificationHelper'
const notificationHelper = new NotificationHelper()
import Event from '@ioc:Adonis/Core/Event'
import User from 'App/Models/User'
import FollowUser from 'App/Models/FollowUser'
import View from 'App/Models/View'

export default class VideosController {

  async saveVideo({ request,response,auth }: HttpContextContract){
    perf.start()
    const uid:any=auth.user?.id
    const data=request.all()
    var video = new  UserVideo()
    video.createdBy=uid
    video.videoUrl=data?.videoUrl||''
    video.publicId=data?.publicId||0
    video.description=data.description
    video.location=data.location
    video.privates=data.privates
    video.views=data.views

    const tagList = await commonHelper.extractHashTag(video?.description)
    if(tagList.length>0){
      await commonHelper.matchHashTagAndSave(tagList,uid)
    }
    video.hash_tags=tagList.toString()
    try {
      await video.save()
      if(data?.userTagged){
        //var ucount=0
         data.userTagged.forEach(async(element) => {
           await VideoTaggedUser.create({userVideoId:video?.id,uid:element?.uid})
         })
       }
       return response.status(200).json({
        "video_id":video?.id,
        "resMsg": "Success",
        "status": true,
        "logout": true
      })
    }catch(error){
      const tres=perf.stop()
      data.description.replace(/[^\x00-\x7F]/g, "")
      await WebServiceLog.create({request:JSON.stringify(data),response:JSON.stringify(error),service_name:'Video Save Service Error',status:1,uid:uid,time_taken:tres.time})
      return response.status(200).json({
          "errors":error,
          "logout": false,
          "resMsg": "error",
          "status": false
      })
    }
  }

  async getVideos({ request,response,auth }: HttpContextContract){
    const authorization =  request.header('authorization')
    var uid
    if(authorization && authorization !='Bearer'){
      try {
        await auth.use('api').authenticate()
      } catch (error) {
        return response.status(200).json({
          "errors":error,
          "resMsg": "An Error",
          "status": false,
          "logout": true
        })
      }
      uid = auth.use('api')?.user?.id
    }
   // const uid:any=auth.user?.id
    const data=request.all()
    const videoId = data?.videoId||0
    const videoUser = data?.userId||0 //  For Only User Video

    var page:any = request.input('page', 1)
    if(page=='null' || page==null ||page=='NULL' || page==0){
      page=1
    }
    var lastPage
    const limit = 9
    var videos:any

    var videoQuery=UserVideo.query().preload('user',query=>{
      query.preload('highestEducation')
          .preload('occupation')
          .preload('currentOrg')
          .preload('collegeUniversity')
    })
    .has('user')
    .preload('userTagged',query=>{
      query.preload('highestEducation')
      .preload('occupation')
      .preload('currentOrg')
      .preload('collegeUniversity')
     })
    .withCount('comments',(q)=>{
       q.has('user')
    })
    .withCount('videoLikes')
    .withCount('videoFollows')
    .withCount('videoViews')
    .where({'status':1,videoType:1,deleted:0})
    if(videoUser>0 && uid){
      videoQuery.where('createdBy',videoUser)
    }
    videos = await videoQuery.orderBy('id','desc').paginate(page,limit)
    lastPage = videos.lastPage
    videos=videos.rows
    videos = await Promise.all(videos.map(async(elm)=>{
      var nw = elm.serialize()
        nw.userProfilePicUrl=nw?.profileImage||''
        nw.userId=nw?.uid
        nw.userName=nw?.name||''
        nw.likesCount=elm?.$extras.videoLikes_count||0
        nw.followCount=elm?.$extras.videoFollows_count||0
        nw.commentsCount=elm?.$extras.comments_count||0
        nw.views = elm?.$extras.videoViews_count||0
        if(uid){
          nw.like = await LikeVideo.query().where({uid:uid,userVideoId:nw?.id}).first()?true:false
          nw.follow= await FollowVideo.query().where({uid:uid,userVideoId:nw?.id}).first()?true:false
          nw.userFollow= await FollowUser.query().where({uid:uid,targetId:nw?.created_by}).first()?true:false
        }else{
          nw.like = false
          nw.follow= false
          nw.userFollow= false
        }
        nw.audioCallRate = ''+nw?.user?.audioCallRatePerSec
        nw.videoCallRate = ''+nw?.user?.videoCallRatePerSec
        nw.expert =nw?.user?.expert||false
        delete nw['user']
        return nw
    }))
    if(videoId>0){
      var firstVideoElm:any = await videoQuery.where('id',videoId).where("deleted",0).first()
      if(firstVideoElm){
        var firstVideo = firstVideoElm?.serialize()
          firstVideo.userProfilePicUrl=firstVideo?.profileImage||''
          firstVideo.userId=firstVideo?.uid
          firstVideo.userName=firstVideo?.name||''
          firstVideo.likesCount=firstVideoElm?.$extras.videoLikes_count||0
          firstVideo.followCount=firstVideoElm?.$extras.videoFollows_count||0
          firstVideo.commentsCount=firstVideoElm?.$extras.comments_count||0
          firstVideo.views = firstVideoElm?.$extras.videoViews_count||0
          if(uid){
            firstVideo.like = await LikeVideo.query().where({uid:uid,userVideoId:firstVideo?.id}).first()?true:false
            firstVideo.follow= await FollowVideo.query().where({uid:uid,userVideoId:firstVideo?.id}).first()?true:false
            firstVideo.userFollow= await FollowUser.query().where({uid:uid,targetId:firstVideo?.created_by}).first()?true:false
          }else{
            firstVideo.like = false
            firstVideo.follow= false
            firstVideo.userFollow= false
          }
          firstVideo.audioCallRate = ''+firstVideo?.user?.audioCallRatePerSec
          firstVideo.videoCallRate = ''+firstVideo?.user?.videoCallRatePerSec
          delete firstVideo['user']
          var fEIndx = videos.findIndex(elm=>elm?.id==firstVideo?.id)
          //console.log('index',fEIndx)
          if(fEIndx>-1){
            videos.splice(fEIndx, 1)
          }else{
            videos.splice(0, 1)
          }
          
          videos.splice(0, 0, firstVideo) 
      }    
    }
    const intorVideosQuery = UserVideo.query().preload('user',query=>{
      query.preload('highestEducation')
          .preload('occupation')
          .preload('currentOrg')
          .preload('collegeUniversity')
    })
    .has('user')
    .preload('userTagged',query=>{
      query.preload('highestEducation')
      .preload('occupation')
      .preload('currentOrg')
      .preload('collegeUniversity')
     })
    .withCount('comments',(q)=>{
       q.has('user')
    })
    .withCount('videoLikes')
    .withCount('videoFollows')
    .withCount('videoViews')
    .where({'status':1,videoType:2,deleted:0})
    if(videoUser>0 && uid){
      intorVideosQuery.where('createdBy',videoUser)
    }
    const introVideos:any = await intorVideosQuery.orderBy('id','desc').paginate(page,3)
    var intVid = introVideos.rows
     intVid = await Promise.all(intVid.map(async (elm)=>{
        var nw = elm.serialize()
        nw.userProfilePicUrl=nw?.profileImage||''
        nw.userId=nw?.uid
        nw.userName=nw?.name||''
        nw.likesCount=elm?.$extras.videoLikes_count||0
        nw.followCount=elm?.$extras.videoFollows_count||0
        nw.commentsCount=elm?.$extras.comments_count||0
        nw.views = elm?.$extras.videoViews_count||0
        if(uid){
          nw.like = await LikeVideo.query().where({uid:uid,userVideoId:nw?.id}).first()?true:false
          nw.follow= await FollowVideo.query().where({uid:uid,userVideoId:nw?.id}).first()?true:false
          nw.userFollow= await FollowUser.query().where({uid:uid,targetId:nw?.created_by}).first()?true:false
        }else{
          nw.like = false
          nw.follow= false
          nw.userFollow= false
        }
        nw.audioCallRate = ''+nw?.user?.audioCallRatePerSec
        nw.videoCallRate = ''+nw?.user?.videoCallRatePerSec
        delete nw['user']
        return nw
    }))
    if(intVid.length>0){
      intVid.forEach((elm,indx) => {
        const nIndx = ((indx+1)*3)+indx
        videos.splice(nIndx, 0, elm)
      });
    }
    return response.status(200).json({
      "videoDTOS":videos,
      "postDTOS":[],
      "requestDTOS":[],
      "logout": false,
      "resMsg": "Success",
      "status": true,
      "currentPage":page,
      "totalPage":lastPage,
      "prevPage":page>1?page-1:null,
      "nextPage":page<lastPage?page+1:null,
      "uid":uid
    })
  }

  async deleteVideo({ request,response,auth }: HttpContextContract){
    const uid:any=auth.user?.id
    const data = request.all()
    const videoId = data?.id||data?.videoId||data?.userVideoId
    if(videoId){
      try {
        const dv = await UserVideo.query().where({id:videoId,createdBy:uid}).update({deleted:1})
        if(dv[0]){
          return response.status(200).json({
            "resMsg": "Success",
            "status": true,
            "logout": false,
          })
        }else{
          return response.status(200).json({
            "resMsg": "Not valid video",
            "status": false,
            "logout": false
          })
        }
      } catch (error) {
        return response.status(200).json({
          "errors":error,
          "resMsg": "An Internal Error",
          "status": false,
          "logout": false
        })
      }
      
    }
  }

  async postVideoComments({ request,response,auth }: HttpContextContract){
    const uid:any=auth.user?.id
    const data=request.all()
    var comment=new VideoComment()
    comment.comment=data.comment?data.comment:data.keyValue
    comment.userVideoId=data?.postId||data?.id||data?.userVideoId
    comment.parentId=data?.parentId
    comment.uid=uid
    try {
      //********************************************** */
      await notificationHelper.sendNotificationForVideoComment(comment,auth?.user)
      //********************************************* */
      await comment.save()
      if(comment.userVideoId){
       // Event.emit('new:sendNotificationForCommentToFollowers',{data:comment,fromUser:auth?.user})
      }
     // return true
      // if (comment.postId) {
      //   var post:any = await Post.find(data.postId)
      //   post.comments=post.comments+1
      //   await post.save()
      // }
      var allComments=await (await VideoComment.query().preload('user',(query)=>{
                            query.select('name','profilePic')
                          }).where('userVideoId',comment.userVideoId).limit(20).orderBy('id','asc'))
                          .map((comment:any)=>{
                            comment.name=comment.user.name
                            comment.profilePic=comment.user.profilePic
                            comment.likestatus=comment.likes>0?true:false
                            return comment
                          })

      return response.status(200).json({
        "block": false,
        "blocked": false,
        "comments":allComments,
        "logout": false,
        "resMsg": "Comment has been saved",
        "status": true
      })
    }catch (error) {
      await WebServiceLog.create({request:JSON.stringify(data),response:JSON.stringify(error),service_name:'Post Video Comment Service error',status:1,uid:uid,time_taken:1})
      return response.status(200).json({
          "errors":error,
          "logout": false,
          "resMsg": "Video comment not saved",
          "status": false
      })
    }
  }

  async getVideoCommnets({ request,response,auth }: HttpContextContract){
    const uid:any=auth.user?.id
    const data=request.all()
    const userVideoId=data?.postId||data?.id||data?.userVideoId
    try {
      var allComments= await Promise.all((await VideoComment.query().preload('user',(query)=>{
                            query.select('name','profilePic')
                          }).has('user')
                          .preload('parents',(query)=>{
                            query.preload('user',(query)=>{
                              query.select('name','profilePic')
                            }).preload('parents',(query)=>{
                              query.preload('user',(query)=>{
                                query.select('name','profilePic')
                              })
                            })
                          })
                          .where('userVideoId',userVideoId)
                          .limit(20).orderBy('id','asc'))
                          .map(async(comment:any)=>{
                            const likeComment = await LikeVideoComment.query().where({targetId:comment?.id,uid:uid}).first()
                            comment.name=comment.user.name
                            comment.profilePic=comment.user.profilePic
                            comment.likestatus=likeComment?true:false
                            return comment
                          }))

      return response.status(200).json({
        "block": false,
        "blocked": false,
        "comments":allComments,
        "logout": false,
        "resMsg": "all post comments",
        "status": true
      })    
    } catch (error) {
      await WebServiceLog.create({request:JSON.stringify(data),response:JSON.stringify(error),service_name:'Get Video Comment Service',status:1,uid:uid,time_taken:1})
      return response.status(200).json({
          "errors":error,
          "logout": false,
          "resMsg": "get post comments",
          "status": false
      })
    }
    
  }

  async likeFollowVideo({ request,response,auth }: HttpContextContract){
    const uid=auth.user?.id
    const data = request.all()
    await WebServiceLog.create({request:JSON.stringify(data),response:JSON.stringify(uid),service_name:'Video Follow Like Service',status:1,uid:uid,time_taken:0})
    var exit:any
    var saveData:any
    if(data.type =='1'){
      saveData=new LikeVideo()
      saveData.uid=uid
      saveData.userVideoId=data?.targetId
      exit=await LikeVideo.query().where({uid:uid,userVideoId:data?.targetId}).delete()
    }
    if(data.type=='2'){
      saveData=new LikeVideoComment()
      saveData.uid=uid
      saveData.targetId=data?.targetId
      exit=await LikeVideoComment.query().where({uid:uid,targetId:data.targetId}).delete()
    }
    if(data.type=='3'){
      saveData=new FollowVideo()
      saveData.uid=uid
      saveData.userVideoId=data?.targetId
      exit=await FollowVideo.query().where({uid:uid,userVideoId:data?.targetId}).delete()
    }
    
    //return {exit,saveData}
    if(exit==0){
      await notificationHelper.sendNotificationForLikesFollowVideo(saveData,auth?.user,data?.type)
    }
    try {
     if(exit==0){
        await saveData.save()
      }
      return response.status(200).json({
        "logout": false,
        "resMsg": "Successful",
        "status": true,
        "action":exit
      })
    } catch (error) {
      await WebServiceLog.create({request:JSON.stringify(uid),response:JSON.stringify(error),service_name:'Video Get Follow Like Service error',status:1,uid:uid,time_taken:0})
      return response.status(200).json({
        "errors":error,
        "resMsg": "Error Like Follow Video",
        "status": false,
        "logout": false
      })
    }
  }

  async viewVideo({ request,response,auth }: HttpContextContract){
    const data = request.all()
    const uid:any=auth.user?.id
    const vID = data?.postId||data?.id||data?.userVideoId
    const existView = await View.query().where({viewBy:uid,targetId:vID,type:3}).first()
    if(!existView){
      const v= new View()
      v.viewBy=uid
      v.targetId=vID
      v.type =3
      await v.save()
    }
    // await UserVideo.query().whereNull('views').update({views:0})
    // const video = await UserVideo.query().where('id',vID).increment('views',1)

    return response.status(200).json({
      "logout": false,
      "resMsg": "Success",
      "status": true
    })
  }

}
