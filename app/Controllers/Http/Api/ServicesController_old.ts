import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import User from "App/Models/User"
import UserToken from "App/Models/UserToken"
import ResumeHelper from "App/Helpers/ResumeHelper"
const resumeHelper:any=new ResumeHelper()
import WebServiceLog from 'App/Models/WebServiceLog'
import Invoice from 'App/Models/Invoice'
import EmailHelper from "App/Helpers/EmailHelper"
import Application from '@ioc:Adonis/Core/Application'

const emailHelper:any=new EmailHelper()
const request = require('request')
const fs = require('fs')
let pdf = require("html-pdf")
const perf = require('execution-time')()
import LedgerHelper from 'App/Helpers/LedgerHelper'
const Ledger = new LedgerHelper()
import FirebaseHelper from 'App/Helpers/FirebaseHelper'
const firebaseHelper=new FirebaseHelper()
const ext = (url)=>{
  return (url = url.substr(1 + url.lastIndexOf("/")).split('?')[0]).split('#')[0].substr(url.lastIndexOf("."))
}
const fileName = (url)=>{
  return (url = url.substr(1 + url.lastIndexOf("/")).split('?')[0])
}

const download = function(uri, filename, callback){
  request.head(uri, function(err, res, body){
   // console.log('content-type:', res.headers['content-type']);
   // console.log('content-length:', res.headers['content-length']);
    request(uri).pipe(fs.createWriteStream(filename)).on('close', callback);
  })
}
import NotificationHelper from 'App/Helpers/NotificationHelper'
const notificationHelper = new NotificationHelper()
import Request from 'App/Models/Request'
import Post from 'App/Models/Post'
import TransactionHistory from 'App/Models/TransactionHistory'
import Chat from 'App/Models/Chat'
import CallHistory from 'App/Models/CallHistory'
import AppNotification from 'App/Models/AppNotification'
import Refund from 'App/Models/Refund'
import ExpertSchedule from 'App/Models/ExpertSchedule'
import { DateTime,Duration } from 'luxon'
const expertSchedules = async(ids)=>{
  return ids
  //var schs:any= await ExpertSchedule.query().whereIn('id',ids).orderBy('id','asc')
  //  return schs
}
import CommonHelper from 'App/Helpers/CommonHelper'
const commonHelper = new CommonHelper()

import TwilioHelper from 'App/Helpers/TwilioHelper'
import AppSetting from 'App/Models/AppSetting'
import LedgerAccount from 'App/Models/LedgerAccount'
import Event from '@ioc:Adonis/Core/Event'
import LikePost from 'App/Models/LikePost'
import FollowPost from 'App/Models/FollowPost'
import BlockList from 'App/Models/BlockList'
import FollowUser from 'App/Models/FollowUser'
import CmsPage from 'App/Models/CmsPage'
import Faq from 'App/Models/Faq'
import LoginHistory from 'App/Models/LoginHistory'
const twilioHelper = new TwilioHelper()

var calculateInterest = function (total,ratePercent, roundToPlaces) {
  var interestRate = ((ratePercent/100) - 1)
  return (total * interestRate).toFixed(roundToPlaces)
}

export default class ServicesController {
  
  async readResume({auth,response}){
    const uid:any=auth.user?.id
    const user=await User.find(uid)
    let resumeFile=user?.resume
   //https://seekex.s3.ap-south-1.amazonaws.com/resume/1585664707790.pdf
   //https://seekex.s3.ap-south-1.amazonaws.com/resume/Resume.doc
   // let resumeFile='./public/Resume.doc'
   // let resumeFile='https://seekex.s3.ap-south-1.amazonaws.com/resume/Resume.doc'
    if(resumeFile){
      let fileExt=ext(resumeFile)
      let fileNme=fileName(resumeFile)
      try {
        await download(resumeFile, './public/'+fileNme, async function(){})
        var parseFile= await resumeHelper.parseResume(fileNme,fileExt)
        if(parseFile){
          console.log('file data',parseFile)
          return response.status(200).json({
            "logout": true,
            "resMsg": "Done",
            "status": true
          })
        }
      } catch (error) {
        console.log(error)
      }
    }
  }

  async updateUserToken({auth,response,request}:HttpContextContract){
    const uid:any=auth.user?.id
    const data=request.all()
    var userToken=await UserToken.query().where({user_id:uid,device_id:data.deviceId}).first()
    if(userToken){
      userToken.userToken=data.userToken
      userToken.fcmToken=data.fcmToken
      try {
        await userToken.save()
        return response.status(200).json({
          "data":userToken,
          "logout": true,
          "resMsg": "Done",
          "status": true
        })
      } catch (error) {
        await WebServiceLog.create({request:JSON.stringify(data),response:JSON.stringify(error),service_name:'User Token Service',status:1,uid:uid,time_taken:0})
      return response.status(201).json({
        "errors":error,
        "resMsg": "Error",
        "status": false,
        "logout": false
      })
      }
    }else{
      var newuserToken=new UserToken()
      newuserToken.userToken=data.userToken
      newuserToken.fcmToken=data.fcmToken
      newuserToken.userId=uid
      newuserToken.deviceId=data.deviceId
      try {
        await newuserToken.save()
        return response.status(200).json({
          "data":newuserToken,
          "logout": true,
          "resMsg": "Done",
          "status": true
        })
      } catch (error) {
        await WebServiceLog.create({request:JSON.stringify(data),response:JSON.stringify(error),service_name:'User Token Service',status:1,uid:uid,time_taken:0})
        return response.status(201).json({
          "errors":error,
          "resMsg": "Error",
          "status": false,
          "logout": false
        })
      }
    }
  }

  async invoiceRequest({auth,response,request,view}:HttpContextContract){
    perf.start()
    const uid:any=auth.user?.id
    const email=auth.user?.email
    const data=request.all()
    const invoiceData = await Invoice.find(data.invoice)
    const user = await User.find(uid)
    var tempFile = view.render('invoices/invoice',{invoiceData,user})
    let options = {
      "height": "11.25in",
      "width": "8.5in",
      "header": {
          "height": "20mm"
      },
      "footer": {
          "height": "20mm",
      },
    }
    const fileName = "invoice"+invoiceData?.id+".pdf"
    pdf.create(tempFile, options).toFile(Application.publicPath(fileName),async function (err,data) {
      if (err) {
        const res = perf.stop()
        await WebServiceLog.create({request:JSON.stringify(data),response:JSON.stringify(err),service_name:'Invoice email Service',status:1,uid:uid,time_taken:res.time})
        fs.unlink(Application.publicPath(fileName))
        return false
      } else {
        var sendData={to:email,subject:'Invoice',message:'Your invoice is'}
        await emailHelper.sendEmailInvoice(sendData,fileName)
        fs.unlink(Application.publicPath(fileName))
        console.log("File Sent successfully")
        return true
      }
    })
    return response.status(200).json({
      "logout": false,
      "resMsg": "Success",
      "status": true
    })
  }


  async cancelAppointmentRequest({auth,response,request}:HttpContextContract){
    perf.start()
    const uid:any=auth.user?.id
    const appId=request.input('requestId')
    const comment=request.input('reasontext')
    const appointment:any= await Request.query().where("id",appId).first()
    appointment.comment=comment
    appointment.canceledBy=uid
    appointment.status=4
    appointment.canceledDate=new Date()
    try {
        //*********************Notifications**********************/
        var notifyUserId
        if(uid==appointment?.requestedBy){
          notifyUserId=appointment?.requestedBy
        }else{
          notifyUserId=appointment?.toUid
        }
        var fromUser = await User.find(uid)
        var notifyUser= await LoginHistory.query().where("uid",notifyUserId).orderBy("id","desc").first()
        var notifyData={fcm:notifyUser?.fcmToken||request.input('fcm',''),date:appointment?.bookingDate}
        var notify:any=await notificationHelper.sendNotifictionForCancelRequest(notifyData,fromUser)

        //*********************Notifications**********************/
      await appointment.save()
      await Ledger.releaseAmountForRequest(appointment)
      return response.status(200).json({
          "logout": false,
          "resMsg": "Appointment successfuly canceled.",
          "status": true
        })
    } catch (error) {
      const exeTime = perf.stop()
      await WebServiceLog.create({request:JSON.stringify(request.all()),response:JSON.stringify(error),service_name:'Appointment cancel service error',status:1,uid:uid,time_taken:exeTime.time})
      return response.status(201).json({
        "logout": false,
        "resMsg": "Internal error, please try again.",
        "status": false
      })
    }
  }

  async changeRequestStatus({auth,response,request}:HttpContextContract){
    perf.start()
    const uid:any=auth.user?.id
    const appId=request.input('requestId')||request.input('id')
    const comment=request.input('text')
    const status =request.input('status')
    const appointment:any= await Request.query().where("id",appId).first()
    appointment.comment=comment
    appointment.status=status
    if(status==2){
      var expert = await User.find(appointment?.toUid)
      var notifyUser= await LoginHistory.query().where("uid",appointment?.requestedBy).orderBy("id","desc").first()
      var notifyData={fcm:notifyUser?.fcmToken||request.input('fcm','')}
      var notify:any=await notificationHelper.sendNotifictionForAcceptRequest(notifyData,expert)
    }
    if(status==4){
      appointment.canceledBy=uid
      appointment.canceledDate=new Date()
    }
    try {
      await appointment.save()
      return response.status(200).json({
          "logout": false,
          "resMsg": "Appointment status changed successfuly.",
          "status": true
        })
    } catch (error) {
      const exeTime = perf.stop()
      await WebServiceLog.create({request:JSON.stringify(request.all()),response:JSON.stringify(error),service_name:'Appointment cancel service error',status:1,uid:uid,time_taken:exeTime.time})
      return response.status(201).json({
        "logout": false,
        "resMsg": "Internal error, please try again.",
        "status": false
      })
    }
  }


/* Search Types
  //SEARCH_TYPE_BOTH = 1
  SEARCH_TYPE_POST = 2
  SEARCH_TYPE_USER = 3
  SEARCH_TYPE_REQUEST = 4
  SEARCH_TYPE_TRANSACTION = 5
  SEARCH_TYPE_CHAT = 6
  SEARCH_TYPE_CALL_HISTORY = 7
  SEARCH_TYPE_NOTIFICATION = 8
  SEARCH_TYPE_RATING = 9
  SEARCH_TYPE_REFUNDS = 10 
*/
  async searchData({auth,response,request}:HttpContextContract){
    const uid:any=auth.user?.id
    const data=request.all()
    const keys=data?.keys||''
    const keyType=data?.keyType||''
    const type=data?.type||1
    var searchData:any
    var searchDataprofile:any
    const page = request.input('page', 1)
    var lastPage
    const limit = 10
      // if(type==1){
      //   var profiles = User.query()
      //   .preload('accomplishmentsList')
      //   .preload('certificatesList')
      //   .preload('projectList')
      //   .preload('country')
      //   .preload('state')
      //   .preload('intExpertiseList')
      //   if(keys && keyType=='profile'){
      //   profiles.preload('highestEducation').preload('occupation')
      //   .preload('skillsList')
            
      //   profiles.where({'expert':1,expertmode:true}).where('name', 'LIKE', "%"+keys+"%").orWhere('about_me', 'LIKE', "%"+keys+"%")
        
      //   }

      //   if(keys && keyType=='education'){  
      //   profiles.preload('highestEducation')
      //   .preload('occupation')
      //   .preload('skillsList')
      //   profiles.where({'expert':1,expertmode:true}).whereHas('highestEducation',(query)=>{
      //   query.whereHas('master',(qu)=>{
      //   qu.where('name', 'LIKE', "%"+keys+"%")
      //   })
      //   })
      //   // searchData=await profiles
      //   }
      //   if(keys && keyType=='occupation'){  
      //   profiles.preload('highestEducation')
      //   .preload('occupation')
      //   .preload('skillsList')
      //   profiles.where({'expert':1,expertmode:true}).whereHas('occupation',(query)=>{
      //   query.whereHas('master',(qu)=>{
      //   qu.where('name', 'LIKE', "%"+keys+"%")
      //   })
      //   })
      //   // searchData=await profiles
      //   }
      //   if(keys && keyType=='skills'){  
      //   profiles.preload('highestEducation')
      //   .preload('occupation')
      //   .preload('skillsList')
      //   profiles.where({'expert':1,expertmode:true}).whereHas('skillsList',(query)=>{
      //   query.whereHas('master',(qu)=>{
      //   qu.where('name', 'LIKE', "%"+keys+"%")
      //   })
      //   })

      //   }
      //   if(keys && keyType=='all'){
      //     profiles.preload('highestEducation')
      //     .preload('occupation')
      //     .preload('skillsList')
      //     profiles.where({'expert':1,expertmode:true})
      //             .where('name', 'LIKE', "%"+keys+"%")
      //             .orWhere('about_me', 'LIKE', "%"+keys+"%")
      //             .orWhere('resume_text', 'LIKE', "%"+keys+"%")
      //             .orWhereHas('highestEducation',(query)=>{
      //               query.whereHas('master',(qu)=>{
      //                 qu.where('name', 'LIKE', "%"+keys+"%")
      //               })
      //             })
      //             .orWhereHas('occupation',(query)=>{
      //               query.whereHas('master',(qu)=>{
      //                 qu.where('name', 'LIKE', "%"+keys+"%")
      //               })
      //             })
      //             .orWhereHas('skillsList',(query)=>{
      //               query.whereHas('master',(qu)=>{
      //                 qu.where('name', 'LIKE', "%"+keys+"%")
      //               })
      //             })
      //             .orWhereHas('hashtags',(query)=>{
      //               query.whereHas('master',(qu)=>{
      //                 qu.where('name', 'LIKE', "%"+keys+"%")
      //               })
      //             })

      //   }
      //   if(keys && keyType=='hashtags'){
      //     profiles.preload('highestEducation')
      //     .preload('occupation')
      //     .preload('skillsList')
      //     profiles.where({'expert':1,expertmode:true}).whereHas('hashtags',(query)=>{
      //       query.whereHas('master',(qu)=>{
      //       qu.where('name', 'LIKE', "%"+keys+"%")
      //       })
      //     })
      //   }else{
      //     profiles.preload('highestEducation').preload('occupation')
      //     .preload('skillsList')
      //     profiles.where({'expert':1,expertmode:true}).where('name', 'LIKE', "%"+keys+"%").orWhere('about_me', 'LIKE', "%"+keys+"%")
      //   }
      //   searchDataprofile=await profiles.paginate(page,limit)
      //    lastPage= searchDataprofile?.lastPage
      //   searchDataprofile=searchDataprofile.rows

      //   searchDataprofile = await Promise.all(searchDataprofile.map(async(elm:any)=>{
      //     if(elm.audioCallRate>0){
      //       elm.audioCallRate= await commonHelper.showCallAmount(elm?.audioCallRate||0,false,'VOIP')
      //      // elm.audioCallRate=parseFloat((elm.audioCallRate/(5*60)).toFixed(3))
      //     }
      //     if(elm.videoCallRate>0){
      //       elm.videoCallRate=await commonHelper.showCallAmount(elm?.videoCallRate||0,true,'VOIP')
      //      // elm.videoCallRate=parseFloat((elm.videoCallRate/(5*60)).toFixed(3))
      //     }

      //   var newElm:any = elm.serialize()
      //   var profileData = elm.serialize()
      //   newElm.profile =  profileData
      //   if(!newElm?.country){
      //   newElm.country={name:'',parentId:0,masterType:0,imageUrl:'',used:0}
      //   }
      //   if(!newElm?.state){
      //   newElm.state={name:'',parentId:0,masterType:0,imageUrl:'',used:0}
      //   }
      //   if(!newElm.highestEducation){
      //   newElm.highestEducation={name:'',parentId:0,masterType:0,imageUrl:'',used:0}
      //   }
      //   if(!newElm.occupation){
      //   newElm.occupation={name:'',parentId:0,masterType:0,imageUrl:'',used:0}
      //   }
      //   if(!newElm.rating){
      //   newElm.rating=0
      //   }
      //   if(!newElm.ratingCount){
      //   newElm.ratingCount=0
      //   }

      //   newElm.block= await BlockList.query().where({byUid:newElm?.id,toUid:uid}).first()?true:false
      //   newElm.blocked= await BlockList.query().where({byUid:uid,toUid:newElm?.id}).first()?true:false
      //   newElm.followstatus = await FollowUser.query().where({uid:uid,targetId:newElm?.id}).first()?true:false
      //   if(newElm.profile?.hasOwnProperty('country')){
      //   delete newElm.profile['country']
      //   }
      //   if(newElm.profile?.hasOwnProperty('state')){
      //   delete newElm.profile['state']
      //   }
      //   if(newElm.profile?.hasOwnProperty('accomplishmentsList')){
      //   delete newElm.profile['accomplishmentsList']
      //   }
      //   if(newElm.profile?.hasOwnProperty('certificatesList')){
      //   delete newElm.profile['certificatesList']
      //   }
      //   if(newElm.profile?.hasOwnProperty('projectList')){
      //   delete newElm.profile['projectList']
      //   }
      //   if(newElm.profile?.hasOwnProperty('skillsList')){
      //   delete newElm.profile['skillsList']
      //   }
      //   if(newElm.profile?.hasOwnProperty('intExpertiseList')){
      //   delete newElm.profile['intExpertiseList']
      //   }
      //   if(newElm.profile?.hasOwnProperty('highestEducation')){
      //   delete newElm.profile['highestEducation']
      //   }
      //   if(newElm.profile?.hasOwnProperty('occupation')){
      //   delete newElm.profile['occupation']
      //   }

      //   newElm.genderId=parseInt(newElm.genderId)
      //   return {userProfileDTO:newElm}

      //   }))

      //   var postQuery = Post.query().preload('user',query=>{
      //     query.preload('highestEducation')
      //         .preload('occupation')
      //   }).preload('userTagged',query=>{
      //     query.preload('accomplishmentsList')
      //     .preload('certificatesList')
      //     .preload('projectList')
      //     .preload('country')
      //     .preload('state')
      //     .preload('highestEducation')
      //     .preload('occupation')
      //     .preload('skillsList')
      //     .preload('intExpertiseList')
      //    }).preload('medias',(query)=>{
      //       query.select('url').where('media_type',5)
      //   })
      //   if(keys){
      //     postQuery.where('description', 'LIKE', "%"+keys+"%").orWhere('hash_tags', 'LIKE', "%"+keys+"%")
      //   }

      //   searchData = await postQuery.paginate(page,limit)
      //   if(searchData?.lastPage>lastPage){
      //     lastPage = searchData?.lastPage
      //   }
      //   searchData=searchData.rows
      //   var newpostData:any=[]
      //   for (const element of searchData) {
      //     const UserLike = await LikePost.query().where({targetId:element.id,uid:uid}).first()
      //       if(UserLike){
      //         element.likestatus=true
      //       }else{
      //         element.likestatus=false
      //       }
      //       const UserFollow = await FollowPost.query().where({targetId:element.id,uid:uid}).first()
      //       if(UserFollow){
      //         element.followstatus=true
      //       }else{
      //         element.followstatus=false
      //       }
      //     var newelement:any=element.serialize()
      //     if(element.medias.length>0){
      //       var mdImg:any=[]
      //       newelement.medias.forEach(elm => {
      //         mdImg.push(elm?.url)
      //       })
      //       newelement.medias=mdImg
      //     }
      //     if(newelement.userTagged.length>0){
      //       for await (const userProf of newelement?.userTagged) {
      //         userProf.profile = await User.find(userProf?.id)
      //       }
      //     }
      //     newelement.viewBy=[]
      //     newpostData.push({postDTOS:newelement})
      //   }
        

      //   searchData={
      //     "searchModels":newpostData.concat(searchDataprofile),
      //     "logout": false,
      //     "resMsg": "Success",
      //     "status": true,
      //     "currentPage":page,
      //     "totalPage":lastPage,
      //     "prevPage":page>1?page-1:null,
      //     "nextPage":page<lastPage?page+1:null
      //   }

      // }
      if(type==2){ //post
        var postQuery = Post.query().preload('user',query=>{
          query.preload('highestEducation')
              .preload('occupation')
        }).preload('userTagged',query=>{
          query.preload('accomplishmentsList')
          .preload('certificatesList')
          .preload('projectList')
          .preload('country')
          .preload('state')
          .preload('highestEducation')
          .preload('occupation')
          .preload('skillsList')
          .preload('intExpertiseList')
         }).preload('medias',(query)=>{
            query.select('url').where('media_type',5)
        })
        if(keys){
          postQuery.where('description', 'LIKE', "%"+keys+"%").orWhere('hash_tags', 'LIKE', "%"+keys+"%")
        }

        searchData = await postQuery.paginate(page,limit)
        lastPage = searchData.lastPage
        searchData=searchData.rows
        var newpostData:any=[]
        for (const element of searchData) {
          const UserLike = await LikePost.query().where({targetId:element.id,uid:uid}).first()
            if(UserLike){
              element.likestatus=true
            }else{
              element.likestatus=false
            }
            const UserFollow = await FollowPost.query().where({targetId:element.id,uid:uid}).first()
            if(UserFollow){
              element.followstatus=true
            }else{
              element.followstatus=false
            }
          var newelement:any=element.serialize()
          if(element.medias.length>0){
            var mdImg:any=[]
            newelement.medias.forEach(elm => {
              mdImg.push(elm?.url)
            })
            newelement.medias=mdImg
          }
          if(newelement.userTagged.length>0){
            for await (const userProf of newelement?.userTagged) {
              userProf.profile = await User.find(userProf?.id)
            }
          }
          newelement.viewBy=[]
          newpostData.push({"postDTOS":newelement})
        }
        searchData={
          "searchModels":newpostData,
          "logout": false,
          "resMsg": "Success",
          "status": true,
          "currentPage":page,
          "totalPage":lastPage,
          "prevPage":page>1?page-1:null,
          "nextPage":page<lastPage?page+1:null
        }
        
      }
      if(type==3){ // expert
        var profiles = User.query()
                          .preload('accomplishmentsList')
                          .preload('certificatesList')
                          .preload('projectList')
                          .preload('country')
                          .preload('state')
                          .preload('intExpertiseList')
        if(keys && keyType=='profile'){
                profiles.preload('highestEducation').preload('occupation')
                  .preload('skillsList').where({'expert':1,expertmode:true})
                      
          profiles.where('name', 'LIKE', "%"+keys+"%")
                  .orWhere('about_me', 'LIKE', "%"+keys+"%")
                  .orWhere('resume_text', 'LIKE', "%"+keys+"%")
                  
         // searchData=await profiles
        }

        if(keys && keyType=='education'){  
          profiles.preload('highestEducation')
          .preload('occupation')
          .preload('skillsList')
          profiles.where({'expert':1,expertmode:true}).whereHas('highestEducation',(query)=>{
            query.whereHas('master',(qu)=>{
              qu.where('name', 'LIKE', "%"+keys+"%")
            })
          })
         // searchData=await profiles
        }
        if(keys && keyType=='occupation'){  
          profiles.preload('highestEducation')
          .preload('occupation')
          .preload('skillsList')
          profiles.where({'expert':1,expertmode:true}).whereHas('occupation',(query)=>{
            query.whereHas('master',(qu)=>{
              qu.where('name', 'LIKE', "%"+keys+"%")
            })
          })
         // searchData=await profiles
        }
        if(keys && keyType=='skills'){  
          profiles.preload('highestEducation')
          .preload('occupation')
          .preload('skillsList')
          profiles.where({'expert':1,expertmode:true}).whereHas('skillsList',(query)=>{
            query.whereHas('master',(qu)=>{
              qu.where('name', 'LIKE', "%"+keys+"%")
            })
          })
         
        }
        if(keys && keyType=='hashtags'){
          profiles.preload('highestEducation')
          .preload('occupation')
          .preload('skillsList')
          profiles.where({'expert':1,expertmode:true}).whereHas('hashtags',(query)=>{
            query.whereHas('master',(qu)=>{
              qu.where('name', 'LIKE', "%"+keys+"%")
            })
          })
        }
        if(keys && keyType=='all'){
          profiles.preload('highestEducation')
          .preload('occupation')
          .preload('skillsList')
          profiles.where({'expert':1,expertmode:true})
                  .where('name', 'LIKE', "%"+keys+"%")
                  .orWhere('about_me', 'LIKE', "%"+keys+"%")
                  .orWhere('resume_text', 'LIKE', "%"+keys+"%")
                  .orWhereHas('highestEducation',(query)=>{
                    query.whereHas('master',(qu)=>{
                      qu.where('name', 'LIKE', "%"+keys+"%")
                    })
                  })
                  .orWhereHas('occupation',(query)=>{
                    query.whereHas('master',(qu)=>{
                      qu.where('name', 'LIKE', "%"+keys+"%")
                    })
                  })
                  .orWhereHas('skillsList',(query)=>{
                    query.whereHas('master',(qu)=>{
                      qu.where('name', 'LIKE', "%"+keys+"%")
                    })
                  })
                  .orWhereHas('hashtags',(query)=>{
                    query.whereHas('master',(qu)=>{
                      qu.where('name', 'LIKE', "%"+keys+"%")
                    })
                  })

        }else{
          profiles.preload('highestEducation').preload('occupation')
          .preload('skillsList')
          profiles.where({'expert':true,expertmode:true}).where('name', 'LIKE', "%"+keys+"%").orWhere('about_me', 'LIKE', "%"+keys+"%")
        }

        searchData=await profiles.paginate(page,limit)
        lastPage = searchData.lastPage
        searchData=searchData.rows
        searchData = await Promise.all(searchData.map(async(elm:any)=>{
          if(elm.audioCallRate>0){
            elm.audioCallRate= await commonHelper.showCallAmount(elm?.audioCallRate||0,false,'VOIP')
           // elm.audioCallRate=parseFloat((elm.audioCallRate/(5*60)).toFixed(3))
          }
          if(elm.videoCallRate>0){
            elm.videoCallRate=await commonHelper.showCallAmount(elm?.videoCallRate||0,true,'VOIP')
           // elm.videoCallRate=parseFloat((elm.videoCallRate/(5*60)).toFixed(3))
          }
          var newElm:any = elm.serialize()
          var profileData = elm.serialize()
          newElm.profile =  profileData
          if(!newElm?.country){
            newElm.country={name:'',parentId:0,masterType:0,imageUrl:'',used:0}
          }
          if(!newElm?.state){
            newElm.state={name:'',parentId:0,masterType:0,imageUrl:'',used:0}
          }
          if(!newElm.highestEducation){
            newElm.highestEducation={name:'',parentId:0,masterType:0,imageUrl:'',used:0}
          }
          if(!newElm.occupation){
            newElm.occupation={name:'',parentId:0,masterType:0,imageUrl:'',used:0}
          }
          if(!newElm.rating){
            newElm.rating=0
          }
          if(!newElm.ratingCount){
            newElm.ratingCount=0
          }
          newElm.block= await BlockList.query().where({byUid:newElm?.id,toUid:uid}).first()?true:false
          newElm.blocked= await BlockList.query().where({byUid:uid,toUid:newElm?.id}).first()?true:false
          newElm.followstatus = await FollowUser.query().where({uid:uid,targetId:newElm?.id}).first()?true:false

          if(newElm.profile?.hasOwnProperty('country')){
            delete newElm.profile['country']
          }
          if(newElm.profile?.hasOwnProperty('state')){
            delete newElm.profile['state']
          }
          if(newElm.profile?.hasOwnProperty('accomplishmentsList')){
            delete newElm.profile['accomplishmentsList']
          }
          if(newElm.profile?.hasOwnProperty('certificatesList')){
            delete newElm.profile['certificatesList']
          }
          if(newElm.profile?.hasOwnProperty('projectList')){
            delete newElm.profile['projectList']
          }
          if(newElm.profile?.hasOwnProperty('skillsList')){
            delete newElm.profile['skillsList']
          }
          if(newElm.profile?.hasOwnProperty('intExpertiseList')){
            delete newElm.profile['intExpertiseList']
          }
          if(newElm.profile?.hasOwnProperty('highestEducation')){
            delete newElm.profile['highestEducation']
          }
          if(newElm.profile?.hasOwnProperty('occupation')){
            delete newElm.profile['occupation']
          }

          
          newElm.genderId=parseInt(newElm.genderId)
          return {userProfileDTO:newElm}

        }))

        searchData={
          "searchModels":searchData,
          "logout": false,
          "resMsg": "Success",
          "status": true,
          "currentPage":page,
          "totalPage":lastPage,
          "prevPage":page>1?page-1:null,
          "nextPage":page<lastPage?page+1:null
        }
        
      }

      if(type==4){
        var requestQuery= Request.query().preload('expert').preload('slots').where({requestedBy:uid})
        if(keys){
          requestQuery
          .where('estimatedCall', 'LIKE', "%"+keys+"%")
          .orWhere('estimatedCost', 'LIKE', "%"+keys+"%")
          .orWhereHas('expert',(query)=>{
            query.where('name', 'LIKE', "%"+keys+"%")
          })
        }
        searchData = await requestQuery.paginate(page,limit)
        lastPage = searchData.lastPage
        searchData=searchData.rows
        if(searchData){
          searchData = await Promise.all(searchData.map(async (item): Promise<any> => {
            if(item.slots){
                  var expertSchIds:any=[]
                  item.slots.forEach((elm:any)=>{
                    expertSchIds.push(elm.slotsId)
                  })
                  item.expertSchedules=await ExpertSchedule.query().whereIn('id',expertSchIds).orderBy('id','asc')
                   return {requestDTOS:item}
               }
          }));
        }
        searchData={
          "searchModels":searchData,
          "logout": false,
          "resMsg": "Success",
          "status": true,
          "currentPage":page,
          "totalPage":lastPage,
          "prevPage":page>1?page-1:null,
          "nextPage":page<lastPage?page+1:null
        }
      }
      if(type==5){
        var transectionQuery = TransactionHistory.query()
        if(keys){
          transectionQuery.where('transactionAmount', 'LIKE', "%"+keys+"%")
                          .orWhere('description', 'LIKE', "%"+keys+"%")
        }
        searchData = await transectionQuery.paginate(page,limit)
        lastPage = searchData.lastPage
        searchData=searchData.rows

        searchData.map(elm=>{
          return {transactionHistories:elm}
        })

        searchData={
          "searchModels":searchData,
          "logout": true,
          "resMsg": "Success",
          "status": true,
          "currentPage":page,
          "totalPage":lastPage,
          "prevPage":page>1?page-1:null,
          "nextPage":page<lastPage?page+1:null
        }
      }
      if(type==6){
        var chatQuery = Chat.query()
        if(keys){
          chatQuery.where('message', 'LIKE', "%"+keys+"%").where({senderId:uid})
        }
        searchData = await chatQuery.paginate(page,limit)
        lastPage = searchData.lastPage
        searchData=searchData.rows
        searchData = searchData.map(elm=>{
          var newelm={chat:elm}
          return {chats:newelm}
        })
        searchData={
          "searchModels":searchData,
          "logout": true,
          "resMsg": "Success",
          "status": true,
          "currentPage":page,
          "totalPage":lastPage,
          "prevPage":page>1?page-1:null,
          "nextPage":page<lastPage?page+1:null
        }
      }
      if(type==7){
        var callHistQuery = CallHistory.query().preload('expert').where({initiater:uid})
        if(keys){
          callHistQuery.whereHas('expert',(query)=>{
            query.where('name', 'LIKE', "%"+keys+"%")
          })
        }
        searchData = await callHistQuery.paginate(page,limit)
        lastPage = searchData.lastPage
        searchData=searchData.rows
        searchData = searchData.map(elm=>{
          var newelm={callHistory:elm}
          return {callHistoryResData:newelm}
        })
        searchData={
          "searchModels":searchData,
          "logout": true,
          "resMsg": "Success",
          "status": true,
          "currentPage":page,
          "totalPage":lastPage,
          "prevPage":page>1?page-1:null,
          "nextPage":page<lastPage?page+1:null
        }
      }
      if(type==8){
        var notificationQuery = AppNotification.query().where({uid:uid})
        if(keys){
          notificationQuery.where('title', 'LIKE', "%"+keys+"%")
                          .orWhere('message', 'LIKE', "%"+keys+"%")
        }
        searchData = await notificationQuery.paginate(page,limit)
        lastPage = searchData.lastPage
        searchData=searchData.rows
        searchData.map(elm=>{
          return {appNotifications:elm}
        })
        searchData={
          "searchModels":searchData,
          "logout": true,
          "resMsg": "Success",
          "status": true,
          "currentPage":page,
          "totalPage":lastPage,
          "prevPage":page>1?page-1:null,
          "nextPage":page<lastPage?page+1:null
        }
      }
      
      // if(type==9){
      //   var ratingQuery

      // }
      // if(type==10){
      //   var refundQuery = Refund.query()
      // }
      if(searchData){
        return response.status(200).json(searchData)
      }else{
        return response.status(401).json({
          "searchModels":[],
          "logout": false,
          "resMsg": "No data",
          "status": false,
          "currentPage":page||0,
          "totalPage":lastPage||0,
          "prevPage":null,
          "nextPage":null
        })
      }
  }


  async searchSuggestion({auth,response,request}:HttpContextContract){
    // const uid:any=auth.user?.id
    // const data=request.all()
    // const keys=data?.keys
    // const type=data?.type
  //  if(type==3 && keys){
      // var suggetionOut={
      //   profile:keys+' search in profile',
      //   occupation:keys+' search in occupation',
      //   education:keys+' search in education',
      //   skills:keys+' search in skills',
      //   hashtags:keys+' search in hash tags',
      //   all:keys+' search in all'
      // }
      var suggetionOut=[
        {keyType:'profile',value:'Profile'},
        {keyType:'occupation',value:'Occupation'},
        {keyType:'education',value:'Education'},
        {keyType:'skills',value:'Skills'},
        {keyType:'hashtags',value:'Hashtags'},
        {keyType:'all',value:'All'}
      ]
      return response.status(200).json(suggetionOut)
   // }
    //return response.status(201).json({})
  }

  async getTimeZone({response}){
    var newElm:any=[]  
    JSON.parse(await fs.readFileSync('public/timezones.json'))
                .forEach((elm)=>{
                  elm.utc.forEach(element => {
                    newElm.push({value:element})
                  })
                })
    return response.status(200).json(newElm)
  }

  async getReferralCode({auth,response,request}:HttpContextContract){
    const uid:any=auth.user?.id
    const amount= await AppSetting.query().where({fieldKey:'user_ref_amount_all'}).first()
    return response.status(200).json({
      "data": {
        "cerditsId": uid,
        "code": auth.user?.referralCode,
        "id": 0,
        "referBy": 0,
        "referTo": 0,
        "status": true,
        "value": amount?.value||10
      },
      "logout": false,
      "resMsg": "string",
      "status": true
    })
    
  }

  async getCms({response,params}){
    const slug=params.slug
    const pageData:any = await CmsPage.query().where({slug:slug}).first()
    
    return response.status(200).json({
      "data":pageData?pageData.content:'',
      "logout": false,
      "resMsg": "all faqs",
      "status": true
    })
  }
  
  async checkApi({auth,response,request}:HttpContextContract){
    perf.start()
    //csi-YJA5QciOZ9W_VnSbsa:APA91bFiXB6Hxa0tK4E3oe8zfsJcDqjh65z7Pn1yUvVmEb7Q6NzxcX50TQ9c4BSG4srkAd6NHZPXYlTusZkHiFFx97ionKGaWoSQgfWjHrpegTWLcZWlsAXxoVdXRlQs_lpU4vKsOfcl
     const uid:any=auth.user?.id
    // const data=request.all()
   // return await notificationHelper.sendTestNotification()
    //return exeTime.time
   // return new Date().getTimezoneOffset()/60
    return twilioHelper.createUserToken(uid,'test-room')
      //console.log("start",new Date())
      // for(let i = 0; i < 1000000000; i++){
      // }
      // for(let i = 0; i < 1000000000; i++){
      // } 
      //   console.log("end",new Date())

      // console.log('start',new Date());
      // const promiseOne = new Promise((res, rej) => {
      //   console.log('first')
      //     for(let i = 0; i < 100000000; i++){

      //     }
      //     console.log('first end')   
      // })
      // const promiseTwo =new Promise((res, rej) => {
      //   console.log('second')
      //     for(let i = 0; i < 100000000; i++){
            
      //     }
      //   console.log('second end')  
      // })
      // Promise.all(([promiseOne, promiseTwo]))
      // console.log('end', new Date())
      const exeTime = perf.stop()

     // console.log('taken time',exeTime.time)
    //  const profile:any=await User.find(1)
    //   Event.emit('new:regEmail',profile)
    //const uid= Math.floor((Math.random() * 100) + 1)
    //await firebaseHelper.userStatus(uid)
  //   return await firebaseHelper.deleteCallRooms()
  //  return  uid
   // return await firebaseHelper.oflineUserStatus()
   // return await firebaseHelper.oflineUserStatusByCall()
    // var answer = 56.6-((56.6/118)*100)
    // return answer
   var call = await CallHistory.find(36)

   return await Ledger.calculateAndTransferAmountForCall(call)

    

    let file='https://seekex.s3.amazonaws.com/temp/1603695848006.wav'
    const fileNme = fileName(file)
    await download(file, './public/'+fileNme, async function(){
      console.log('success')
    })
    await download(file, './public/'+fileNme, async function(){
      console.log('success')
    })
    return 'Success'
    //return  Math.ceil(await commonHelper.estimatedDuration(uid,1,false))

    return (Math.round(0 * 100) / 100).toFixed(2);

    
  }

  async checkNotify({auth,response,request}:HttpContextContract){
      const data=request.all()
      var notifyUser= await LoginHistory.query().where("uid",data?.uid).orderBy("id","desc").first()
      const info:any={}
      const payload:any={}
      if(notifyUser){
        info.title=data?.title
        info.body=data?.title
        info.payload=payload
        info.type=data?.type
        const status=await firebaseHelper.sendNotifictionNew(info,notifyUser?.fcmToken)
        return response.status(200).json(status)
      }
      return response.status(200).json({'msg':'Not Send'})
  }
}
