import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import Razorpay from "razorpay"
import BankDetail from 'App/Models/BankDetail'
import WebServiceLog from 'App/Models/WebServiceLog'
import TransactionHistory from 'App/Models/TransactionHistory'
import LedgerAccount from 'App/Models/LedgerAccount'
import Env from '@ioc:Adonis/Core/Env'
import Refund from 'App/Models/Refund'
import PayloadData from 'App/Models/PayloadData'
const RazorPayKey=Env.get('RazorPayKey') as string
const RazorPaySecret=Env.get('RazorPaySecret') as string
const AccountNumber=Env.get('AccountNumber') as string
const axios = require('axios')
const RZP = new Razorpay({
  key_id: RazorPayKey,
  key_secret: RazorPaySecret
})
import NotificationHelper from 'App/Helpers/NotificationHelper'
const notificationHelper = new NotificationHelper()
import LedgerHelper from "App/Helpers/LedgerHelper"
import User from 'App/Models/User'
import CallHistory from 'App/Models/CallHistory'
import RefundHistory from 'App/Models/RefundHistory'
import AppSetting from 'App/Models/AppSetting'
import Event from '@ioc:Adonis/Core/Event'
const Ledger=new LedgerHelper()
const perf = require('execution-time')()
export default class UserWalletsController {

  async addBankDetail({ request,response,auth }:HttpContextContract){
    const uid:any=auth.user?.id
    const data=request.all()
    var userBankDetail=data.bankDetail
    //return response.status(200).json(data)
    if(userBankDetail.upi){
      var existDetail=await BankDetail.query().where({upiAddress:userBankDetail?.upiAddress,uid:uid}).first()
    }else{
      var existDetail=await BankDetail.query().where({accountNo:userBankDetail?.accountNo,uid:uid}).first()
    }
    //return existDetail
    if(userBankDetail && !existDetail){
      // const bankDetails= await Ledger.addBankDetails(userBankDetail)
      // return bankDetails
      userBankDetail.uid=uid
      try {
        await BankDetail.create(userBankDetail)
        const userBankDetails=await BankDetail.query().where('uid',uid)

        return response.status(200).json({
          "bankDetails":userBankDetails,
          "resMsg": "Success",
          "status": true,
          "logout": false
        })
      } catch (error) {
        await WebServiceLog.create({request:JSON.stringify(data),response:JSON.stringify(error),service_name:'Create Bank Details Service',status:1,uid:uid,time_taken:0})
        return response.status(200).json({
          "errors":error,
          "bankDetails":[],
          "resMsg": "Failed",
          "status": false,
          "logout": false
        })
      }
    }else{
      const userBankDetails= await BankDetail.query().where('uid',uid)
      return response.status(200).json({
        "bankDetails":userBankDetails,
        "resMsg": "Not created",
        "status": true,
        "logout": false
      })
    }
  }

  async addMoney({ request,response,auth }:HttpContextContract){
    perf.start()
    const uid:any=auth.user?.id
    const data=request.all()
    const transId=data.addMoneyDTO?.razorPayId
    const amount=data.addMoneyDTO?.amt
    try {
      var payment = await Ledger.createGatewayToUserWallate({razorPayId:transId,amt:amount,uid:uid})

      if(payment.status=='success'){
        var userWallet:any=await LedgerAccount.query().where({name:'User',uid:uid}).first()
        userWallet= userWallet.serialize()
        return response.status(200).json({
          "logout": true,
          "resMsg": "Successful",
          "status": true,
          "walletDTO": {
            "availableAmount": userWallet?.availableAmount,
            "holdAmount": userWallet?.holdAmount,
            "disputedAmount":userWallet?.disputedAmount,
            "promoAmount":userWallet?.promoAmount,
            "totalAmount": userWallet?.totalAmount
          }
        })
      }else{
        const res = perf.stop()
        await WebServiceLog.create({request:JSON.stringify(data),response:JSON.stringify(payment),service_name:'Wallet recharge Service, An error for update wallet amount transection.',status:1,uid:uid,time_taken:res.time})
        return response.status(201).json({
          "error":payment,
          "logout": false,
          "resMsg": payment?.msg,
          "status": false,
          "walletDTO": {}
        })
      }
    } catch (error) {
      const res = perf.stop()
       await WebServiceLog.create({request:JSON.stringify(data),response:JSON.stringify(error),service_name:'Wallet recharge Service, An error for update wallet amount.',status:1,uid:uid,time_taken:res.time})
        return response.status(201).json({
          "errors":error,
          "resMsg":"Transection failed, An error for update wallet amount. ",
          "status": false,
          "logout": true,
          "walletDTO": {}
        })
    }
  }

  async applyForRefund ({ request,response,auth }:HttpContextContract){
    const uid:any=auth.user?.id
    const data=request.all()
    if(data.refund){
      var refundData=data.refund
      refundData.appliedBy=uid
      await WebServiceLog.create({request:JSON.stringify(data),response:JSON.stringify(uid),service_name:'User Refund Service data',status:1,uid:uid,time_taken:0})
     // return response.status(200).json(refundData)
        const refData= await Refund.query().where('callId',refundData?.callId).first()
        if(refData){
          return response.status(200).json({
            "logout": false,
            "resMsg": "You are already applied for refund.",
            "status": false
          })
        }
      try {
        var refund = await Ledger.applyRefundAmount(refundData)
       // return response.status(200).json(refundData)
        if(refund?.status=='success'){
        /**************************Notification********************************/
        var callDetails = await CallHistory.find(refundData?.callId)
       // return callDetails
       const notify= await notificationHelper.sendNotifictionForApplyRefund(callDetails,auth.user)
        /**************************Notification********************************/
        refundData.appliedFor=refund?.expertId||0
        const refundSaveData=new Refund()
        refundSaveData.callId=refundData.callId
        refundSaveData.amt=refund?.refundAmount||0
        refundSaveData.expertRefundAmount=refund?.expertRefundAmount||0
        refundSaveData.type=refundData.type
        refundSaveData.appliedBy=refundData.appliedBy
        refundSaveData.appliedFor=refundData.appliedFor
        refundSaveData.reason=refundData.reason
        refundSaveData.status=1
        
        var saveddate=await refundSaveData.save()
        if(saveddate && refundData?.type==2){
          var refundHistory:any=new RefundHistory()
          refundHistory.status='Applied'
          refundHistory.Type='Refund Applied'
          refundHistory.comment='Seeker Applied '+refundData?.amt+ '% call amount for refund'
          refundHistory.approveBy=0
          refundHistory.refundId=saveddate?.id
          await refundHistory.save()
        }
       // return response.status(200).json(saveddate)
        return response.status(200).json({
          "logout": false,
          "resMsg": "Refund initiated",
          "status": true
        })
        }else{
          return response.status(200).json({
            "logout": false,
            "resMsg": refund?.msg,
            "status": false
          })
        }
      } catch (error) {
        await WebServiceLog.create({request:JSON.stringify(data),response:JSON.stringify(error),service_name:'User Refund Service error',status:1,uid:uid,time_taken:0})
        return response.status(200).json({
          "errors":error,
          "resMsg":"Some error, Refund failed",
          "status": false,
          "logout": false,
        })
      }
    }else{
      return response.status(200).json({
        "resMsg":"Refund failed",
        "status": false,
        "logout": false
      })
    }
  }

  async getRefunds({ request,response,auth }: HttpContextContract){
		const uid:any=auth.user?.id
		const data=request.all()
		const page = request.input('page', 1)
		const limit = 20
		const refunds:any = await Refund.query().preload('callDetail').preload('appliedByUser').preload('appliedForUser')
														//	.whereNot({status:1})
															.where((query)=>{
																query.where('appliedBy',uid).orWhere('appliedFor',uid)
															}).orderBy('id','desc')
                              .paginate(page,limit)
    const lastPage = refunds.lastPage||1
    if(refunds.rows){
      var resdata:any=[]
      for (const element of refunds.rows) {
          if(element?.appliedFor==uid){
            element.amt=element?.expertRefundAmount||element?.amt
            element.headline=element?.appliedByUser?.name + ' applied for refund against you'
          }else{
            element.headline='You applied a refund to '+element?.appliedForUser?.name
          }
          resdata.push(element)
      } 
    }
            
		return response.status(200).json({
			"data": resdata||[],
			"logout": false,
			"resMsg": "Refund service",
      "status": true,
      "currentPage":page,
      "totalPage":lastPage,
      "prevPage":page>1?page-1:null,
      "nextPage":page<lastPage?page+1:null
		})											
															
	}

  public async approveRefund({request,response,auth}:HttpContextContract){
    
    perf.start()
    const uid = auth.user?.id
    const data = request.all()
    const type=data?.type
    const rId=data.id||0
    const refund:any=await Refund.query()
                              .where((query)=>{
                                query.where('id',rId).orWhere('callId',data?.callId)
                              }).first()
    var refundHistory:any=new RefundHistory()
    //return refund
      if(refund?.status!=1){
        return response.status(200).json({
          "logout": false,
          "resMsg": "You are not able to take action on this refund",
          "status": false
        })
      }
    if(data.type==1){
      refund.expertApprovalStatus=1
      refund.status=2
      refundHistory.status='Approved'
      refundHistory.Type='Borne From Expert'
      refundHistory.comment='Refund Approved By Expert'
      refundHistory.approveBy=uid
      refundHistory.refundId=refund?.id
      try {
          /*******************************************Code for amount transection*******************************************/
          const refStatus = await Ledger.refundAmountByExpert(refund)
          var notify
          if(refStatus?.status=='success'){
            /**************************Notification********************************/
            var callDetails = await CallHistory.find(refund?.callId)
          // return callDetails
            notify= await notificationHelper.sendNotifictionForApproveRefund(callDetails,auth.user,refund?.amt)
            /**************************Notification********************************/
           //return notify
           const results = perf.stop()
          await WebServiceLog.create({request:JSON.stringify(data),response:JSON.stringify(refStatus),service_name:'Expert Refund Action After Success',status:1,uid:auth.user?.id,time_taken:results.time})
          /*******************************************Code for amount transection[End]*************************************/
            await refund.save()
            await refundHistory.save()
            return response.status(200).json({
              "logout": false,
              "resMsg": "Refund approved",
              "status": true,
              "outdata":refStatus,
              "notify":notify
            })
          }else{
            const results = perf.stop()
            await WebServiceLog.create({request:JSON.stringify(data),response:JSON.stringify(refStatus),service_name:'Expert Refund Action Error',status:2,uid:auth.user?.id,time_taken:results.time})
            return response.status(200).json({
              "logout": false,
              "resMsg": "Internal error, please try after some time",
              "status": false
            })
          }
      } catch (error) {
        const results = perf.stop()
        await WebServiceLog.create({request:JSON.stringify(data),response:JSON.stringify(error),service_name:'Expert Refund Action Error',status:2,uid:auth.user?.id,time_taken:results.time})
        return response.status(200).json({
          "logout": false,
          "resMsg": "Internal error",
          "status": false
        })
      }  
    }else if(data.type==2){
      try{
        refund.expertApprovalStatus=2
        refundHistory.comment='Rejected By Expert'
        refundHistory.refundId=refund?.id
        await refund.save()
        await refundHistory.save()
       // const seeker = await User.find(refund?.appliedBy)
        
        // const allData={
        //   refundId:refund?.id,
        //   seekerName:seeker?.name,
        //   seekerEmail:seeker?.email,
        //   expertName:auth.user?.name,
        //   refundPolicy:''
        // }
        // Event.emit('new:refundRejectEmail',allData)

        return response.status(200).json({
          "logout": false,
          "resMsg": "Refund rejected",
          "status": true
        }) 
      } catch (error) {
        const results = perf.stop()
        await WebServiceLog.create({request:JSON.stringify(data),response:JSON.stringify(error),service_name:'Expert Refund Action Error',status:2,uid:auth.user?.id,time_taken:results.time})
        return response.status(200).json({
          "logout": false,
          "resMsg": "Internal error",
          "status": false
        })
      }
    } 
  }

  async getBankDetail({ request,response,auth }:HttpContextContract){
    const uid:any=auth.user?.id
    const data=request.all()
    try {
      const userBankDetails= await BankDetail.query().where('uid',uid)
      return response.status(200).json({
        "bankDetails":userBankDetails,
        "resMsg": "Success",
        "status": true,
        "logout": true
      })
    } catch (error) {
      await WebServiceLog.create({request:JSON.stringify(data),response:JSON.stringify(error),service_name:'Get Bank Details Service',status:1,uid:uid,time_taken:0})
        return response.status(201).json({
          "errors":error,
          "bankDetails":[],
          "resMsg": "Failed Bank Details Service",
          "status": false,
          "logout": false
        })
    }
  }

  async getTransactionHistory({response,auth,request }:HttpContextContract){
      const uid:any=auth.user?.id
      const page = request.input('page', 1)
      const limit = 20
      try {
        var userWallet:any=await LedgerAccount.query().where({name:'User',uid:uid}).first()
        var transH:any= await TransactionHistory.query().where({toWallet:userWallet?.id,uid:uid}).orWhere({fromWallet:userWallet?.id,uid:uid}).orderBy('id','desc').paginate(page,limit) //,transactionTo:'User'
        const lastPage=transH?.lastPage
        transH=transH.rows
        return response.status(200).json({
          "data":transH,
          "resMsg": "Success",
          "status": true,
          "logout": false,
          "currentPage":page,
          "totalPage":lastPage,
          "prevPage":page>1?page-1:null,
          "nextPage":page<lastPage?page+1:null
        })
      } catch (error) {
        await WebServiceLog.create({request:JSON.stringify(uid),response:JSON.stringify(error),service_name:'Get Transaction Histor Service',status:1,uid:uid,time_taken:0})
        return response.status(201).json({
          "errors":error,
          "resMsg": "Failed Transaction Histor",
          "status": false,
          "logout": false
        })
      }
  }

  async getWalletSummary({response,auth}){
    const uid:any=auth.user?.id
    try {
      const userWallet:any=await LedgerAccount.query().where({name:'User',uid:uid}).first()
      const newWalletValue=userWallet.serialize()
      return response.status(200).json({
        "logout": true,
        "resMsg": "Successful",
        "status": true,
        "walletDTO": {
          "availableAmount": newWalletValue?.availableAmount,
          "holdAmount": newWalletValue?.holdAmount,
          "disputedAmount":newWalletValue?.disputedAmount,
          "promoAmount":newWalletValue?.promoAmount,
          "totalAmount": newWalletValue?.totalAmount
        }
      })
    } catch (error) {
      await WebServiceLog.create({request:JSON.stringify(uid),response:JSON.stringify(error),service_name:'Get Wallet Summary Service',status:1,uid:uid,time_taken:0})
      return response.status(201).json({
        "errors":error,
        "resMsg": "Get Wallet Summary Service",
        "status": false,
        "logout": false
      })
    }
  }

  async withdrawMoney({ request,response,auth }:HttpContextContract){
    const uid:any=auth.user?.id
    const data=request.all()
    var userWallet:any=await LedgerAccount.query().where({name:'User',uid:uid}).first()
    //userWallet=userWallet.serialize()
    const minAmount = await AppSetting.getValueByKey('min_wallet')
    await WebServiceLog.create({request:JSON.stringify(data),response:JSON.stringify(userWallet),service_name:'User Payout service Payment gatway init',status:1,uid:uid,time_taken:0})
    if(minAmount>data?.amt){
      return response.status(200).json({
        "resMsg": "Not enough funds, minimum "+minAmount+" for withdraw",
        "status": false,
        "logout": false
      })
    }
    if(data?.amt>(userWallet?.totalAmount-userWallet?.promoAmount)){
      return response.status(200).json({
        "resMsg": "You can't withdraw promotional amount",
        "status": false,
        "logout": false
      })
    }
    const userWalletAvlAmount = userWallet?.totalAmount-(userWallet?.holdAmount+userWallet?.disputedAmount+userWallet?.promoAmount)
    if(userWalletAvlAmount>=data.amt){
      var user:any = await User.find(uid)
      if(!auth.user?.contactId){
        var userContactId= await Ledger.createContactOnPaymentGateway(auth.user)  
        if(userContactId?.id){
          user.contactId=userContactId?.id
          await user.save()
        }
      }
      var bankDetails:any= await BankDetail.query().where({id:data?.bankId,uid:uid}).first()
      if(!bankDetails?.accountId){
        try {
          var fac= await Ledger.fundAccount(bankDetails,user)
          var bankStatus=fac?.status
          if(fac?.data?.id && bankStatus=='success'){
            bankDetails.accountId=fac?.data?.id
            await bankDetails.save()
          }else{
            await WebServiceLog.create({request:JSON.stringify(data),response:JSON.stringify(fac),service_name:'User Payout service Payment gatway error with account details',status:1,uid:uid,time_taken:0})
            const error = JSON.parse(fac?.data?.error)
            return response.status(200).json({
              "resMsg": error?.error?.description||'Server error, please check you data',
              "status": false,
              "logout": false
            })
          }
        } catch (error) {
          await WebServiceLog.create({request:JSON.stringify(data),response:JSON.stringify(error),service_name:'User Payout service Payment gatway error with account details error',status:1,uid:uid,time_taken:0})
          const nerror = JSON.parse(error?.data?.error)
          return response.status(200).json({
            "resMsg": nerror?.error?.description||'Server error, please check you data',
            "status": false,
            "logout": false
          })
        }
      }
      
      try {
        const paymentDetailData = await Ledger.withdrawFromWallet(data,bankDetails)
        const paymentDetail = paymentDetailData?.data
        const status= paymentDetailData?.status
       //return paymentDetail
        if(status=='success'){
          var payout=new PayloadData()
          payout.uid=uid
          payout.payload=paymentDetail.id
          payout.amount=(paymentDetail.amount/100)||0
          payout.status=paymentDetail.status
          payout.fundAccountId=paymentDetail.fund_account_id
          await payout.save()
         // userWallet.availableAmount=userWallet.availableAmount-data.amt
          userWallet.totalAmount=userWallet?.totalAmount-data?.amt
          await userWallet.save()
          await TransactionHistory.create({bankId:0,callId:0,description:(paymentDetail?.amount/100).toFixed(2)+' withdrew by You to Your bank Account.',fromWallet:userWallet?.id,isdebit:true,razorPayPaymentId:paymentDetail.id,status:2,toWallet:0,transactionAmount:(paymentDetail?.amount/100),transactionType:12,transactionWalletId:userWallet?.id,uid:uid,payout:true,transactionAction:'Payout'})
        // return {bankId:0,callId:0,description:(paymentDetail?.amount/100).toFixed(2)+' withdrew by You to Your bank Account.',fromWallet:0,isdebit:true,razorPayPaymentId:paymentDetail.id,status:2,toWallet:0,transactionAmount:(paymentDetail?.amount/100),transactionType:4,transactionWalletId:uid,payout:true}
          userWallet=userWallet.serialize()
          return response.status(200).json({
            "logout": true,
            "resMsg": "User payout initiated",
            "status": true,
            "walletDTO": {
              "availableAmount": userWallet?.availableAmount,
              "holdAmount": userWallet?.holdAmount,
              "disputedAmount":userWallet?.disputedAmount,
              "promoAmount":userWallet?.promoAmount,
              "totalAmount": userWallet?.totalAmount
            }
          })
        }else{
          await WebServiceLog.create({request:JSON.stringify(data),response:JSON.stringify(paymentDetail),service_name:'User Payout service Payment gatway error on pay',status:1,uid:uid,time_taken:0})
          return response.status(200).json({
            "resMsg": paymentDetail?.error?.description||'Server error, please check you data',
            "status": false,
            "logout": false
          })
        }
      } catch (error) {
        await WebServiceLog.create({request:JSON.stringify(data),response:JSON.stringify(error),service_name:'User Payout service Payment gatway error',status:1,uid:uid,time_taken:0})
        return response.status(200).json({
          "errors":error,
          "resMsg": "User Payout service error",
          "status": false,
          "logout": false
        })
      }
    }else{
      return response.status(200).json({
        "resMsg": "Not enough funds in your wallet",
        "status": false,
        "logout": false
      })
    }
  }
}