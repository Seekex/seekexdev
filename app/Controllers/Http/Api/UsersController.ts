import User from 'App/Models/User'
import { schema, rules,validator } from '@ioc:Adonis/Core/Validator'
import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import Application from '@ioc:Adonis/Core/Application'
import UploadHelper from "App/Helpers/UploadHelper"
const uploader:any=new UploadHelper()
import Database from '@ioc:Adonis/Lucid/Database'
import WebServiceLog from 'App/Models/WebServiceLog'
import BlockList from 'App/Models/BlockList'
import Reported from 'App/Models/Reported'
import AppNotification from 'App/Models/AppNotification'
import ExpertSchedule from 'App/Models/ExpertSchedule'
import LikeComment from 'App/Models/LikeComment'
import LikePost from 'App/Models/LikePost'
import FollowPost from 'App/Models/FollowPost'
import FollowUser from 'App/Models/FollowUser'
import UserAdvanceDetail from 'App/Models/UserAdvanceDetail'
import UserLocation from 'App/Models/UserLocation'
import UserCountryState from 'App/Models/UserCountryState'
import UserMasterGodown from 'App/Models/UserMasterGodown'
import NotificationHelper from 'App/Helpers/NotificationHelper'
import LoginHistory from 'App/Models/LoginHistory'
const notificationHelper = new NotificationHelper()

const days:any={"1":"Sun","2":"Mon","3":"Tue","4":"Wed","5":"Thu","6":"Fri","7":"Sat"}
import { DateTime } from 'luxon'
const changeTime = (expertTimeZone,time)=>{
  if(!expertTimeZone || expertTimeZone=='Asia/Kolkata'){
      return time
  }
  var newtime = time.split(':')
  var expLocal = DateTime.local().setZone(expertTimeZone)
  var newloc = expLocal.set({hour: newtime[0],minute: newtime[1]}).toUTC()
  var localTime = newloc.toLocal()
  var hour = localTime.hour<10?'0'+localTime.hour:localTime.hour
  var mint = localTime.minute<10?'0'+localTime.minute:localTime.minute
  return hour+":"+mint
}

const onlyUnique=(value, index, self)=>{
  return self.indexOf(value) === index
}
import CommonHelper from 'App/Helpers/CommonHelper'
const commonHelper = new CommonHelper()
import FirebaseHelper from 'App/Helpers/FirebaseHelper'
const firebaseHelper=new FirebaseHelper()
import Event from '@ioc:Adonis/Core/Event'
const ext = (url)=>{
  return (url = url.substr(1 + url.lastIndexOf("/")).split('?')[0]).split('#')[0].substr(url.lastIndexOf("."))
}
const fileName = (url)=>{
  return (url = url.substr(1 + url.lastIndexOf("/")).split('?')[0])
}
import ResumeHelper from "App/Helpers/ResumeHelper"
import AppMedia from 'App/Models/AppMedia'
import MasterGodown from 'App/Models/MasterGodown'
import BookedSlot from 'App/Models/BookedSlot'
import Request from 'App/Models/Request'
import EmailHelper from 'App/Helpers/EmailHelper'
import Organization from 'App/Models/Organization'
import WithoutRegUser from 'App/Models/WithoutRegUser'
import YoutubeVideo from 'App/Models/YoutubeVideo'
import Chat from 'App/Models/Chat'
import AppUserEvent from 'App/Models/AppUserEvent'
import UserVideo from 'App/Models/UserVideo'
const resumeHelper:any=new ResumeHelper()
export default class UsersController {

  async registration({ request,response,auth }: HttpContextContract){

    const requestdata:any=request.all()
    const social:any=requestdata?.social
    await WebServiceLog.create({request:JSON.stringify(requestdata),response:JSON.stringify({}),service_name:'Reg Service Start',status:1,uid:0,time_taken:0})
  // return response.status(200).send(requestdata.profile)
    const validationSchema = schema.create({
      name:schema.string({ trim: true }),
      // email: schema.string({ trim: true }, [
      //  // rules.email(),
      //   rules.unique({ table: 'users', column: 'email' }),
      // ]),
      // password: schema.string({ trim: true }, [
      //   rules.required(),
      // ]),
    })

   //var userDetails
    try {
       await validator.validate({
        schema: validationSchema,
        data:requestdata.profile
        //reporter: validator.reporters.api,
       // reporter: ApiReporter,
      })
    } catch (error) {
      var resData={
        "errors":error.messages,
        "resMsg": "Invalid Data",
        "status": false,
        "logout": false,
      }
      return response.status(200).send(resData)
    }
    if(requestdata.referralCode){
      const refCode:any = await User.query().where({referralCode:requestdata?.referralCode}).first()
      if(!refCode){
        return response.status(200).json({
          "errors":[],
          "resMsg": "Invalid referral code.",
          "status": false,
          "logout": false,
        })
      }
    }
  //  return 'hello'
  var data:any=requestdata.profile
  await WebServiceLog.create({request:JSON.stringify(data),response:JSON.stringify({}),service_name:'Reg Service Start 2',status:1,uid:0,time_taken:0})
      // if(data?.email){
      //   var existEmail = await User.query().where("email",data?.email).first()
      //   if(existEmail){
      //     return response.status(200).json({
      //       "resMsg": "Email is already registered with another account.",
      //       "status": false,
      //       "logout": false,
      //     })
      //   }
      // }
   // const trx = await Database.transaction()
      data.mobile= data?.mobile.replace(/\D/g, '').slice(-10)
      var fUser:any = await User.query().where("mobile",data?.mobile).orWhere("email",data?.email).first()
      if(fUser && fUser?.regCompleted==true){
        return response.status(200).json({
          "resMsg": "Sorry! Mobile no or email already attached with seekex account please enter other.",
          "status": false,
          "logout": false,
        })
      }
      data.age= data.dob?new Date(Date.now()-new Date(data.dob).getTime()).getUTCFullYear()-1970:0
     // return response.status(200).send(data)
      if(fUser){
        var regData:any=fUser
      }else{
        var regData:any=new User()
      }
       regData.email=data?.email||''
       regData.mobile=data?.mobile
       regData.password=data?.password
       regData.age=data?.age
       regData.dob=data?.dob
       regData.name=data?.name
       regData.aboutMe=data?.aboutMe||''
       regData.genderId=data?.genderId
       regData.profilePic=data?.profilePic||''
       regData.mobileverified=data?.mobileverified
       regData.emailverified=data?.emailverified
       regData.regCompleted=true
       regData.mobileOtp=''
       regData.emailOtp=''
       regData.followers=0
       regData.following=0
       regData.conversations=0
       regData.views=0
       regData.timeZone=data.timeZone||'Asia/Kolkata'
       regData.exotelCallOption=1
       regData.voipCallOption=1
       regData.requested=data?.expert==1?1:regData?.requested?regData.requested:false
       regData.expert=data?.expert||0
       regData.expertmode=data?.expertmode?1:0
    try {
      // regData.useTransaction(trx)
       const newUser = await regData.save()
      if(requestdata.referralCode){
        await commonHelper.referralAmount(requestdata?.referralCode,newUser?.id)
      }
      if(!data?.profilePic || data?.profilePic==''){
        const profPic:any = await commonHelper.genTextImageForProfileNew(data?.name,newUser)
        if(profPic?.Location){
          newUser.profilePic=profPic?.Location
          await newUser.save()
        }
      }

     // await trx.commit()
    //  return regData
      const profile:any = await User.find(newUser?.id)
      const logintoken = await auth.use('api').login(profile)
      if(profile.email){
        Event.emit('new:regEmail',profile)
      }
      if(requestdata?.deviceId){
        await LoginHistory.create({deviceId:requestdata?.deviceId||'',fcmToken:requestdata?.fcm||'',geoLocation:'',uid:regData?.id,timeStamp:data?.created})
        await WithoutRegUser.query().where({deviceId:requestdata?.deviceId}).update({status:true,uid:regData?.id})
      }
      await WebServiceLog.create({request:JSON.stringify(newUser),response:JSON.stringify(profile),service_name:'Reg Service Completed',status:1,uid:0,time_taken:0})
      return response.status(200).json({
        "logout": false,
        "resMsg": "User Registration successful",
        "status": true,
        "userProfile":{profile:profile},
        "token":logintoken.token
      })
    } catch (error) {
    //  await trx.rollback()
    //return error

      return response.status(200).json({
        "errors":error,
        "resMsg": "Registration failed, please try again.",
        "status": false,
        "logout": false,
      })
    }
  }

  async login({ request,response,auth }: HttpContextContract){
    const data=request.all()
    const input =data.email||data.username
    const social=data?.social||false
    //console.log(data)
    //return data
    await WebServiceLog.create({request:JSON.stringify(data),response:JSON.stringify(social),service_name:'Login Api  Service init',status:1,uid:0,time_taken:0})
    if(input.match(/^-?\d+$/)){
      const userDta=await User.query().where('mobile',input).first()
      if(userDta){
        data.email =userDta?.email
      }else{
        var resData1={
          "errors":'',
          "resMsg": "User not exist, please try another credentials ",
          "status": false,
          "logout": false,
          "uid": null,
          "profile": null
        }
        return response.status(200).json(resData1)
      }
    }

    if(!social){
      const validationSchema = schema.create({
        email: schema.string({ trim: true }, [
          rules.email()
        ]),
        password: schema.string({ trim: true }, [
          rules.required(),
        ]),
      })
      try {
         await validator.validate({
          schema: validationSchema,
          data:data
        })
      } catch (error) {
        var resData={
          "errors":error.messages,
          "resMsg": "Invalid Username or Password",
          "status": false,
          "logout": false,
          "uid": null,
          "profile": null
        }
        return response.status(200).json(resData)
      }
    }
    try {
      const email = data.email||data.username
      const password = data.password
      if(social){
        const user:any = await User.query().where('email',email).first()
       // const logintoken =await auth.use('api').attempt(email, password)
        var logintoken = await auth.use('api').login(user)
      }else{
        var logintoken =await auth.use('api').attempt(email, password)
      }
     

      if(logintoken){
        var user=auth?.toJSON()
        const lastT= await Database.from('api_tokens').where('user_id', user?.guards?.api?.user?.id).orderBy('id','desc').first()
        await Database.from('api_tokens').where('user_id', user?.guards?.api?.user?.id).whereNot('id',lastT?.id).delete()
       // await notificationHelper.logOutNotification(user?.guards?.api?.user,data?.login?.deviceId)
        const userFullProfile = await commonHelper.userProfile(user?.guards?.api?.user?.id)
        const userAllEvents = await (await AppUserEvent.query().where({userId:user?.guards?.api?.user?.id})).map(elm=>{
          const nelm = elm.serialize()
          nelm.event_id=nelm?.eventId
          nelm.event_name=nelm?.eventName
          return nelm
        })
        const userData={
          "logout": true,
          "profile":userFullProfile,
          "events":userAllEvents,
          "resMsg": "login successful",
          "status": true,
          "uid": user?.guards?.api?.user?.id,
          "token":logintoken?.token
        }
        if(data?.login?.deviceId){
          await LoginHistory.query()
          .where((q)=>{
            q.where('uid',user?.guards?.api?.user?.id).orWhere('deviceId',data?.login?.deviceId)
          })
          .update({'status':true})
        }
        await LoginHistory.create({deviceId:data?.login?.deviceId||'No Device',fcmToken:data?.login?.fcmToken||'',geoLocation:data?.login?.geoLocation||'',uid:user?.guards?.api?.user?.id,timeStamp:data?.login?.timeStamp})
        await firebaseHelper.changeUserStatus(user?.guards?.api?.user?.id,1)
        await WebServiceLog.create({request:JSON.stringify(data),response:JSON.stringify({user:user?.guards?.api?.user,profile:userFullProfile}),service_name:'Login Api  Service Success',status:1,uid:0,time_taken:0})

        return response.status(200).json(userData)
      }
    } catch (error) {
      await WebServiceLog.create({request:JSON.stringify(data),response:JSON.stringify(error),service_name:'Login Api  Service Error',status:1,uid:0,time_taken:0})
      var resData={
        "errors":error,
        "resMsg": "Invalid Username or Password, please try with correct credentials ",
        "status": false,
        "logout": false,
        "uid": null,
        "profile": null
      }
      return response.status(200).json(resData)

    }
  }

  async logout({auth,response}){
    const uid=auth.user?.id
    try{
      await auth.use('api').logout()
      return response.status(200).json({
        "resMsg": "Logout Successful!",
        "status": true,
        "logout": false
      })
    }catch(e){
      await WebServiceLog.create({request:JSON.stringify({uid}),response:JSON.stringify('An Error'),service_name:'Logout Service',status:1,uid:uid,time_taken:0})
      return response.status(200).json({
        "resMsg": "An error, please try again.",
        "status": false,
        "logout": true
      })
    }
  }

  async getProfile({ request,response,auth }: HttpContextContract){
    const authorization =  request.header('authorization')
    var uid
    if(authorization && authorization !='Bearer'){
      try {
        await auth.use('api').authenticate()
      } catch (error) {
        await WebServiceLog.create({request:JSON.stringify(request),response:JSON.stringify(error),service_name:'get user profile Service auth error',status:1,uid:0||0,time_taken:0})
        return response.status(200).json({
          "errors":error,
          "resMsg": "An Auth Error",
          "status": false,
          "logout": true
        })
      }
      uid = auth.use('api')?.user?.id
    }
    const data = request.all()
    //return data
    try {
      var user:any= await User.query()
                .where('id',data.uid).first()

      var userAll:any= await User.query()
                .preload('accomplishmentsList')
                .preload('certificatesList')
                .preload('projectList')
                .preload('country')
                .preload('state')
                .preload('highestEducation')
                .preload('occupation')
                .preload('skillsList')
                .preload('intExpertiseList')
                .preload('currentOrg')
                .preload('collegeUniversity')
                .withCount('userVideos')
                .where('id',data?.uid).first()
  //      return user        
    //  return response.status(200).json(userAll)
      /** Rating And Responce Count */
      const rat= await commonHelper.userRating(data.uid)
      const respo=await commonHelper.expertResponceRate(data.uid)
      user.rating=rat?.rating
      user.ratingCount=rat?.count||0
      user.callResponseRate=respo||0
      user.userAudioCallRate=user?.audioCallRate
      user.userVideoCallRate=user?.videoCallRate
      
      if(user.audioCallRate>0){
        user.audioCallRate= await commonHelper.showCallAmount(user?.audioCallRate,false,'VOIP')
      }
      if(user.videoCallRate>0){
        user.videoCallRate=await commonHelper.showCallAmount(user?.videoCallRate,true,'VOIP')
      }
      user.conversations=await commonHelper.userConversations(user?.id)||0
       /** Rating And Responce Count */

      if(!userAll?.country){
        userAll.country={id:0,name:'',parentId:0,masterType:0,imageUrl:''}
      }else{
        userAll.country={
          id:userAll.country?.countryStateId||0,
          name:userAll.country?.name||'',
          parentId:userAll.country?.parentId||0,
          masterType:userAll.country?.masterType||0,
          imageUrl:userAll.country?.imageUrl||'',
          used: userAll.country?.used||0,
        }
      }
      if(!userAll?.state){
        userAll.state={id:0,name:'',parentId:0,masterType:0,imageUrl:''}
      }else{
        userAll.state={
          id:userAll.state?.countryStateId||0,
          name:userAll.state?.name||'',
          parentId:userAll.state?.parentId||0,
          masterType:userAll.state?.masterType||0,
          imageUrl:userAll.state?.imageUrl||'',
          used: userAll.state?.used||0,
        }
      }
      if(userAll.intExpertiseList){
        userAll.intExpertiseList.map(elm=>{
          elm.id=elm.masterGodownId
          return elm
        })
      }
      if(userAll.skillsList){
        userAll.skillsList.map(elm=>{
          elm.id=elm.masterGodownId
          return elm
        })
      }
      if(userAll.highestEducation){
        userAll.highestEducation.id=userAll.highestEducation.masterGodownId
      }
      if(userAll.occupation){
        userAll.occupation.id=userAll.occupation.masterGodownId
      }
      if(userAll?.currentOrg){
        userAll.currentOrg.id=userAll?.currentOrg?.masterGodownId
      }
      if(userAll?.collegeUniversity){
        userAll.collegeUniversity.id=userAll?.collegeUniversity?.masterGodownId
      }
      var newAcc:any=[]
      if(userAll.accomplishmentsList){
       for (const accElement of userAll.accomplishmentsList) {
        let mdImg:any=[]
          var newAccElement:any=accElement.serialize()
          newAccElement.medias.forEach(elm => {
            mdImg.push(elm?.url) 
          })
          newAccElement.medias=mdImg
          newAccElement.media=mdImg
          newAcc.push(newAccElement) 
       }
      }
      var newCer:any=[]
      if(userAll.certificatesList){ 
        for (const accElement of userAll.certificatesList) {
          let mdImg:any=[]
           var newCerElement:any=accElement.serialize()
           newCerElement.medias.forEach(elm => {
             mdImg.push(elm?.url) 
           })
           newCerElement.medias=mdImg
           newCerElement.media=mdImg
           newCer.push(newCerElement) 
        }
       }
       var newPro:any=[]
      if(userAll.projectList){
        for (const accElement of userAll.projectList) {
          let mdImg:any=[]
           var newProElement:any=accElement.serialize()
           newProElement.medias.forEach(elm => {
             mdImg.push(elm?.url) 
           })
           newProElement.medias=mdImg
           newProElement.media=mdImg
           newPro.push(newProElement) 
        }
       }


     
      // var block= await BlockList.query().where({byUid:userAll?.id,toUid:uid}).first()?true:false
      // var blocked= await BlockList.query().where({byUid:uid,toUid:userAll?.id}).first()?true:false
      if(uid){
        var blocked= await BlockList.query().where({byUid:userAll?.id,toUid:uid}).first()?true:false
        var block= await BlockList.query().where({byUid:uid,toUid:userAll?.id}).first()?true:false
        var followStatus = await FollowUser.query().where({uid:uid,targetId:userAll?.id}).first()?true:false
      }else{
        var blocked= false
        var block= false
        var followStatus = false
      }
      var videos
      if(data?.uid){
         videos = await YoutubeVideo.query().where({'uid':data?.uid})
      }else{
         videos=[]
      }
      const dataCounts = await commonHelper.userPostsAndCallsCount(data?.uid)
      var userdata:any = {
                  accomplishmentsList:newAcc,
                  certificatesList:newCer,
                  projectList:newPro,
                  profile:user,
                  country:userAll?.country,
                  state:userAll?.state,
                  highestEducation:userAll?.highestEducation,
                  intExpertiseList:userAll?.intExpertiseList,
                  occupation:userAll?.occupation,
                  skillsList:userAll?.skillsList,
                  currentOrg:userAll?.currentOrg,
                  collegeUniversity:userAll?.collegeUniversity,
                  viewBy:[],
                  followstatus:followStatus,
                  block: block,
                  blocked: blocked,
                  rating:rat?.rating,
                  ratingCount:rat?.count||0,
                  callResponseRate:respo||0,
                  youtubeVideos:videos,
                  totalPost:dataCounts?.totalPost,
                  totalMsg:dataCounts?.totalMsg,
                  recivedCalls:dataCounts?.recivedCalls,
                  initiatCalls:dataCounts?.initiatCalls,
                  moreVideo:userAll.$extras.userVideos_count
              }  
      await WebServiceLog.create({request:JSON.stringify(request),response:JSON.stringify(userdata),service_name:'get user profile Service res',status:1,uid:0||0,time_taken:0})                   
      return response.status(200).json({
        "uid":uid||data.uid,
        "logout": false,
        "resMsg": "string",
        "status": true,
        "userProfile":userdata
      })  
    } catch (error) {
      await WebServiceLog.create({request:JSON.stringify(data),response:JSON.stringify(error),service_name:'get user profile Service',status:1,uid:uid||0,time_taken:0})
        return response.status(200).json({
          "errors":error,
          "resMsg": "An Error",
          "status": false,
          "logout": true
        })
    }
    //return response.status(200).send(auth.user)
  }

  async verifyMobile({ request,response,auth }: HttpContextContract){
    const data=request.all()
    const uid=data?.uid
    const email=request.input('email')||''
    const mobile=request.input('mobile')||''
    const isRegs=request.input('reg')||false
    const social=request.input('social')||false
    //const social=request.input('social')||false
    await WebServiceLog.create({request:JSON.stringify(data),response:JSON.stringify({}),service_name:'verify Mobile init',status:1,uid:0,time_taken:0})
    var user:any = await User.query().where({email:email}).orWhere({mobile:mobile}).first()
      if(isRegs){
        if(uid){
          var uidUser:any= await User.query().where({id:uid}).first()
          var mobUser=await User.query().where({mobile:mobile}).orWhere({email:email}).first()

          if(!mobUser || uid==mobUser?.id){
            if(mobile){
              uidUser.mobile=mobile
              uidUser.mobileverified=false
              await uidUser.save()
              await commonHelper.sendMobileOtp(mobile);
            }else if(email){
              uidUser.email=email
              uidUser.emailverified=false
              await uidUser.save()
              await commonHelper.sendEmailOtp(email);
            }
            return response.status(200).json({
              "resMsg": "OTP Sent",
              "status": true,
              "logout": false
            })
          }else{
            return response.status(200).json({
              "resMsg": "Sorry! Mobile no or email already attached with seekex account please enter other.",
              "status": false,
              "logout": false
            })
          }
        }
  if(social){
    if(user && user?.regCompleted){
      return response.status(200).json({
        "resMsg": "Number already registered, please login.",
        "status": false,
        "logout": false
      })
    }else{
      if(mobile){
        await commonHelper.sendMobileOtp(mobile);
      }
      return response.status(200).json({
        "resMsg": "Complete user registration",
        "status": true,
        "logout": false
      })
    }

  }else{
    if(user && user?.regCompleted){
      return response.status(200).json({
        "resMsg": "Sorry! Mobile no or email already attached with seekex account please enter other.",
        "status": false,
        "logout": false
      })
    }else{
      if(email){
        await commonHelper.sendEmailOtp(email);
      }else if(mobile){
        await commonHelper.sendMobileOtp(mobile);
      }
      return response.status(200).json({
        "resMsg": "Otp Sent",
        "status": true,
        "logout": false
      })
    }
      }
        
      }else if(user && user?.regCompleted){
        if(email && mobile){
          await commonHelper.sendEmailOtp(user?.email);
          await commonHelper.sendMobileOtp(user?.mobile);
        }else if(mobile){
          await commonHelper.sendMobileOtp(user?.mobile);
        }else{
          await commonHelper.sendEmailOtp(user?.email);
        }
        return response.status(200).json({
          "resMsg": "OTP Sent",
          "status": true,
          "logout": false
        })
      }else{
        return response.status(200).json({
          "resMsg": "Sorry! User not Exist.",
          "status": false,
          "logout": false
        })
      }
  }

  async sendOtpOnEmail({ request,response,auth }: HttpContextContract){
    const uid=auth.user?.id
    const data=request.all()
    const email=request.input('email')
    const mobile=request.input('mobile')
    const isVerification=request.input('isVerification')||request.input('verification')
    await WebServiceLog.create({request:JSON.stringify(data),response:JSON.stringify({}),service_name:'Send OTP On Email for Mobile Service Start',status:1,uid:0,time_taken:0})
    var emailuser:any = await User.query().where({email:email}).first()
    await WebServiceLog.create({request:JSON.stringify(data),response:JSON.stringify(emailuser),service_name:'Send OTP On Email for Mobile Service Exist user',status:1,uid:0,time_taken:0})
    if(emailuser && emailuser?.regCompleted){
      await WebServiceLog.create({request:JSON.stringify(data),response:JSON.stringify(emailuser),service_name:'Send OTP On Email for Mobile Service Exist user end',status:1,uid:0,time_taken:0})
      return response.status(200).json({
        "resMsg": "Sorry! Email already attached with seekex account please enter other.",
        "status": false,
        "logout": false
      })
    }
    if(mobile){
       var user:any = await User.query().where({mobile:mobile}).first()
      // const otp=await commonHelper.sendMobleOtpOnEmail(email,mobile);
        user.email=email
        await user.save()
       const otp=await commonHelper.sendEmailOtp(email)
      if(otp){
        return response.status(200).json({
          "resMsg": "Email sent",
          "status": true,
          "logout": false
        })
      }else{
        return response.status(200).json({
          "resMsg": "Email not sent",
          "status": false,
          "logout": false
        })
      }
      
    }else{
      return response.status(200).json({
        "resMsg": "Email not sent",
        "status": false,
        "logout": false
      })
    }
  }
  async verifyOtp({ request,response }: HttpContextContract){
    const data=request.all()
    const email=request.input('email')
    const mobile=request.input('mobile')
    const otp =request.input('otp')
    const isRegs=request.input('reg')||false
    await WebServiceLog.create({request:JSON.stringify(data),response:JSON.stringify({}),service_name:'Validate OTP Service Start',status:1,uid:0,time_taken:0})
    const nmobile = mobile.replace(/\D/g, '').slice(-10)

          var vUser:any=  User.query()
          // .where(q=>{
          //   q.where({mobile:nmobile}).orWhere({email:email})
          // }).first()
     if(nmobile){
        vUser.where({mobile:nmobile})
     }
     if(email){
        vUser.orWhere({email:email})
      } 
    vUser = await vUser.first()

    if(vUser){
      const motp=vUser?.mobileOtp
      const eotp=vUser?.emailOtp
      await WebServiceLog.create({request:JSON.stringify({data,nmobile}),response:JSON.stringify(vUser),service_name:'Validate OTP Service init',status:1,uid:0,time_taken:0})
      if(otp==motp){
        if(!isRegs){
          vUser.mobileverified=1
        }
        //vUser.mobileverified=1
        vUser.mobileOtp=''
        await vUser.save()
        return response.status(200).json({
          "resMsg": "validate",
          "status": true,
          "logout": false,
          "validate":"mobile"
        })
      }
      if(otp==eotp){
        if(!isRegs){
          vUser.emailverified=1
        }
        //vUser.emailverified=1
        vUser.emailOtp=''
        await vUser.save()
        return response.status(200).json({
          "resMsg": "validated",
          "status": true,
          "logout": false,
          "validate":"email"
        })
      }
      await WebServiceLog.create({request:JSON.stringify({data,nmobile}),response:JSON.stringify(vUser),service_name:'Validate OTP Service error',status:1,uid:0,time_taken:0})
      return response.status(200).json({
        "resMsg": "Invalid otp",
        "status": false,
        "logout": false
      })
    }else{
      await WebServiceLog.create({request:JSON.stringify(data),response:JSON.stringify({}),service_name:'Validate OTP Service',status:1,uid:0,time_taken:0})
      return response.status(200).json({
        "resMsg": "Invalid Data",
        "status": false,
        "logout": false
      })
    }
  }

  async verifyEmailMobile({ request,response,auth }: HttpContextContract){
    const uid:any=auth.user?.id
    const data=request.all()
    if(data.emailverified || data.mobileverified){
      var user:any= await User.query().where("id",uid).first()
      user.emailverified=data.emailverified
      user.mobileverified=data.mobileverified
      try {
        await user.save()
        return response.status(200).json({
          "resMsg": "Verification Successfull",
          "status": true,
          "logout": false
        })
      } catch (error) {
        await WebServiceLog.create({request:JSON.stringify(data),response:JSON.stringify(error),service_name:'Verify Email Mobile Service',status:1,uid:uid,time_taken:0})
        return response.status(200).json({
          "errors":error,
          "resMsg": "An Error",
          "status": false,
          "logout": false
        })
      }

    }else{
      await WebServiceLog.create({request:JSON.stringify(data),response:JSON.stringify('An Error'),service_name:'Verify Email Mobile Service',status:1,uid:uid,time_taken:0})
      return response.status(200).json({
        "resMsg": "An Error",
        "status": false,
        "logout": false
      })
    }
  }

  async changePassword({ request,response}: HttpContextContract){
    const data=request.all()
    const uid=data?.uid
    const password=request.input('newPassword')
    const userId = data?.mobile||data?.email
    await WebServiceLog.create({request:JSON.stringify(data),response:JSON.stringify(userId),service_name:'Change Password',status:1,uid:0,time_taken:0})
    if(password){
      try {
        if(uid){
          const user = await User.findOrFail(uid)
          user.password=password
          await user.save()
          return response.status(200).json({
            "resMsg": "Success",
            "newPassword":password,
            "status": true,
            "logout": false
          })
        }else if(userId){
          const userDta:any=await User.query().where('mobile',userId).orWhere('email',userId).first()
          userDta.password=password
          await userDta.save()
          return response.status(200).json({
            "resMsg": "Success",
            "newPassword":password,
            "status": true,
            "logout": false
          })
        }

      } catch (error) {
        await WebServiceLog.create({request:JSON.stringify(data),response:JSON.stringify('An Error'),service_name:'Password Change Service',status:1,uid:uid,time_taken:0})
        return response.status(200).json({
          "errors":error,
          "resMsg": "Error",
          "status": false,
          "logout": false
        })
      }
    }else{
      return response.status(200).json({
          "resMsg": "Error",
          "status": false,
          "logout": false
      })
    }
  }

  async blockUnblockUser({ request,response,auth }: HttpContextContract){
    const uid=auth.user?.id
    const data = request.all()
    if(data.block){
      await BlockList.query().where({byUid:uid,toUid:data.id}).delete()
      var blockUser:any=new BlockList()
      blockUser.byUid=uid
      blockUser.toUid=data.id
     // blockUser.toUid=data.id
      blockUser.reason=data.reason
      try {
        await blockUser.save()
        return response.status(200).json({
          "logout": false,
          "resMsg": "User blocked",
          "status": true
        })
      } catch (error) {
        await WebServiceLog.create({request:JSON.stringify(data),response:JSON.stringify('An Error'),service_name:'Block User Service faield',status:1,uid:uid,time_taken:0})
        return response.status(200).json({
          "errors":error,
          "resMsg": "Error",
          "status": false,
          "logout": false
      })
      }
    }else{ 
      try {
        await BlockList.query().where({byUid:uid,toUid:data.id}).delete()
        return response.status(200).json({
          "logout": false,
          "resMsg": "User Un blocked",
          "status": true
        })
      } catch (error) {
        await WebServiceLog.create({request:JSON.stringify(data),response:JSON.stringify('An Error'),service_name:'Un Block User Service faield',status:1,uid:uid,time_taken:0})
        return response.status(200).json({
          "errors":error,
          "resMsg": "Error",
          "status": false,
          "logout": false
      })
      }
    }
  }

  async changeSetting({ request,response,auth }: HttpContextContract){
    const uid=auth.user?.id
    const data = request.all()
    var userData:any= await User.find(uid)
    if(data.available!=userData.available){
      if(data.available==false){
        await firebaseHelper.changeUserStatus(uid,0)
      }else if(data.available==true){
        await firebaseHelper.changeUserStatus(uid,1)
      }
    }
    if(data?.available){
      userData.available=data.available
    }
    if(data?.settings){
      userData.incomingCallSetting=data.settings?.callSetting
      userData.incomingCallSettingOption=data.settings?.apointmentOption
    }

    if (data.expertmode==false){
      userData.expertmode=data?.expertmode
      var restart=true
      var form=false
    }else{
      if(userData?.expert){
        userData.expertmode=data?.expertmode
      }
      if(userData?.regCompleteStep<4){
        var restart=false
        var form=true
      }else{
        var restart=true
        var form=false
      }
    }
    try {
      await userData.save()
      return response.status(200).json({
        "form": form,
        "logout": false,
        "resMsg": "Success",
        "restart": restart,
        "status": true
      })
    } catch (error) {
      await WebServiceLog.create({request:JSON.stringify(data),response:JSON.stringify(error),service_name:'Expert Change Seeting Service',status:1,uid:uid,time_taken:0})
      return response.status(200).json({
        "errors":error,
        "resMsg": "Error",
        "status": false,
        "logout": false
      })
    }
  }

  async doReported({ request,response,auth }: HttpContextContract){
    const uid=auth.user?.id
    const data = request.all()
    var reportData:any=new Reported()
      reportData.byUid=uid
      reportData.targetId=data.targetId
      reportData.reason=data.reason
      reportData.type=data?.type||1
    try {
      if(data?.type==1){
        /********************************************** */
        await notificationHelper.sendNotificationForReportPost(reportData,auth?.user)
       //********************************************* */
      }
      if(data?.type==2){
        /********************************************** */
        await notificationHelper.sendNotificationForReportUser(reportData,auth?.user)
       //********************************************* */
      }

      await reportData.save()
      return response.status(200).json({
        "logout": true,
        "resMsg": "Reported Successfuly",
        "status": true
      })
    } catch (error) {
      await WebServiceLog.create({request:JSON.stringify(data),response:JSON.stringify(error),service_name:'Reported Service',status:1,uid:uid,time_taken:0})
      return response.status(200).json({
        "errors":error,
        "resMsg": "Error",
        "status": false,
        "logout": false
      })
    }
  }

  async getBlockedUser({ request,response,auth }: HttpContextContract){
    const uid=auth.user?.id
    const data = request.all()
    const blockedUid=data.uid
    try {
          var blockedUsers = await BlockList.query().where({'byUid':uid})
          var bUser:any=[]
          if(blockedUsers){
            blockedUsers.forEach((elm:any)=>{
              bUser.push(elm.toUid)
            })
          }
       // return  bUser 
      var user:any = await (await User.query().preload('highestEducation').preload('occupation')
                      .whereIn('id',bUser)).map((element)=>{
                        var newElm:any={}
                        newElm.highestEducation = element?.highestEducation?.name||''
                        newElm.name= element?.name||''
                        newElm.occupation = element?.occupation?.name||''
                        newElm.profilePic= element?.profilePic||''
                        newElm.uid =  element?.id
                        return newElm
                      })
      //.where('id',blockedUid)
      //return response.status(201).json(user)
      
      return response.status(200).json({
        "data": user,
        "logout": false,
        "resMsg": "Blocked User Deatils",
        "status": true
      })
    } catch (error) {
      await WebServiceLog.create({request:JSON.stringify(data),response:JSON.stringify(error),service_name:'Get Blocked User Service',status:1,uid:uid,time_taken:0})
      return response.status(200).json({
        "errors":error,
        "resMsg": "Error",
        "status": false,
        "logout": false
      })
    }
  }

  async getBlockStatus({ request,response,auth }: HttpContextContract){
    const uid=auth.user?.id
    const data = request.all()
    const blockedUid=data.uid
    try {
      var blockUser= await BlockList.query().where({byUid:uid,toUid:blockedUid}).first()
      var blockExpert= await BlockList.query().where({byUid:blockedUid,toUid:uid}).first()
    //  return blockUser
      if(blockUser || blockExpert){
        return response.status(200).json({
          "block": true,
          "blocked": true,
          "logout": true,
          "resMsg": "Blocked User Status",
          "status": true
        })
      }else{
        return response.status(200).json({
          "block": false,
          "blocked": false,
          "logout": true,
          "resMsg": "Blocked User Status",
          "status": true
        })
      }
    } catch (error) {
      await WebServiceLog.create({request:JSON.stringify(data),response:JSON.stringify(error),service_name:'Get Blocked Status Service',status:1,uid:uid,time_taken:0})
      return response.status(200).json({
        "errors":error,
        "resMsg": "Error",
        "status": false,
        "logout": false
      })
    }
  }

  async getNotifications({response,auth,request }){
    const uid:any=auth.user?.id
    var page:any = request.input('page', 1)
    var data=request.all()
    await WebServiceLog.create({request:JSON.stringify({data}),response:JSON.stringify(page),service_name:'User Notifications Service Init',status:1,uid:uid,time_taken:0})
    if(page=='null' || page==null ||page=='NULL' || page==0){
      page=1
    }
    var lastPage
    const limit = 20
    try {
      var notifications:any = await AppNotification.query().where('uid',uid).whereNot('type',2).whereNot('type',4).whereNot('type',1).orderBy('created','desc').paginate(page,limit)
      await WebServiceLog.create({request:JSON.stringify({data}),response:JSON.stringify(notifications),service_name:'User Notifications Service Data',status:1,uid:uid,time_taken:0})
      lastPage = notifications.lastPage
      notifications=notifications.rows
      return response.status(200).json({
        "data":notifications||[],
        "logout": false,
        "resMsg": "User Notifications",
        "status": true,
        "currentPage":page,
        "totalPage":lastPage,
        "prevPage":page>1?page-1:null,
        "nextPage":page<lastPage?page+1:null
      })
    } catch (error) {
      await WebServiceLog.create({request:JSON.stringify({uid}),response:JSON.stringify(error),service_name:'User Notifications Service',status:1,uid:uid,time_taken:0})
      return response.status(200).json({
        "errors":error,
        "resMsg": "Error",
        "status": false,
        "logout": false,
        "currentPage":page,
        "totalPage":lastPage,
        "prevPage":page>1?page-1:null,
        "nextPage":page<lastPage?page+1:null
      })
    }
  }

  async getPendingNotification({response,auth }){
    const uid:any=auth.user?.id
    try {
      var notifications:any = await AppNotification.query()
      .pojo<{ total: number }>()
      .count('* as total').where({'uid':uid,status:false}).whereNot('type',2).whereNot('type',4).whereNot('type',1).first()
      var masgcount:any = await Chat.query().where({'receiverId':uid,'status':1})
      .pojo<{ total: number }>()
      .count('id as total').first()
      var count =notifications?.total||0
      var tMsgCount =masgcount?.total||0

      //return response.json(notifications)
      return response.status(200).json({
        "data":count?count:0,
        "unreadNotifications":count,
        "unreadMessages":tMsgCount,
        "logout": false,
        "resMsg": "User Notifications",
        "status": true
      })
    } catch (error) {
      await WebServiceLog.create({request:JSON.stringify({uid}),response:JSON.stringify(error),service_name:'User Notifications Service',status:1,uid:uid,time_taken:0})
      return response.status(200).json({
        "errors":error,
        "resMsg": "Error",
        "status": false,
        "logout": false
      })
    }
  }

  async getSchedule({ request,response,auth }: HttpContextContract){
    const uid=auth.user?.id
    const data = request.all()
    const expertId=data.expertId
    const expertTimeZone:any = await User.query().select('timeZone').where('id',expertId).first()
    //return expertTimeZone.timeZone
    try {
      const expertSch=await (await BookedSlot.query().where({"toUid":expertId,"status":1})).map((selm)=>{
        const elm=selm?.slotsId
        return elm
      }).filter(onlyUnique)
      //return expertSch
      var outData:any=[]
       await (await ExpertSchedule.query().preload('timingSlots')
                        .where({'uid':expertId,parentId:0})
                      //  .whereNotIn('id',expertSch)
                        .orderBy('daysId','asc').orderBy('id','asc'))
                        .map((elm)=>{
                          elm.fromTime= changeTime(expertTimeZone?.timeZone,elm.fromTime)
                          elm.toTime= changeTime(expertTimeZone?.timeZone,elm.toTime)
                          elm.timingSlots.forEach((t:any)=>{
                            t.booked=expertSch.indexOf(t.id)>-1?true:false
                            if(!elm.booked){
                              elm.booked=expertSch.indexOf(t.id)>-1?true:false
                            }
                            t.fromTime= changeTime(expertTimeZone?.timeZone,t.fromTime)
                            t.toTime= changeTime(expertTimeZone?.timeZone,t.toTime)
                          })
                          return elm
                        })
                        .forEach((a)=>{
                            if (!this[a.daysId]) {
                              this[a.daysId] = { daysId: a.daysId,daysName:days[a.daysId],timingList:[]}
                             // , timingList: [] 
                              outData.push(this[a.daysId])
                            }
                            this[a.daysId].timingList.push(a)
                        }, Object.create(null))
            //  return outData          
        for (const [key, value] of Object.entries(days)) {
          const existData = outData.find(elm=>elm.daysId==key)
          if(!existData){
            outData.push({ daysId: key,daysName:value,selected:false, timingList: [] })
          }
        }
        outData.sort(function(a, b) {
          if (a.daysId < b.daysId) return -1
        })

        // var newOutData:any=[]
        // for await (const data of outData) {
        //   const dates = await commonHelper.getAllDates(data?.daysId,4)
        //  // const dates = await commonHelper.getAllDates(data?.daysId-1,4) //For live
        //   var timeSlot=data?.dates
        //   //console.log('Data',JSON.stringify(data))
        //   var newSlots:any=[]
        //   for await(const d of dates){
        //     const bookingDate=d.toISOString().slice(0, 10)
        //     var booked_to = new Date(bookingDate)
        //     booked_to.setUTCHours(0,0,0,0)
        //     var booked_from = new Date(bookingDate)
        //     booked_from.setUTCHours(23,59,0,0)
        //     const bookedSlotsList=await (await BookedSlot.query().whereHas('bookedRequest',(query)=>{
        //       query.where({"daysId":data?.daysId,"toUid":expertId}).whereBetween('bookingDate',[booked_to,booked_from]).whereIn("status",[1,2])
        //     })).map((selm)=>{
        //       const elm=selm?.slotsId
        //       return elm
        //     }).filter(onlyUnique)
        //    // console.log('booked schedule',bookedSlotsList)
        //    //var newArr:any=[]
        //     timeSlot.map(element => {
        //       element.timingSlots.map((t:any)=>{
        //        // var e =t.serialize()
        //         t.booked=bookedSlotsList.indexOf(t.id)>-1?true:false
        //        // console.log('index',bookedSlotsList.indexOf(t.id))
        //         return t
        //       })
        //       return element
        //     })
        //     //console.log('element',JSON.stringify(timeSlot))
        //     newSlots.push({date:bookingDate,timingList:timeSlot})
        //   }
        //   data.dates=newSlots
        //   newOutData.push(data)
        // }
      return response.status(200).json({
        "data":outData,
        "logout": false,
        "resMsg": "User Schedule",
        "status": true
      })
   } catch (error) {
      await WebServiceLog.create({request:JSON.stringify({uid}),response:JSON.stringify(error),service_name:'User Get Schedule Service error',status:1,uid:uid,time_taken:0})
      return response.status(200).json({
        "errors":error,
        "resMsg": "Error Get Schedule",
        "status": false,
        "logout": false
      })
   }
  }

  async getScheduleDate({ request,response,auth }: HttpContextContract){
    const uid=auth.user?.id
    const data = request.all()
    const expertId=data?.expertId
    const daysId=data?.daysId
    const expertTimeZone:any = await User.query().select('timeZone').where('id',expertId).first()
    const dates = await commonHelper.getAllDates(daysId-1,4) //for live
   // const dates = await commonHelper.getAllDates(daysId,4)
    var outDataArr:any=[]
    for await(const d of dates){
      var currentDate:any = new Date()
      const newCurrentDate=currentDate.toISOString().slice(0, 10)
      const bookingDate=d.toISOString().slice(0, 10)
      //console.log('date',newCurrentDate,bookingDate)
      var booked_to = new Date(bookingDate)
      booked_to.setUTCHours(0,0,0,0)
      var booked_from = new Date(bookingDate)
      booked_from.setUTCHours(23,59,0,0)
      const bookedSlotsList=await (await BookedSlot.query().whereHas('bookedRequest',(query)=>{
        query.where({"daysId":daysId,"toUid":expertId}).whereBetween('bookingDate',[booked_to,booked_from]).whereIn("status",[1,2])
      })
      //.where({"toUid":expertId,"daysId":daysId})
      )
      .map((selm)=>{
        const elm=selm?.slotsId
        return elm
      }).filter(onlyUnique)
      //,"daysId":daysId
       const sh= await (await ExpertSchedule.query().preload('timingSlots')
                        .where({'uid':expertId,parentId:0,"daysId":daysId})
                      //  .whereNotIn('id',expertSch)
                        .orderBy('daysId','asc').orderBy('id','asc'))
                        .map((elm)=>{
                          // elm.fromTime= changeTime(expertTimeZone?.timeZone,elm.fromTime)
                          // elm.toTime= changeTime(expertTimeZone?.timeZone,elm.toTime)
                          elm.timingSlots.forEach(async(t:any)=>{
                            t.booked=bookedSlotsList.indexOf(t.id)>-1?true:false
                            // t.fromTime= changeTime(expertTimeZone?.timeZone,t.fromTime)
                            // t.toTime= changeTime(expertTimeZone?.timeZone,t.toTime)
                            if(newCurrentDate==bookingDate){
                              const st=await commonHelper.timeComp(t.fromTime)
                              t.booked=st
                            }
                          })
                          return elm
                        })
      //console.log('booking date',bookingDate,newCurrentDate)              
      const newBookingDate=bookingDate    
      const dt={
        date:newBookingDate.split("-").reverse().join('-'),
        timingList: sh 
      }
      outDataArr.push(dt)
    }
    if(outDataArr.length>0){
      await WebServiceLog.create({request:JSON.stringify(data),response:JSON.stringify({
        daysId:daysId,
        daysName:days[daysId],
        dates:outDataArr
      }),service_name:'Get Schedule with Date',status:1,uid:uid,time_taken:0})
      return response.status(200).json({
        "data":{
          daysId:daysId,
          daysName:days[daysId],
          dates:outDataArr
        },
        "logout": false,
        "resMsg": "User new Schedule with date by day",
        "status": true
      })
    }else{
      return response.status(200).json({
        "errors":'No any Schedule',
        "resMsg": "Error Get Schedule",
        "status": false,
        "logout": false
      })
    }
  }

  async likeFollow({ request,response,auth }: HttpContextContract){
    const uid=auth.user?.id
    const data = request.all()
    await WebServiceLog.create({request:JSON.stringify(data),response:JSON.stringify(uid),service_name:'User Follow Like Service',status:1,uid:uid,time_taken:0})
    var exit:any
    var saveData:any
    if(data.type =='1'){
      saveData=new LikePost()
      saveData.uid=uid
      saveData.targetId=data.targetId
      exit=await LikePost.query().where({uid:uid,targetId:data.targetId}).delete()
    }
    if(data.type=='2'){
      saveData=new LikeComment()
      saveData.uid=uid
      saveData.targetId=data.targetId
      exit=await LikeComment.query().where({uid:uid,targetId:data.targetId}).delete()
    }
    if(data.type=='3'){
      saveData=new FollowPost()
      saveData.uid=uid
      saveData.targetId=data.targetId
      exit=await FollowPost.query().where({uid:uid,targetId:data.targetId}).delete()
    }
    if(data.type=='4'){
      saveData=new FollowUser()
      saveData.uid=uid
      saveData.targetId=data.targetId
      exit=await FollowUser.query().where({uid:uid,targetId:data.targetId}).delete()
      if(exit==1){
        await User.query().where({'id':data?.targetId}).decrement('followers',1)
        await User.query().where({'id':uid}).decrement('following',1)
      }else{
        await User.query().where({'id':data?.targetId}).increment('followers',1)
        await User.query().where({'id':uid}).increment('following',1)
      }
    }
    //return {exit,saveData}
    if(exit==0){
     // await notificationHelper.sendNotificationForLikesFollow(saveData,auth?.user,data?.type)
    }
    try {
     if(exit==0){
        await saveData.save()
      /**************************Notifications********************************/
        await notificationHelper.sendNotificationForLikesFollow(saveData,auth?.user,data?.type)
      ////////////////////////////////////////////////////////////////////////
      }
      return response.status(200).json({
        "logout": false,
        "resMsg": "Successful",
        "status": true,
        "action":exit
      })
    } catch (error) {
      await WebServiceLog.create({request:JSON.stringify(uid),response:JSON.stringify(error),service_name:'User Get Follow Like Service',status:1,uid:uid,time_taken:0})
      return response.status(200).json({
        "errors":error,
        "resMsg": "Error Like Follow",
        "status": false,
        "logout": false
      })
    }
  }

  async rateSetting({ request,response,auth }: HttpContextContract){
    const uid=auth.user?.id
    const data = request.all()
    var user:any=await User.find(uid)
      if(user?.resume){
        let resumeFile=user?.resume
        let fileNme=fileName(resumeFile)
        try {
          var parseFile= await resumeHelper.parseResume(fileNme)
          user.resumeText=parseFile
        } catch (error) {
          await WebServiceLog.create({request:JSON.stringify(data),response:JSON.stringify(error),service_name:'Resume Parse error',status:1,uid:uid,time_taken:0})
        }
      }
    try {
        user.audioCallRate=data?.callRate
        user.videoCallRate=data?.videoCallRate
        user.expert=true
       // user.expertmode=true
        user.requested=true
       // user.chanalUrl=data?.chanalUrl||''
        user.audioCallRatePerSec=await commonHelper.showCallAmount(data?.callRate,false,'VOIP')||0
        user.videoCallRatePerSec=await commonHelper.showCallAmount(data?.videoCallRate,true,'VOIP')||0
        user.regCompleteStep=4
        await user.save()
        await WebServiceLog.create({request:JSON.stringify(data),response:JSON.stringify(user),service_name:'User Call Rate Setting Service Success',status:1,uid:uid,time_taken:0})
      return response.status(200).json({
        "id":user?.id,
        "logout": true,
        "resMsg": "Successful",
        "status": true
      })
    } catch (error) {
      await WebServiceLog.create({request:JSON.stringify(data),response:JSON.stringify(error),service_name:'User Call Rate Setting Service',status:1,uid:uid,time_taken:0})
      return response.status(200).json({
        "errors":error,
        "resMsg": "Call Rate Setting Error",
        "status": false,
        "logout": false
      })
    }
  }

  async readNotification({ request,response,auth }: HttpContextContract){
    const uid=auth.user?.id
    const data = request.all()
    const ids = data?.ids

    try {
      if(data.id){
        var notification:any=await AppNotification.find(data.id)
        notification.status=true
        await notification.save()
      }
      if(ids.length>0){
        await AppNotification.query().whereIn('id',ids).update({status:true})
      }
      return response.status(200).json({
        "logout": false,
        "resMsg": "Successful",
        "status": true
      })
    } catch (error) {
      await WebServiceLog.create({request:JSON.stringify(data),response:JSON.stringify(error),service_name:'User Call Rate Setting Service',status:1,uid:uid,time_taken:0})
      return response.status(200).json({
        "errors":error,
        "resMsg": "Call Rate Setting Error",
        "status": false,
        "logout": false
      })
    }
  }

  async submitSchedule({ request,response,auth }: HttpContextContract){
    const uid=auth.user?.id
    const data = request.all()
    const scheduleData=data.data
    await WebServiceLog.create({request:JSON.stringify(scheduleData),response:JSON.stringify({}),service_name:'Scheduled data save Service init',status:1,uid:uid,time_taken:0})

    await ExpertSchedule.query().where({'uid':uid}).delete()
    scheduleData.forEach(element => {
      element.uid=uid
      if(element.timingList.length>0){
        // console.log('elm length',element.timingList.length)
        // console.log('elm',element.timingList)
        element.timingList.forEach(async (ele,indx) => {
         const filterData= ele.timingSlots.filter(e=>e.selected==true)
              filterData.map((e)=>{
                e.uid=uid
                if(e.selected){
                  delete e.selected
                }
              })

        //  console.log('elm2 '+indx, filterData)
          const expSch:any=new ExpertSchedule()
          expSch.uid=uid
          expSch.booked=false
          expSch.daysId=element.daysId
          expSch.fromTime=ele.fromTime
          expSch.toTime=ele.toTime
          expSch.parentId=0
         // console.log('fd',expSch)
         // expSch.$trx = await Database.transaction()
          try {
             await expSch.related('timingSlots').createMany(filterData)
            // await expSch.$trx.commit()
  
            } catch (error) {
             // await expSch.$trx.rollback()
              await WebServiceLog.create({request:JSON.stringify(data),response:JSON.stringify(error),service_name:'Scheduled data save Service error',status:1,uid:uid,time_taken:0})
              return response.status(200).json({
                "errors":error,
                "logout": false,
                "resMsg": "Scheduled data not save error",
                "status": true
              })
            }
        })
      }
    })
    return response.status(200).json({
      "logout": false,
      "resMsg": "Scheduled data has been saved",
      "status": true
    })
  }
 
  async updateAdvanceDetail({ request,response,auth }: HttpContextContract){
    const uid=auth.user?.id
    const data = request.all()
    //const medias = request.files('media')
    const details=data.data
    if(!details.organizationId){
      const newOrg:any= new Organization()
      newOrg.name=details?.company
      newOrg.created_by=uid
      newOrg.type=details?.type||1
      newOrg.used=0
      const existOrg= await Organization.getIDByName(details?.company)
      if(existOrg){
        details.organizationId=existOrg
      }else{
        var org= await newOrg.save()
        details.organizationId=org?.id
      }
    }
    var advDetail:any=new UserAdvanceDetail()
    advDetail.uid=uid
    advDetail.company=details?.company
    advDetail.description=details?.description
    advDetail.detailType=details?.detailType
    advDetail.title=details?.title
    advDetail.organizationId=details?.organizationId
    advDetail.startDate=details?.startDate||details?.start_date
    advDetail.endDate=details?.endDate||details?.end_date
    try {
      //advDetail.save()
      if(details?.media){
        var allMedia:any=[];
        details?.media.forEach(element => {
          var appMedia:any = new AppMedia()
          //appMedia.mediaFor=advDetail.id
          appMedia.mediaType=advDetail.detailType
          appMedia.url=element
          allMedia.push(appMedia)
        });
        await advDetail.related('medias').saveMany(allMedia)
        await Organization.query().where('id',details?.organizationId).increment('used',1)
      }
      // if(advDetail?.id && medias){
      //   var mediaFolder=advDetail.detailType==1?'accomplishments':advDetail.detailType==2?'certificate':advDetail.detailType==3?'project':''
      //   medias.forEach(async (ufile)=>{
      //     const fileName=`${new Date().getTime()+ufile.size}.${ufile.extname}`
      //     await ufile.move(Application.publicPath(),{
      //       name: fileName,
      //     })
      //     uploader.uploadFile(fileName,ufile.type,uid,advDetail.detailType,advDetail?.id,mediaFolder)
      //   }) 
      // }
      await WebServiceLog.create({request:JSON.stringify(data),response:JSON.stringify(advDetail),service_name:'Advance Detail data save Service',status:1,uid:uid,time_taken:0})
      return response.status(200).json({
        "logout": true,
        "resMsg": "Advance Detail has been saved",
        "status": true,
       // "data":advDetail
      })
    } catch (error) {
      await WebServiceLog.create({request:JSON.stringify(data),response:JSON.stringify(error),service_name:'Advance Detail data save Service',status:1,uid:uid,time_taken:0})
      return response.status(200).json({
        "errors":error,
        "logout": false,
        "resMsg": "Advance Detail data not save error",
        "status": false
      })
    }
  }

  async updateProfileMedia({ request,response,auth }: HttpContextContract){
    const uid=auth.user?.id
    const data = request.all()
    var user:any = await User.find(uid)
    var url=''
      if(data?.type==7){
        user.profilePic=data?.url||''
      }else if(data?.type==8){
        user.resume=data?.url||''
      }else if(data?.type==9){
        user.intro=data?.url||''
        user.introType=data?.introType
      }else{
        user.profilePic=data?.url||''
      }

      try {
        await user.save()
        if(data?.introType==2 && data?.type==9){
          await UserVideo.query().where({createdBy:uid,videoType:2}).update({status:0})
          var video:any = new  UserVideo()
          video.createdBy=uid
          video.videoUrl=data?.url||''
          video.publicId=data?.publicId||0
          video.description='Intro Video'
          video.location=''
          video.privates=false
          video.videoType=2
          video.views=0
          video.hash_tags=''
          await video.save()
        }
        await WebServiceLog.create({request:JSON.stringify(data),response:JSON.stringify(user),service_name:'User media upload Service',status:1,uid:0,time_taken:0})
        return response.status(200).json({
          'data':user,
          "logout": false,
          "resMsg": url||data?.url||'',
          "status": true
        })
      } catch (error) {
        await WebServiceLog.create({request:JSON.stringify(data),response:JSON.stringify(error),service_name:'User media upload Service error',status:1,uid:uid,time_taken:0})
        return response.status(200).json({
          "errors":error,
          "logout": false,
          "resMsg": "User media upload Service",
          "status": false
        })
      }
    
  }
   

  async updateStatus({ request,response,auth }: HttpContextContract){
    const uid=auth.user?.id
    const data = request.all()
    try {
      data.locations.map((eml)=>{
        eml.uid=uid
      })
      await UserLocation.createMany(data.locations)
      return response.status(200).json({
        "logout": true,
        "resMsg": "Update Status Successful",
        "status": true
      })
    } catch (error) {
      await WebServiceLog.create({request:JSON.stringify(data),response:JSON.stringify(error),service_name:'User media upload Service',status:1,uid:uid,time_taken:0})
        return response.status(200).json({
          "errors":error,
          "logout": false,
          "resMsg": "Update Status",
          "status": false
        })
    }
  }

  async updateProfile({ request,response,auth }: HttpContextContract){
    const uid=auth.user?.id
    const data = request.all()
    var user:any = await User.find(uid)
    const profileMedia=request.file('file')
    await WebServiceLog.create({request:JSON.stringify(data),response:JSON.stringify(user),service_name:'Upload Profile Service Api init',status:1,uid:uid,time_taken:0})
    if(profileMedia){
      const fileName=`${new Date().getTime()}.${profileMedia.extname}`
      await profileMedia.move(Application.tmpPath('uploads'),{
        name: fileName,
      })
      const uploadUrl= await uploader.uploadChatFile(fileName,profileMedia.type,uid,7,0,'profile')
      user.profilePic=uploadUrl?.Location
    }else if(data?.profile?.profilePic){
      user.profilePic=data?.profile?.profilePic
    }else{
      user.profilePic=user?.profilePic
    }
    
    try {
      user.name=data.profile.name

      if(user.email !=data.profile.email){
        user.email = data.profile.email
        user.emailverified = false
      }
      if(user.mobile !=data.profile.mobile){
        user.mobile = data.profile.mobile
        user.mobileverified = false
      }
      if(data.profile.timeZone){
        user.timeZone=data.profile.timeZone||'NA'
      }
      user.intro = data.profile.intro
      user.aboutMe=data.profile.aboutMe
      if(data.profile.expert){
        user.expert=data.profile.expert
      }
      if(data.profile.genderId){
        user.genderId=data.profile.genderId
      }
      if(data.profile.dob){
        user.dob=data.profile.dob
      }
      /***************************User Age calculation *******************************/
      // user.age= new Date(Date.now()-new Date(data.profile.dob).getTime()).getUTCFullYear() - 1970
      user.age = data.profile.dob?new Date(Date.now()-new Date(data.profile.dob).getTime()).getUTCFullYear() - 1970:0
      /***************************User Age calculation [End] *******************************/
     
      if(data.countryState.length>0){
        var countryStateData:any=[]
        data.countryState.forEach(element => {
          countryStateData.push({countryStateId:element.id,countryStateParentId:element.parentId,uid:uid})
        })
        await UserCountryState.query().where({'uid':uid}).delete()
        await UserCountryState.createMany(countryStateData)
      }
      var godownData=data.mastergodowns||data.masterGodowns
      if(godownData.length>0){
        var masterGodowns:any=[]
        for (const element of godownData) {
          if(!element.id){
            if(element?.mastertype==7 || element?.masterType==7){
              const newOrg:any= new Organization()
                newOrg.name=element?.name.trim()
                newOrg.created_by=uid
                newOrg.type=1
                newOrg.used=1
                const existOrg= await Organization.getIDByName(element?.name.trim())
                if(existOrg){
                  await Organization.query().where('id',existOrg).increment('used',1)
                  masterGodowns.push({masterGodownId:existOrg,masterType:element?.masterType||element.mastertype,masterFor:uid})
                }else{
                  var org= await newOrg.save()
                  masterGodowns.push({masterGodownId:org?.id,masterType:element?.masterType||element.mastertype,masterFor:uid})
                }
            }else if(element?.mastertype==9 || element?.masterType==9){
              const newOrg:any= new Organization()
                newOrg.name=element?.name.trim()
                newOrg.created_by=uid
                newOrg.type=2
                newOrg.used=1
                const existOrg= await Organization.getIDByName(element?.name.trim())
                if(existOrg){
                  await Organization.query().where('id',existOrg).increment('used',1)
                  masterGodowns.push({masterGodownId:existOrg,masterType:element?.masterType||element.mastertype,masterFor:uid})
                }else{
                  var org= await newOrg.save()
                  masterGodowns.push({masterGodownId:org?.id,masterType:element?.masterType||element.mastertype,masterFor:uid})
                }
            }else{
              const newMasterGodown:any= new MasterGodown();
              newMasterGodown.imageUrl=element?.imageurl||element?.imageUrl||''
              newMasterGodown.masterType=element?.masterType||element?.mastertype
              newMasterGodown.name=element?.name.trim()
              newMasterGodown.createdBy=uid
              newMasterGodown.used=1
              const existTag= await MasterGodown.getIDByName(newMasterGodown?.name)
              if(existTag){
                await MasterGodown.query().where('id',existTag).increment('used',1)
                masterGodowns.push({masterGodownId:existTag,masterType:element?.masterType||element.mastertype,masterFor:uid})
              }else{
                var newMdata = await newMasterGodown.save()
                masterGodowns.push({masterGodownId:newMdata?.id,masterType:element?.masterType||element.mastertype,masterFor:uid})
              }
            }
            
            //await MasterGodown.query().where('id',newMdata?.id).increment('used',1)
            
         }else{
           await MasterGodown.query().where('id',element?.id).increment('used',1)
           masterGodowns.push({masterGodownId:element?.id,masterType:element.masterType||element.mastertype,masterFor:uid})
         }
        }
        
        await UserMasterGodown.query().where({'masterFor':uid}).delete()
        await UserMasterGodown.createMany(masterGodowns)
       // return response.status(200).json(masterGodowns)
      }
      user.regCompleteStep=2
      await user.save()
      await WebServiceLog.create({request:JSON.stringify(data),response:JSON.stringify(user),service_name:'Upload Profile Service Api',status:1,uid:uid,time_taken:0})
      return response.status(200).json({
        "logout": true,
        "resMsg": "User Profile updated",
        "status": true,
        'data':data
      })
    } catch (error) {
      await WebServiceLog.create({request:JSON.stringify(data),response:JSON.stringify(error),service_name:'Update Profile Service Api',status:1,uid:uid,time_taken:0})
        return response.status(200).json({
          "errors":error,
          "logout": false,
          "resMsg": "Update Status failed",
          "status": false,
          'data':data
        })
    }
  }

  async getExperts({ request,response,auth }: HttpContextContract){
    const uid:any=auth.user?.id
    const data = request.all()
    const type=data.type||1
    const userType=data?.userType||1
    var page:any = request.input('page', 1)
    if(page=='null' || page==null ||page=='NULL' || page==0){
      page=1
    }
    var lastPage
    const limit = 10
    try {
      var expertsQuery = User.query()
                      .preload('highestEducation')
                      .preload('occupation')
                      .preload('currentOrg')
                      .preload('collegeUniversity')
                      //.whereNot({id:uid})
                      // .preload('accomplishmentsList')
                      // .preload('certificatesList')
                      // .preload('projectList')
                      // .preload('country')
                      // .preload('state')
                      // .preload('skillsList')
                      // .preload('intExpertiseList')
                      .where((query)=>{
                        query.where('status',1).whereNot({id:uid})
                      })
                      .apply((scopes) => scopes.visible())
      if(type==1){
        if(data.expertId){
          if(userType==2){
            expertsQuery.where({expert:1})
          }
          expertsQuery.where({'id':data.expertId})
        }else{
          if(userType==2){
            expertsQuery.where({expert:1})
          }
        }
      }else if(type==2){
        var follows= await FollowUser.query().where({target_id:uid})
        var followersuids:any=[]
          follows.forEach((elm:any)=>{
            followersuids.push(elm.uid)
          })
          if(userType==2){
            expertsQuery.where({expert:1})
          }
          expertsQuery.whereIn('id',followersuids)
      }else if(type==3){
        var following= await FollowUser.query().where({uid:uid})
        var followsuids:any=[]
          following.forEach((elm:any)=>{
            followsuids.push(elm.targetId)
          })
          if(userType==2){
            expertsQuery.where({expert:1})
          }
          expertsQuery.whereIn('id',followsuids) 
      }
      if(userType==2){
        // expertsQuery.where((query)=>{
        //   query.where('audioCallRate','>',0).orWhere('videoCallRate','>',0)
        // })
      }
      
      var experts:any = await expertsQuery
                            // .apply((scopes) => scopes.rate())
                            // .apply((scopes) => scopes.active())
                           // .apply((scopes) => scopes.noSameUser(auth?.user))
                           .orderBy('id','desc').paginate(page,limit)
      lastPage = experts?.lastPage
      experts=experts.rows

          experts = await Promise.all(experts.map(async(elm:any)=>{
            if(elm.audioCallRate>0){
              elm.audioCallRate= await commonHelper.showCallAmount(elm?.audioCallRate||0,false,'VOIP')
             // elm.audioCallRate=parseFloat((elm.audioCallRate/(5*60)).toFixed(3))
            }
            if(elm.videoCallRate>0){
              elm.videoCallRate=await commonHelper.showCallAmount(elm?.videoCallRate||0,true,'VOIP')
             // elm.videoCallRate=parseFloat((elm.videoCallRate/(5*60)).toFixed(3))
            }
            var newElm:any = elm.serialize()
            var profileData = elm.serialize()
            const respo=await commonHelper.expertResponceRate(newElm?.id)
            profileData.callResponseRate=respo||0
            newElm.profile =  profileData
            // if(!newElm?.country){
            //   newElm.country={name:'',parentId:0,masterType:0,imageUrl:'',used:0}
            // }
            // if(!newElm?.state){
            //   newElm.state={name:'',parentId:0,masterType:0,imageUrl:'',used:0}
            // }
            if(!newElm.highestEducation){
              newElm.highestEducation={name:'',parentId:0,masterType:0,imageUrl:'',used:0}
            }
            if(!newElm.occupation){
              newElm.occupation={name:'',parentId:0,masterType:0,imageUrl:'',used:0}
            }
            newElm.block= await BlockList.query().where({byUid:uid,toUid:newElm?.id}).first()?true:false
            newElm.blocked= await BlockList.query().where({byUid:newElm?.id,toUid:uid}).first()?true:false
            newElm.followstatus = await FollowUser.query().where({uid:uid,targetId:newElm?.id}).first()?true:false
            // if(newElm.profile?.hasOwnProperty('country')){
            //   delete newElm.profile['country']
            // }
            // if(newElm.profile?.hasOwnProperty('state')){
            //   delete newElm.profile['state']
            // }
            // if(newElm.profile?.hasOwnProperty('accomplishmentsList')){
            //   delete newElm.profile['accomplishmentsList']
            // }
            // if(newElm.profile?.hasOwnProperty('certificatesList')){
            //   delete newElm.profile['certificatesList']
            // }
            // if(newElm.profile?.hasOwnProperty('projectList')){
            //   delete newElm.profile['projectList']
            // }
            // if(newElm.profile?.hasOwnProperty('skillsList')){
            //   delete newElm.profile['skillsList']
            // }
            // if(newElm.profile?.hasOwnProperty('intExpertiseList')){
            //   delete newElm.profile['intExpertiseList']
            // }

            if(newElm.profile?.hasOwnProperty('highestEducation')){
              delete newElm.profile['highestEducation']
            }
            if(newElm.profile?.hasOwnProperty('occupation')){
              delete newElm.profile['occupation']
            }
            newElm.genderId=parseInt(newElm.genderId)
            /** Rating And Responce Count */
            const rat = await commonHelper.userRating(newElm?.id)
            newElm.profile.rating=rat?.rating
            newElm.profile.ratingCount=rat?.count||0
            newElm.profile.callResponseRate=respo||0
            newElm.rating=rat?.rating
            newElm.ratingCount=rat?.count||0
            newElm.callResponseRate=respo||0
            
            /** Rating And Responce Count */
            return newElm

          }))

      return response.status(200).json({
              "uid":uid,
              "data":experts||[],
              "logout": false,
              "resMsg": "Get experts",
              "status": true,
              "currentPage":page,
              "totalPage":lastPage,
              "prevPage":page>1?page-1:null,
              "nextPage":page<lastPage?page+1:null
      })
      
    } catch (error) {
      await WebServiceLog.create({request:JSON.stringify(data),response:JSON.stringify(error),service_name:'Get Experts Service Api',status:1,uid:uid,time_taken:0})
      return response.status(200).json({
        "errors":error,
        "logout": false,
        "resMsg": "Get experts failed",
        "status": false,
        "data":[]
      })
    }
  }

  async getFollowers({ request,response,auth }: HttpContextContract){
    const uid:any=auth.user?.id
    const data = request.all()
    const type=data.type||1
    var page:any = request.input('page', 1)
    if(page=='null' || page==null ||page=='NULL' || page==0){
      page=1
    }
    var lastPage
    const limit = 10
    try {
      var expertsQuery = User.query()
                      .preload('highestEducation')
                      .preload('occupation')
                      .where((query)=>{
                        query.where('status',1).whereNot({id:uid})
                      })
      if(type==1){
        if(data.expertId){
          expertsQuery.where({'id':data.expertId})
        }else{
         // expertsQuery.where({expert:1})
        }
      }else if(type==2){
        var follows= await FollowUser.query().where({target_id:uid})
        var followersuids:any=[]
          follows.forEach((elm:any)=>{
            followersuids.push(elm.uid)
          })
         // expertsQuery.where({expert:1})
          expertsQuery.whereIn('id',followersuids)
      }else if(type==3){
        var following= await FollowUser.query().where({uid:uid})
        var followsuids:any=[]
          following.forEach((elm:any)=>{
            followsuids.push(elm.targetId)
          })
         // expertsQuery.where({expert:1})
          expertsQuery.whereIn('id',followsuids) 
      }
      // expertsQuery.where((query)=>{
      //  // query.where('audioCallRate','>',0).orWhere('videoCallRate','>',0)
      // })
      var experts:any = await expertsQuery
                            // .apply((scopes) => scopes.rate())
                            // .apply((scopes) => scopes.active())
                           // .apply((scopes) => scopes.noSameUser(auth?.user))
                           .orderBy('id','desc').paginate(page,limit)
      lastPage = experts?.lastPage
      experts=experts.rows

          experts = await Promise.all(experts.map(async(elm:any)=>{
            if(elm.audioCallRate>0){
              elm.audioCallRate= await commonHelper.showCallAmount(elm?.audioCallRate||0,false,'VOIP')
            }
            if(elm.videoCallRate>0){
              elm.videoCallRate=await commonHelper.showCallAmount(elm?.videoCallRate||0,true,'VOIP')
            }
            var newElm:any = elm.serialize()
            var profileData = elm.serialize()
            const respo=await commonHelper.expertResponceRate(newElm?.id)
            profileData.callResponseRate=respo||0
            newElm.profile =  profileData
            if(!newElm.highestEducation){
              newElm.highestEducation={name:'',parentId:0,masterType:0,imageUrl:'',used:0}
            }
            if(!newElm.occupation){
              newElm.occupation={name:'',parentId:0,masterType:0,imageUrl:'',used:0}
            }
            newElm.block= await BlockList.query().where({byUid:uid,toUid:newElm?.id}).first()?true:false
            newElm.blocked= await BlockList.query().where({byUid:newElm?.id,toUid:uid}).first()?true:false
            newElm.followstatus = await FollowUser.query().where({uid:uid,targetId:newElm?.id}).first()?true:false

            if(newElm.profile?.hasOwnProperty('highestEducation')){
              delete newElm.profile['highestEducation']
            }
            if(newElm.profile?.hasOwnProperty('occupation')){
              delete newElm.profile['occupation']
            }
            newElm.genderId=parseInt(newElm.genderId)
            /** Rating And Responce Count */
            const rat = await commonHelper.userRating(newElm?.id)
            newElm.profile.rating=rat?.rating
            newElm.profile.ratingCount=rat?.count||0
            newElm.profile.callResponseRate=respo||0
            newElm.rating=rat?.rating
            newElm.ratingCount=rat?.count||0
            newElm.callResponseRate=respo||0
            /** Rating And Responce Count */
            return newElm
          }))

      return response.status(200).json({
              "uid":uid,
              "data":experts||[],
              "logout": false,
              "resMsg": "Get experts",
              "status": true,
              "currentPage":page,
              "totalPage":lastPage,
              "prevPage":page>1?page-1:null,
              "nextPage":page<lastPage?page+1:null
      })
      
    } catch (error) {
      await WebServiceLog.create({request:JSON.stringify(data),response:JSON.stringify(error),service_name:'Get Experts Service Api',status:1,uid:uid,time_taken:0})
      return response.status(200).json({
        "errors":error,
        "logout": false,
        "resMsg": "Get experts failed",
        "status": false,
        "data":[]
      })
    }
  }

  async userOnlineOflineStatus({ request,response,auth }: HttpContextContract){
      const uid=auth.user?.id
      const data = request.all()
      const status = data?.status

      try {
        var userStatus = await firebaseHelper.changeUserStatus(uid,status)
        return response.status(200).json({
          "userStatus":userStatus?.status,
          "logout": false,
          "resMsg": "User Status updated",
          "status": true
        })
      } catch (error) {
        await WebServiceLog.create({request:JSON.stringify(data),response:JSON.stringify(error),service_name:'Change User Status Service Api',status:1,uid:uid,time_taken:0})
      return response.status(200).json({
        "errors":error,
        "logout": false,
        "resMsg": "User Status update failed",
        "status": false
      })
      }
      return userStatus
  }

  async updateAccessibility({ request,response,auth }: HttpContextContract){
    const uid=auth.user?.id
    const data = request.all()
    const exotel=data?.exotelCallOption?1:0
    const voip=data?.voipCallOption?1:0
    const video=data?.videoCallOption?1:0
    const accessibility=data?.accessibility?1:0
    var user:any = await User.find(uid)
    user.exotelCallOption=exotel
    user.voipCallOption=voip
    user.videoCallOption=video
    user.accessibility=accessibility
    try {
      await user.save()
      await WebServiceLog.create({request:JSON.stringify(data),response:JSON.stringify(user),service_name:'Accessibility Status update Service Api success',status:1,uid:uid,time_taken:0})
      return response.status(200).json({
        "data":user,
        "logout": false,
        "resMsg": "Accessibility update",
        "status": true
      })
    } catch (error) {
      await WebServiceLog.create({request:JSON.stringify(data),response:JSON.stringify(error),service_name:'Accessibility Status update Service Api',status:1,uid:uid,time_taken:0})
      return response.status(200).json({
        "errors":error,
        "logout": false,
        "resMsg": "User Accessibility Status update failed",
        "status": false
      })
    }
  }

  async resendOtp({ request,response,auth }: HttpContextContract){
    const uid=auth.user?.id
    const data = request.all()
    const email=request.input('email')||''
    const mobile=request.input('mobile')||''
    const social=request.input('social')||false
    //var user=await User.query().where({mobile:mobile}).orWhere({email:email}).first()
    await WebServiceLog.create({request:JSON.stringify(data),response:JSON.stringify({}),service_name:'Resend init',status:1,uid:0,time_taken:0})
      if(email && mobile && !social){
          await commonHelper.sendEmailOtp(email);
          await commonHelper.sendMobileOtp(mobile);
        }else if(mobile){
          await commonHelper.sendMobileOtp(mobile);
        }else{
          await commonHelper.sendEmailOtp(email);
        }
        return response.status(200).json({
          "resMsg": "OTP Sent",
          "status": true,
          "logout": false
        })
  }


  async getAllExperts({ request,response }: HttpContextContract){
    //const uid:any=auth.user?.id
    const data = request.all()
    const type=data.type||1
    const userType=data?.userType||1
    var page:any = request.input('page', 1)
    if(page=='null' || page==null ||page=='NULL' || page==0){
      page=1
    }
    var lastPage
    const limit = 10
    try {
      var expertsQuery = User.query()
                      .preload('highestEducation')
                      .preload('occupation')
                      .preload('currentOrg')
                      .preload('collegeUniversity')
                      //.whereNot({id:uid})
                      // .preload('accomplishmentsList')
                      // .preload('certificatesList')
                      // .preload('projectList')
                      // .preload('country')
                      // .preload('state')
                      // .preload('skillsList')
                      // .preload('intExpertiseList')
                      .where((query)=>{
                        query.where('status',1).where({expert:1})
                      })
                      .apply((scopes) => scopes.visible())
      // if(type==1){
      //   if(data.expertId){
      //     if(userType==2){
      //       expertsQuery.where({expert:1})
      //     }
      //     expertsQuery.where({'id':data.expertId})
      //   }else{
      //     if(userType==2){
      //       expertsQuery.where({expert:1})
      //     }
      //   }
      // }else if(type==2){
      //   var follows= await FollowUser.query().where({target_id:uid})
      //   var followersuids:any=[]
      //     follows.forEach((elm:any)=>{
      //       followersuids.push(elm.uid)
      //     })
      //     if(userType==2){
      //       expertsQuery.where({expert:1})
      //     }
      //     expertsQuery.whereIn('id',followersuids)
      // }else if(type==3){
      //   var following= await FollowUser.query().where({uid:uid})
      //   var followsuids:any=[]
      //     following.forEach((elm:any)=>{
      //       followsuids.push(elm.targetId)
      //     })
      //     if(userType==2){
      //       expertsQuery.where({expert:1})
      //     }
      //     expertsQuery.whereIn('id',followsuids) 
      // }
        expertsQuery.where((query)=>{
         // query.where('audioCallRate','>',0).orWhere('videoCallRate','>',0)
        })
      
      var experts:any = await expertsQuery
                           .orderBy('id','desc').paginate(page,limit)
      lastPage = experts?.lastPage
      experts=experts.rows

          experts = await Promise.all(experts.map(async(elm:any)=>{
            if(elm.audioCallRate>0){
              elm.audioCallRate= await commonHelper.showCallAmount(elm?.audioCallRate||0,false,'VOIP')
             // elm.audioCallRate=parseFloat((elm.audioCallRate/(5*60)).toFixed(3))
            }
            if(elm.videoCallRate>0){
              elm.videoCallRate=await commonHelper.showCallAmount(elm?.videoCallRate||0,true,'VOIP')
             // elm.videoCallRate=parseFloat((elm.videoCallRate/(5*60)).toFixed(3))
            }
            var newElm:any = elm.serialize()
            var profileData = elm.serialize()
            const respo=await commonHelper.expertResponceRate(newElm?.id)
            profileData.callResponseRate=respo||0
            newElm.profile =  profileData
            // if(!newElm?.country){
            //   newElm.country={name:'',parentId:0,masterType:0,imageUrl:'',used:0}
            // }
            // if(!newElm?.state){
            //   newElm.state={name:'',parentId:0,masterType:0,imageUrl:'',used:0}
            // }
            if(!newElm.highestEducation){
              newElm.highestEducation={name:'',parentId:0,masterType:0,imageUrl:'',used:0}
            }
            if(!newElm.occupation){
              newElm.occupation={name:'',parentId:0,masterType:0,imageUrl:'',used:0}
            }
            newElm.block= false
            newElm.blocked= false
            newElm.followstatus = false

            if(newElm.profile?.hasOwnProperty('highestEducation')){
              delete newElm.profile['highestEducation']
            }
            if(newElm.profile?.hasOwnProperty('occupation')){
              delete newElm.profile['occupation']
            }
            
            newElm.genderId=parseInt(newElm.genderId)
            /** Rating And Responce Count */
            const rat = await commonHelper.userRating(newElm?.id)
            newElm.profile.rating=rat?.rating
            newElm.profile.ratingCount=rat?.count||0
            newElm.profile.callResponseRate=respo||0
            newElm.rating=rat?.rating
            newElm.ratingCount=rat?.count||0
            newElm.callResponseRate=respo||0
            
            /** Rating And Responce Count */
            return newElm

          }))

      return response.status(200).json({
              "data":experts||[],
              "logout": false,
              "resMsg": "Get experts",
              "status": true,
              "currentPage":page,
              "totalPage":lastPage,
              "prevPage":page>1?page-1:null,
              "nextPage":page<lastPage?page+1:null
      })
      
    } catch (error) {
      await WebServiceLog.create({request:JSON.stringify(data),response:JSON.stringify(error),service_name:'Get All Experts Service Api',status:1,uid:0,time_taken:0})
      return response.status(200).json({
        "errors":error,
        "logout": false,
        "resMsg": "Get experts failed",
        "status": false,
        "data":[]
      })
    }
  }

  async saveAppFcm({ request,response }: HttpContextContract){
    const data=request.all()
    const existDevice= await WithoutRegUser.query().where('deviceId',data?.deviceId).first()
    if(existDevice){
      var userData=existDevice
      userData.fcm=data?.fcm||data?.fcmToken
    }else{
      var userData= new WithoutRegUser()
      userData.deviceId=data?.deviceId
      userData.fcm=data?.fcm||data?.fcmToken
      userData.uid=0
      userData.status=false
    }
    try {
      const saveData= await userData.save()
      await WebServiceLog.create({request:JSON.stringify(data),response:JSON.stringify(saveData),service_name:'Save App Fcm Success',status:1,uid:0,time_taken:0})
      return response.status(200).json({
        "resMsg": "Data saved",
        "status": true,
        "logout": false
      })
    } catch (error) {
      await WebServiceLog.create({request:JSON.stringify(data),response:JSON.stringify(error),service_name:'Save App Fcm Error',status:1,uid:0,time_taken:0})
      return response.status(200).json({
        "resMsg": "Data not saved",
        "status": false,
        "logout": false
      })
    }
  }

  async saveSocial({request,response,auth}: HttpContextContract){
    const uid=auth.user?.id
    const data = request.all()
    var user:any = await User.find(uid)
    user.facebook=data?.facebook||''
    user.linkedin=data?.linkedin||''
    user.instagram=data?.instagram||''
    user.chanalUrl=data?.chanalUrl||''
    user.regCompleteStep=2
    try {
      await user.save()
      if(data?.chanalUrl){
        commonHelper.addYoutubeVideos(data?.chanalUrl,user?.id)
      }else{
        await YoutubeVideo.query().where('uid',user?.id).delete();
      }
      await WebServiceLog.create({request:JSON.stringify(data),response:JSON.stringify(user),service_name:'Social update Service Api success',status:1,uid:uid,time_taken:0})
      return response.status(200).json({
        "data":user,
        "logout": false,
        "resMsg": "Social update",
        "status": true
      })
    } catch (error) {
      await WebServiceLog.create({request:JSON.stringify(data),response:JSON.stringify(error),service_name:'Social update Service Api error',status:1,uid:uid,time_taken:0})
      return response.status(200).json({
        "errors":error,
        "logout": false,
        "resMsg": "Social update failed",
        "status": false
      })
    }
  }

  async getYoutubeVideos({request,response,auth}: HttpContextContract){
    const uid = auth.user?.id
    const data = request.all()
    const userId=data?.userId
    try {
      const videos = await YoutubeVideo.query().where({'uid':userId})
      await WebServiceLog.create({request:JSON.stringify(data),response:JSON.stringify(videos),service_name:'Get Youtube Videos Service Api success',status:1,uid:uid,time_taken:0})
      return response.status(200).json({
        "data":videos,
        "logout": false,
        "resMsg": "Get Youtube Videos",
        "status": false
      })
    } catch (error) {
      await WebServiceLog.create({request:JSON.stringify(data),response:JSON.stringify(error),service_name:'Get Youtube Videos Service Api error',status:1,uid:uid,time_taken:0})
      return response.status(200).json({
        "errors":error,
        "logout": false,
        "resMsg": "Get Youtube Videos",
        "status": false
      })
    }
  }


}
