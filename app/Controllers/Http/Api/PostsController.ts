import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import Application from '@ioc:Adonis/Core/Application'
import UploadHelper from "App/Helpers/UploadHelper"
const uploader:any=new UploadHelper()
import Post from 'App/Models/Post'
import LikePost from 'App/Models/LikePost'
import FollowPost from 'App/Models/FollowPost'
import WebServiceLog from 'App/Models/WebServiceLog'
import TaggedUser from 'App/Models/TaggedUser'
import Comment from 'App/Models/Comment'
import Request from 'App/Models/Request'
import BookedSlot from 'App/Models/BookedSlot'
import AppMedia from 'App/Models/AppMedia'
import LedgerHelper from "App/Helpers/LedgerHelper"
const Ledger:any=new LedgerHelper()
const perf = require('execution-time')()
import NotificationHelper from 'App/Helpers/NotificationHelper'
const notificationHelper = new NotificationHelper()
import FirebaseHelper from 'App/Helpers/FirebaseHelper'
const firebaseHelper=new FirebaseHelper()
import { DateTime } from 'luxon'
import User from 'App/Models/User'
import BlockList from 'App/Models/BlockList'
import ExpertSchedule from 'App/Models/ExpertSchedule'
import Event from '@ioc:Adonis/Core/Event'
import LedgerAccount from 'App/Models/LedgerAccount'
import CommonHelper from 'App/Helpers/CommonHelper'
import LikeComment from 'App/Models/LikeComment'
import MasterGodown from 'App/Models/MasterGodown'
import FollowUser from 'App/Models/FollowUser'
const  commonHelper = new CommonHelper()
const days:any={"1":"Sun","2":"Mon","3":"Tue","4":"Wed","5":"Thu","6":"Fri","7":"Sat"}
const monthNames = ["Jan", "Feb", "March", "April", "May", "June",
  "July", "Aug", "Sept", "Oct", "Nov", "Dec"
];
const getNextDayOfTheWeek=(fSlotTime,day, excludeToday = true, refDate = new Date())=> {
  const dayOfWeek = day-1
  const stime=fSlotTime.split(':')
  if (dayOfWeek < 0) return
  refDate.setHours(0,0,0,0)
  refDate.setDate(refDate.getDate() + +!!excludeToday + 
               (dayOfWeek + 7 - refDate.getDay() - +!!excludeToday) % 7)     
  refDate.setHours(stime[0],stime[1])
  return refDate
}

const date_suffix=(dt)=>
{
  return dt+(dt % 10 == 1 && dt != 11 ? 'st' : (dt % 10 == 2 && dt != 12 ? 'nd' : (dt % 10 == 3 && dt != 13 ? 'rd' : 'th')))
}

import SearchHelper from 'App/Helpers/SearchHelper'
import VideoComment from 'App/Models/VideoComment'
import LikeVideoComment from 'App/Models/LikeVideoComment'
const searchHelper = new SearchHelper()

export default class PostsController {

  async savePostRequest({ request,response,auth }: HttpContextContract){
    perf.start()
    const uid:any=auth.user?.id
    const data=request.all()
    
    const allMedai=request.files('media')
   // return response.send(allMedai)
   await WebServiceLog.create({request:JSON.stringify(data),response:JSON.stringify(allMedai),service_name:'Request Post Service Data Start',status:1,uid:uid,time_taken:0})
   if(data?.post){
    var postData=new Post()
      postData.createdBy=uid
      //postData.description=data.post.description.replace(/[^\x00-\x7F]/g, "")
      postData.description=data.post.description
      postData.comments=data.post.comments||0
      postData.followers=data.post.followers||0
      postData.likes=data.post.likes||0
      postData.location=data.post.location
      postData.privates=data.post.privates
      postData.views=data.post.views
      postData.tagged_user=data.userTagged?.length||0
      //var tags:any=[]

      const tagList = await commonHelper.extractHashTag(postData?.description)
      if(tagList.length>0){
        await commonHelper.matchHashTagAndSave(tagList,uid)
      }

      // if(data?.hashtags){
      //   for await (const elm of data?.hashtags) {
      //     tags.push('#'+elm.name)
      //     if(elm?.id){
      //       await MasterGodown.query().where({'id':elm?.id}).increment('used',1)
      //     }else{
      //       const newMasterGodown:any= new MasterGodown();
      //       newMasterGodown.imageUrl=elm?.imageurl||elm?.imageUrl||''
      //       newMasterGodown.masterType=6
      //       newMasterGodown.name=elm?.name.trim()
      //       newMasterGodown.createdBy=uid
      //       newMasterGodown.used=1
      //       await newMasterGodown.save()
      //     }
      //   }
      // }
      postData.hash_tags=tagList.toString()
      //postData.hash_tags=tags.toString()
      
     //return postData
    try {
      await postData.save()
      await searchHelper.addData({
        name:postData?.description,
        title:postData?.description,
        type:'post',
        dataId:postData?.id,
        userId:uid
      })
      await firebaseHelper.changeUserStatus(uid,1)
      if(allMedai){
          allMedai.forEach(async (ufile)=>{
            const fileName=`${new Date().getTime()+ufile.size}.${ufile.extname}`
            await ufile.move(Application.publicPath(),{
              name: fileName,
            })
            uploader.uploadFile(fileName,ufile.type,uid,5,postData.id,'post')
          })
      }
      if(data.userTagged){
       //var ucount=0
        data.userTagged.forEach(async(element) => {
          await TaggedUser.create({postId:postData.id,uid:element.uid})
         // ucount=ucount+1
        })
        // postData.tagged_user=ucount
        // await postData.save()
      }
      if(data?.media.length>0){
        for(const element of data?.media) {
          var mediaData={media_for:postData.id,media_type:5,url:element}
          await AppMedia.create(mediaData)
        }
      }
      return response.status(200).json({
        "post_id":postData.id,
        "resMsg": "Success",
        "status": true,
        "logout": true
      })
    }catch (error){
      const tres=perf.stop()
      data.post.description.replace(/[^\x00-\x7F]/g, "")
      await WebServiceLog.create({request:JSON.stringify(data),response:JSON.stringify(error),service_name:'Posts Post Service',status:1,uid:uid,time_taken:tres.time})
      return response.status(200).json({
          "errors":error,
          "logout": false,
          "resMsg": "error",
          "status": false
      })
    }
   }else if(data?.request){
    await WebServiceLog.create({request:JSON.stringify(data),response:JSON.stringify({}),service_name:'Request Post Service Data',status:1,uid:uid,time_taken:0})
     var requestData:any=data?.request
     requestData.requestedBy=uid
     requestData.daysId=data?.daysId
     //requestData.bookingDate=new Date()
     requestData.callCount=0
    // requestData.callingType
     var slots = data?.slots
     if(slots.length==0){
      return response.status(200).json({
        "logout": false,
        "resMsg": "Please select a time slot",
        "status": false
      })
     }
     const expertData:any = await User.find(requestData?.toUid)
     if(expertData?.verified){
      const userid=uid
      var userWallet:any=await LedgerAccount.query().where({'uid':userid,name:'User'}).first()
      var availableAmount:any=userWallet?.totalAmount-(userWallet?.disputedAmount+userWallet?.holdAmount)
      const callRate=(requestData?.videocall?expertData.videoCallRate:expertData.audioCallRate)||0
      const rate=await commonHelper.showCallAmount(callRate,requestData?.videocall,requestData?.callingType)||0
      var estimatedCost=((requestData.estimatedCall*60)*rate).toFixed(2)
        if(estimatedCost>availableAmount){
          await WebServiceLog.create({request:JSON.stringify(data),response:JSON.stringify({availableAmount,estimatedCost}),service_name:'No enough balance in user wallet from request',status:1,uid:uid,time_taken:0})
          return response.status(200).json({
            "data":{availableAmount,estimatedCost},
            "resMsg": "No enough balance in user wallet",
            "status": false,
            "logout": false
          })
        }
        requestData.estimatedCost=estimatedCost
        requestData.rate=rate
     }else{
      requestData.estimatedCost=0
      requestData.rate=0
     }
      slots= slots.map((elm:any)=>{
          elm.byUid=uid
          elm.status=1
          elm.toUid=data?.request.toUid
          return elm
      })
     // return requestData
      const fSlot=slots[0]
      const lSlot=slots[slots.length-1]
      const fSlotDetails= await ExpertSchedule.find(fSlot?.slotsId)
      const lSlotDetails=await ExpertSchedule.find(lSlot?.slotsId)
      const fSlotFromTime=fSlotDetails?.fromTime
      const lSlotToTime=lSlotDetails?.toTime
      // const bookingDate= await getNextDayOfTheWeek(fSlotFromTime,requestData?.daysId)
      // requestData.bookingDate=bookingDate
      const bookingDate=(requestData?.bookingDate||data?.bookingDate).split("-").reverse().join('-')
      requestData.bookingDate=bookingDate+' '+fSlotFromTime
      //return requestData
     try {
     //  return {slots,requestData}
      const reqData = await Request.create(requestData)
      const reqslots:any=await reqData.related('slots').createMany(slots)
      if(requestData.estimatedCost>0){
        await Ledger.holdAmountForCall({id:reqData?.id,uid:uid,amount:requestData?.estimatedCost||0})
      }
      if(data?.media.length>0){
        for(const element of data?.media) {
          var mediaData={media_for:reqData?.id,media_type:4,url:element}
          await AppMedia.create(mediaData)
        }
      }
      if(reqData?.id){
        var notifyData={fcm:data?.fcm,date:reqData?.bookingDate,toUid:reqData?.toUid,id:reqData?.id}
        var notify:any=await notificationHelper.sendNotifictionForRequest(notifyData,auth?.user)
        const bdate:any = new Date(reqData?.bookingDate.toString())
        var allData={
                expertName:expertData?.name,
                seekerName:auth?.user?.name,
                expertEmail:expertData?.email,
                seekerEmail:auth?.user?.email,
                callTime:fSlotFromTime,
                callDate:date_suffix(bdate.getDate())+' '+monthNames[bdate.getMonth()]+ ' '+bdate.getFullYear(),
                interest:'',
                callType:reqData?.videocall?'video call':'audio call',
                fullSlotDateTime:fSlotFromTime+' to '+lSlotToTime+' '+date_suffix(bdate.getDate())+' '+monthNames[bdate.getMonth()]+ ' '+bdate.getFullYear(),
                estimatedTime:reqData?.estimatedCall+' mins',
                estimatedAmount:reqData?.estimatedCost,
                agenda:reqData?.description,
                percentageSettings:'30%',
                ethicalPolicy:'click here'
              }
         // console.log(allData,bdate)
        Event.emit('new:scheduleEmail',allData)
      }
      await firebaseHelper.changeUserStatus(uid,1)
      return response.status(200).json({
       "request_id":reqData.id,
       "resMsg": "Success",
       "status": true,
       "logout": false
     })
     } catch (error) {
      const tres=perf.stop()
      await WebServiceLog.create({request:JSON.stringify(data),response:JSON.stringify(error),service_name:'Request Post Service error',status:1,uid:uid,time_taken:tres.time})
      return response.status(200).json({
          "errors":error,
          "logout": false,
          "resMsg": "error",
          "status": false
      })
     }
   }else{
      const tres=perf.stop()
      await WebServiceLog.create({request:JSON.stringify(data),response:JSON.stringify(uid),service_name:'Request Post Service With Invalide request',status:1,uid:uid,time_taken:tres.time})
      return response.status(200).json({
        "logout": false,
        "resMsg": "no data error",
        "status": false
      })
   }
  }

  async getPostRequest({ request,response,auth }: HttpContextContract){
    const uid:any=auth.user?.id
    const data=request.all()
    const type=data?.type||1
   // const status=data?.status||1
    var page:any = request.input('page', 1)
    if(page=='null' || page==null ||page=='NULL' || page==0){
      page=1
    }
    var lastPage
    const limit = 10
   if(data.post){
    var posts:any
    var postQuery=Post.query().preload('user',query=>{
      query.preload('highestEducation')
          .preload('occupation')
          .preload('currentOrg')
          .preload('collegeUniversity')
    })
    //.has('user')
    .preload('userTagged',query=>{
      query.preload('highestEducation')
      .preload('occupation')
      .preload('currentOrg')
      .preload('collegeUniversity')
     }).preload('medias').withCount('comment',(q)=>{
       q.has('user')
     }).withCount('postLikes')
      if(data?.id){
        postQuery.where('id',data.id)
      }
      if(type==3){
       postQuery.where('created_by',data.uid||uid)
      }else if(type==2){
        postQuery.whereHas('userTagged',(query)=>{
          query.where('uid',uid)
        })
      }else{
        postQuery.whereNot('created_by',uid)
      }
      
      postQuery.whereHas('user',(q)=>{
        q.where('status',1)
      }).where('status',1)
       posts = await postQuery.orderBy('id','desc').paginate(page,limit)
       lastPage = posts.lastPage
       posts=posts.rows
       if(posts){
        // posts=posts.serialize()
        var postData:any=[]
        for (const element of posts) {
            const UserLike = await LikePost.query().where({targetId:element.id,uid:uid}).first()
            if(UserLike){
              element.likestatus=true
            }else{
              element.likestatus=false
            }
            const UserFollow = await FollowPost.query().where({targetId:element.id,uid:uid}).first()
            if(UserFollow){
              element.followstatus=true
            }else{
              element.followstatus=false
            }
            if(element.$extras.comment_count){
              element.comments=element.$extras.comment_count||0
            }
            element.likes=element.$extras.postLikes_count||0

            const userfollowstatus=await FollowUser.query().where({targetId:element?.uid,uid:uid}).first()
            if(userfollowstatus){
              element.userfollowstatus=true
            }else{
              element.userfollowstatus=false
            }
            var newelement:any=element.serialize()
            if(element.medias.length>0){
              var mdImg:any=[]
              newelement.medias.forEach(elm => {
                mdImg.push(elm?.url)
              })
              newelement.medias=mdImg
            }
            if(newelement.userTagged.length>0){
              for await (const userProf of newelement?.userTagged) {
                userProf.profile = await User.find(userProf?.id)
                // var block= await BlockList.query().where({byUid:userProf?.id,toUid:uid}).first()?true:false
                // var blocked= await BlockList.query().where({byUid:uid,toUid:userProf?.id}).first()?true:false
                /** Rating And Responce Count */
                const rat = await commonHelper.userRating(userProf?.id)
                userProf.rating=rat?.rating||0
                userProf.ratingCount=rat?.count||0
              /** Rating And Responce Count */
                // userProf.block=block
                // userProf.blocked=blocked
              }
            }
            if(newelement?.hasOwnProperty('user')){
              delete newelement['user']
            }
            newelement.viewBy=[]
            postData.push(newelement)
          }
       }
      
       
    if(postData){
      return response.status(200).json({
        "postDTOS":postData,
        "requestDTOS":[],
        "logout": false,
        "resMsg": "Success",
        "status": true,
        "currentPage":page,
        "totalPage":lastPage,
        "prevPage":page>1?page-1:null,
        "nextPage":page<lastPage?page+1:null
      })
    }else{
      return response.status(200).json({
        "postDTOS":[],
        "requestDTOS":[],
        "logout": false,
        "resMsg": "NO Post",
        "status": true,
        "currentPage":page||0,
        "totalPage":lastPage||0,
        "prevPage":null,
        "nextPage":null
      })
    }
    //return response.status(200).json(posts)
   }else{
      var requestData:any = await Request.query().preload('slots',(query)=>{
        query.preload('schedule')
      }).preload('medias')
      .where((query)=>{
          query.where("toUid",uid)
        if(type==3){
          query.whereIn("status",[3,4])
        }else{
          query.where("status",type)
        }
      })
      .orWhere((query)=>{
        query.where("requestedBy",uid)
        if(type==3){
          query.whereIn("status",[3,4])
        }else{
          query.where("status",type)
        }
      })
      //.orderBy('id','desc')
      .orderBy('bookingDate','desc')
      .paginate(page,limit)

     lastPage = requestData.lastPage
     requestData=requestData.rows
      if(requestData){
        const userTimeZone = auth.user?.timeZone||'UTC'
       // requestData=requestData?.slots
      //  requestData.forEach(async(element:any) => {
        var resdata:any=[]
        for (const relement of requestData) {
          var schData:any=[]
          var element:any=relement
          element.slots.forEach(elm=>{
            if(elm?.schedule && !elm?.schedule.booked){
              elm.schedule.booked=true
            }
            schData.push(elm?.schedule)
          })
          element.expertSchedules=schData
          element.bookingDate=new Date(element.bookingDate)
          element.estimatedCall=element.estimatedCall+' mins'
          element.bookingDate=date_suffix(element.bookingDate.getDate())+' '+monthNames[element.bookingDate.getMonth()]
          //element.days=days[element?.daysId]
          if(element.requestedBy==uid){
           
            var userData={profile:await User.query().where({id:element.toUid}).first()}
             element.userProfile=userData
          }else{
            var userData={profile:await User.query().where({id:element.requestedBy}).first()}
            element.userProfile=userData
          }
          var newelement:any=element.serialize()
          if(element.medias.length>0){
            var mdImg:any=[]
            newelement.medias.forEach(elm => {
              mdImg.push(elm?.url)
            })
            newelement.medias=mdImg
            resdata.push(newelement)
          }else {
            resdata.push(element)
          }
        }
       // await firebaseHelper.changeUserStatus(uid,1)
        return response.status(200).json({
          "postDTOS":[],
          "requestDTOS":resdata,
          "logout": false,
          "resMsg": "Success",
          "status": true,
          "currentPage":page,
          "totalPage":lastPage,
          "prevPage":page>1?page-1:null,
          "nextPage":page<lastPage?page+1:null
        })
      }else{
        return response.status(200).json({
          "postDTOS":[],
          "requestDTOS":[],
          "logout": false,
          "resMsg": "NO data",
          "status": true,
          "currentPage":page||0,
          "totalPage":lastPage||0,
          "prevPage":null,
          "nextPage":null
        })
      }
   }
  }

  async postComments({ request,response,auth }: HttpContextContract){
    const uid:any=auth.user?.id
    const data=request.all()
    if(data?.commentType==1){
    var comment=new Comment()
    comment.comment=data.comment?data.comment:data.keyValue
    comment.postId=data?.postId||data.id
    comment.parentId=data?.parentId
    comment.uid=uid
    comment.likes=0
    try { 
      comment = await comment.save()
      //********************************************** */
      await notificationHelper.sendNotificationForComment(comment,auth?.user)
      //********************************************* */
      if(comment.postId){  
        Event.emit('new:sendNotificationForCommentToFollowers',{data:comment,fromUser:auth?.user})
      }
     // return true
      // if (comment.postId) {
      //   var post:any = await Post.find(data.postId)
      //   post.comments=post.comments+1
      //   await post.save()
      // }
      var allComments=await (await Comment.query().preload('user',(query)=>{
                            query.select('name','profilePic')
                          }).where('postId',comment.postId).limit(20).orderBy('id','asc'))
                          .map((comment:any)=>{
                            comment.name=comment.user.name
                            comment.profilePic=comment.user.profilePic
                            comment.likestatus=comment.likes>0?true:false
                            return comment
                          })

      return response.status(200).json({
        "block": false,
        "blocked": false,
        "comments":allComments,
        "logout": false,
        "resMsg": "Comment has been saved",
        "status": true
      })    
    } catch (error) {
      await WebServiceLog.create({request:JSON.stringify(data),response:JSON.stringify(error),service_name:'Post Comment Service',status:1,uid:uid,time_taken:1})
      return response.status(200).json({
          "errors":error,
          "logout": false,
          "resMsg": "Post comment not saved",
          "status": false
      })
    }
    }else if(data?.commentType==2){
    var vcomment=new VideoComment()
    vcomment.comment=data.comment?data.comment:data.keyValue
    vcomment.userVideoId=data?.postId||data?.id||data?.userVideoId
    vcomment.parentId=data?.parentId
    vcomment.uid=uid
   // return vcomment
    try {
      await vcomment.save()
      //********************************************** */
      await notificationHelper.sendNotificationForVideoComment(vcomment,auth?.user)
      //********************************************* */
      if(vcomment.userVideoId){
       // Event.emit('new:sendNotificationForCommentToFollowers',{data:vcomment,fromUser:auth?.user})
      }
      var allComments=await (await VideoComment.query().preload('user',(query)=>{
                            query.select('name','profilePic')
                          }).where('userVideoId',vcomment.userVideoId).limit(20).orderBy('id','asc'))
                          .map((comment:any)=>{
                            comment.name=comment.user.name
                            comment.profilePic=comment.user.profilePic
                            comment.likestatus=comment.likes>0?true:false
                            return comment
                          })

      return response.status(200).json({
        "block": false,
        "blocked": false,
        "comments":allComments,
        "logout": false,
        "resMsg": "Comment has been saved",
        "status": true
      })
    }catch (error) {
      await WebServiceLog.create({request:JSON.stringify(data),response:JSON.stringify(error),service_name:'Post Video Comment Service error',status:1,uid:uid,time_taken:1})
      return response.status(200).json({
          "errors":error,
          "logout": false,
          "resMsg": "Video comment not saved",
          "status": false
      })
    }
    }
  }

  async getCommnets({ request,response,auth }: HttpContextContract){
    const uid:any=auth.user?.id
    const data=request.all()
    const postId=data?.postId||data?.id
    if(data?.commentType==1){
      try {
        var allComments= await Promise.all((await Comment.query().preload('user',(query)=>{
                              query.select('name','profilePic')
                            }).has('user')
                            .preload('parents',(query)=>{
                              query.preload('user',(query)=>{
                                query.select('name','profilePic')
                              }).preload('parents',(query)=>{
                                query.preload('user',(query)=>{
                                  query.select('name','profilePic')
                                })
                              })
                            })
                            .where('postId',postId)
                            .whereNull('parentId')
                            .limit(20)
                            .orderBy('id','asc'))
                            .map(async(comment:any)=>{
                              const likeComment = await LikeComment.query().where({targetId:comment?.id,uid:uid}).first()
                              comment.name=comment.user.name
                              comment.profilePic=comment.user.profilePic
                              comment.likestatus=likeComment?true:false
                              return comment
                            }))
  
        return response.status(200).json({
          "block": false,
          "blocked": false,
          "comments":allComments,
          "logout": false,
          "resMsg": "all post comments",
          "status": true
        })    
      } catch (error) {
        await WebServiceLog.create({request:JSON.stringify(data),response:JSON.stringify(error),service_name:'Get Comment Service',status:1,uid:uid,time_taken:1})
        return response.status(200).json({
            "errors":error,
            "logout": false,
            "resMsg": "get post comments",
            "status": false
        })
      }
    }else if(data?.commentType==2){
      const userVideoId=data?.postId||data?.id||data?.userVideoId
      try {
        var allComments= await Promise.all((await VideoComment.query().preload('user',(query)=>{
                              query.select('name','profilePic')
                            }).has('user')
                            .preload('parents',(query)=>{
                              query.preload('user',(query)=>{
                                query.select('name','profilePic')
                              }).preload('parents',(query)=>{
                                query.preload('user',(query)=>{
                                  query.select('name','profilePic')
                                })
                              })
                            })
                            .where('userVideoId',userVideoId)
                            .limit(20).orderBy('id','asc'))
                            .map(async(comment:any)=>{
                              const likeComment = await LikeVideoComment.query().where({targetId:comment?.id,uid:uid}).first()
                              comment.name=comment.user.name
                              comment.profilePic=comment.user.profilePic
                              comment.likestatus=likeComment?true:false
                              return comment
                            }))
  
        return response.status(200).json({
          "block": false,
          "blocked": false,
          "comments":allComments,
          "logout": false,
          "resMsg": "all post comments",
          "status": true
        })    
      } catch (error) {
        await WebServiceLog.create({request:JSON.stringify(data),response:JSON.stringify(error),service_name:'Get Video Comment Service',status:1,uid:uid,time_taken:1})
        return response.status(200).json({
            "errors":error,
            "logout": false,
            "resMsg": "get post comments",
            "status": false
        })
      }
    }
   
  }

  async getPost({ request,response }: HttpContextContract){
    const data=request.all()
    var page:any = request.input('page', 1)
    if(page=='null' || page==null ||page=='NULL' || page==0){
      page=1
    }
    var lastPage
    const limit = 10
    var posts:any

    var postQuery=Post.query().preload('user',query=>{
      query.preload('highestEducation')
          .preload('occupation')
          .preload('currentOrg')
          .preload('collegeUniversity')
    })
    .has('user')
    .preload('userTagged',query=>{
      query.preload('highestEducation')
      .preload('occupation')
      .preload('currentOrg')
      .preload('collegeUniversity')
     }).preload('medias').withCount('comment',(q)=>{
       q.has('user')
     }).withCount('postLikes')

      postQuery.whereHas('user',(q)=>{
        q.where('status',1)
      }).where('status',1)
       posts = await postQuery.orderBy('id','desc').paginate(page,limit)
       lastPage = posts.lastPage
       posts=posts.rows
       if(posts){
        // posts=posts.serialize()
        var postData:any=[]
        for (const element of posts) {
            element.likestatus=false
            element.followstatus=false
            if(element.$extras.comment_count){
              element.comments=element.$extras.comment_count||0
            }
            element.likes=element.$extras.postLikes_count||0
            element.userfollowstatus=false
            var newelement:any=element.serialize()
            if(element.medias.length>0){
              var mdImg:any=[]
              newelement.medias.forEach(elm => {
                mdImg.push(elm?.url)
              })
              newelement.medias=mdImg
            }
            if(newelement.userTagged.length>0){
              for await (const userProf of newelement?.userTagged) {
                userProf.profile = await User.find(userProf?.id)
                /** Rating And Responce Count */
                const rat = await commonHelper.userRating(userProf?.id)
                userProf.rating=rat?.rating||0
                userProf.ratingCount=rat?.count||0
              /** Rating And Responce Count */

              }
            }
            if(newelement?.hasOwnProperty('user')){
              delete newelement['user']
            }
            newelement.viewBy=[]
            postData.push(newelement)
          }
       }
      
       
    if(postData){
      return response.status(200).json({
        "postDTOS":postData,
        "requestDTOS":[],
        "logout": false,
        "resMsg": "Success",
        "status": true,
        "currentPage":page,
        "totalPage":lastPage,
        "prevPage":page>1?page-1:null,
        "nextPage":page<lastPage?page+1:null
      })
    }else{
      return response.status(200).json({
        "postDTOS":[],
        "requestDTOS":[],
        "logout": false,
        "resMsg": "NO Post",
        "status": true,
        "currentPage":page||0,
        "totalPage":lastPage||0,
        "prevPage":null,
        "nextPage":null
      })
    }
  }
}
