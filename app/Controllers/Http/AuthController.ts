import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import AclGroup from 'App/Models/AclGroup'

import WebServiceLog from 'App/Models/WebServiceLog'

export default class AuthController {

  async login({ auth, request, response,session }: HttpContextContract){
    const data=request.all()
    const email = request.input('email')
    const password = request.input('password')
    try {
      await auth.use('web').attempt(email, password)
      const userGroupId = auth?.user?.userRole
     //return auth?.user?.userRole
      if(userGroupId && userGroupId!='admin'){
        const acls = await AclGroup.getGroupAcls(userGroupId)
        session.put('acls',JSON.stringify(acls))
        if(acls.indexOf('/admin/dashboard')> -1 ){
          return response.redirect('/admin/dashboard')
        }else{
          return response.redirect('/admin/user-dashboard')
        }
      }else{
        return response.redirect('/admin/dashboard')
      }  
    } catch (error) {
     // return error
      await WebServiceLog.create({request:JSON.stringify(data),response:JSON.stringify(error),service_name:'Admin Login Service',status:1,uid:0,time_taken:0})
      if (error.code === 'E_INVALID_AUTH_UID') {
        session.flash({email:email, error:'These credentials do not match in our records.'})
        return response.redirect('back')
      }
      if (error.code === 'E_INVALID_AUTH_PASSWORD') {
        session.flash({ email:email,error:'Your password is invalid.'})
        return response.redirect('back')
      }
      return response.status(201).send(error)
    }
  }

  async logout({auth,response}){
    await auth.logout()
    return response.redirect('/admin')
  }
}
