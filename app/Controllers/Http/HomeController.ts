import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import CmsPage from 'App/Models/CmsPage'
import CommonHelper from 'App/Helpers/CommonHelper'
const commonHelper = new CommonHelper()
//import AppNotification from 'App/Models/AppNotification'
import User from 'App/Models/User'
import Blog from 'App/Models/Blog'
import NewsletterSubscriber from 'App/Models/NewsletterSubscriber'
import ContactData from 'App/Models/ContactData'
import SearchHelper from 'App/Helpers/SearchHelper'
const searchHelper = new SearchHelper()
import UtilityHelper from 'App/Helpers/UtilityHelper'
const utility = new UtilityHelper()
import BlogUser from 'App/Models/BlogUser'
import BlogComment from 'App/Models/BlogComment'
import WebServiceLog from 'App/Models/WebServiceLog'
import ReferralHistory from 'App/Models/ReferralHistory'
//import MasterGodown from 'App/Models/MasterGodown'

//import NotificationHelper from 'App/Helpers/NotificationHelper'
// import WebServiceLog from 'App/Models/WebServiceLog'
// const notify = new NotificationHelper()
// import Hash from '@ioc:Adonis/Core/Hash'
// import Encryption from '@ioc:Adonis/Core/Encryption'
// const fs = require('fs')
// // const { google } = require('googleapis')
// const readline = require('readline')
// const ytch = require('yt-channel-info')

import FirebaseHelper from 'App/Helpers/FirebaseHelper'
const firebaseHelper = new FirebaseHelper()
// import ZoomHelper from 'App/Helpers/ZoomHelper'
// const zoomHelper = new ZoomHelper()
import ResumeHelper from "App/Helpers/ResumeHelper"
import LikeBlog from 'App/Models/LikeBlog'
import TaskHelper from 'App/Helpers/TaskHelper'
const task = new TaskHelper()
const resumeHelper:any=new ResumeHelper()
// const ext = (url)=>{
//   return (url = url.substr(1 + url.lastIndexOf("/")).split('?')[0]).split('#')[0].substr(url.lastIndexOf("."))
// }
// const fileName = (url)=>{
//   return (url = url.substr(1 + url.lastIndexOf("/")).split('?')[0])
// }
//import Route from '@ioc:Adonis/Core/Route'

export default class HomeController {

  async index({request,view }: HttpContextContract){
    const mobile = await commonHelper.checkMobileDevice(request.header('user-agent'))
    const title = 'Looking for an online consultation for your problems?'
    const keywords = 'online consultation, Experts advice, app based solution, consultants near you, video call with experts, fitness solution, astrology experts online, horoscope reader, become an expert, consultants in india, online solution, relationship advice, online doctor consultation, sports nutrition, weight management'
    const description='Consult 1000+ experts online and connect with them through call, chat or video call and get results instantly. Download the Seekex App today'
    if(mobile){
      return view.render('front/mobile_home',{keywords,description,title})
    }else{
      return view.render('front/home',{keywords,description,title})
    }
  }

  async expert({request,view }: HttpContextContract){
    const mobile = await commonHelper.checkMobileDevice(request.header('user-agent'))
    const title = 'Become an expert - Offer Online consultation'
    const keywords = 'online consultation, Experts advice, app based solution, consultants near you, video call with experts, fitness solution, astrology experts online, horoscope reader, become an expert, consultants in india, online solution, relationship advice, online doctor consultation, sports nutrition, weight management'
    const description='Are you an expert in your field and believe you can help others with your expertise and earn money in return? Download the Seekex app today and begin counseling'
    if(mobile){
      return view.render('front/mobile_expert',{keywords,description,title})
    }else{
      return view.render('front/expert',{keywords,description,title})
    }
  }

  async about({request,view }: HttpContextContract){
    const mobile = await commonHelper.checkMobileDevice(request.header('user-agent'))
    const title = 'Online Conusltation - App Based Solution - Chat with 1000+ experts'
    const keywords = 'online consultation, Experts advice, app based solution, consultants near you, video call with experts, fitness solution, astrology experts online, horoscope reader, become an expert, consultants in india, online solution, relationship advice, online doctor consultation, sports nutrition, weight management'
    const description='Seekex App is a solution that enables people to PRIVATELY connect with established EXPERTS (Individuals Or Organizations) from diverse fields.'
    if(mobile){
      return view.render('front/mobile_about',{keywords,description,title})
    }else{
      return view.render('front/about',{keywords,description,title})
    }
  }

  async contact({ view,request }){
    const mobile = await commonHelper.checkMobileDevice(request.header('user-agent'))
    const title = 'Looking for an online consultation for your problems? Reach Us'
    const keywords = 'online consultation, Experts advice, app based solution, consultants near you, video call with experts, fitness solution, astrology experts online, horoscope reader, become an expert, consultants in india, online solution, relationship advice, online doctor consultation, sports nutrition, weight management'
    const description='Consult 1000+ experts online and connect with them through call, chat or video call and get results instantly. Download the Seekex App today'
    if(mobile){
      return view.render('front/mobile_contact',{keywords,description,title})
    }else{
      return view.render('front/contact',{keywords,description,title})
    }
  }

  async blog({ view,request }){
    const page = request.input('page', 1)
    const limit = 20
    const mobile = await commonHelper.checkMobileDevice(request.header('user-agent'))
    const title = 'Blogs about fitness, horoscope, relationship, immigration, online consultation and many more'||''
    const keywords = 'online consultation, Experts advice, app based solution, consultants near you, video call with experts, fitness solution, astrology experts online, horoscope reader, become an expert, consultants in india, online solution, relationship advice, online doctor consultation, sports nutrition, weight management'
    const description='Connecting you with the right experts to give you the right perspective is our priority. Download the Seekex App Now'
    var blogs:any= await Blog.query().preload('user').withCount('likes').where('status',1).orderBy('id','desc').paginate(page,limit)                    
    blogs.baseUrl(request.url())
   // return blogs
    if(mobile){
      return view.render('front/blog',{blogs,keywords,description,title})
    }else{
      return view.render('front/blog',{blogs,keywords,description,title})
    }
  }
  
  async blogDetails({ view,request,params }){
    const slug=params.slug
    //return slug
    const blog:any = await Blog.query()
                      .preload('user',(query)=>{
                        query.preload('highestEducation')
                            .preload('occupation')
                            .preload('currentOrg')
                            .preload('collegeUniversity')
                      })
                      .preload('comments',(query)=>{
                        query.preload('user').preload('childComments',(q)=>{
                          q.preload('user')
                        }).whereNull('parentId')
                      }).withCount('likes')
                      .where({slug:slug}).first()
    //  return blog
    const commentCount = await BlogComment.query()
                                .where('blogId',blog?.id)
                                .pojo<{ count: number}>()
                                .count('id as count').first() 
    //  return commentCount        
    const mobile = await commonHelper.checkMobileDevice(request.header('user-agent'))
    const title = blog?.seoTitle||'Blogs about fitness, horoscope, relationship, immigration, online consultation and many more'
    const keywords = blog?.seoKeywords||'online consultation, Experts advice, app based solution, consultants near you, video call with experts, fitness solution, astrology experts online, horoscope reader, become an expert, consultants in india, online solution, relationship advice, online doctor consultation, sports nutrition, weight management'
    const description=blog?.seoDescription||'Connecting you with the right experts to give you the right perspective is our priority. Download the Seekex App Now'
    if(mobile){
      return view.render('front/blog_details',{blog,keywords,description,title,commentCount})
    }else{
      return view.render('front/blog_details',{blog,keywords,description,title,commentCount})
    }
   // return blog
  }

  async saveBlogComment({request,params,response,session }){
    const id=params.id
    const data = request.all()
    const email=data?.email
    const name = data?.username
    const message = data?.message
    var user = await BlogUser.findBy("email",email)
    if(!user){
      user = await BlogUser.create({name:name,email:email})

      const profPic:any = await commonHelper.genTextImageForProfileNew(name,user)
        if(profPic?.Location){
          user.profilePic=profPic?.Location
          await user.save()
        }
    }
    try {
      const blogComment = new BlogComment()
      blogComment.comment=message
      blogComment.blogId=id
      blogComment.parentId=data?.parentId
      blogComment.uid=user?.id
      await blogComment.save()
      session.flash({comment:'Thank you for posting your comment.'})
      return response.redirect('back')
    } catch (error) {
      await WebServiceLog.create({request:JSON.stringify(data),response:JSON.stringify(error),service_name:'Blog comment Service Error',status:1,uid:user?.id,time_taken:0})
      session.flash({comment:'Your comment is not posted, please try again.'})
      return response.redirect('back')
    }
  }

  async likeBlog({request,params,response,auth}){
      const data = request.all()
      const uid = auth?.user?.id||0
      if(data?.id){
        await LikeBlog.create({blogId:data?.id,uid:uid})
      }
  }

  async cms({ view,params }){
    const slug=params.slug
    const data:any= await CmsPage.query().where({slug:slug}).first()
    const title =''
    const keywords = ''
    const description=''
    return view.render('front/page',{data,keywords,description,title})
  }

  async newsletter({request,session,response}: HttpContextContract){
    const data = request.all()
    const existData = await NewsletterSubscriber.query().where('email',data?.email).first()
    if(existData){
      existData.email=data?.email
      await existData.save()
    }else{
      const newsletter = new NewsletterSubscriber()
      newsletter.email=data?.email
      newsletter.status =1
      await newsletter.save()
    }
    session.flash({newsletter:'Thank you, your email subscribe for newsletter.'})
    return response.redirect('back') 
  }

  async saveContact({request,session,response,auth}: HttpContextContract){
    const data = request.all()
    const contact = new ContactData()
    contact.name=data?.name||''
    contact.email=data?.email||''
    contact.subject=data?.subject||''
    contact.message=data?.message||''
    contact.userId=auth.user?.id||0
    await contact.save()
    //session.flash({contactform:'Thank you for submit your query.,'})
    return response.redirect('/thankyou') 
  }

  async expertProfile({ view,params }){
    const id:any=params.id
   //const nd=Encryption.encrypt(1)
   // const uid:any = Encryption.decrypt(id)
    var b = Buffer.from(id, 'base64')
    var uid = b.toString()

   // return {nd,uid}
    //const data:any= await User.query().where({remember_me_token:id}).first()
      var user:any= await User.query()
                .where('id',uid).first()

      var userAll:any= await User.query()
                .preload('accomplishmentsList')
                .preload('certificatesList')
                .preload('projectList')
                .preload('country')
                .preload('state')
                .preload('highestEducation')
                .preload('occupation')
                .preload('currentOrg')
                .preload('collegeUniversity')
                .preload('skillsList')
                .preload('intExpertiseList')
                .where('id',user?.id).first()
                
     // return userAll
      /** Rating And Responce Count */
      const rat= await commonHelper.userRating(user?.id)
      const respo=await commonHelper.expertResponceRate(user?.id)
      user.rating=rat?.rating
      user.ratingCount=rat?.count||0
      user.callResponseRate=respo||0
      user.userAudioCallRate=user?.audioCallRate
      user.userVideoCallRate=user?.videoCallRate
      if(user.audioCallRate>0){
        user.audioCallRate= await commonHelper.showCallAmount(user?.audioCallRate,false,'VOIP')
      }
      if(user.videoCallRate>0){
        user.videoCallRate=await commonHelper.showCallAmount(user?.videoCallRate,true,'VOIP')
      }
      user.conversations=await commonHelper.userConversations(user?.id)||0
       /** Rating And Responce Count */

      if(!userAll?.country){
        userAll.country={id:0,name:'',parentId:0,masterType:0,imageUrl:''}
      }else{
        userAll.country={
          id:userAll.country?.countryStateId||0,
          name:userAll.country?.name||'',
          parentId:userAll.country?.parentId||0,
          masterType:userAll.country?.masterType||0,
          imageUrl:userAll.country?.imageUrl||'',
          used: userAll.country?.used||0,
        }
      }
      if(!userAll?.state){
        userAll.state={id:0,name:'',parentId:0,masterType:0,imageUrl:''}
      }else{
        userAll.state={
          id:userAll.state?.countryStateId||0,
          name:userAll.state?.name||'',
          parentId:userAll.state?.parentId||0,
          masterType:userAll.state?.masterType||0,
          imageUrl:userAll.state?.imageUrl||'',
          used: userAll.state?.used||0,
        }
      }
      if(userAll.intExpertiseList){
        userAll.intExpertiseList.map(elm=>{
          elm.id=elm.masterGodownId
          return elm
        })
      }
      if(userAll.skillsList){
        userAll.skillsList.map(elm=>{
          elm.id=elm.masterGodownId
          return elm
        })
      }
      if(userAll.highestEducation){
        userAll.highestEducation.id=userAll.highestEducation.masterGodownId
      }
      if(userAll.occupation){
        userAll.occupation.id=userAll.occupation.masterGodownId
      }
      if(userAll?.currentOrg){
        userAll.currentOrg.id=userAll?.currentOrg?.masterGodownId
      }
      var newAcc:any=[]
      if(userAll.accomplishmentsList){
       for (const accElement of userAll.accomplishmentsList) {
        let mdImg:any=[]
          var newAccElement:any=accElement.serialize()
          newAccElement.medias.forEach(elm => {
            mdImg.push(elm?.url) 
          })
          newAccElement.medias=mdImg
          newAccElement.media=mdImg
          newAcc.push(newAccElement) 
       }
      }
      var newCer:any=[]
      if(userAll.certificatesList){ 
        for (const accElement of userAll.certificatesList) {
          let mdImg:any=[]
           var newCerElement:any=accElement.serialize()
           newCerElement.medias.forEach(elm => {
             mdImg.push(elm?.url) 
           })
           newCerElement.medias=mdImg
           newCerElement.media=mdImg
           newCer.push(newCerElement) 
        }
       }
       var newPro:any=[]
      if(userAll.projectList){
        for (const accElement of userAll.projectList) {
          let mdImg:any=[]
           var newProElement:any=accElement.serialize()
           newProElement.medias.forEach(elm => {
             mdImg.push(elm?.url) 
           })
           newProElement.medias=mdImg
           newProElement.media=mdImg
           newPro.push(newProElement) 
        }
       }
      var blocked= false
      var block= false
      var followStatus = false
      var userdata:any = {
                  accomplishmentsList:newAcc,
                  certificatesList:newCer,
                  projectList:newPro,
                  profile:user,
                  country:userAll?.country,
                  state:userAll?.state,
                  highestEducation:userAll?.highestEducation,
                  intExpertiseList:userAll?.intExpertiseList,
                  occupation:userAll?.occupation,
                  skillsList:userAll?.skillsList,
                  currentOrg:userAll?.currentOrg,
                  viewBy:[],
                  followstatus:followStatus,
                  block: block,
                  blocked: blocked,
                  rating:rat?.rating,
                  ratingCount:rat?.count||0,
                  callResponseRate:respo||0
              }
    //return userdata
    const title =''
    const keywords = ''
    const description=''
    return view.render('front/expert_profile',{userdata,title,keywords,description})
  }



  async check({request}: HttpContextContract){
      // const st = '15:15:00'
      // const et = '15:15:13'
      // const stT = st.split(':')
      // const etT = et.split(':')
      // var ct = new Date()
      // var stime = ct.setUTCHours(parseInt(stT[0]),parseInt(stT[1]),parseInt(stT[2]))
      // var etime = ct.setUTCHours(parseInt(etT[0]),parseInt(etT[1]),parseInt(etT[2]))
      // const dif = (etime-stime)/1000
      // if(dif>10){
      //   const rt = (dif-10)
      //   return rt-1
      // }else{
      //   return dif+1
      // }

    // const meetConfig = {
    //   "topic": "TOPIC 1",
    //   "type": "2",
    //   "duration": "30",
    //   "start_time": new Date(),
    //   "timezone": "Asia/Kolkata",
    //   "password": "123456",
    //   //"agenda": "AGENDA"
    //   }
  //  return zoomHelper.getToken()
    //return zoomHelper.validateAuth()
    //return zoomHelper.createMeeting(meetConfig)
   // return await resumeHelper.parseResume('1601902881697.pdf','.pdf')
    // const url ='https://www.youtube.com/c/UCJJ72ZXSDbom7oVkvIkp9Tw'
    // var b = Buffer.from('1')
    // var s = b.toString('base64')
    // return s
  //   const OAuth2 = google.auth.OAuth2
  //  // return OAuth2
  //   const SCOPES = [
  //     'https://www.googleapis.com/auth/youtube.upload',
  //     'https://www.googleapis.com/auth/youtube.readonly'
  //   ]
  //   const TOKEN_DIR = (process.env.HOME || process.env.HOMEPATH || process.env.USERPROFILE) +'/.credentials/'
  //   return TOKEN_DIR
    //  const chId = await commonHelper.chanalIdByUrl(url)
    //  return chId
    // const sortBy = 'popular'
   
  //  return commonHelper.addYoutubeVideos(url,1)
    //const url = ('https://drive.google.com/file/d/1XYs_bTRnJvMctqJUdBwr4bMl6vxHicqj/view?usp=drivesdk').split("?")[0]
     // const url = 'https://image.shutterstock.com/image-photo/sunset-coast-lake-nature-landscape-600w-1960131820.jpg'
    //return url
    // const https = require('https')
    // https.get(url,function(resp){
    //       console.log(resp.statusCode);
    //       console.log(resp.headers['content-type']);
    //       resp.pipe(fs.createWriteStream("./public/test.jpg"));
    //   });
    //   return
  //  return await commonHelper.imageCropAndUploadByUrl(url)
          // var reg = /^[0-9]{1,10}$/
          // var checking = reg.test('1882204776')
          // return checking
  //return firebaseHelper.createEvent('test')
    // const data=request.all()
    // const text= data?.name
    // const ch = new Promise( function(resolve,reject) {
    //     ytch.getChannelInfo(chId).then((response) => {
    //           console.log(response)
    //           resolve(response)
    //       }).catch((err) => {
    //           console.log(err)
    //           reject(err)
    //       })
    //   })
    //  // return ch

    //   const chVideos = new Promise( function(resolve,reject) {
    //     ytch.getChannelVideos(chId, sortBy).then((response) => {
    //       console.log('Videos',response)
    //       resolve(response)
    //     }).catch((err) => {
    //       console.log(err)
    //       reject(err)
    //     })
    //   })
    // return chVideos
    //return MasterGodown.findType('Hashtag')
    // const users = await User.all()
    // if(users){
    //   for await (const user of users) {
    //     if(user?.audioCallRate>0){
    //       user.audioCallRatePerSec=await commonHelper.showCallAmount(user?.audioCallRate,false,'VOIP')||0
    //     }
    //     if(user?.videoCallRate>0){
    //       user.videoCallRatePerSec=await commonHelper.showCallAmount(user?.videoCallRate,true,'VOIP')||0
    //     }
    //     await user.save()
    //   }
    // }
   // return await commonHelper.genTextImageForProfileNew(text,'')
   // return await AppNotification.query().select('uid').where('status',0).whereNot('type',2).whereNot('type',4).whereNot('type',1).groupBy('uid').count('* as count')
    //return await WebServiceLog.deleteOldLog()
  // return notify.sendNotificationToIncompleteExpert()

    //return commonHelper.sendEmailOtp('pk.maurya22@gmail.com')

    // return searchHelper.addData({
    //     name:'Post 1',
    //     title: 'Post 1',
    //     type: 'post',
    //     data_id:1,
    //     user_id:2,
    //     min_call_rate:0,
    //     max_call_rate:0
    // })
   // return searchHelper.clear()
    // const allUsers = await User.query()
    //                   .select('id')
    //                   .where('expert',true)
     //var allList:any=new Array()
    // for await (const item of allUsers) {
    //   //  const resText = await resumeHelper.parseResumeFromUrl(item?.resume)
    //   //  item.resumeText = resText
    //   // await item.save()
    //  // allList.push(await searchHelper.expertData(item?.id))
    //  await searchHelper.addExpertData(await searchHelper.expertData(item?.id))
    // }
    //  if(allList.length>0){
    //   //await searchHelper.addExpertsListData(allList)
    //   }
    //return task.transferUserReferalAmount();
    //return firebaseHelper.updateCampaignActions(68,35,"home")
    return firebaseHelper.updateCampaignNotificationStatus(81,307,1)

   // return ReferralHistory.referalCodeUsedCount('LZT04E')
   // return allList
   // return searchHelper.expertData(1)
  // return utility.restartPm2Server()

  //return resumeHelper.parseResumeFromUrl('https://seekex.s3.ap-south-1.amazonaws.com/resume/Amrita_Resume.pdf')
  }

}
