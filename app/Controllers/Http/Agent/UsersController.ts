import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import AclGroup from 'App/Models/AclGroup'
import Agent from 'App/Models/Agent'

export default class UsersController {

  public async index({view,request,auth}:HttpContextContract){
    //return auth?.user
    return view.render('agent/dashboard')
  }
  public async login({view,request,auth}:HttpContextContract){
    // const email='agent@agent.com'
    // const password='hello@123'
    // return await auth.use('agent').attempt(email, password)
    return view.render('agent/login')
  }

  public async agentLogin({view,request,auth,response,session}:HttpContextContract){
    const email = request.input('email')
    const password = request.input('password')
    try {
      await auth.use('web').attempt(email, password)
      const userGroupId = auth?.user?.userRole
     //return auth?.user?.userRole
      if(userGroupId){
        const acls = await AclGroup.getGroupAcls(userGroupId)
        session.put('acls',JSON.stringify(acls))
        if(acls.indexOf('/admin/dashboard')>-1){
          return response.redirect('/admin/dashboard')
        }else{
          return response.redirect('/admin/user-dashboard')
        }
      }else{
        return response.redirect('/admin/dashboard')
      } 
     // return response.redirect('/agent/dashboard')
    } catch (error) {
      if (error.code === 'E_INVALID_AUTH_UID') {
        session.flash({email:email, error:'These credentials do not match in our records.'})
        return response.redirect('back')
      }
      if (error.code === 'E_INVALID_AUTH_PASSWORD') {
        session.flash({ email:email,error:'Your password is invalid.'})
        return response.redirect('back')
      }
    }
  }

  async logout({auth,response}){
    await auth.logout()
    return response.redirect('/agent')
  }
  
}
