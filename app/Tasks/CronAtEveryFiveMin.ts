//import { BaseTask } from 'adonis5-scheduler/build'
const schedule = require('node-schedule')
import TwilioHelper from 'App/Helpers/TwilioHelper'
const twilioHelper = new TwilioHelper()

// export default class CronAtEveryFiveMin extends BaseTask {
// 	public static get schedule() {
// 		//return '0 0/10 * * * *'
// 		return '*/5 * * * *'
// 	}
// 	/**
// 	 * Set enable use .lock file for block run retry task
// 	 * Lock file save to `build/tmpTaskLock`
// 	 */
// 	public static get useLock() {
// 		return false
// 	}

// 	public async handle() {
// 	//	await twilioHelper.retryUpdateCallDetails()
//    	console.info('Task complited For Every 5 minuts ', new Date())
//   }
// }
export default schedule.scheduleJob('*/5 * * * *', async() => {
		// await twilioHelper.retryUpdateCallDetails()
		console.info('Task complited For Every 5 minuts ', new Date())
})
