// import { BaseTask } from 'adonis5-scheduler/build'
const schedule = require('node-schedule')
//import { scheduledJobs } form 'node-schedule'
import TaskHelper from 'App/Helpers/TaskHelper'
const taskHelper = new TaskHelper()
import FirebaseHelper from 'App/Helpers/FirebaseHelper'
import CronLogs from 'App/Models/CronLogs'
const firebaseHelper = new FirebaseHelper()
const perf = require('execution-time')()

// export default class UserWalletUpdate extends BaseTask {
// 	public static get schedule() {
// 		return '0 0/10 * * * *'
// 		//return '*/5 * * * *'
// 	}
// 	/**
// 	 * Set enable use .lock file for block run retry task
// 	 * Lock file save to `build/tmpTaskLock`
// 	 */
// 	public static get useLock() {
// 		return false
// 	}

// 	public async handle() {
// 		perf.start()
// 		await taskHelper.callHoldAmountTransferToAvailable()
// 		await taskHelper.updateResponseRate()
// 		await taskHelper.moveUnAcceptedRequests()
// 		await firebaseHelper.deleteCallRooms()
// 		await firebaseHelper.oflineUserStatus()
// 		await firebaseHelper.oflineUserStatusByCall()
// 		const time = perf.stop()
// 	//	await firebaseHelper.updateUnreadNotifications()
// 		//await taskHelper.freeImageApiForInterest()
// 		await CronLogs.saveLog({
// 			cronName:'callHoldAmountTransferToAvailable,updateResponseRate,moveUnAcceptedRequests,deleteCallRooms,oflineUserStatus,oflineUserStatusByCall',
// 			data:'',
// 			type:'',
// 			status:'success',
// 			takenTime:time?.time||0
// 		})
//    	console.info('Task complited', new Date())
//   }
// }

export default schedule.scheduleJob('0 0/10 * * * *', async() => {
  		perf.start()
			await taskHelper.callHoldAmountTransferToAvailable()
			await taskHelper.updateResponseRate()
			await taskHelper.moveUnAcceptedRequests()
			await firebaseHelper.deleteCallRooms()
			await firebaseHelper.oflineUserStatus()
			await firebaseHelper.oflineUserStatusByCall()
			const time = perf.stop()
			//	await firebaseHelper.updateUnreadNotifications()
			//await taskHelper.freeImageApiForInterest()
			await CronLogs.saveLog({
				cronName:'callHoldAmountTransferToAvailable,updateResponseRate,moveUnAcceptedRequests,deleteCallRooms,oflineUserStatus,oflineUserStatusByCall',
				data:'',
				type:'',
				status:'success',
				takenTime:time?.time||0
			})
			console.info('Task complited', new Date())
})
