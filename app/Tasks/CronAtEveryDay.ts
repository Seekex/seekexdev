const schedule = require('node-schedule')
import TaskHelper from 'App/Helpers/TaskHelper'
const taskHelper = new TaskHelper()
import FirebaseHelper from 'App/Helpers/FirebaseHelper'
import CronLogs from 'App/Models/CronLogs'
import WebServiceLog from 'App/Models/WebServiceLog'
const firebaseHelper = new FirebaseHelper()
const perf = require('execution-time')()

export default schedule.scheduleJob('0 0 */24 * * *', async() => {
		perf.start()
		const time = perf.stop()
		const ret = await WebServiceLog.deleteOldLog()
		const oldCronLog = await CronLogs.deleteOldLog()
		await CronLogs.saveLog({
			cronName:'Delete old Web Log',
			data:JSON.stringify({ret,oldCronLog}),
			type:'',
			status:'success',
			takenTime:time?.time||0
		})
	const allUsers = await taskHelper.transferUserReferalAmount()
	await CronLogs.saveLog({
		cronName:'Transfer User Referal Amount',
		data:JSON.stringify(allUsers),
		type:'',
		status:'success',
		takenTime:time?.time||0
	})
})
