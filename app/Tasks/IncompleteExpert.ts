//import { BaseTask } from 'adonis5-scheduler/build'
const schedule = require('node-schedule')
import NotificationHelper from 'App/Helpers/NotificationHelper'
import CronLogs from 'App/Models/CronLogs'
const notification = new NotificationHelper()
const perf = require('execution-time')()
// export default class IncompleteExpert extends BaseTask {
// 	public static get schedule() {
// 		return '0 0 */12 * * *'
// 	}
// 	/**
// 	 * Set enable use .lock file for block run retry task
// 	 * Lock file save to `build/tmpTaskLock`
// 	 */
// 	public static get useLock() {
// 		return false
// 	}

// 	public async handle() {
// 		perf.start()
// 		const allUser = await notification.sendNotificationToIncompleteExpert()
// 		const time = perf.stop()
// 		await CronLogs.saveLog({
// 			cronName:'Send notification to Incomplete Expert For Every 12 Hr',
// 			data:JSON.stringify(allUser),
// 			type:'',
// 			status:'success',
// 			takenTime:time?.time||0
// 		})
//    	console.info('Incomplete Expert For Every 12 Hr', new Date())
//   }
// }

export default schedule.scheduleJob('0 0 */12 * * *', async() => {
		perf.start()
		const allUser = await notification.sendNotificationToIncompleteExpert()
		const time = perf.stop()
		await CronLogs.saveLog({
			cronName:'Send notification to Incomplete Expert For Every 12 Hr',
			data:JSON.stringify(allUser),
			type:'',
			status:'success',
			takenTime:time?.time||0
		})
	console.info('Incomplete Expert For Every 12 Hr', new Date())
})
