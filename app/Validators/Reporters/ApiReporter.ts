
import {
  MessagesBagContract,
  ErrorReporterContract,
} from '@ioc:Adonis/Core/Validator'

const ValidationException_1 = require("@adonisjs/validator/build/src/ValidationException");

type ErrorNode = {
  message: string,
  field: string,
}

export class ApiReporter implements ErrorReporterContract<{ errors: ErrorNode[] }> {
  public hasErrors = false

  /**
   * Tracking reported errors
   */
  private errors: ErrorNode[] = []
 // public errors = []
  constructor (
    private messages: MessagesBagContract,
    private bail: boolean,
  ) {
  }

  /**
   * Invoked by the validation rules to
   * report the error
   */
  public report (
    pointer: string,
    rule: string,
    message: string,
    arrayExpressionPointer?: string,
    args?: any
  ) {
    /**
     * Turn on the flag
     */
    this.hasErrors = true

    /**
     * Use messages bag to get the error message
     */
    const errorMessage = this.messages.get(
      pointer,
      rule,
      message,
      arrayExpressionPointer,
      args,
    )

    /**
     * Track error message
     */
    this.errors.push({ message: errorMessage, field: pointer })
     // this.errors[pointer]=errorMessage
    /**
     * Bail mode means, stop validation on the first
     * error itself
     */
    if (this.bail) {
      
     // throw this.toError()
    }
  }

  /**
   * Converts validation failures to an exception
   */
  public toError () {
    throw new ValidationException_1.ValidationException(false, this.toJSON())
  }

  /**
   * Get error messages as JSON
   */
  public toJSON () {
    return {
      errors: this.errors,
    }
  }
}