import { DateTime } from 'luxon'
import { BaseModel, column, afterSave, afterDelete} from '@ioc:Adonis/Lucid/Orm'

export default class LikeBlog extends BaseModel {
  @column({ isPrimary: true })
  public id: number
  @column()
  public uid: number
  @column({serializeAs:'blogId'})
  public blogId: number
  @column.dateTime({ autoCreate: true })
  public created: DateTime

}
