import { DateTime } from 'luxon'
import { BaseModel, column,belongsTo,BelongsTo,afterSave,afterFind } from '@ioc:Adonis/Lucid/Orm'
import User from './User'
import SnapshotUserWallet from './SnapshotUserWallet'
import Database from '@ioc:Adonis/Lucid/Database'
import TransactionHistory from 'App/Models/TransactionHistory'
export default class LedgerAccount extends BaseModel {
  @column({ isPrimary: true })
  public id: number

  @column({serializeAs: 'availableAmount',serialize:(value,newValu,data:LedgerAccount)=>{
      return data?.totalAmount-(data.holdAmount+data.disputedAmount)
  }})
  public availableAmount : number

  @column({serializeAs: 'holdAmount'})
  public holdAmount : number
  @column({serializeAs: 'disputedAmount'})
  public disputedAmount : number
  @column()
  public isuser : boolean
  @column()
  public name : string
  @column({serializeAs: 'promoAmount'})
  public promoAmount : number
  @column({serializeAs: 'totalAmount'})
  public totalAmount : number

  @column()
  public uid: number
  @column.dateTime({ autoCreate: true })
  public created: DateTime

  @belongsTo(() => User,{
    localKey: 'id',
    foreignKey: 'uid',
  })
  public user: BelongsTo<typeof User>

  // @afterFind()
  // public static async beforeWallet (data: LedgerAccount) {
  //   const suw= new SnapshotUserWallet()
  //   suw.uid=data?.uid
  //   suw.walletId=data?.id
  //   suw.availableAmount=data?.availableAmount
  //   suw.holdAmount=data?.holdAmount
  //   suw.disputedAmount=data?.disputedAmount
  //   suw.totalAmount=data?.totalAmount
  //   suw.isuser=data?.isuser
  //   await suw.save()
  // }

  @afterSave()
  public static async afterWallet (data: LedgerAccount) {
    const suw= new SnapshotUserWallet()
    suw.uid=data?.uid
    suw.walletId=data?.id
    suw.availableAmount=data?.totalAmount-(data.holdAmount+data.disputedAmount)||0
    suw.holdAmount=data?.holdAmount||0
    suw.disputedAmount=data?.disputedAmount||0
    suw.promoAmount=data?.promoAmount||0
    suw.totalAmount=data?.totalAmount||0
    suw.isuser=data?.isuser
    await suw.save()
  }

  public static async addPromoAmount(data){
    if(!data.description.trim()){
      data.description=data.amount+' Added by admin for campaign promotion. campaignId => '+data?.campaignId
    }
    var userWallet:any=  await this.query().where({'uid':data?.uid,name:'User'}).first()
    const promotionWallet:any = await this.query().where({name:'Promotion'}).first()
    if(userWallet){
      const trns_id= await Database.transaction(async (trx) => {
        const addToUserWallet=new TransactionHistory()
        addToUserWallet.callId=0
        addToUserWallet.description=data.amount + ' Add to user wallet ' + data.description
        addToUserWallet.fromWallet=promotionWallet?.id
        addToUserWallet.toWallet=userWallet?.id
        addToUserWallet.isdebit=false
        addToUserWallet.status=1
        addToUserWallet.transactionAmount=data.amount
        addToUserWallet.transactionType=9
        addToUserWallet.transactionWalletId=userWallet?.id
        addToUserWallet.transactionTo='User'
        addToUserWallet.transactionAction='plus'
        addToUserWallet.uid=data?.uid
        addToUserWallet.payout=false
        addToUserWallet.useTransaction(trx)
        await addToUserWallet.save()
        userWallet.promoAmount=parseFloat(data?.amount)>0?(userWallet?.promoAmount+parseFloat(data?.amount)):userWallet?.promoAmount
        userWallet.totalAmount=parseFloat(data?.amount)>0?(userWallet?.totalAmount+parseFloat(data?.amount)):parseFloat(data.amount)
      //  console.log('userWallet',userWallet,data)
       userWallet.useTransaction(trx)
       await userWallet.save()
  
        const deductedToPromotionWallet=new TransactionHistory()
        deductedToPromotionWallet.callId=0
        deductedToPromotionWallet.description=data.amount+' Amount deducted by admin and add to user for promotion userId=> '+data?.uid
        deductedToPromotionWallet.fromWallet=promotionWallet?.id
        deductedToPromotionWallet.toWallet=userWallet?.id
        deductedToPromotionWallet.isdebit=true
        deductedToPromotionWallet.status=1
        deductedToPromotionWallet.transactionAmount=data.amount
        deductedToPromotionWallet.transactionType=9
        deductedToPromotionWallet.transactionWalletId=promotionWallet?.id
        deductedToPromotionWallet.transactionTo='User'
        deductedToPromotionWallet.transactionAction='minus'
        deductedToPromotionWallet.uid=promotionWallet?.uid
        deductedToPromotionWallet.payout=false
        deductedToPromotionWallet.useTransaction(trx)
        await deductedToPromotionWallet.save()
        promotionWallet.availableAmount=promotionWallet.availableAmount-parseFloat(data.amount)
        promotionWallet.totalAmount=promotionWallet.totalAmount-parseFloat(data.amount)
        promotionWallet.useTransaction(trx)
        await promotionWallet.save()
        return addToUserWallet?.id
      })
      return trns_id
    }
   
  }

}
