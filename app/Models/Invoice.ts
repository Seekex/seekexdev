import { DateTime } from 'luxon'
import { BaseModel, column } from '@ioc:Adonis/Lucid/Orm'

export default class Invoice extends BaseModel {
  @column({ isPrimary: true })
  public id: number
  @column({serializeAs:'callId'})
  public callId: number
  @column({serializeAs:'transactionId'})
  public transactionId: number
  @column({serializeAs:'commissionAmount'})
  public commissionAmount : number
  @column({serializeAs:'commissionId'})
  public commissionId : number
  @column({serializeAs:'finalAmount'})
  public finalAmount : number
  @column({serializeAs:'gstAmount'})
  public gstAmount : number
  @column({serializeAs:'gstCommissionAmount'})
  public gstCommissionAmount  : number

  @column({serializeAs:'otherChargeValue'})
  public otherChargeValue:number
  
  @column({serializeAs:'invoiceDate '})
  public invoiceDate  : DateTime
  @column()
  public isigst : boolean
  @column({serializeAs:'salesAmount'})
  public salesAmount  : number
  @column({serializeAs:'totalAmount'})
  public totalAmount  : number

  @column.dateTime({ autoCreate: true })
  public created: DateTime

}
