import { DateTime } from 'luxon'
import { BaseModel, column } from '@ioc:Adonis/Lucid/Orm'

export default class UserBusyStatus extends BaseModel {
  public static table = 'user_busy_status'
  @column({ isPrimary: true })
  public id: number
  @column()
  public uid: number
  @column.dateTime({ autoCreate: true, serializeAs:'timeStamp' })
  public timeStamp: DateTime

}
