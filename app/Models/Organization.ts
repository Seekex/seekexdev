import { DateTime } from 'luxon'
import { BaseModel, column,scope,belongsTo,BelongsTo,computed } from '@ioc:Adonis/Lucid/Orm'
import User from './User'
const types={1:'Company',2:'College/University'}
export default class Organization extends BaseModel {
  @column({ isPrimary: true })
  public id: number
  @column()
  public name: string
  @column()
  public used: number
  @column.dateTime({ autoCreate: true })
  public created: DateTime
  @column({serializeAs:'parentId'})
  public parentId: number
  @column({serializeAs:'type'})
  public type: number
  @column({serializeAs:'createdBy'})
  public created_by: number

  @column({serializeAs:'imageUrl'})
  public image_url : string
  
  @column({serializeAs:'masterType'})
  public master_type : number
  @column({serializeAs:'isVerified'})
  public isVerified : boolean

  @computed()
  public get orgType(){
    return types[this.type]
  }
  public static isVerified = scope((query:any) => {
    query.where('isVerified',true)
  })


  @belongsTo(() => User,{
    localKey: 'id',
    foreignKey: 'created_by'
  })
  public user: BelongsTo<typeof User>

  @belongsTo(() => Organization,{
    localKey: 'id',
    foreignKey: 'parentId',
  })
  public parent: BelongsTo<typeof Organization>

  public static async getIDByName(name){
    const tagRef= await this.query().where('name','LIKE',"%"+name+"%").first()
    return tagRef?.id
  }
  public static async getCompanies(){
    const tagRef= await (await this.query().where('parentId',0).where('type',1).apply((scopes) => scopes.isVerified())).map(elm=>{
      return elm?.name
    })
    return tagRef
  }

  public static async getColleges(){
    const tagRef= await (await this.query().where('parentId',0).where('type',2).apply((scopes) => scopes.isVerified())).map(elm=>{
      return elm?.name
    })
    return tagRef
  }
  
}
