import { DateTime } from 'luxon'
import { BaseModel, column } from '@ioc:Adonis/Lucid/Orm'

export default class SeekerCallMap extends BaseModel {
  @column({ isPrimary: true })
  public id: number
  @column({serializeAs:'fromNo'})
  public fromNo: string
  @column({serializeAs:'toNo'})
  public toNo: string
  @column({serializeAs:'estimatedDuration'})
  public estimatedDuration: string
  @column({serializeAs:'callId'})
  public callId: string
  @column({serializeAs:'flowId'})
  public flowId: string
  @column()
  public status:number

  @column()
  public message:string

  @column.dateTime({ autoCreate: true })
  public created: DateTime

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updated: DateTime

  @column({serializeAs:'exotelNo'})
  public exotelNo: string
  @column({serializeAs:'exotelNoCircle'})
  public exotelNoCircle: string
}
