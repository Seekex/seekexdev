import { DateTime } from 'luxon'
import { BaseModel, column } from '@ioc:Adonis/Lucid/Orm'

export default class BlockList extends BaseModel {
  @column({ isPrimary: true })
  public id: number
  @column({serializeAs:'byUid'})
  public byUid: number
  @column({serializeAs:'toUid'})
  public toUid: number
  @column()
  public reasonId: number
  @column()
  public reason: string
  @column.dateTime({ autoCreate: true })
  public created: DateTime
  
}
