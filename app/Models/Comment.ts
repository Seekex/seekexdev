import { DateTime } from 'luxon'
import { BaseModel, column,belongsTo, BelongsTo,afterSave,afterDelete,hasMany,HasMany} from '@ioc:Adonis/Lucid/Orm'
import User from 'App/Models/User'
import Post from 'App/Models/Post'
export default class Comment extends BaseModel {
  @column({ isPrimary: true })
  public id: number
  @column()
  public parentId: number

  @column()
  public comment: string
  @column()
  public likes: number
  @column({serializeAs:'postId'})
  public postId: number
  @column()
  public uid: number
  @column()
  public likestatus: boolean
  @column()
  public name: string
  @column({serializeAs:'profilePic'})
  public profilePic: string
  @column.dateTime({ autoCreate: true })
  public created: DateTime

  /**********************Code for increase post comments [Start]**********************/
  // @afterSave()
  // public static async increasePostLike (data: Comment) {
  //   // if (data.postId) {
  //   //   var post:any = await Post.find(data.postId)
  //   //   post.comments=post.comments+1
  //   //   await post.save()
  //   // }
  // }
  /**********************Code for increase post comments [End]**********************/

  /**********************Code for decrease post comments [Start]**********************/
  @afterDelete()
  public static async decreasePostLike (data: Comment) {
    if (data.postId) {
      var post:any = await Post.find(data.postId)
      post.comments=post.comments>0?post.comments-1:0
      await post.save()
    }
  }
  /**********************Code for decrease post comments [End]**********************/

  @belongsTo(() => User,{
    localKey: 'id',
    foreignKey: 'uid',
  })
  public user: BelongsTo<typeof User>
  
  @hasMany(()=>Comment,{
    localKey: 'id',
    foreignKey: 'parentId',
  })
  public parents: HasMany<typeof Comment>
}
