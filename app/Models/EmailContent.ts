import { DateTime } from 'luxon'
import { BaseModel, column } from '@ioc:Adonis/Lucid/Orm'

export default class EmailContent extends BaseModel {
  public static table = 'email_contents'

  @column({ isPrimary: true })
  public id: number

  @column()
  public name:string
  @column()
  public slug:string
  @column()
  public subject:string

  @column({serializeAs:'fromEmail'})
  public fromEmail: string

  @column()
  public content:string

  @column()
  public status:boolean

  @column({serializeAs:'createdBy'})
  public createdBy: number

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updated: DateTime

  @column.dateTime({ autoCreate: true })
  public created: DateTime
}
