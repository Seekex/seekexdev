import { DateTime ,Duration} from 'luxon'
import { BaseModel, column } from '@ioc:Adonis/Lucid/Orm'

export default class WebServiceLog extends BaseModel {
  @column({ isPrimary: true })
  public id: number
  @column()
  public uid: number
  @column()
  public time_taken: number
  @column()
  public status: number
  @column()
  public service_name: string
  @column()
  public response: string
  @column()
  public request: string

  @column.dateTime({ autoCreate: true })
  public created: DateTime

  public static async deleteOldLog(){
    const currentDate= DateTime.local()
    const durFrom = Duration.fromObject({days:7});
    const oldDate = currentDate.minus(durFrom).toSQLDate()
    const oldData:any = await this.query().where("created",'<',oldDate).delete()
    return oldData
  }
}
