import { DateTime } from 'luxon'
import { BaseModel, column,computed } from '@ioc:Adonis/Lucid/Orm'
const transTypes = {0:'NA',1:'User/Expert',2:'Razorpay',3:'Seekex',4:'Sgst',5:'Cgst',6:'Igst',7:'Commission',8:'Exotel',9:'Promotion',10:'Refund',11:'VOIP',12:'Withdraw',13:'Referal Transfer'}
export default class TransactionHistory extends BaseModel {
  @column({ isPrimary: true })
  public id: number

  @column({serializeAs: 'transactionId',serialize:(value)=>{
    return value?value:0
  }})
  public transactionId: number
  @column({serializeAs: 'bankId',serialize:(value)=>{
    return value?value:0
  }})
  public bankId: number
  @column({serializeAs: 'callId',serialize:(value)=>{
    return value?value:0
  }})
  public callId: number

  @column()
  public description: string
  @column({serializeAs: 'fromWallet',serialize:(value)=>{
    return value?value:0
  }})
  public fromWallet : number
  @column({serialize:(value)=>{
    return value?true:false
  }})
  public isdebit : boolean
  @column({serializeAs: 'razorPayPaymentId',serialize:(value)=>{
    return value?value:''
  }})
  public razorPayPaymentId : string
  @column({serialize:(value)=>{
    return value?value:0
  }})
  public status : number
  @column({serializeAs:'toWallet',serialize:(value)=>{
    return value?value:0
  }})
  public toWallet : number
  @column({serializeAs:'transactionAmount'})
  public transactionAmount : number
  @column({serializeAs:'transactionType'})
  public transactionType : number

  @computed()
  public get transactionFor () {
    return transTypes[this.transactionType]
   }
  @column({serializeAs:'transactionWalletId'})
  public transactionWalletId : number
  
  @column({serialize:(value)=>{
    return value?true:false
  }})
  public payout : boolean

  @column({serializeAs:'transactionTo'})
  public transactionTo:string
  
  @column({serializeAs:'transactionAction'})
  public transactionAction:string

  @column({serializeAs:'transactionStatus'})
  public transactionStatus:string

  @column()
  public uid:number

  @column.dateTime({ autoCreate: true,serializeAs:'transactionDate' })
  public transactionDate: DateTime
  @column.dateTime({ autoCreate: true })
  public created: DateTime

}
