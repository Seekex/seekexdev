import { DateTime } from 'luxon'
import { BaseModel, column } from '@ioc:Adonis/Lucid/Orm'
const type={1:"Post",2:"Profile",3:"Video"}
export default class View extends BaseModel {
  @column({ isPrimary: true })
  public id: number

  @column()
  public uid: number

  @column({serializeAs:'targetId'})
  public targetId: number
  @column({serializeAs:'viewBy'})
  public viewBy: number
  @column()
  public type: number

  @column.dateTime({ autoCreate: true })
  public created: DateTime

}
