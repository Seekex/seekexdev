import { DateTime } from 'luxon'
import { BaseModel, column, beforeSave,computed} from '@ioc:Adonis/Lucid/Orm'
import Hash from '@ioc:Adonis/Core/Hash'
const fs = require('fs')
const Groups={1:'Sales',2:'Marketing'}

export default class GroupAcl extends BaseModel {

  @column({ isPrimary: true })
  public id: number

  @column()
  public acl_group_id : number
  @column()
  public acl_list_id : number
  
  @column.dateTime({ autoCreate: true })
  public created: DateTime

}
