import { DateTime } from 'luxon'
import { BaseModel, column,belongsTo,BelongsTo, computed, hasMany,HasMany } from '@ioc:Adonis/Lucid/Orm'
import User from './User'
import AppSetting from './AppSetting'
import ReferralHistory from './ReferralHistory'
const couponTypes = {"1":"Created By User","2":"Created By System"}
export default class CouponCode extends BaseModel {
  @column({ isPrimary: true })
  public id: number
  @column()
  public uid: number
  @column()
  public code:string

  @column.dateTime({serializeAs:"fromDate"})
  public fromDate:DateTime
  @column.dateTime({serializeAs:"toDate"})
  public toDate:DateTime
  
  @column({serializeAs:'couponType'})
  public couponType:number
  @column()
  public status:number
  @column()
  public amount:number

  @column({serializeAs:"maxUses"})
  public maxUses:number

  @column.dateTime({ autoCreate: true })
  public created: DateTime

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updated: DateTime

  @computed()
  public get couponCreatedType () {
    return couponTypes[this.couponType]
  }

  @belongsTo(() => User,{
    localKey: 'id',
    foreignKey: 'uid',
  })
  public user: BelongsTo<typeof User>

  @hasMany(() => ReferralHistory,{
    localKey: 'code',
    foreignKey: 'referralCode',
    onQuery:(query)=>{
      if (!query.isRelatedSubQuery) {
        query.preload('usedUser',(q)=>{
          q.select('id','name','profilePic')
        }).whereNotColumn('uid','=','refer_by')
      }
    }
  })
  public usedCoupon: HasMany<typeof ReferralHistory>


  public static async newCode(){
      var len=8
      var outStr = "SX", newStr
      while (outStr.length < len)
    {
        newStr = Math.random().toString(36).slice(2)
        outStr += newStr.slice(0, Math.min(newStr.length, (len - outStr.length)))
    }
    return outStr.toUpperCase()
  }

  public static async createUserTypeCoupon(code,uid){
    const amount = await AppSetting.getValueByKey('user_ref_amount_all')
    const maxUses = await AppSetting.getValueByKey('max_referral')
    var fromDate = DateTime.now();
    var toDate = fromDate.plus({year:1});
    const coupon:any = new CouponCode()
    coupon.code=code
    coupon.amount=amount
    coupon.maxUses=maxUses
    coupon.fromDate=fromDate
    coupon.toDate=toDate
    coupon.couponType=1
    coupon.status=1
    coupon.uid=uid
    await coupon.save()
  }

  public static async getSystemTypeCoupons(code){
    const cQuery = this.query()
    if(code){
      cQuery.where('code','LIKE','%'+code+'%')
    }
    const coupons = await (await cQuery.where({couponType:2})).map(elm=>{
      return elm?.code
    })
    return coupons
  }
}
