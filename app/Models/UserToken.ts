import { DateTime } from 'luxon'
import { BaseModel, column } from '@ioc:Adonis/Lucid/Orm'

export default class UserToken extends BaseModel {
  public static table = 'user_tokens'

  @column({ isPrimary: true })
  public id: number

  @column({serializeAs:'deviceId'})
  public deviceId:string

  @column({serializeAs:'userToken'})
  public userToken:string

  @column({serializeAs:'userId'})
  public userId: number

  @column({serializeAs:'fcmToken'})
  public fcmToken: number

  @column.dateTime({ autoCreate: true })
  public created: DateTime

  @column.dateTime({autoUpdate: true })
  public updated: DateTime

}
