import { DateTime } from 'luxon'
import { BaseModel, column,belongsTo, BelongsTo,computed } from '@ioc:Adonis/Lucid/Orm'
import User from 'App/Models/User'

export default class PendingRating extends BaseModel {
  @column({ isPrimary: true })
  public id: number
  @column({ serializeAs: 'callId' })
  public callId: number
  @column()
  public skip: number
  @column()
  public status: number
  @column()
  public uid: number
  @column.dateTime({ autoCreate: true })
  public created: DateTime

  @column()
  public name: string

  @column({serializeAs:"userType"})
  public userType: string

  @column()
  public pic: string

  // @computed()
  // public get name () {
  //   return this.user?.name?this.user?.name:''
  // }

  // @computed()
  // public get pic () {
  //   return this.user?.profilePic?this.user?.profilePic:''
  // }
  

  @belongsTo(() => User,{
    localKey: 'id',
    foreignKey: 'uid',
  })
  public user: BelongsTo<typeof User>
}
