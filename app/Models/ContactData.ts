import { DateTime } from 'luxon'
import { BaseModel, column, beforeSave,computed} from '@ioc:Adonis/Lucid/Orm'

export default class ContactData extends BaseModel {

  @column({ isPrimary: true })
  public id: number

  @column()
  public name : string
  @column()
  public email : string
  @column()
  public subject : string
  @column()
  public message : string
 
  @column({serializeAs:'userId'})
  public userId : number
  
  @column.dateTime({ autoCreate: true })
  public created: DateTime

}
