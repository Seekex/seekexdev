import { DateTime } from 'luxon'
import { BaseModel, column,belongsTo, BelongsTo } from '@ioc:Adonis/Lucid/Orm'
import User from 'App/Models/User'
import CallHistory from './CallHistory'

export default class UserRating extends BaseModel {
  @column({ isPrimary: true })
  public id: number
  @column()
  public uid: number
  @column({serializeAs:'callFeedBack'})
  public callFeedBack:string
  @column({serializeAs:'callId'})
  public callId:string
  @column({serializeAs:'callRating'})
  public callRating:string
  @column({serializeAs:'givenBy'})
  public givenBy:number
  @column({serializeAs:'userFeedBack'})
  public userFeedBack:string
  @column({serializeAs:'name'})
  public name:string
  @column({serializeAs:'profilePic'})
  public profilePic:string
  
  @column({serializeAs:'userRating'})
  public userRating:number
  @column({serializeAs:'video'})
  public video:boolean

  @column.dateTime({ autoCreate: true })
  public created: DateTime
  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updated: DateTime

  @belongsTo(() => User,{
    localKey: 'id',
    foreignKey: 'uid',
  })
  public user: BelongsTo<typeof User>

  @belongsTo(() => CallHistory,{
    localKey: 'id',
    foreignKey: 'callId',
  })
  public callHistory: BelongsTo<typeof CallHistory>

}
