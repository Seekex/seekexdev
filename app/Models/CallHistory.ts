import { DateTime } from 'luxon'
import { BaseModel, column ,belongsTo, BelongsTo,hasOne,HasOne,computed} from '@ioc:Adonis/Lucid/Orm'
import User from 'App/Models/User'
import Refund from './Refund'
import UserRating from './UserRating'

export default class CallHistory extends BaseModel {
  @column({ isPrimary: true })
  public id: number

  @column({serializeAs:'requestId',serialize:(value)=>{
    return value?value:0
  }})
  public requestId : number
  @column({serializeAs:'callTime',serialize:(value)=>{
    return value?value:new Date()
  }})
  public callTime : DateTime
  @column({serializeAs:'startTime',serialize:(value)=>{
    return value?value:new Date()
  }})
  public startTime : DateTime
  @column({serializeAs:'endTime',serialize:(value)=>{
    return value?value:new Date()
  }})
  public endTime : DateTime
  @column({serializeAs:'initiaterFeedback',serialize:(value)=>{
    return value?value:''
  }})
  public initiaterFeedback : string
  @column({serializeAs:'initiaterRating',serialize:(value)=>{
    return value?value:0
  }})
  public  initiaterRating : number
  @column()
  public initiater : number
  @column()
  public receiver : number
  @column({serializeAs:'receiverFeedback',serialize:(value)=>{
    return value?value:''
  }})
  public receiverFeedback : string
  @column({serializeAs:'receiverRating',serialize:(value)=>{
    return value?value:0
  }})
  public receiverRating : number

  @column({serializeAs:'recordingUrl',serialize:(value,recordingSize,call:CallHistory)=>{
    return call?.recordingNewSize>call?.recordingSize?call?.recordingUrlNew:value||''
  }})
  public recordingUrl : string

  @column({serializeAs:'recordingSize',serialize:(value)=>{
    return value?value:0
  }})
  public recordingSize : string

  @column({serializeAs:'recordingUrlNew',serialize:(value)=>{
    return value?value:''
  }})
  public recordingUrlNew : string

  @column({serializeAs:'recordingNewSize',serialize:(value)=>{
    return value?value:''
  }})
  public recordingNewSize : string

  @column({serializeAs:'endedBy',serialize:(value)=>{
    return value?value:0
  }})
  public endedBy  : number
  @column({serializeAs:'endedReason',serialize:(value)=>{
    return value?value:''
  }})
  public endedReason  : string
  @column()
  public rate  : number
  @column({serialize:(value)=>{
    return value?value:0
  }})
  public duration  : number

  @column({serializeAs:'estimatedDuration',serialize:(value)=>{
    return value?value:0
  }})
  public estimatedDuration  : number

  @column({serializeAs:'callDuration',serialize:(value)=>{
    return value?value:0
  }})
  public callDuration : number

  @column({serialize:(value)=>{
    return value?true:false
  }})
  public video  : boolean
  
  @column({serialize:(value)=>{
    return value?true:false
  }})
  public isigst  : boolean
  @column.dateTime({ autoCreate: true })
  public created: DateTime
  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updated: DateTime
   @column()
  public startCall: string


  @column({serializeAs:'callAmount'})
  public callAmount:number

  @column({serializeAs:'gstPercentage',serialize:(value)=>{
    return value?value:0
  }})
  public gstPercentage:number

  @column({serializeAs:'commissionPercentage',serialize:(value)=>{
    return value?value:0
  }})
  public commissionPercentage:number

  @column({serializeAs:'otherCharge',serialize:(value)=>{
    return value?value:0
  }})
  public otherCharge:number

  // @column()
  // public name:string
  // @column()
  // public profilePic:string
  @column()
  public saleAmt:number
  @column()
  public totalAmt:number
  @column()
  public gstCommission:number
  @column()
  public gstAmt:number
  @column()
  public finalPayout:number
  @column()
  public commission:number
  @column()
  public otherChargeValue:number

  @column({serializeAs:'callingType'})
  public callingType:string
  @column({serializeAs:'callResponse'})
  public callResponse:string
  @column({serializeAs:'apiResponse'})
  public apiResponse:string

  @column({serializeAs:'receiverToken'})
  public receiverToken:string
  @column({serializeAs:'initiaterToken'})
  public initiaterToken:string
  @column({serializeAs:'token'})
  public token:string
  @column({serializeAs:'sessionName'})
  public sessionName:string
  @column({serializeAs:'roomId'})
  public roomId:string
  @column({serializeAs:'waitingTime'})
  public waitingTime:string
  @column({serializeAs:'callMoneyTransactionStatus'})
  public callMoneyTransactionStatus:string

  

  @column()
  public status:string

  @column({serializeAs:'payStatus'})
  public payStatus:boolean

  @column({serializeAs:'finishedStatus'})
  public finishedStatus:boolean

  @column({serializeAs:'roomSid'})
  public roomSid:boolean

  @belongsTo(() => User,{
    localKey: 'id',
    foreignKey: 'initiater',
  })
  public user: BelongsTo<typeof User>

  @belongsTo(() => User,{
    localKey: 'id',
    foreignKey: 'receiver',
  })
  public expert: BelongsTo<typeof User>

  @computed()
  public get name () {
    return this.expert?this.expert.name:''
  }
  @computed()
  public get profileImage () {
    return this.expert?this.expert.profilePic:''
  }

  @computed()
  public get expertCallBackTime(){
    const callT:DateTime=this.callTime
    //return callT
    var setTime:any= DateTime.local(callT?.year,callT?.month,callT?.day,callT?.hour,callT?.minute,callT?.second).plus({hours:2})
    return new Date(setTime).getTime()
    return (setTime?.milliseconds)/1000
  }

  @computed()
  public get callTotalAmount(){
    return parseFloat((this.rate*this.duration).toFixed(3))
  }


  @hasOne(() => Refund,{
    localKey: 'id',
    foreignKey: 'callId',
  })
  public refund: HasOne<typeof Refund>

  @hasOne(() => UserRating,{
    localKey: 'id',
    foreignKey: 'callId',
  })
  public callRating: HasOne<typeof UserRating>


  public static async finishCall(callId){
    return this.query().where('id',callId).update({finished_status:true})
  }

  public static async finishCallPayStatus(callId){
    return this.query().where('id',callId).update({pay_status:true})
  }
}
