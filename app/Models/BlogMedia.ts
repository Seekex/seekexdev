import { DateTime } from 'luxon'
import { BaseModel, column } from '@ioc:Adonis/Lucid/Orm'

export default class BlogMedia extends BaseModel {
  @column({ isPrimary: true })
  public id: number
  @column()
  public url: string
  @column.dateTime({ autoCreate: true })
  public created: DateTime
}
