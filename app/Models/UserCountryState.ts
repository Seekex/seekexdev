import { DateTime } from 'luxon'
import { BaseModel, column,belongsTo,BelongsTo,computed } from '@ioc:Adonis/Lucid/Orm'
import CountryState from 'App/Models/CountryState'
export default class UserCountryState extends BaseModel {
  @column({ isPrimary: true })
  public id: number
  @column()
  public uid: number
  @column({serializeAs:'countryStateId'})
  public countryStateId:number
  @column({serializeAs:'countryStateParentId'})
  public countryStateParentId: number

  @column.dateTime({ autoCreate: true })
  public created: DateTime

  @belongsTo(() => CountryState,{
    localKey: 'id',
    foreignKey: 'countryStateId',
  })
  public countryStateData: BelongsTo<typeof CountryState>

  @computed()
  public get name () {
    return this.countryStateData?this.countryStateData?.name:''
  }
  @computed()
  public get used () {
    return this.countryStateData?.used||0
  }
  @computed()
  public get imageUrl () {
    return this.countryStateData?this.countryStateData.imageUrl:''
  }

  @computed()
  public get parentId () {
    return this.countryStateData?.parentId||0
  }
  @computed()
  public get masterType () {
    return this.countryStateData?.masterType||0
  }

  public static async getValueByUid(uid){
    const val:any = await this.query().where("uid",uid).whereNot('countryStateParentId',0).first()
    return val?.countryStateId||0
  }
}
