import { DateTime } from 'luxon'
import { BaseModel, column } from '@ioc:Adonis/Lucid/Orm'

export default class VideoTaggedUser extends BaseModel {
  @column({ isPrimary: true })
  public id: number

  @column({serializeAs:'userVideoId'})
  public userVideoId: number

  @column()
  public uid: number

  @column.dateTime({ autoCreate: true })
  public created: DateTime
}
