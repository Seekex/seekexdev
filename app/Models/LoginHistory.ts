import { DateTime } from 'luxon'
import { BaseModel, column,belongsTo,BelongsTo } from '@ioc:Adonis/Lucid/Orm'
import User from './User'

export default class LoginHistory extends BaseModel {
  @column({ isPrimary: true })
  public id: number
  @column({serializeAs:'deviceId'})
  public deviceId: string
  @column({serializeAs:'fcmToken'})
  public fcmToken: string
  @column({serializeAs:'geoLocation'})
  public geoLocation: string
  @column()
  public uid: number
  @column.dateTime({ autoCreate: true })
  public timeStamp : DateTime

  @column()
  public status: boolean
  // @column.dateTime({ autoCreate: true, autoUpdate: true })
  // public updated: DateTime
  @belongsTo(() => User,{
    localKey: 'id',
    foreignKey: 'uid',
  })
  public user: BelongsTo<typeof User>

  public static async getUserFcm(uid){
    const fcm:any = await this.query().where("uid",uid).where('status',false).orderBy("id","desc").first()
    return fcm
  }
}
