import { DateTime } from 'luxon'
import { BaseModel, column,hasMany,HasMany } from '@ioc:Adonis/Lucid/Orm'

export default class ExpertSchedule extends BaseModel {
  @column({ isPrimary: true })
  public id: number
  @column({serialize:(value)=>{
    return value?true:false
  }})
  public booked: boolean
  @column({serializeAs:'daysId'})
  public daysId: number
  @column({serializeAs:'fromTime'})
  public fromTime: string
  @column({serializeAs:'toTime'})
  public toTime: string
  @column({serializeAs:'parentId'})
  public parentId: number
  @column()
  public uid: number

  @column.dateTime({ autoCreate: true })
  public created: DateTime
  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updated: DateTime

  /*********************** Self Association *****************************/
  
  @hasMany(() => ExpertSchedule,{
    localKey: 'id',
    foreignKey: 'parentId',
  })
  public timingSlots: HasMany<typeof ExpertSchedule>

}
