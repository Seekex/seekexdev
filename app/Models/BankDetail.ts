import { DateTime } from 'luxon'
import { BaseModel, column } from '@ioc:Adonis/Lucid/Orm'

export default class BankDetail extends BaseModel {
  @column({ isPrimary: true })
  public id: number
  @column({serializeAs: 'accountNo'})
  public accountNo: string
  @column()
  public address: string
  @column({serializeAs: 'bankName'})
  public bankName : string
  @column({serializeAs: 'branchName'})
  public branchName: string
  @column({serializeAs: 'holderName'})
  public holderName: string
  @column({serializeAs: 'ifscCode'})
  public ifscCode: string
  @column()
  public uid: number
  @column({serializeAs: 'accountId'})
  public accountId: string
  @column({serialize:(value)=>{
    return value?true:false
  }})
  public upi: boolean
  @column({serializeAs: 'upiAddress'})
  public upiAddress: string

  @column.dateTime({ autoCreate: true })
  public created: DateTime

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updated: DateTime
}
