import { DateTime } from 'luxon'
import { BaseModel, column } from '@ioc:Adonis/Lucid/Orm'

export default class OnlineOfflineStatus extends BaseModel {
  public static table = 'online_offline_status'
  @column({ isPrimary: true })
  public id: number

  @column()
  public uid: number

  @column()
  public status: boolean

  @column.dateTime({ autoCreate: true,serializeAs:'timeStamp' })
  public timeStamp: DateTime

  // @column.dateTime({ autoCreate: true, autoUpdate: true })
  // public updated: DateTime
}
