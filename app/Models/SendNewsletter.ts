import { DateTime } from 'luxon'
import { BaseModel, column, beforeSave,computed} from '@ioc:Adonis/Lucid/Orm'

export default class SendNewsletter extends BaseModel {

  @column({ isPrimary: true })
  public id: number

  @column()
  public title : string

  @column()
  public content : string

  @column({serializeAs:"emailData"})
  public emailData : string

  @column()
  public output : string
  
  @column.dateTime({ autoCreate: true })
  public created: DateTime

}
