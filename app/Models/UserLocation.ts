import { DateTime } from 'luxon'
import { BaseModel, column } from '@ioc:Adonis/Lucid/Orm'

export default class UserLocation extends BaseModel {
  @column({ isPrimary: true })
  public id: number
  @column()
  public uid: number
  @column()
  public address: string
  @column()
  public altitude: number
  @column()
  public latitude : number
  @column()
  public longitude : number
  @column.dateTime({ autoCreate: true })
  public created: DateTime
  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updated: DateTime
}
