import { DateTime } from 'luxon'
import { BaseModel, column } from '@ioc:Adonis/Lucid/Orm'

export default class PendingRecording extends BaseModel {
  @column({ isPrimary: true })
  public id: number
  @column({serializeAs:'callId'})
  public callId: number
  @column({serializeAs:'initiaterUrl'})
  public initiaterUrl: string
  @column({serializeAs:'receiverUrl'})
  public receiverUrl: string
  @column()
  public status: number
  
  @column.dateTime({ autoCreate: true })
  public created: DateTime

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updated: DateTime
}
