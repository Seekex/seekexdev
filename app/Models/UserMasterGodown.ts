import { DateTime } from 'luxon'
import { BaseModel, column,belongsTo,BelongsTo,computed } from '@ioc:Adonis/Lucid/Orm'
import MasterGodown from './MasterGodown'
import Organization from './Organization'

export default class UserMasterGodown extends BaseModel {
  @column({ isPrimary: true })
  public id: number

  @column({serializeAs:'createdBy'})
  public masterFor:number

  @column({serializeAs:'masterGodownId'})
  public masterGodownId:number

  @column({serializeAs:'masterType'})
  public masterType:number

  @column.dateTime({ autoCreate: true })
  public created: DateTime

  @belongsTo(() => MasterGodown,{
    localKey: 'id',
    foreignKey: 'masterGodownId',
    onQuery: (query) =>{
      if (!query.isRelatedSubQuery) {
          query.preload('parent')
      }
    } 
  })
  public master: BelongsTo<typeof MasterGodown>

  @belongsTo(() => Organization,{
    localKey: 'id',
    foreignKey: 'masterGodownId',
    onQuery: (query) =>{
      if (!query.isRelatedSubQuery) {
          query.preload('parent')
      }
    } 
  })
  public orgs: BelongsTo<typeof Organization>



  @computed()
  public get name () {
    var name=''
    if(this.master?.parent){
      name=this.master.parent?.name
    }else if(!this.master?.parent && this.orgs?.parent){
      name=this.orgs.parent?.name
    }else if(this.master){
      name=this.master?.name
    }else if(this.orgs){
      name=this.orgs?.name
    }else{
      name=''
    }
    return name
   // return this.master?.parent?this.master.parent?.name:this.master?.name||this.orgs?.parent?this.orgs.parent?.name:this.orgs?.name||''
  }
  @computed()
  public get used () {
    return this.master?.used||0
  }
  @computed()
  public get imageUrl () {
    var imageUrl=''
    if(this.master?.parent){
      imageUrl=this.master.parent?.imageUrl
    }else if(!this.master?.imageUrl && this.orgs?.image_url){
      imageUrl=this.orgs.parent?.image_url
    }else if(this.master){
      imageUrl=this.master?.imageUrl
    }else if(this.orgs){
      imageUrl=this.orgs?.image_url
    }else{
      imageUrl=''
    }
    return imageUrl
    return ''
  }

  @computed()
  public get parentId () {
    var parentId=0
    if(this.master?.parent){
      parentId=this.master.parent?.id
    }else if(!this.master?.parent && this.orgs?.parent){
      parentId=this.orgs.parent?.id
    }else if(this.master){
      parentId=this.master?.parentId
    }else if(this.orgs){
      parentId=this.orgs?.parentId
    }else{
      parentId=0
    }
    return parentId
    return this.master?.parentId||0
  }
  
}
