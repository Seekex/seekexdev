import { DateTime,Duration } from 'luxon'
import { BaseModel, column } from '@ioc:Adonis/Lucid/Orm'

export default class CronLogs extends BaseModel {
  @column({ isPrimary: true })
  public id: number
  @column({serializeAs:'cronName'})
  public cronName: string
  @column({serializeAs:'data'})
  public data: string
  @column()
  public type: string
  @column({serializeAs:'takenTime'})
  public takenTime: string
  @column()
  public status: string
  @column.dateTime({ autoCreate: true })
  public time : DateTime

  public static async saveLog(data){
      return this.create({
        cronName:data?.cronName||'',
        data:data?.data||'',
        type:data?.type||'',
        status:data?.status||'',
        takenTime:data?.takenTime||0
      })

  }

  public static async deleteOldLog(){
    const currentDate= DateTime.local()
    const durFrom = Duration.fromObject({days:7});
    const oldDate = currentDate.minus(durFrom).toSQLDate()
    const oldData:any = await this.query().where("created",'<',oldDate).delete()
    return oldData
  }
}
