import { DateTime } from 'luxon'
import { BaseModel, column,afterSave,afterDelete } from '@ioc:Adonis/Lucid/Orm'
import Comment from 'App/Models/Comment'
export default class LikeComment extends BaseModel {
  @column({ isPrimary: true })
  public id: number
  @column()
  public uid: number
  @column({serializeAs:'targetId'})
  public targetId: number
  @column.dateTime({ autoCreate: true })
  public created: DateTime

  /**********************Code for increase Comment like [Start]**********************/
  @afterSave()
  public static async increasePostLike (data: LikeComment) {
    if (data.targetId) {
      var comment:any = await Comment.find(data.targetId)
      comment.likes=comment.likes+1
      await comment.save()
    }
  }
  /**********************Code for increase Comment like [End]**********************/

  /**********************Code for decrease Comment like [Start]**********************/
  @afterDelete()
  public static async decreasePostLike (data: LikeComment) {
    if (data.targetId) {
      var comment:any = await Comment.find(data.targetId)
      comment.likes=comment.likes>0?comment.likes-1:0
      await comment.save()
    }
  }
  /**********************Code for decrease Comment like [End]**********************/
}
