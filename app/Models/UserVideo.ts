import { DateTime } from 'luxon'
import { BaseModel, column,manyToMany,ManyToMany,hasMany,HasMany,belongsTo,BelongsTo,computed } from '@ioc:Adonis/Lucid/Orm'
import User from 'App/Models/User'
import AppMedia from 'App/Models/AppMedia'
import Reported from './Reported'
import truncatise from 'truncatise'
import VideoComment from './VideoComment'
import LikeVideo from './LikeVideo'
import FollowVideo from './FollowVideo'
import View from './View'
const videoTypes = {"1":"Posted Video","2":"Intro video"}
export default class UserVideo extends BaseModel {
  public static table = 'user_videos'
  @column({ isPrimary: true })
  public id:number

  @column({serializeAs:'videoUrl'})
  public videoUrl:string
 
  @column({serializeAs:'publicId'})
  public publicId:number

  @column()
  public description:string

  @column({serialize:(value)=>{
    return value?value:0
  }})

  public location:string
  @column({serialize:(value)=>{
    return value?true:false
  }})
  public privates:boolean
  
  @column({serialize:(value)=>{
    return value?value:0
  }})
  public views:number
  @column({serializeAs:'hashtags',serialize:(value)=>{
    return value?value:''
  }})
  public hash_tags:string

  @column({columnName:'created_by'})
  public createdBy:number
  @column({serialize:(value)=>{
    return value?value:0
  }})
  public status:number

  @column({serializeAs:'videoType'})
  public videoType:number

  @column.dateTime({ autoCreate: true })
  public created: DateTime
  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updated: DateTime

  @computed()
  public get videoTypeTxt () {
    return videoTypes[this.videoType]
  }

  @computed()
  public get uid () {
    return this.createdBy
  }

  @computed()
  public get title () {
    return truncatise(this.description, { TruncateLength: 50 })
  }

  @computed()
  public get name () {
    return this.user?this.user.name:''
  }
  @computed()
  public get profileImage () {
    return this.user?.profilePic||''
  }
  @column({serialize:(value)=>{
    return value?true:false
  }})
  public likestatus:boolean

  @column({serialize:(value)=>{
    return value?true:false
  }})
  public followstatus:boolean

  @column()
  public deleted:boolean

  @computed()
  public get verified () {
    return this.user?.verified?true:false
  }

  @column({serialize:(value)=>{
    return value?true:false
  }})
  public userfollowstatus:boolean

  @computed()
  public get userOcuEdu () {

    var oeStr=''
    if(this.user?.occupation){
      oeStr+=this.user?.occupation.name
    }
    if(this.user?.currentOrg){
      oeStr+=' at '+this.user?.currentOrg.name
    }
    if(this.user?.highestEducation){
      oeStr+='\n'+this.user?.highestEducation?.name
    }
    if(this.user?.collegeUniversity){
      oeStr+=' from '+this.user?.collegeUniversity?.name
    }
    return oeStr
  }

  @belongsTo(() => User,{
    localKey: 'id',
    foreignKey: 'createdBy',
  })
  public user: BelongsTo<typeof User>

  @manyToMany(() => User, {
    pivotTable: 'video_tagged_users',
    localKey: 'id',
    pivotForeignKey: 'user_video_id',
    relatedKey: 'id',
    pivotRelatedForeignKey: 'uid',
  })
  public userTagged: ManyToMany<typeof User>

  // @hasMany(() => Reported,{
  //   localKey: 'id',
  //   foreignKey: 'targetId',
  //   onQuery: (query) =>{
  //     if (!query.isRelatedSubQuery) {
  //       query.where('type', 1)
  //     }
  //   }
  // })
  // public reported: HasMany<typeof Reported>

  @hasMany(() => VideoComment,{
    localKey: 'id',
    foreignKey: 'userVideoId'
  })
  public comments: HasMany<typeof VideoComment>

  @hasMany(() => LikeVideo,{
    localKey: 'id',
    foreignKey: 'userVideoId'
  })
  public videoLikes: HasMany<typeof LikeVideo>

  @hasMany(() => FollowVideo,{
    localKey: 'id',
    foreignKey: 'userVideoId'
  })
  public videoFollows: HasMany<typeof FollowVideo>

  @hasMany(() => View,{
    localKey: 'id',
    foreignKey: 'targetId',
    onQuery: (query) =>{
      if (!query.isRelatedSubQuery) {
        query.where('type', 3)
      }
    }
  })
  public videoViews: HasMany<typeof View>

}
