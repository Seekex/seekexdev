import { DateTime } from 'luxon'
import { BaseModel, column, beforeSave, afterCreate, hasMany, HasMany, hasOne,HasOne,computed,afterFind,afterFetch,beforeFind,scope} from '@ioc:Adonis/Lucid/Orm'
import Hash from '@ioc:Adonis/Core/Hash'
import UserAdvanceDetail from 'App/Models/UserAdvanceDetail'
import UserCountryState from 'App/Models/UserCountryState'
import UserMasterGodown from './UserMasterGodown'
import LedgerAccount from 'App/Models/LedgerAccount'
import FirebaseHelper from 'App/Helpers/FirebaseHelper'
const firebaseHelper = new FirebaseHelper()
import Reported from './Reported'
import UserRating from './UserRating'
import Encryption from '@ioc:Adonis/Core/Encryption'
//import Database from '@ioc:Adonis/Lucid/Database'
import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import Env from '@ioc:Adonis/Core/Env'
import Post from './Post'
import Request from './Request'
import LikePost from './LikePost'
import LikeVideo from './LikeVideo'
import Comment from './Comment'
import UserVideo from './UserVideo'
import LoginHistory from './LoginHistory'
import CallHistory from './CallHistory'
import Chat from './Chat'
import ExpertSchedule from './ExpertSchedule'
import FollowPost from './FollowPost'
import CouponCode from './CouponCode'
import ReferralHistory from './ReferralHistory'
const host=Env.get('SERVER_HOST') as string
export default class User extends BaseModel {
  @column({ isPrimary: true })
  public id: number
  @column()
  public uid: number
  @column({ serializeAs: 'userRole' })
  public userRole:string

  @column({
    serialize:(value)=>{
    return value?true:false
  }
  })
  public expert:boolean
  @column({
    serialize:(value)=>{
    return value?true:false
  }
  })
  public verified:boolean

  @column({
    serialize:(value)=>{
    return value?true:false
  }
  })
  public featured:boolean 
  
  @column({serialize:(value)=>{
    return value?value:''
  },serializeAs:'referralCode'
  })
  public referralCode:string

  @computed()
  public get isnew () {
   return (this.verified==true && this.featured==true)?false:true
  }

  @computed()
  public get profileUrl () {
    const uid:any = this?.id
    //return uid
    var b = Buffer.from(''+uid)
    var s = b.toString('base64')
    return host+'/expert-profile/'+s
  }

  @column({serialize:(value)=>{
      return value?value:0
  },serializeAs:'callResponseRate'})
  public callResponseRate:number

//   // public get callResponseRate () {
//   //  return commonHelper.expertResponceRate(this.id)
//   // }
// @column({serialize:(value)=>{
//     return value?value:0
// },serializeAs:'rating'})
// public rating:number

// @column({serialize:(value)=>{
//   return value?value:0
// },serializeAs:'ratingCount'})
// public ratingCount:number


@column({
  serialize:(value)=>{
    return value?true:false
  }
})
public requested:boolean
  @column({
    serialize:(value)=>{
    return value?true:false
  }
  })
  public emailverified:boolean
  @column({
    serialize:(value)=>{
    return value?true:false
  }
  })
  public mobileverified:boolean
  
  @column()
  public name: string
  @column()
  public email: string
  @column({ 
    serializeAs: 'contactId',
    serialize:(value)=>{
      return value?value:''
    }
   })
  public contactId: string
  @column({ serializeAs: null })
  public password: string
  @column({
    serialize:(value)=>{
    return value?value:''
  }
  })
  public mobile: string

  @column({ serializeAs: 'genderId',
    serialize:(value)=>{
    return value?parseInt(value):0
  }
  })
  public genderId: number

  @column({ serializeAs: 'profilePic' ,serialize:(value)=>{
    return value?value:''
  }})
  public profilePic: string

  @column()
  public profile:object
  
  @column({
    serialize:(value)=>{
      return value?value:''
    }
  })
  public dob: string

  @column({ serializeAs: 'aboutMe',serialize:(value)=>{
    return value?value:''
  } })
  public aboutMe: string

  @column({serialize:(value)=>{
    return value?value:0
  }})
  public age: number
  @column({serialize:(value)=>{
    return value?value:0
  }})
  public views: number

  @column({serialize:(value)=>{
    return value?value:0
  },serializeAs:'moreVideo'})
  public moreVideo: number

  @column({serializeAs: 'audioCallRate',serialize:(value)=>{
    return value?value:0
  } })
  public audioCallRate: number

  @column({serializeAs: 'audioCallRatePerSec',serialize:(value)=>{
    return value?value:0
  } })
  public audioCallRatePerSec: number
  // @computed()
  // public get userAudioCallRate () {
  //  return this.audioCallRate||0
  // }
  @column({ serializeAs: 'userAudioCallRate',serialize:(value)=>{
    return value?value:0
  } })
  public userAudioCallRate: number

  @column({ serializeAs: 'videoCallRate',serialize:(value)=>{
    return value?value:0
  } })
  public videoCallRate: number
  @column({ serializeAs: 'videoCallRatePerSec',serialize:(value)=>{
    return value?value:0
  }})
  public videoCallRatePerSec: number

  @column({ serializeAs: 'userVideoCallRate',serialize:(value)=>{
    return value?value:0
  } })
  public userVideoCallRate: number

  // @computed()
  // public get userVideoCallRate () {
  //  return this.videoCallRate||0
  // }

  @column({ serializeAs: 'show_mobile',serialize:(value)=>{
    return value?true:false
  } })
  public showMobile: boolean
  
  @column({ serializeAs: 'regCompleted',serialize:(value)=>{
    return value?true:false
  } })
  public regCompleted: boolean
  //1 - Basic Profile Step, 2 - Advance Detail Step, 3 - Social Profile Step, 4 - Rate Setting Profile 
  @column({ serializeAs: 'regCompleteStep' })
  public regCompleteStep: number
  
  @column({serializeAs: 'countryCode',serialize:(value)=>{
    return value?value:91
  } })
  public countryCode: number

  @column({serializeAs: 'incomingCallSetting',serialize:(value)=>{
    return value?value:0
  }})
  public incomingCallSetting: number

  @computed()
  public get callSetting () {
   return this.incomingCallSetting||0
  }

  @column({serializeAs: 'incomingCallSettingOption',serialize:(value)=>{
    return value?value:0
  }})
  public incomingCallSettingOption: number
  @computed()
  public get apointmentOption () {
   return this.incomingCallSettingOption||0
  }

  @column({serialize:(value)=>{
    return value?true:false
  }})
  public available: boolean
  
  @column({serialize:(value)=>{
    return value?value:''
  }})
  public resume: string

  @column({serialize:(value)=>{
    return value?value:''
  },serializeAs:'resumeText'})
  public resumeText: string

  @column({serialize:(value)=>{
    return value?value:''
  },serializeAs:'currentCompany'})
  public currentCompany: string
  
  @column({serialize:(value)=>{
    return value?value:''
  }})
  public intro: string

  @column({serialize:(value)=>{
    return value?true:false
  }})
  public expertmode: boolean
  @column({
    serialize:(value)=>{
    return value?value:0
  }
})
  public followers: number

  @column({
    serialize:(value)=>{
    return value?value:0
  }})
  public following: number

  @column({serialize:(value)=>{
    return value?value:0
  }})
  public conversations: number

  @column({serializeAs:'rememberMeToken'})
  public remember_me_token: string

  // @column()
  // public country: object

  @column({serialize:(value)=>{
    return value?value:0
  }})
  public status: number

  @column({serializeAs:'isVisible'})
  public isVisible: boolean

  @column({serializeAs:'mobileOtp'})
  public mobileOtp: number

  @column({serializeAs:'emailOtp'})
  public emailOtp: number


  @column({serialize:(value)=>{
    return value?value:0
  },serializeAs:'ratingCount'})
  public ratingCount: number

  @column({serialize:(value)=>{
    return value?value:0
  }})
  public rating: number
  
  @column({serialize:(value)=>{
    return value?value:''
  },serializeAs:"timeZone"})
  public timeZone: string

  @column({serialize:(value)=>{
    return value?true:false
  },serializeAs:'exotelCallOption'})
  public exotelCallOption: boolean

  @column({serialize:(value)=>{
    return value?true:false
  },serializeAs:'voipCallOption'})
  public voipCallOption: boolean

  @column({serialize:(value)=>{
    return value?true:false
  },serializeAs:'videoCallOption'})
  public videoCallOption: boolean

  @column({serialize:(value)=>{
    return value?true:false
  }})
  public accessibility: boolean

  @column({serializeAs:'chanalUrl'})
  public chanalUrl: string

  @column({serializeAs:'instagram'})
  public instagram: string

  @column({serializeAs:'linkedin'})
  public linkedin: string

  @column({serializeAs:'facebook'})
  public facebook: string

  @column()
  tagline : string

  @column({serializeAs:'introType'})
  public introType: string
  
  @column()
  public acl:object

  @column.dateTime({ autoCreate: true })
  public created: DateTime

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updated: DateTime

  // @column({serialize: (value)=>{
  //   return value?value:''
  // },serializeAs:'country'})
  // public country: string

  @beforeSave()
  public static async hashPassword (user: User) {
    if (user.$dirty.password) {
      user.password = await Hash.make(user.password)
      user.remember_me_token=await Hash.make(user.email)
    }
    if (!user?.referralCode){
        var len=6
        var outStr = "", newStr
        while (outStr.length < len)
      {
          newStr = Math.random().toString(36).slice(2)
          outStr += newStr.slice(0, Math.min(newStr.length, (len - outStr.length)))
      }
      user.referralCode = outStr.toUpperCase()
    }
      /* User Referral Code*/
      
     /* User Referral Code*/
  }
  
  @afterCreate()
  public static async afterCreateHook (user: User) {
     // const trx = await Database.transaction()
       ////////////////////////// Create User Default Wallet ////////////////////////////////
       const ledgerAccount = new LedgerAccount()
       ledgerAccount.availableAmount=0.0
       ledgerAccount.holdAmount=0.0
       ledgerAccount.totalAmount=0.0
       ledgerAccount.disputedAmount=0.0
       ledgerAccount.promoAmount=0.0
       ledgerAccount.name='User'
       ledgerAccount.isuser=true
       ledgerAccount.uid=user.id
      // ledgerAccount.useTransaction(trx)
       await ledgerAccount.save()
       //*=================Save user referral Code in Coupon Table==================* */
       await CouponCode.createUserTypeCoupon(user?.referralCode,user?.id)
      ////////////////////////// Create User in firebase database  ////////////////////////////////
       await firebaseHelper.userStatus(user.id)
  }
  
  @hasMany(() => UserAdvanceDetail,{
    localKey: 'id',
    foreignKey: 'uid',
    onQuery: (query) =>{
      if (!query.isRelatedSubQuery) {
        query.preload('medias',(q)=>{
          q.where('mediaType', 1)
        }).preload('organization').where('detailType', 1).limit(10)
      }
    }
  })
  public accomplishmentsList: HasMany<typeof UserAdvanceDetail>

  @hasMany(() => UserAdvanceDetail,{
    localKey: 'id',
    foreignKey: 'uid',
    onQuery: (query) => {
      if (!query.isRelatedSubQuery) {
        query.preload('medias',(q)=>{
          q.where('mediaType', 2)
        }).preload('organization').where('detailType', 2).limit(10)
      }
    }
  })
  public certificatesList: HasMany<typeof UserAdvanceDetail>

  @hasMany(() => UserAdvanceDetail,{
    localKey: 'id',
    foreignKey: 'uid',
    onQuery: (query) => {
      if (!query.isRelatedSubQuery) {
        query.preload('medias',(q)=>{
          q.where('mediaType', 3)
        }).preload('organization').where('detailType', 3).limit(10)
      }
    }
  })
  public projectList: HasMany<typeof UserAdvanceDetail>

  @hasOne(() => UserCountryState,{
    localKey: 'id',
    foreignKey: 'uid',
    onQuery: (query) =>{
      if (!query.isRelatedSubQuery) {
        query.preload('countryStateData').where('countryStateParentId', 0)
      }
    }
  })
  public country: HasOne<typeof UserCountryState>

  @hasOne(() => UserCountryState,{
    localKey: 'id',
    foreignKey: 'uid',
    onQuery: (query) => {
      if (!query.isRelatedSubQuery) {
        query.preload('countryStateData').whereNot('countryStateParentId', 0)
      }
    }
  })

  public state: HasOne<typeof UserCountryState>
 
  @hasOne(() => UserMasterGodown,{
    localKey: 'id',
    foreignKey: 'masterFor',
    onQuery: (query) =>{
      if (!query.isRelatedSubQuery) {
        query.preload('master').where('masterType', 4)
      }
    } 
  })
  public highestEducation: HasOne<typeof UserMasterGodown>


  @hasOne(() => UserMasterGodown,{
    localKey: 'id',
    foreignKey: 'masterFor',
    onQuery: (query) => {
      if (!query.isRelatedSubQuery) {
        query.preload('master').where('masterType', 1)
      }
    }
  })
  public occupation: HasOne<typeof UserMasterGodown>

  @hasOne(() => UserMasterGodown,{
    localKey: 'id',
    foreignKey: 'masterFor',
    onQuery: (query) => {
      if (!query.isRelatedSubQuery) {
        query.preload('orgs').where('masterType', 7)
      }
    }
  })
  public currentOrg: HasOne<typeof UserMasterGodown>

  @hasOne(() => UserMasterGodown,{
    localKey: 'id',
    foreignKey: 'masterFor',
    onQuery: (query) => {
      if (!query.isRelatedSubQuery) {
        query.preload('orgs').where('masterType', 9)
      }
    }
  })
  public collegeUniversity: HasOne<typeof UserMasterGodown>
  

  @hasMany(() => UserMasterGodown,{
    localKey: 'id',
    foreignKey: 'masterFor',
    onQuery: (query) =>{
      if (!query.isRelatedSubQuery) {
        query.preload('master').where('masterType', 5).limit(10)
      }
    }
  })
  public intExpertiseList: HasMany<typeof UserMasterGodown>

  @hasMany(() => UserMasterGodown,{
    localKey: 'id',
    foreignKey: 'masterFor',
    onQuery: (query) =>{
      if (!query.isRelatedSubQuery) {
        query.preload('master').where('masterType', 3).limit(10)
      }
    } 
  })
  public skillsList: HasMany<typeof UserMasterGodown>

  @hasMany(() => UserMasterGodown,{
    localKey: 'id',
    foreignKey: 'masterFor',
    onQuery: (query) =>{
      if (!query.isRelatedSubQuery) {
        query.preload('master').where('masterType', 6).limit(10)
      }
    } 
  })
  public hashtags: HasMany<typeof UserMasterGodown>


  @hasMany(() => Reported,{
    localKey: 'id',
    foreignKey: 'targetId',
    onQuery: (query) =>{
     // if (!query.isRelatedSubQuery) {
          query.where('type', 2)
     // }
    }
  })
  public reported: HasMany<typeof Reported>


  @hasMany(() => UserRating,{
    localKey: 'id',
    foreignKey: 'uid'
  })
  public feedbacks: HasMany<typeof UserRating>
  
   ///////////////////////////For event filltes relations/////////////////////////////////////
   
  @hasMany(() => Post,{
    localKey: 'id',
    foreignKey: 'createdBy'
  })
  public userPosts: HasMany<typeof Post>

  @hasMany(() => UserVideo,{
    localKey: 'id',
    foreignKey: 'createdBy'
  })
  public userVideos: HasMany<typeof UserVideo>

  @hasMany(() => Request,{
    localKey: 'id',
    foreignKey: 'requestedBy'
  })
  public callRequests: HasMany<typeof Request>

  @hasMany(() => LikePost,{
    localKey: 'id',
    foreignKey: 'uid'
  })
  public userPostLikes: HasMany<typeof LikePost>

  @hasMany(() => FollowPost,{
    localKey: 'id',
    foreignKey: 'uid'
  })
  public userPostFollows: HasMany<typeof FollowPost>

  @hasMany(() => LikeVideo,{
    localKey: 'id',
    foreignKey: 'uid'
  })
  public userVideoLikes: HasMany<typeof LikeVideo>

  @hasMany(() => Comment,{
    localKey: 'id',
    foreignKey: 'uid'
  })
  public postComments: HasMany<typeof Comment>

  @hasMany(() => CallHistory,{
    localKey: 'id',
    foreignKey: 'initiater'
  })
  public userCalls: HasMany<typeof CallHistory>

  @hasMany(() => Chat,{
    localKey: 'id',
    foreignKey: 'senderId'
  })
  public userMessages: HasMany<typeof Chat>

  @hasMany(() => ExpertSchedule,{
    localKey: 'id',
    foreignKey: 'uid'
  })
  public userScheduleSetting: HasMany<typeof ExpertSchedule>

  @hasMany(() => ReferralHistory,{
    localKey: 'id',
    foreignKey: 'uid'
  })
  public earnReferrals: HasMany<typeof ReferralHistory>

  ///////////////////////////For event filltes relations/////////////////////////////////////
  


  @hasOne(() => LoginHistory,{
    localKey: 'id',
    foreignKey: 'uid',
    onQuery: (query) =>{
      if (!query.isRelatedSubQuery) {
        query.orderBy('id','desc')
      }
    } 
  })
  public userFcm: HasOne<typeof LoginHistory>
  
  @computed()
  public get userOcuEdu () {
    //return this.user?this.user.profilePic:''
    var oeStr=''
    if(this.occupation){
      oeStr+=this.occupation?.name
    }
    if(this.currentOrg){
      oeStr+=' at '+this.currentOrg?.name
    }
    if(this.highestEducation){
      oeStr+='\n'+this.highestEducation?.name
    }
    if(this.collegeUniversity){
      oeStr+=' from '+this.collegeUniversity?.name
    }
    return oeStr
  }


  //** Scopes */
  public static hightEdu = scope((query:any) => {
    query.has('highestEducation')
  })

  public static rate = scope((query:any) => {
    query.where('audioCallRate','>',0).orWhere('videoCallRate','>',0)
  })

  public static active = scope((query:any) => {
    query.where('status',1)
  })

  public static noSameUser = scope((query:any,user) => {
    query.whereNot('id',user?.id)
  })

  public static visible = scope((query:any) => {
    query.where('isVisible',1)
  })

  public static async userListByIdOrName(nameOrId){
    const list = await (await this.query().where('name','LIKE','%'+nameOrId+'%').orWhere('id','LIKE','%'+nameOrId+'%')).map((user)=>{
        const elm={
          id:user?.id,
          name:user?.name
        }
      return elm
    })
    return list
  }

}
