import { DateTime } from 'luxon'
import { BaseModel, column ,hasMany, HasMany, belongsTo, BelongsTo,computed} from '@ioc:Adonis/Lucid/Orm'
import AppMedia from 'App/Models/AppMedia'
import Organization from 'App/Models/Organization'
export default class UserAdvanceDetail extends BaseModel {
  @column({ isPrimary: true })
  public id: number
  @column()
  public uid: number

  @column({serialize:(value,name,userOrg:UserAdvanceDetail)=>{
    const val:any = userOrg?.name
    return val
  }})
  public company: string

  @computed()
  public get name () {
    return this.organization?.parent?this.organization.parent?.name:this?.organization?.name
  }

  @column()
  public description: string
  @column()
  public detailType: number

  @column()
  public title: string

  @column()
  public organizationId: number
  @column()
  public startDate: string
  @column()
  public endDate: string
  
  @column.dateTime({ autoCreate: true })
  public created: DateTime

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updated: DateTime

  @hasMany(() => AppMedia,{
    localKey: 'id',
    foreignKey: 'mediaFor'
  })
  public medias: HasMany<typeof AppMedia>

  @belongsTo(() => Organization,{
    localKey: 'id',
    foreignKey: 'organizationId',
    onQuery: (query) =>{
      if (!query.isRelatedSubQuery) {
          query.preload('parent')
      }
    }
  })
  public organization: BelongsTo<typeof Organization>

}
