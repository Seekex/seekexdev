import { DateTime } from 'luxon'
import { BaseModel, column,belongsTo,BelongsTo, computed, hasMany,HasMany } from '@ioc:Adonis/Lucid/Orm'
import User from './User'
import AppSetting from './AppSetting'
import ReferralHistory from './ReferralHistory'
import AppUserEvent from './AppUserEvent'
// const appActions = {
//   "Home":"com.seekx.module.home.HomeActivity"
// }
const eventActionsDatas = {
  0:{
    name:"Dashboard",
    activity:"com.seekx.module.home.HomeActivity",
    fragment:"DashboardFragment"
  },
  1:{
    name:"Video",
    activity:"com.seekx.module.home.HomeActivity",
    fragment:"HomeFragment"
  },2:{
    name:"Post",
    activity:"com.seekx.module.home.HomeActivity",
    fragment:"TimeLineFragment"
  },
  3:{
    name:"Expert",
    activity:"com.seekx.module.home.HomeActivity",
    fragment:"ExpertFragment"
  },
  4:{
    name:"Appointment Setting",
    activity:"com.seekx.module.home.HomeActivity",
    fragment:"AppointmentSetting"
  },
  5:{
    name:"Edit profile",
    activity:"com.seekx.module.home.HomeActivity",
    fragment:"EditProfileFragment"
  },
  6:{
    name:"Wallet Transaction",
    activity:"com.seekx.module.home.HomeActivity",
    fragment:"WalletTransactionFragment"
  },
  7:{
    name:"Inbox",
    activity:"com.seekx.module.home.HomeActivity",
    fragment:"InboxFragment"
  },
  8:{
    name:"Call Histories",
    activity:"com.seekx.module.home.HomeActivity",
    fragment:"CallHistoryFragment"
  },
  9:{
    name:"Request",
    activity:"com.seekx.module.home.HomeActivity",
    fragment:"RequestFragment"
  }
}

const campaignTypes = {"1":"Show","2":"Sent"}
const campaignStatus = {"0":"For Show","1":"Start","2":"Sending","3":"Done"}
const notificationStatus={0:"sent",1:"delivered",2:"read"}

export default class AudienceCampaign extends BaseModel {
  @column({ isPrimary: true })
  public id: number

  @column({serializeAs:"filtersData"})
  public filtersData:string
  @column({serializeAs:"addAmount"})
  public addAmount:boolean
  @column({serializeAs:"amountValue"})
  public amountValue:number
  @column()
  public coupon:boolean
  @column({serializeAs:"couponCode"})
  public couponCode:string
  @column({serializeAs:"notifyTitle"})
  public notifyTitle:string
  @column({serializeAs:"notifyMsg"})
  public notifyMsg:string
  @column()
  public image:string
  @column()
  public action:string
  @column()
  public actionId:number
   @column({serializeAs:"createdBy"})
  public createdBy:number
  @column()
  public type:number
  @column()
  public status:number
 @column({serializeAs:"totalUsers"})
  public totalUsers:number
  @column.dateTime({ autoCreate: true })
  public created: DateTime

  @computed()
  public get campaignStatus () {
    return campaignTypes[this.type]
  }
  @computed()
  public get campaignSentStatus () {
    return campaignStatus[this.status]
  }
  @computed()
  public get notifyAction () {
    return eventActionsDatas[this.action]?.name||'NA';
  }
  
}
