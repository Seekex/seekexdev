import { DateTime } from 'luxon'
import { BaseModel, column } from '@ioc:Adonis/Lucid/Orm'

export default class WithoutRegUser extends BaseModel {
  @column({ isPrimary: true })
  public id: number
  @column({serializeAs:'deviceId'})
  public deviceId: string
  @column({serializeAs:'fcm'})
  public fcm: string

  @column()
  public uid: number

  @column.dateTime({ autoCreate: true })
  public created : DateTime

  @column()
  public status: boolean

  public static async getUserFcm(uid){
    const fcm:any = await this.query().where("uid",uid).where('status',false).orderBy("id","desc").first()
    return fcm
  }
}
