import { DateTime } from 'luxon'
import { BaseModel, column } from '@ioc:Adonis/Lucid/Orm'

export default class AppControl extends BaseModel {
  @column({ isPrimary: true })
  public id: number

  @column()
  public isactive : boolean
  @column()
  public type: number
  @column()
  public value:any
  
  @column.dateTime({ autoCreate: true })
  public created: DateTime
}
