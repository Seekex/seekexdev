import { DateTime } from 'luxon'
import { BaseModel, column } from '@ioc:Adonis/Lucid/Orm'

export default class CmsPage extends BaseModel {
  public static table = 'cms_pages'

  @column({ isPrimary: true })
  public id: number

  @column()
  public name:string
  @column()
  public slug:string

  @column()
  public content:string

  @column()
  public status:boolean

  @column({serializeAs:'createdBy'})
  public createdBy: number

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updated: DateTime

  @column.dateTime({ autoCreate: true })
  public created: DateTime
}
