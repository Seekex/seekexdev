import { DateTime } from 'luxon'
import { BaseModel, column } from '@ioc:Adonis/Lucid/Orm'

export default class PayloadData extends BaseModel {
  public static table = 'payload_datas'
  @column({ isPrimary: true })
  public id: number
  @column()
  public uid: number
  @column()
  public payload : string
  @column()
  public amount : number
  @column({serializeAs:'fundAccountId'})
  public fundAccountId : string
  @column()
  public status : string
  @column.dateTime({ autoCreate: true })
  public created: DateTime

}
