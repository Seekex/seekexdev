import { DateTime } from 'luxon'
import { BaseModel, column, beforeSave,computed,manyToMany,ManyToMany} from '@ioc:Adonis/Lucid/Orm'
import Hash from '@ioc:Adonis/Core/Hash'
const fs = require('fs')
const Groups={1:'Sales',2:'Marketing'}
import { string } from '@ioc:Adonis/Core/Helpers'

export default class AclList extends BaseModel {

  @column({ isPrimary: true })
  public id: number

  @column()
  public controller: string

  @column()
  public action: string

  @column()
  public url: string

  @column()
  public name: string
  
 @column({serializeAs:'displayName'})
  public displayName: string
  
  @column()
  public method: string

  @column.dateTime({ autoCreate: true })
  public created: DateTime
  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updated: DateTime

  @computed()
  public get actionName(){
    return string.capitalCase(this.displayName?this.displayName:this.action)
  }

  @manyToMany(() => AclList,{
    localKey: 'id',
    pivotForeignKey: 'acl_list_id',
    relatedKey: 'id',
    pivotRelatedForeignKey: 'acl_group_id',
    pivotTable: 'group_acls',
    pivotColumns: ['created']
  })
  public groups: ManyToMany<typeof AclList>

  public static async aclListInFile(){
    const aclList = await this.all()
      const allList:any={}
      for await (const elm of aclList) {
        const nelm=elm.serialize()
        const key=nelm?.controller
        var cList:any=[]
        cList=allList[key]?allList[key]:[]
        if(cList && cList.length>0){
          cList[cList.length]=nelm
        }else{
          cList[0]=nelm
        }
        allList[key]=cList
      }
      fs.writeFile('./public/acl.json', JSON.stringify(allList), err => {
        if (err) {
          console.error(err)
          return
        }
      })
    return allList
  }

  public static async getList(){
    const aclList = await this.all()
      var allList:any={}
      for (const elm of aclList) {
        const nelm=elm.serialize()
       // nelm.actionName=string.capitalCase(nelm.action)
        const key=nelm?.controller
        var cList:any=[]
        cList=allList[key]?allList[key]:[]
        if(cList && cList.length>0){
          cList[cList.length]=nelm
        }else{
          cList[0]=nelm
        }
        allList[key]=cList
      }
    return allList
  }

}
