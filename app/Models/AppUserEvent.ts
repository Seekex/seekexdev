import { DateTime } from 'luxon'
import { BaseModel, column} from '@ioc:Adonis/Lucid/Orm'
const eventName = {
    1:'ExpertCall',
    2:'Message',
    5:'ScheduleSetting',
    6:'ProfileCompletion',
    7:'SocialMediaHandlesFacebook',
    8:'SocialMediaHandlesLinkedin',
    9:'SocialMediaHandlesInstagram',
    10:'SocialMediaHandlesYoutube',
    12:'FirstTimePost',
    13:'FirstTimeRequest',
    14:'ProfileMedia',
    15:'ProfileResume',
    16:'ProfileIntro',
    17:'LikeAPost',
    18:'FollowAPost',
    19:'CommentOnPost',
    20:'Search'
  }
const eventNameForAdmin = {
  1:'ExpertCall',
  2:'Message',
  5:'ScheduleSetting',
  6:'ProfileCompletion',
  7:'SocialMediaHandlesFacebook',
  8:'SocialMediaHandlesLinkedin',
  9:'SocialMediaHandlesInstagram',
  10:'SocialMediaHandlesYoutube',
  12:'FirstTimePost',
  13:'FirstTimeRequest',
  14:'ProfileMedia',
  15:'ProfileResume',
  16:'ProfileIntro',
  17:'LikeAPost',
  18:'FollowAPost',
  19:'CommentOnPost',
  20:'Search'
}
const eventActions = {
  "Home":"com.seekx.module.home.HomeActivity",
  "Post":"1",
  "Video":"2",
  "Expert":"3"
}

const eventActionsDatas = {
  0:{
    name:"Dashboard",
    activity:"com.seekx.module.home.HomeActivity",
    fragment:"DashboardFragment"
  },
  1:{
    name:"Video",
    activity:"com.seekx.module.home.HomeActivity",
    fragment:"HomeFragment"
  },2:{
    name:"Post",
    activity:"com.seekx.module.home.HomeActivity",
    fragment:"TimeLineFragment"
  },
  3:{
    name:"Expert",
    activity:"com.seekx.module.home.HomeActivity",
    fragment:"ExpertFragment"
  },
  4:{
    name:"Appointment Setting",
    activity:"com.seekx.module.home.HomeActivity",
    fragment:"AppointmentSetting"
  },
  5:{
    name:"Edit profile",
    activity:"com.seekx.module.home.HomeActivity",
    fragment:"EditProfileFragment"
  },
  6:{
    name:"Wallet Transaction",
    activity:"com.seekx.module.home.HomeActivity",
    fragment:"WalletTransactionFragment"
  },
  7:{
    name:"Inbox",
    activity:"com.seekx.module.home.HomeActivity",
    fragment:"InboxFragment"
  },
  8:{
    name:"Call Histories",
    activity:"com.seekx.module.home.HomeActivity",
    fragment:"CallHistoryFragment"
  },
  9:{
    name:"Request",
    activity:"com.seekx.module.home.HomeActivity",
    fragment:"RequestFragment"
  }
}

export default class AppUserEvent extends BaseModel {

  @column({ isPrimary: true })
  public id: number

  @column({serializeAs:'userId'})
  public eventId : number

  @column({serializeAs:'userId'})
  public eventName : string

  @column()
  public status : boolean

  @column({serializeAs:'userId'})
  public userId : number
  
  @column.dateTime({ autoCreate: true })
  public created: DateTime

  public static async eventsName(){
    return eventNameForAdmin
  }


  public static async eventsActions(){
    return eventActionsDatas
  }

}
