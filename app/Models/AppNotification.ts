import { DateTime } from 'luxon'
import { BaseModel, column } from '@ioc:Adonis/Lucid/Orm'
const notificationStatus={0:"sent",1:"delivered",2:"read"}
export default class AppNotification extends BaseModel {
  @column({ isPrimary: true })
  public id: number
  
  @column()
  public message:string
  @column()
  public payload:string
  @column({serialize:(value)=>{
    return value?true:false
  }})
  public status :boolean
  @column()
  public title:string
  @column()
  public type:number
  @column()
  public uid:number
  @column({serializeAs:'targetId'})
  public targetId:number
  
  @column.dateTime({ autoCreate: true })
  public created: DateTime

  public static async getExistNotify(uid,type,targetId){
    const notify = await this.query().where({uid:uid,type:type,targetId:targetId}).first()
    return notify
  }
}
