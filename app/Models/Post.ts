import { DateTime } from 'luxon'
import { BaseModel, column,manyToMany,ManyToMany,hasMany,HasMany,belongsTo,BelongsTo,computed } from '@ioc:Adonis/Lucid/Orm'
import User from 'App/Models/User'
import AppMedia from 'App/Models/AppMedia'
import Reported from './Reported'
import truncatise from 'truncatise'
import Comment from './Comment'
import LikePost from './LikePost'

export default class Post extends BaseModel {
  public static table = 'posts'
  @column({ isPrimary: true })
  public id:number

  @column()
  public comments:number
 
  @column()
  public description:string
  @column({serialize:(value)=>{
    return value?value:0
  }})
  public followers:number
  @column({serialize:(value)=>{
    return value?value:0
  }})
  public likes:number
  @column()
  public location:string
  @column({serialize:(value)=>{
    return value?true:false
  }})
  public privates:boolean
  
  @column({serialize:(value)=>{
    return value?value:''
  }})
  public tagged_user:number
  @column({serialize:(value)=>{
    return value?value:0
  }})
  public views:number
  @column({serializeAs:'hashtags',serialize:(value)=>{
    return value?value:''
  }})
  public hash_tags:string

  @column({columnName:'created_by'})
  public createdBy:number
  @column({serialize:(value)=>{
    return value?value:0
  }})
  public status:number

  @column.dateTime({ autoCreate: true })
  public created: DateTime
  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updated: DateTime


  @computed()
  public get uid () {
    return this.createdBy
  }

  @computed()
  public get title () {
    return truncatise(this.description, { TruncateLength: 50 })
  }
  @computed()
  public get content () {
    return this.description
  }
  @computed()
  public get name () {
    return this.user?this.user.name:''
  }
  @computed()
  public get profileImage () {
    return this.user?.profilePic||''
  }
  @column({serialize:(value)=>{
    return value?true:false
  }})
  public likestatus:boolean
  @column({serialize:(value)=>{
    return value?true:false
  }})
  public followstatus:boolean
  @computed()
  public get verified () {
    return this.user?.verified?true:false
  }

  @column({serialize:(value)=>{
    return value?true:false
  }})
  public userfollowstatus:boolean
  // @computed()
  // public get profileImage () {
  //   return this.user?.profilePic||''
  // }


  @computed()
  public get userOcuEdu () {
    //return this.user?this.user.profilePic:''
    var oeStr=''
    if(this.user?.occupation){
      oeStr+=this.user?.occupation.name
    }
    if(this.user?.currentOrg){
      oeStr+=' at '+this.user?.currentOrg.name
    }
    if(this.user?.highestEducation){
      oeStr+='\n'+this.user?.highestEducation?.name
    }
    if(this.user?.collegeUniversity){
      oeStr+=' from '+this.user?.collegeUniversity?.name
    }
    return oeStr
  }

  @computed()
  public get occupation () {
    //return this.user?this.user.profilePic:''
      var oeStr=''
      if(this.user?.occupation){
        oeStr+=this.user?.occupation.name
      }
      if(this.user?.currentOrg){
        oeStr+=' at '+this.user?.currentOrg.name
      }
       return oeStr
  }

  @computed()
  public get highestEdu () {
    //return this.user?this.user.profilePic:''
      var oeStr=''
        if(this.user?.highestEducation){
          oeStr+=this.user?.highestEducation?.name
        }
        if(this.user?.collegeUniversity){
          oeStr+=' from '+this.user?.collegeUniversity?.name
        }

       return oeStr
  }


  @hasMany(() => AppMedia,{
    localKey: 'id',
    foreignKey: 'mediaFor',
    onQuery:(query)=>{
      if (!query.isRelatedSubQuery) {
        query.select('url').where('media_type',5)
      }
    }
  })
  public medias: HasMany<typeof AppMedia>

  @belongsTo(() => User,{
    localKey: 'id',
    foreignKey: 'createdBy',
  })
  public user: BelongsTo<typeof User>

  @manyToMany(() => User, {
    pivotTable: 'tagged_users',
    localKey: 'id',
    pivotForeignKey: 'post_id',
    relatedKey: 'id',
    pivotRelatedForeignKey: 'uid',
  })
  public userTagged: ManyToMany<typeof User>

  @hasMany(() => Reported,{
    localKey: 'id',
    foreignKey: 'targetId',
    onQuery: (query) =>{
      if (!query.isRelatedSubQuery) {
        query.where('type', 1)
      }
    }
  })
  public reported: HasMany<typeof Reported>

  @hasMany(() => Comment,{
    localKey: 'id',
    foreignKey: 'postId'
  })
  public comment: HasMany<typeof Comment>

  @hasMany(() => LikePost,{
    localKey: 'id',
    foreignKey: 'targetId'
  })
  public postLikes: HasMany<typeof LikePost>

}
