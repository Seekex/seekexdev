import { DateTime } from 'luxon'
import { BaseModel, column, scope,belongsTo,BelongsTo,computed,hasMany,HasMany, hasOne,HasOne} from '@ioc:Adonis/Lucid/Orm'
import User from './User'
const types:any={1:"Occupation",2:"Reason",3:"Skills",4:"Education",5:"Interest",6:"Hashtag",7:'Orgnization',8:'Country/State',9:"College/University"}
export default class MasterGodown extends BaseModel {
  @column({ isPrimary: true })
  public id: number
  @column({serializeAs:'createdBy',serialize:(value)=>{
    value?value:0
  }})
  public createdBy: number
  
  @column({serializeAs:'imageUrl'})
  public imageUrl : string
  @column({serializeAs:'masterType'})
  public masterType : number
  @column()
  public name : string
  @column({serializeAs:'parentId'})
  public parentId : number
  @column()
  public used : number
  @column({serializeAs:'isVerified'})
  public isVerified : boolean

  @column.dateTime({ autoCreate: true })
  public created: DateTime

  public static isVerified = scope((query:any) => {
    query.where('isVerified',true)
  })

  @computed()
  public get tagType () {
    return types[this.masterType]
   }

  @belongsTo(() => User,{
    localKey: 'id',
    foreignKey: 'createdBy'
  })
  public user: BelongsTo<typeof User>

  @belongsTo(() => MasterGodown,{
    localKey: 'id',
    foreignKey: 'parentId',
  })
  public parent: BelongsTo<typeof MasterGodown>

  public static allTypes(){
    delete types[7]
    delete types[2]
    delete types[8]
    return types
  }

  public static findType(tag){
    return Object.keys(types).find(key => types[key] === tag)
  }

  public static async getIDByName(name){
    const tagRef= await this.query().where('name','LIKE',"%"+name+"%").first()
    return tagRef?.id
  }
  public static async getOccupations(){
    const tagRef= await (await this.query().where('parentId',0).where('masterType',1).apply((scopes) => scopes.isVerified())).map(elm=>{
      return elm?.name
    })
    return tagRef
  }
  public static async getSkills(){
    const tagRef= await (await this.query().where('parentId',0).where('masterType',3).apply((scopes) => scopes.isVerified())).map(elm=>{
      return elm?.name
    })
    return tagRef
  }

  public static async getExpertises(){
    const tagRef= await (await this.query().where('parentId',0).where('masterType',5).apply((scopes) => scopes.isVerified())).map(elm=>{
      return elm?.name
    })
    return tagRef
  }

  public static async getEducations(){
    const tagRef= await (await this.query().where('parentId',0).where('masterType',4).apply((scopes) => scopes.isVerified())).map(elm=>{
      return elm?.name
    })
    return tagRef
  }
}
