import { DateTime } from 'luxon'
import { BaseModel, column,hasMany, HasMany,belongsTo,BelongsTo,computed } from '@ioc:Adonis/Lucid/Orm'
import BookedSlot from './BookedSlot'
import User from './User'
import AppMedia from './AppMedia'
const days:any={"1":"Sunday","2":"Monday","3":"Tuesday","4":"Wednesday","5":"Thursday","6":"Friday","7":"Saturday"}
const allStatus = {1:'Booked',2:'Approved',3:'Completed',4:'Canceled'}
export default class Request extends BaseModel {
  public static table = 'request'

  @column({ isPrimary: true })
  public id: number
  @column({serializeAs:'callCount'})
  public callCount: number
  @column({serializeAs:'daysId'})
  public daysId: number

  @computed()
  public get days () {
    return days[this.daysId]
  }

  @computed()
  public get reqStatus(){
    return allStatus[this.status]
  }

  @column()
  public description: string
  @column({serializeAs:'estimatedCall'})
  public estimatedCall: string
  @column({serializeAs:'estimatedCost'})
  public estimatedCost: number
  @column()
  public rate: number
  @column({serializeAs:'requestedBy'})
  public requestedBy: number
  @column()
  public status: number
  @column({serializeAs:'toUid'})
  public toUid: number
  @column({serialize:(value)=>{
    return value?true:false
  }})
  public videocall: boolean

  @column({ serializeAs: "callingType" })
  public callingType: string

  @column({serialize:(value)=>{
    return value?true:false
  }})
  public view: boolean
  @column.dateTime({ autoCreate: true })
  public created: DateTime

  @column({serializeAs:'bookingDate'})
  public bookingDate: DateTime
  
  @column({serializeAs:'userProfile'})
  public userProfile:object
  @column({serializeAs:'expertSchedules'})
  public expertSchedules:object

  @column({serializeAs:'canceledBy'})
  public canceledBy: number
  @column()
  public comment: number

  @column({ serializeAs: "canceledDate" })
  public canceledDate: DateTime
  

  @hasMany(() => BookedSlot,{
    localKey: 'id',
    foreignKey: 'requestId'
  })
  public slots: HasMany<typeof BookedSlot>

  
  @belongsTo(() => User,{
    localKey: 'id',
    foreignKey: 'requestedBy',
  })
  public user: BelongsTo<typeof User>

  @belongsTo(() => User,{
    localKey: 'id',
    foreignKey: 'toUid',
  })
  public expert: BelongsTo<typeof User>

  @hasMany(() => AppMedia,{
    localKey: 'id',
    foreignKey: 'mediaFor',
    onQuery:(query)=>{
      if (!query.isRelatedSubQuery) {
        query.select('url').where('media_type',4)
      }
    }
  })
  public medias: HasMany<typeof AppMedia>

  public static async getAmount(rId){
    const requestData=await this.find(rId)
    return requestData?.estimatedCost||0
  }

  public static async completeRequest(rId){
    await this.query().where('id',rId).update({status:3,'comment':'Request completed after call'})
    await BookedSlot.query().where('requestId',rId).update({status:2})
  }
}
