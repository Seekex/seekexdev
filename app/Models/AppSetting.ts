import { DateTime } from 'luxon'
import {
  column,
  beforeSave,
  BaseModel,
} from '@ioc:Adonis/Lucid/Orm'

export default class AppSetting extends BaseModel {
  @column({ isPrimary: true })
  public id: number

  @column()
  public isactive : boolean

  @column({serializeAs:'fieldKey'})
  public fieldKey : string

  @column()
  public type : number

  @column()
  public value : any

  @column.dateTime({ autoCreate: true })
  public created : DateTime

  public static async getValueByKey(key){
    const val:any = await this.query().where("fieldKey",key).first()
    return val?.value
  }
}
