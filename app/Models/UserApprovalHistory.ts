import { DateTime } from 'luxon'
import { BaseModel, column } from '@ioc:Adonis/Lucid/Orm'

export default class UserApprovalHistory extends BaseModel {
  @column({ isPrimary: true })
  public id: number
  @column()
  public uid: number
  @column({serializeAs:'approveBy'})
  public approveBy: number
  @column()
  public comment: string
  @column()
  public status: string
  @column.dateTime({ autoCreate: true })
  public created: DateTime
  
}
