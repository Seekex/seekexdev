import { DateTime } from 'luxon'
import { BaseModel, column } from '@ioc:Adonis/Lucid/Orm'

export default class CountryState extends BaseModel {
  @column({ isPrimary: true })
  public id: number
  @column()
  public name: string
  @column({serializeAs:'parentId'})
  public parentId: number
  @column()
  public used: number
  @column()
  public created: DateTime
  
  @column({serializeAs:'createdBy'})
  public createdBy: number
  @column({serializeAs:'imageUrl'})
  public imageUrl : string
  @column({serializeAs:'masterType'})
  public masterType : number

  // @column.dateTime({ autoCreate: true })
  // public createdAt: DateTime

  public static async getCountries(){
    const tagRef= await (await this.query().where('parentId',0)).map(elm=>{
      return elm?.name
    })
    return tagRef
  }

  public static async getCountriesList(){
    const lists = await this.query().where('parentId',0)
    var cList:any={}
    for await (const elm of lists) {
      cList[elm?.id]=elm?.name
    }
    return cList
  }

  public static async getCountryIdByName(name){
    const tagRef= await this.query().where('name','LIKE',"%"+name+"%").first()
    return tagRef?.id
  }

}
