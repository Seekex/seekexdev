import { DateTime } from 'luxon'
import { BaseModel, column,belongsTo, BelongsTo,afterSave,afterDelete,hasMany,HasMany} from '@ioc:Adonis/Lucid/Orm'
export default class BlogUser extends BaseModel {
  @column({ isPrimary: true })
  public id: number
  @column()
  public name: string
  @column()
  public email: string
  @column({serializeAs:'profilePic'})
  public profilePic: string
  
  @column.dateTime({ autoCreate: true })
  public created: DateTime

}
