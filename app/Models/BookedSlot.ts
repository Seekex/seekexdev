//import { DateTime } from 'luxon'
import { BaseModel, column ,belongsTo,BelongsTo} from '@ioc:Adonis/Lucid/Orm'
import ExpertSchedule from './ExpertSchedule'
import Request from './Request'

export default class BookedSlot extends BaseModel {
  @column({ isPrimary: true })
  public id: number

  @column({serializeAs:"byUid"})
  public byUid: number
 
  @column({serializeAs:"requestId"})
  public requestId: number

  @column({serializeAs:"slotsId"})
  public slotsId: number

  @column({serializeAs:"status"})
  public status: number

  @column({serializeAs:"toUid"})
  public toUid: number

  @belongsTo(() => ExpertSchedule,{
    localKey: 'id',
    foreignKey: 'slotsId',
  })
  public schedule: BelongsTo<typeof ExpertSchedule>

  @belongsTo(() => Request,{
    localKey: 'id',
    foreignKey: 'requestId',
  })
  public bookedRequest: BelongsTo<typeof Request>
}
