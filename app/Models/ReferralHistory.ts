import { DateTime } from 'luxon'
import { BaseModel, column,afterCreate,belongsTo,BelongsTo } from '@ioc:Adonis/Lucid/Orm'
import LedgerHelper from 'App/Helpers/LedgerHelper'
import User from './User'
const Ledger = new LedgerHelper()
const codeStatus = {0:"Not Earned",1:"Earned",2:"Used",3:"Expired"}
export default class ReferralHistory extends BaseModel {
  public static table = 'referral_histories'

  @column({ isPrimary: true })
  public id: number
  @column()
  public uid: number

  @column({serializeAs:'referBy'})
  public referBy :number

  @column({serializeAs:'referTo'})
  public referTo:number

  @column({serializeAs:'referralCode'})
  public referralCode:string

  @column()
  public amount: number

  @column()
  public status:number

  @column({serializeAs:'couponType'})
  public couponType:number

  @column({serializeAs:'transectionId'})
  public transectionId:number

  @column.dateTime({ autoCreate: true })
  public created: DateTime

  @column.dateTime({serializeAs:'transectionDate',autoUpdate:true})
  public transectionDate: DateTime

  @afterCreate()
  public static async afterCreateHook (referral: ReferralHistory) {
    if(referral?.status==1){
      await Ledger.addReferalAmount(referral)
    }
  }

  @belongsTo(() => User,{
    localKey: 'id',
    foreignKey: 'referTo',
  })
  public usedUser: BelongsTo<typeof User>

  public static async referalCodeUsedCount(code){
    const userReferralUsed:any = await this.query().where((q)=>{
      q.where('referralCode',code).whereNotColumn('uid','=','refer_by')
    })
    .pojo<{ count: number}>()
    .count('id as count').first()
    return userReferralUsed?.count
  }

}
