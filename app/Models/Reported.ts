import { DateTime } from 'luxon'
import { BaseModel, column,belongsTo,BelongsTo } from '@ioc:Adonis/Lucid/Orm'
import User from './User'

export default class Reported extends BaseModel {
  public static table = 'reported'
  @column({ isPrimary: true })
  public id: number
  @column()
  public byUid: number
  @column()
  public targetId: number
  @column()
  public reasonId: number
  @column()
  public reason: string
  @column()
  public type: number
  
  @column.dateTime({ autoCreate: true })
  public created: DateTime


  @belongsTo(() => User,{
    localKey: 'id',
    foreignKey: 'byUid',
  })
  public reportedUser: BelongsTo<typeof User>
}
