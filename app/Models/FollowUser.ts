import { DateTime } from 'luxon'
import { BaseModel, column, afterSave, afterDelete,beforeDelete } from '@ioc:Adonis/Lucid/Orm'
import User from 'App/Models/User'

export default class FollowUser extends BaseModel {
  @column({ isPrimary: true })
  public id: number
  @column()
  public uid: number
  @column({serializeAs:'targetId'})
  public targetId: number
  @column.dateTime({ autoCreate: true })
  public created: DateTime

  /**********************Code for increase User Follower and Following [Start]**********************/
  // @afterSave()
  // public static async increaseUserFollower (data: FollowUser) {
  //   // await User.query().where({'id':data?.targetId}).increment('followers',1)
  //   // await User.query().where('id',data?.uid).increment('following',1)
  //   //if (data.targetId) {
      
  //     //await User.query().where('id',data?.uid).increment('following',1)
  //     // var followersuser:any = await User.find(data.targetId)
  //     // if(followersuser){
  //     //   followersuser.followers=followersuser.followers+1
  //     //   await followersuser.save()
  //     // }
  //     // var followinguser:any = await User.find(data.uid)
  //     // if(followinguser){
  //     //   followinguser.following =followinguser.following +1
  //     //   await followinguser.save()
  //     // }
      
  //   //}
  // }
  /**********************Code for increase User Follower and Following [End]**********************/

  /**********************Code for decrease User Follower and Following [Start]**********************/
  // @beforeDelete()
  // public static async decreaseUserFollower (data: FollowUser) {
  //   //  await User.query().where({'id':data?.targetId}).decrement('followers',1)
  //   //  await User.query().where('id',data?.uid).decrement('following',1)
    
  // }
  /**********************Code for decrease User Follower and Following [End]**********************/
}
