import { DateTime } from 'luxon'
import { BaseModel, column,afterSave,afterDelete } from '@ioc:Adonis/Lucid/Orm'

export default class LikeVideoComment extends BaseModel {
  @column({ isPrimary: true })
  public id: number
  @column()
  public uid: number
  @column({serializeAs:'targetId'})
  public targetId: number
  // @column({serializeAs:'userVideoId'})
  // public userVideoId: number
  @column.dateTime({ autoCreate: true })
  public created: DateTime

}
