import { DateTime } from 'luxon'
import { BaseModel, column,belongsTo,BelongsTo } from '@ioc:Adonis/Lucid/Orm'
import User from './User'

export default class SnapshotUserWallet extends BaseModel {

  @column({ isPrimary: true })
  public id: number

  @column({serializeAs: 'availableAmount'})
  public availableAmount : number

  @column({serializeAs: 'holdAmount'})
  public holdAmount : number

  @column({serializeAs: 'disputedAmount'})
  public disputedAmount : number

  @column({serializeAs: 'promoAmount'})
  public promoAmount : number

  @column()
  public isuser : boolean

  @column({serializeAs: 'totalAmount'})
  public totalAmount : number

  @column()
  public uid: number

  @column({serializeAs: 'walletId'})
  public walletId: number

  @column.dateTime({ autoCreate: true })
  public created: DateTime

  @belongsTo(() => User,{
    localKey: 'id',
    foreignKey: 'uid',
  })
  public user: BelongsTo<typeof User>
}
