import { DateTime } from 'luxon'
import { BaseModel, column,belongsTo,BelongsTo,computed,beforeSave,hasMany,HasMany} from '@ioc:Adonis/Lucid/Orm'
import truncatise from 'truncatise'
import User from 'App/Models/User'
import Slug from './Slug'
import BlogComment from './BlogComment'
import LikeBlog from './LikeBlog'

export default class Blog extends BaseModel {
  public static table = 'blogs'

  @column({ isPrimary: true })
  public id: number

  @column()
  public title:string
  @column()
  public slug:string

  @column()
  public content:string

  @computed()
  public get images () {
    var rex = /<img[^>]+src="(https:\/\/[^">]+)"/g
		var urls:any=[]
		var thumbnail:any
		while ( thumbnail = rex.exec( this.content ) ) {
			urls.push( thumbnail[1] )
		}
    return urls
  }

  @computed()
  public get shortDescription () {
    return truncatise(this.content.replace(/<img[^>]*>/g,""), { TruncateLength: 30 })
  }


  @column({serializeAs:'seoTitle'})
  public seoTitle:string
  @column({serializeAs:'seoKeywords'})
  public seoKeywords:string
  @column({serializeAs:'seoDescription'})
  public seoDescription:string

  @column()
  public status:boolean

  @column({serializeAs:'createdBy'})
  public createdBy: number

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updated: DateTime

  @column.dateTime({ autoCreate: true })
  public created: DateTime

  @beforeSave()
  public static async generateSlug(blog: Blog) {
    if (blog.$dirty.title) {
      blog.slug = await Slug.make(this, blog.title)
    }
  }

  @belongsTo(() => User,{
    localKey: 'id',
    foreignKey: 'createdBy',
  })
  public user: BelongsTo<typeof User>

  @hasMany(()=>BlogComment,{
    localKey: 'id',
    foreignKey: 'blogId',
  })
  public comments: HasMany<typeof BlogComment>

  @hasMany(()=>LikeBlog,{
    localKey: 'id',
    foreignKey: 'blogId',
  })
  public likes: HasMany<typeof LikeBlog>
}
