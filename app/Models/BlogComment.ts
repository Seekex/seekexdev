import { DateTime } from 'luxon'
import { BaseModel, column,belongsTo, BelongsTo,afterSave,afterDelete,hasMany,HasMany} from '@ioc:Adonis/Lucid/Orm'
import BlogUser from 'App/Models/BlogUser'
export default class BlogComment extends BaseModel {
  @column({ isPrimary: true })
  public id: number
  @column()
  public parentId: number
  @column()
  public comment: string
  @column()
  public likes: number
  @column({serializeAs:'blogId'})
  public blogId: number
  @column()
  public uid: number
  @column()
  public likestatus: boolean
  @column()
  public name: string
  @column({serializeAs:'profilePic'})
  public profilePic: string
  @column.dateTime({ autoCreate: true })
  public created: DateTime

  

  @belongsTo(() => BlogUser,{
    localKey: 'id',
    foreignKey: 'uid',
  })
  public user: BelongsTo<typeof BlogUser>
  
  @hasMany(()=>BlogComment,{
    localKey: 'id',
    foreignKey: 'parentId',
  })
  public childComments: HasMany<typeof BlogComment>
}
