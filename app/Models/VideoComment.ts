import { DateTime } from 'luxon'
import { BaseModel, column,belongsTo, BelongsTo,afterSave,afterDelete,hasMany,HasMany} from '@ioc:Adonis/Lucid/Orm'
import User from 'App/Models/User'

export default class VideoComment extends BaseModel {
  @column({ isPrimary: true })
  public id: number
  @column()
  public parentId: number
  @column()
  public comment: string
  @column({serializeAs:'userVideoId'})
  public userVideoId: number
  @column()
  public uid: number
  @column()
  public likestatus: boolean
  @column()
  public name: string
  @column({serializeAs:'profilePic'})
  public profilePic: string
  @column.dateTime({ autoCreate: true })
  public created: DateTime

  @belongsTo(() => User,{
    localKey: 'id',
    foreignKey: 'uid',
  })
  public user: BelongsTo<typeof User>

  @hasMany(()=>VideoComment,{
    localKey: 'id',
    foreignKey: 'parentId',
  })
  public parents: HasMany<typeof VideoComment>
}
