import { DateTime } from 'luxon'
import { BaseModel, column, afterSave, afterDelete } from '@ioc:Adonis/Lucid/Orm'
import Post from 'App/Models/Post'

export default class FollowPost extends BaseModel {

  @column({ isPrimary: true })
  public id: number
  @column()
  public uid: number
  @column({serializeAs:'targetId'})
  public targetId: number
  @column.dateTime({ autoCreate: true })
  public created: DateTime

  /**********************Code for increase post Follower [Start]**********************/
  @afterSave()
  public static async increasePostFollower (data: FollowPost) {
    if (data.targetId) {
      var post:any = await Post.find(data.targetId)
      post.followers=post.followers+1
      await post.save()
    }
  }
  /**********************Code for increase post Follower [End]**********************/

  /**********************Code for decrease post Follower [Start]**********************/
  @afterDelete()
  public static async decreasePostFollower (data: FollowPost) {
    if (data.targetId) {
      var post:any = await Post.find(data.targetId)
      post.followers=post.followers>0?post.followers-1:0
      await post.save()
    }
  }
  /**********************Code for decrease post Follower [End]**********************/

}
