import { DateTime } from 'luxon'
import { BaseModel, column, afterSave, afterDelete} from '@ioc:Adonis/Lucid/Orm'
//import Post from 'App/Models/Post'
export default class LikePost extends BaseModel {
  @column({ isPrimary: true })
  public id: number
  @column()
  public uid: number
  @column({serializeAs:'targetId'})
  public targetId: number
  @column.dateTime({ autoCreate: true })
  public created: DateTime

  /**********************Code for increase post like [Start]**********************/
  // @afterSave()
  // public static async increasePostLike (data: LikePost) {
  //   if (data.targetId) {
  //     var post:any = await Post.find(data.targetId)
  //     post.likes=post.likes+1
  //     await post.save()
  //   }
  // }
  /**********************Code for increase post like [End]**********************/

  /**********************Code for decrease post like [Start]**********************/
  // @afterDelete()
  // public static async decreasePostLike (data: LikePost) {
  //   if (data.targetId) {
  //     var post:any = await Post.find(data.targetId)
  //     post.likes=post.likes>0?post.likes-1:0
  //     await post.save()
  //   }
  // }
  /**********************Code for decrease post like [End]**********************/
}
