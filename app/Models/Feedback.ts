import { DateTime } from 'luxon'
import { BaseModel, column,belongsTo,BelongsTo } from '@ioc:Adonis/Lucid/Orm'
import User from './User'

export default class Feedback extends BaseModel {
  
  @column({ isPrimary: true })
  public id: number
  @column()
  public uid: number
  @column()
  public description: string

  @column.dateTime({ autoCreate: true })
  public created: DateTime

  @belongsTo(() => User,{
    localKey: 'id',
    foreignKey: 'uid',
  })
  public user: BelongsTo<typeof User>

}
