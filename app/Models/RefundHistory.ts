import { DateTime } from 'luxon'
import { BaseModel, column ,belongsTo,BelongsTo} from '@ioc:Adonis/Lucid/Orm'
import User from './User'
export default class RefundHistory extends BaseModel {
  @column({ isPrimary: true })
  public id: number

  @column({serializeAs:'refundId'})
  public refundId: number

  @column({serializeAs:'approveBy'})
  public approveBy: number

  @column()
  public comment: string

  @column()
  public status: string

  @column()
  public type: string

  
   @column({serializeAs:'sendTo'})
  public sendTo: string

  @column.dateTime({ autoCreate: true })
  public created: DateTime

  @belongsTo(() => User,{
    localKey: 'id',
    foreignKey: 'sendTo',
  })
  public sent_user: BelongsTo<typeof User>

}
