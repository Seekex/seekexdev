import { DateTime } from 'luxon'
import { BaseModel, column,belongsTo,BelongsTo,computed } from '@ioc:Adonis/Lucid/Orm'
import User from './User'
import CallHistory from './CallHistory'
const refundStatusList={1:'Applied',2:'Approved',3:'Rejected'}

export default class Refund extends BaseModel {
  @column({ isPrimary: true })
  public id: number
  @column({serializeAs:'transactionHistoryId'})
  public transactionHistoryId: number
  @column({serializeAs:'callId'})
  public callId : number
  @column()
  public amt: number
  @column({serializeAs:'appliedBy'})
  public appliedBy: number
  
  @column({serializeAs:'appliedFor'})
  public appliedFor: number
  @column({serializeAs:'expertRefundAmount'})
  public expertRefundAmount: number
  @column()
  public reason: string
  @column()
  public status: number
  @column({serializeAs:'expertApprovalStatus'})
  public expertApprovalStatus: number

  @column({serializeAs:'autoRefund'})
  public autoRefund: number

  @column()
  public type: number

  @column()
  public headline: string

  @computed()
  public get refundStatus(){
    return refundStatusList[this.status]
  }

  @column.dateTime({ autoCreate: true })
  public created: DateTime
  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public modified: DateTime

  @belongsTo(() => User,{
    localKey: 'id',
    foreignKey: 'appliedBy',
  })
  public appliedByUser: BelongsTo<typeof User>

  @belongsTo(() => User,{
    localKey: 'id',
    foreignKey: 'appliedFor',
  })
  public appliedForUser: BelongsTo<typeof User>

  @belongsTo(() => CallHistory,{
    localKey: 'id',
    foreignKey: 'callId',
    onQuery:(query)=>{
     // query.whereNot('duration',0)
    }
  })
  public callDetail: BelongsTo<typeof CallHistory>
  
}
