import { DateTime } from 'luxon'
import { BaseModel, column, computed } from '@ioc:Adonis/Lucid/Orm'
import truncatise from 'truncatise'
export default class Faq extends BaseModel {
  @column({ isPrimary: true })
  public id: number

  @column()
  public question: string
  @column()
  public answere: string
  @column()
  public views: string
  @column()
  public type: string
  @column({serializeAs:'faqFor'})
  public faqFor: string
  @column()
  public section: string
  @column()

  public status:boolean
  @column.dateTime({ autoCreate: true })
  public created: DateTime

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updated: DateTime

  @computed()
  public get questionExcerpt () {
    return truncatise(this.question, { TruncateLength: 50 })
  }

  @computed()
  public get answereExcerpt () {
    return truncatise(this.answere, { TruncateLength: 50 })
  }

  @computed()
  public get typeText () {
    const type=["Protection","Operations"]
    return type[this.type]
  }
  @computed()
  public get faqForText () {
    const faqfor=["Both","Seeker","Expert"]
    return faqfor[this.faqFor]
  }
  @computed()
  public get sectionText () {
    const subsections=["General","Payments and deposits","Refund policies","Performance and visibility"]
    return subsections[this.section]
  }
  
  public static async getSubSection(){
    const subsections=["General","Payments and deposits","Refund policies","Performance and visibility"]
    return subsections
  }

  public static async getFaqFor(){
    const faqfor=["Both","Seeker","Expert"]
    return faqfor
  }

  public static async getType(){
    const type=["Protection","Operations"]
    return type
  }

}
