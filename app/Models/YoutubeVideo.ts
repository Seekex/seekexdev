import { DateTime } from 'luxon'
import { BaseModel, column, beforeSave,computed} from '@ioc:Adonis/Lucid/Orm'
import Hash from '@ioc:Adonis/Core/Hash'

export default class YoutubeVideo extends BaseModel {
  @column({ isPrimary: true })
  public id: number
  @column()
  public uid: number
  @column()
  public title: string
  @column()
  public description: string
  @column({serializeAs:'videoId'})
  public videoId: string
  @column({serializeAs:'videoThumbnails',serialize:(value)=>{
    return value?JSON.parse(value):[]
  }})
  public videoThumbnails: string
  @column({serializeAs:'viewCount'})
  public viewCount: number
  @column({serializeAs:'videoLength'})
  public videoLength: string
  @column.dateTime({ autoCreate: true })
  public created : DateTime

}
