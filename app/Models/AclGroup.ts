import { DateTime } from 'luxon'
import { BaseModel, column, beforeSave,computed,manyToMany,ManyToMany} from '@ioc:Adonis/Lucid/Orm'
import Hash from '@ioc:Adonis/Core/Hash'
import GroupAcl from './GroupAcl'
import AclList from './AclList'
const fs = require('fs')
const Groups={1:'Sales',2:'Marketing'}
import { string } from '@ioc:Adonis/Core/Helpers'
export default class AclGroup extends BaseModel {

  @column({ isPrimary: true })
  public id: number

  @column()
  public name: string

  @column.dateTime({ autoCreate: true })
  public created: DateTime
  // @column.dateTime({ autoCreate: true, autoUpdate: true })
  // public updated: DateTime

  @manyToMany(() => AclList,{
    localKey: 'id',
    pivotForeignKey: 'acl_group_id',
    relatedKey: 'id',
    pivotRelatedForeignKey: 'acl_list_id',
    pivotTable: 'group_acls',
    pivotColumns: ['created']
  })
  public acls: ManyToMany<typeof AclList>

  public static async getGroup(id){
    const group = await this.query().where('id',id).first()
    return group?.serialize()
  }
  public static async getGroupList(){
    // const groups = await (await this.all()).map(elm=>{
    //   return elm?.name
    // })
    const groups =await this.all()
    return groups
  }

  public static async getGroupAcls(id){
    const group = await this.query().preload('acls').where('id',id).first()
    var groupAcls:any = group?.acls
    if(groupAcls){
      groupAcls = await (groupAcls.map(elm=>{
          return elm?.url
      }))
    }else{
      groupAcls=[]
    }
   return groupAcls
  }

  public static async getAclListForGroup(gid){
    const aclList = await AclList.all()
    var groupAclList:any
      if(gid){
        groupAclList = await (await GroupAcl.query().where('acl_group_id',gid)).map(elm=>{
          return elm?.acl_list_id
        })
        //return groupAclList
      }
      var allList:any={}
      for (const elm of aclList) {
        const nelm=elm.serialize()
       // nelm.actionName=string.capitalCase(nelm?.action)
        if(groupAclList.indexOf(nelm?.id)>-1){
          nelm.selected=true
        }else{
          nelm.selected=false
        }
        const key=nelm?.controller
        var cList:any=[]
        cList=allList[key]?allList[key]:[]
        if(cList && cList.length>0){
          cList[cList.length]=nelm
        }else{
          cList[0]=nelm
        }
        allList[key]=cList
      }
    return allList
  }
}
