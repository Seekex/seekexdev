import { DateTime } from 'luxon'
import { BaseModel, column, beforeSave,computed} from '@ioc:Adonis/Lucid/Orm'
import Hash from '@ioc:Adonis/Core/Hash'
const Groups={1:'Sales',2:'Marketing'}
export default class Agent extends BaseModel {
  @column({ isPrimary: true })
  public id: number
  @column()
  public uid: number

  @column({serialize:(value)=>{
    return 'agent'
  },serializeAs:"userRole"})
  public userRole: string

  @column()
  public name: string
  @column()
  public email: string
 
  @column({ serializeAs: null })
  public password: string
  @column({
    serialize:(value)=>{
    return value?value:''
  }
  })
  public mobile: string

  @column({ serializeAs: 'profilePic' ,serialize:(value)=>{
    return value?value:''
  }})
  public profilePic: string


  @column({
    serialize:(value)=>{
      return value?value:''
    }
  })
  public dob: string

  @column({ serializeAs: 'aboutMe',serialize:(value)=>{
    return value?value:''
  } })
  public aboutMe: string

  @column({serialize:(value)=>{
    return value?value:0
  }})
  public age: number

 

  @column({serialize:(value)=>{
    return value?value:0
  }})
  public status: number

  @column({serialize:(value)=>{
    return value?value:0
  },serializeAs:'agentGroup'})
  public agentGroup: number
  
  @computed()
  public get group () {
    return Groups[this.agentGroup]
   }
  
  @column({serialize:(value)=>{
    return value?value:''
  },serializeAs:"timeZone"})
  public timeZone: string

  @column()
  public remember_me_token: string

  @column.dateTime({ autoCreate: true })
  public created: DateTime

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updated: DateTime

  @column({serialize:(value)=>{
    return value?value:''
  },serializeAs:'referralCode'
  })
  public referralCode:string

  @column({serialize:(value)=>{
    return value?value:''
  },serializeAs:'contactId'
  })
  public contactId:string

  @beforeSave()
  public static async hashPassword (user: Agent) {
    if (user.$dirty.password) {
      user.password = await Hash.make(user.password)
      user.remember_me_token=await Hash.make(user.email)
    }
    
  }
  
  public static allGroups(){
    return Groups
  }

}
