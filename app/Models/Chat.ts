import { DateTime } from 'luxon'
import { BaseModel, column,belongsTo, BelongsTo } from '@ioc:Adonis/Lucid/Orm'
import User from 'App/Models/User'
export default class Chat extends BaseModel {
  public static table = 'chats'
  @column({ isPrimary: true })
  public id: number
  @column({serialize:(value)=>{
    return value==1?true:false
  }})
  public media:boolean
  @column()
  public message:string
  @column({serializeAs:'receiverId'})
  public receiverId:number
  @column({serializeAs:'senderId'})
  public senderId:number
  @column()
  public size:string
  @column()
  public status:number
  @column()
  public type:number
  @column()
  public url:string

  @column.dateTime({serializeAs:'timeStamp',serialize:(value)=>{
    return value
    //?DateTime.fromISO(value).toFormat('dd, LLL yyyy t'):''
  },autoCreate: true})
  public timeStamp:DateTime

  @column.dateTime({ autoCreate: true })
  public created: DateTime

  @belongsTo(() => User,{
    localKey: 'id',
    foreignKey: 'senderId',
  })
  public sender_user: BelongsTo<typeof User>

  @belongsTo(() => User,{
    localKey: 'id',
    foreignKey: 'receiverId',
  })
  public receiver_user: BelongsTo<typeof User>

}
