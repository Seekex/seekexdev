import { DateTime } from 'luxon'
import { BaseModel, column } from '@ioc:Adonis/Lucid/Orm'

export default class AppMedia extends BaseModel {
  @column({ isPrimary: true })
  public id: number
  @column()
  public mediaFor: number
  @column()
  public mediaType: number
  @column()
  public url: string
  
  @column.dateTime({ autoCreate: true })
  public created: DateTime
}
