import { DateTime } from 'luxon'
import { BaseModel, column, afterSave, afterDelete } from '@ioc:Adonis/Lucid/Orm'
import Post from 'App/Models/Post'

export default class FollowVideo extends BaseModel {

  @column({ isPrimary: true })
  public id: number
  @column()
  public uid: number
  @column({serializeAs:'userVideoId'})
  public userVideoId: number
  @column.dateTime({ autoCreate: true })
  public created: DateTime
  
}
