import { DateTime } from 'luxon'
import { BaseModel, column, beforeSave,computed} from '@ioc:Adonis/Lucid/Orm'

export default class NewsletterSubscriber extends BaseModel {

  @column({ isPrimary: true })
  public id: number

  @column()
  public email : string

  @column()
  public status : number
  
  @column.dateTime({ autoCreate: true })
  public created: DateTime

}
