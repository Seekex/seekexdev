import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import AclGroup from 'App/Models/AclGroup'
export default class Acl {
  protected async authenticate (auth: HttpContextContract['auth'], guard:any) {
    if(await auth.use(guard).check()){
      auth.defaultGuard = guard
      return true
    }
  }
  public async handle (
    {auth, request,session,response}: HttpContextContract,
    next: () => Promise<void>,
    allowedRoles: string[]
  ) {

    try {
      await this.authenticate(auth,auth.name)
      if(!auth.user||auth.user?.userRole=='user'){
        session.flash({ error:'This user can\'t access this.'})
        await auth.logout()
        return response.redirect('/admin')
      }else{
        const userRole=auth?.user?.userRole
        const acls = await AclGroup.getGroupAcls(userRole)
        if(request.url()!='/admin/user-dashboard' && userRole!='admin'){
         // console.log(request.ctx?.route?.pattern,acls)
          if(acls.indexOf(request.ctx?.route?.pattern)==-1){
            session.flash({ error:'This user can\'t access this page.'})
            return response.redirect('/admin/user-dashboard')
          }
        }
       // console.log(request.url()) 
      }
    } catch (error) {
      return response.redirect('/admin')
      console.log(`"${request.url()}" enforces "${allowedRoles}" roles`)
    }
    await next()
  }
}
