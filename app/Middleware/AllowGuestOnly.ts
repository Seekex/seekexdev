import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'

export default class AllowGuestOnlyMiddleware {
  public async handle ({ auth,response,session }: HttpContextContract, next: () => Promise<void>) {
    let loggedIn = false
    try {
      await auth.check()
      if(auth.user){
        loggedIn = true
      }
    }catch (e) {}
    if (loggedIn) {
      session.flash({ guestAccess:'Only guest user can access the page.'})
      response.redirect('/admin/dashboard')
    }
    await next()
  }
}
