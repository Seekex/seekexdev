import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import { AuthenticationException } from '@adonisjs/auth/build/standalone'
import { GuardsList } from '@ioc:Adonis/Addons/Auth'
import WebServiceLog from 'App/Models/WebServiceLog'

/**
 * Auth middleware is meant to restrict un-authenticated access to a given route
 * or a group of routes.
 *
 * You must register this middleware inside `start/kernel.ts` file under the list
 * of named middleware.
 */
export default class AuthMiddleware {
  /**
  * The URL to redirect to when request is Unauthorized
  */
  protected redirectTo = '/login'

  /**
   * Authenticates the current HTTP request against a custom set of defined
   * guards.
   *
   * The authentication loop stops as soon as the user is authenticated using any
   * of the mentioned guards and that guard will be used by the rest of the code
   * during the current request.
   */
  protected async authenticate (auth: HttpContextContract['auth'], guards: (keyof GuardsList)[]) {
    for (let guard of guards) {

      if (await auth.use(guard).check()) {
        /**
         * Instruct auth to use the given guard as the default guard for
         * the rest of the request, since the user authenticated
         * succeeded here
         */
        auth.defaultGuard = guard
        return true
      }
    }

    /**
     * Unable to authenticate using any guard
     */

    // throw new AuthenticationException(
    //   'Unauthorized access',
    //   'E_UNAUTHORIZED_ACCESS',
    //   this.redirectTo,
    // )
  }

  /**
   * Handle request
   */
  public async handle ({ auth,response,request }: HttpContextContract, next: () => Promise<void>, customGuards: (keyof GuardsList)[]) {
    /**
     * Uses the user defined guards or the default guard mentioned in
     * the config file
     * Bearer token
     */
    const guards = customGuards.length ? customGuards : [auth.name]
    await this.authenticate(auth, guards)
    if(!auth?.user && guards[0]=='api'){
      const data=request.all()
      const headers=request.headers()
      const allMedai=request.file('file')
      await WebServiceLog.create({request:JSON.stringify({data,allMedai,headers}),response:JSON.stringify({error:'api access error'}),service_name:'Middleware Access error',status:1,uid:0,time_taken:0})
      return response.status(200).json({resMsg:'Unauthorized access',"status": false,"logout": true})
    }else if(!auth?.user && guards[0]=='web'){
      // throw new AuthenticationException(
      // 'Unauthorized access',
      // 'E_UNAUTHORIZED_ACCESS',
      // this.redirectTo,
      // )
    }else if(!auth?.user && guards[0]=='agent'){
      throw new AuthenticationException(
        'Unauthorized access',
        'E_UNAUTHORIZED_ACCESS',
        this.redirectTo,
        )
    }
    await next()
  }
}
