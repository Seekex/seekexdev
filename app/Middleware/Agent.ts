import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'

export default class Acl {


  protected async authenticate (auth: HttpContextContract['auth'], guard:any) {
    if(await auth.use(guard).check()){
      auth.defaultGuard = guard
      return true
    }
  }
  public async handle (
    {auth, request,session,response}: HttpContextContract,
    next: () => Promise<void>,
    allowedRoles: string[]
  ) {

    try { 
      if(allowedRoles[0]=='agent'){
        await this.authenticate(auth,'agent')
      }else{
        await this.authenticate(auth,auth.name)
      }
      if(auth.user?.userRole!=allowedRoles[0]){
        session.flash({ error:'This user can\'t access this.'})
         await auth.logout()
         if(allowedRoles[0]=='agent'){
          return response.redirect('/agent')
         }
         return response.redirect('/admin')
      }
    } catch (error) {
      console.log(`"${request.url()}" enforces "${allowedRoles}" roles`)
    }
    await next()
  }
}
