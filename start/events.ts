import Event from '@ioc:Adonis/Core/Event'
import NotificationHelper from "App/Helpers/NotificationHelper"
const notify=new NotificationHelper()
import EmailHelper from "App/Helpers/EmailHelper"
const email=new EmailHelper()
import CommonHelper from 'App/Helpers/CommonHelper'
const commonHelper = new CommonHelper()
import Mail from '@ioc:Adonis/Addons/Mail'
import Database from '@ioc:Adonis/Lucid/Database'
// import Application from '@ioc:Adonis/Core/Application'
import Logger from '@ioc:Adonis/Core/Logger'

Event.on('db:query', (query:any) => {
  //if (Application.inProduction) {
    Logger.info(query)
  //} else {
    //Database.prettyPrint(query)
  //}
})

Event.on('new:notification', async (data) => {
  await notify.sendNotifiction(data.msg,data.fcm)
})

Event.on('new:sendNotificationForCommentToFollowers', async (data) => {
  await notify.sendNotificationForCommentToFollowers(data?.data,data?.fromUser)
})

Event.on('new:email', async (data) => {
 await email.sendEmail(data)
})

Event.on('new:regEmail', async (data) => {
 await email.sendRegEmail(data)
})
Event.on('new:scheduleEmail', async (data) => {
  await email.sendScheduleACallForExpertEmail(data)
  await email.sendScheduleACallForSeekerEmail(data)
})

Event.on('new:expertApprovalEmail', async (data) => {
  await email.sendExpertApprovalEmail(data)
})
Event.on('new:refundRejectEmail', async (data) => {
  await email.sendRefundRejectEmail(data)
})

/**For sent email debug */
Event.on('adonis:mail:sent', Mail.prettyPrint)
/**For sent email debug */

Event.on('new:referral', async (data) => {
  await commonHelper.referralAmount(data.code,data.uid)
})

Event.on('new:bulkNotification', async (data) => {
  await notify.sendBulkNotifiction(data.msg,data.alldevices)
})
Event.on('new:bulkNotificationWithImg', async (data) => {
  await notify.sendBulkNotifictionWithImg(data.data,data.devices)
})

Event.on('new:sendNewsletter', async (data) => {
  await email.sendNewsletter(data.data,data.emails)
})
