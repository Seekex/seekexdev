/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| This file is dedicated for defining HTTP routes. A single file is enough
| for majority of projects, however you can define routes in different
| files and just make sure to import them inside this file. For example
|
| Define routes in following two files
| ├── start/routes/cart.ts
| ├── start/routes/customer.ts
|
| and then import them inside `start/routes/index.ts` as follows
|
| import './cart'
| import './customer'
|
*/

import Route from '@ioc:Adonis/Core/Route'

import HealthCheck from '@ioc:Adonis/Core/HealthCheck'
//import Database from '@ioc:Adonis/Lucid/Database'
Route.get('health', async ({ response }) => {
  const report = await HealthCheck.getReport()
  return report.healthy ? response.ok(report) : response.badRequest(report)
})

Route.get('/', 'HomeController.index').as('home')
Route.get('/expert','HomeController.expert').as('expert')
Route.get('/about','HomeController.about').as('about')
Route.get('/blog','HomeController.blog').as('blog')
Route.get('/blog/:slug?','HomeController.blogDetails').as('blog_details')
Route.on('/thankyou').render('front/thankyou')

Route.get('/contact', 'HomeController.contact').as('contact')
Route.get('/cms/:slug?', 'HomeController.cms').as('cms')
Route.get('/expert-profile/:id?','HomeController.expertProfile').as('expert_profile')
Route.post('/newsletter','HomeController.newsletter').as('newsletter')
Route.post('/save-contact','HomeController.saveContact').as('saveContact')
Route.post('/post-comment/:id?','HomeController.saveBlogComment').as('saveBlogComment')
Route.post('/like-blog','HomeController.likeBlog').as('likeBlog')

Route.get('/checkNotify', 'Api/ServicesController.checkNotify')
Route.get('/checkNotifyForCall', 'Api/ServicesController.checkNotifyForCall')

//Route.on('/invoice').render('invoices/invoice')
Route.get('/check', 'HomeController.check')
Route.on('/errors/not-found').render('errors/not-found')
Route.on('/errors/server-error').render('errors/server-error')
Route.on('/errors/unauthorized').render('errors/unauthorized')
/* ===========================Api routes======================================= */
Route.group(() => {
  Route.post('/registration','Api/UsersController.registration')
  Route.post('/login','Api/UsersController.login')
  Route.post('/verifyMobile','Api/UsersController.verifyMobile')
  Route.post('/sendOtpOnEmail','Api/UsersController.sendOtpOnEmail')
  Route.post('/verifyOtp','Api/UsersController.verifyOtp')
  Route.post('/getMasterData','Api/DashboardController.getMasterData')
  Route.post('/getSkills','Api/DashboardController.getSkills')
  Route.get('/connectCall','Api/CallsController.connectCall')
  Route.get('/callResponse','Api/CallsController.callResponse')
  Route.get('/callResponseNoAnswer','Api/CallsController.callResponseNoAnswer')
  Route.get('/callResponseNoDial','Api/CallsController.callResponseNoDial')
  Route.get('/getTimeZone','Api/ServicesController.getTimeZone')
  Route.get('/room-ended','Api/CallsController.twilioCallEnded')
  //Route.post('/callDuration','Api/CallsController.callDuration')
  Route.post('/uploadTempFile','Api/DashboardController.uploadTempFile')
  Route.post('/changePassword','Api/UsersController.changePassword')
  Route.get('/getCms/:slug?','Api/ServicesController.getCms')
  Route.post('/resendOtp','Api/UsersController.resendOtp')
  Route.post('/zoomToken','Api/ServicesController.zoomToken')

  Route.post('/getPost','Api/PostsController.getPost')
  Route.post('/getAllExperts','Api/UsersController.getAllExperts')
  Route.post('/getProfile','Api/UsersController.getProfile')
  Route.post('/searchData','Api/ServicesController.searchData')
  Route.get('/getFaq','Api/DashboardController.getFaq')
  Route.post('/saveAppFcm','Api/UsersController.saveAppFcm')
  Route.get('/getAppVersion','Api/ServicesController.getAppVersion')
  Route.get('/connectToAgent','Api/CrmController.connectToAgent')
  Route.post('/getVideos','Api/VideosController.getVideos')
}).prefix('/api')



Route.group(() => {
  //Route.post('/getProfile','Api/UsersController.getProfile')
  Route.post('/callDuration','Api/CallsController.callDuration')
  Route.post('/logout','Api/UsersController.logout')
  //Route.post('/verifyMobile','Api/UsersController.verifyMobile')
  Route.post('/verifyEmailMobile','Api/UsersController.verifyEmailMobile')
 
  Route.post('/blockUnblockUser','Api/UsersController.blockUnblockUser')
  Route.post('/changeSetting','Api/UsersController.changeSetting')
  Route.post('/doReported','Api/UsersController.doReported')
  Route.post('/getBlockedUser','Api/UsersController.getBlockedUser')
  Route.post('/getBlockStatus','Api/UsersController.getBlockStatus')
  Route.post('/getNotifications','Api/UsersController.getNotifications')
  Route.post('/getPendingNotification','Api/UsersController.getPendingNotification')
  Route.post('/getSchedule','Api/UsersController.getSchedule')
  Route.post('/getScheduleDate','Api/UsersController.getScheduleDate')
  Route.post('/likeFollow','Api/UsersController.likeFollow')
  Route.post('/rateSetting','Api/UsersController.rateSetting')
  Route.post('/readNotification','Api/UsersController.readNotification')
  Route.post('/submitSchedule','Api/UsersController.submitSchedule')
  Route.post('/updateAdvanceDetail','Api/UsersController.updateAdvanceDetail')
  Route.post('/updateProfileMedia','Api/UsersController.updateProfileMedia')
  Route.post('/updateProfile','Api/UsersController.updateProfile')
  Route.post('/saveSocial','Api/UsersController.saveSocial')
  Route.post('/updateStatus','Api/UsersController.updateStatus')
  Route.post('/getExperts','Api/UsersController.getExperts')
  Route.post('/getFollowers','Api/UsersController.getFollowers')
  Route.post('/getYoutubeVideos','Api/UsersController.getYoutubeVideos')

  Route.post('/userOnlineOflineStatus','Api/UsersController.userOnlineOflineStatus')
  Route.post('/updateAccessibility','Api/UsersController.updateAccessibility')
  
  
  Route.post('/savePostRequest','Api/PostsController.savePostRequest')
  Route.post('/getPostRequest','Api/PostsController.getPostRequest')
  Route.post('/postComments','Api/PostsController.postComments')
  Route.post('/getCommnets','Api/PostsController.getCommnets')
  Route.post('/cancelRequest','Api/ServicesController.cancelAppointmentRequest')
  Route.post('/deletePost','Api/ServicesController.cancelAppointmentRequest')
  Route.post('/changeRequestStatus','Api/ServicesController.changeRequestStatus')
  Route.post('/getReferralCode','Api/ServicesController.getReferralCode')

  Route.post('/addBankDetail','Api/UserWalletsController.addBankDetail')
  Route.post('/addMoney','Api/UserWalletsController.addMoney')
  Route.post('/applyForRefund','Api/UserWalletsController.applyForRefund')
  Route.post('/getBankDetail','Api/UserWalletsController.getBankDetail')
  Route.post('/getTransactionHistory','Api/UserWalletsController.getTransactionHistory')
  Route.post('/getWalletSummary','Api/UserWalletsController.getWalletSummary')
  Route.post('/withdrawMoney','Api/UserWalletsController.withdrawMoney')
  Route.post('/approveRefund','Api/UserWalletsController.approveRefund')
  Route.post('/getRefunds','Api/UserWalletsController.getRefunds')

  Route.post('/call','Api/CallsController.call')
  Route.post('/zoomCall','Api/CallsController.zoomCall')
  Route.post('/zoomCallEnd','Api/CallsController.zoomCallEnd')
  
  Route.post('/getCallHistory','Api/CallsController.getCallHistory')
  Route.post('/getConversation','Api/CallsController.getConversation')
  Route.post('/getPendingRating','Api/CallsController.getPendingRating')
  Route.post('/getRating','Api/CallsController.getRating')
  Route.post('/giveRating','Api/CallsController.giveRating')
  Route.post('/skipRating','Api/CallsController.skipRating')
  Route.post('/updateBusyStatus','Api/CallsController.updateBusyStatus')
  Route.post('/uploadCallRecordingS','Api/CallsController.uploadCallRecordingS')
  Route.post('/uploadCallRec','Api/CallsController.uploadCallRec')
  Route.post('/getCallDetails','Api/CallsController.getCallDetails')
  Route.post('/viewRequest','Api/CallsController.viewRequest')
  Route.post('/getExpertNo','Api/CallsController.getExpertNo')
  Route.post('/createZoomMeeting','Api/CallsController.createZoomMeeting')
 
  
  Route.post('/submitFeedback','Api/DashboardController.submitFeedback')
  Route.post('/uploadMultipleFile','Api/DashboardController.uploadMultipleFile')
  
  Route.post('/delete','Api/DashboardController.delete')
  Route.post('/deleteFile','Api/DashboardController.deleteFile')
  Route.post('/getDashboardData','Api/DashboardController.getDashboardData')
  
  
  Route.post('/getInboxUser','Api/ChatsController.getInboxUser')
  Route.post('/sendChat','Api/ChatsController.sendChat')
  Route.post('/changeMsgStatus','Api/ChatsController.changeMsgStatus')

  Route.post('/readResume','Api/ServicesController.readResume')
  Route.post('/updateUserToken','Api/ServicesController.updateUserToken')
  Route.post('/invoiceRequest','Api/ServicesController.invoiceRequest')
  //Route.post('/searchData','Api/ServicesController.searchData')
  Route.post('/searchSuggestion','Api/ServicesController.searchSuggestion')
  Route.post('/checkApi','Api/ServicesController.checkApi')
  Route.post('/saveUserEvent','Api/ServicesController.saveUserEvent')
  Route.post('/saveVideo','Api/VideosController.saveVideo')
  Route.post('/postVideoComments','Api/VideosController.postVideoComments')
  Route.post('/getVideoCommnets','Api/VideosController.getVideoCommnets')
  Route.post('/likeFollowVideo','Api/VideosController.likeFollowVideo')
  Route.post('/viewVideo','Api/VideosController.viewVideo')
  Route.post('/deleteVideo','Api/VideosController.deleteVideo')

}).middleware('auth:api').prefix('/api')
/* ===========================Api routes======================================= */

/* ===========================Admin routes======================================= */

Route.group(() => {
    Route.on('/login').render('auth/login')
}).middleware('guest')

Route.group(() => {
  Route.on('/').render('auth/login')
  Route.on('/login').render('auth/login')
  Route.post('/login','AuthController.login').as('user.login')
}).middleware('guest').prefix('/admin')

Route.group(() => {
  Route.get('/dashboard','Admin/DashboardsController.dashboard').as('admin.dashboard')
  Route.on('/user-dashboard').render('admin/user_dashboard')
  Route.get('/user-list','Admin/UsersController.index').as('admin.user.list')
  Route.get('/expert-list','Admin/UsersController.expertIndex').as('admin.expert.list')
  Route.get('/pending-list','Admin/UsersController.pendingExpert').as('admin.pending.expert')
  Route.get('/user-wallet/:id?','Admin/UsersController.userWallet').as('admin.user.wallet')
  Route.get('/expert-sample','Admin/UsersController.expertSample').as('admin.expert.sample')
  Route.post('/expert-import','Admin/UsersController.expertImport').as('admin.user.import')
  Route.get('/admin-profile','Admin/UsersController.profile').as('admin.profile')
  Route.post('/expert-edit-profile','Admin/UsersController.profileEdit').as('admin.user.profileEdit')

  Route.get('/pending-export','Admin/UsersController.pendingExport').as('admin.user.export.pending')
  Route.get('/user-export','Admin/UsersController.userExport').as('admin.user.export.user')
  Route.get('/expert-export','Admin/UsersController.expertExport').as('admin.user.export.expert')

  Route.get('/unregistered-user-list','Admin/UsersController.unregistered').as('admin.unregistered.list')
  Route.post('/send-notification','Admin/UsersController.notificationSend').as('admin.notification.send')

  Route.get('/user-detail/:id?','Admin/UsersController.userDetail').as('admin.user.detail')
  Route.get('/user-edit/:id?','Admin/UsersController.userEdit').as('admin.user.edit')
  Route.get('/change-password/:id?','Admin/UsersController.userEditPassword').as('admin.user.editPassword')
  Route.post('/change-user-status','Admin/UsersController.changeUserStatus').as('admin.change_user_status')
  Route.post('/user-save/:id?','Admin/UsersController.userSave').as('admin.user.save')
  Route.post('/user-save-password/:id?','Admin/UsersController.savePassword').as('admin.user.savePassword')

  Route.get('/send-verify-email/:id?','Admin/UsersController.sendVerifyEmail').as('admin.verify_email')
  Route.get('/send-verify-sms/:id?','Admin/UsersController.sendVerifySms').as('admin.verify_sms')
  Route.post('/approve-expert','Admin/UsersController.approveExpert').as('admin.approve_expert')
  Route.post('/visible-expert','Admin/UsersController.visibleExpert').as('admin.visible_expert')
  Route.post('/featured-expert','Admin/UsersController.featuredExpert').as('admin.featured_expert')
  Route.post('/add-wallet-amount/:id?','Admin/UsersController.addAmountInWallet').as('admin.user.add.wallet')
  Route.get('/call-histories/:id?','Admin/UsersController.callHistory').as('admin.user.callHistory')

  Route.get('/reported-users','Admin/UsersController.reportedUsers').as('admin.reported.users')
  Route.get('/reported-user-detail/:id?','Admin/UsersController.reportedUserDetail').as('admin.reported.user.detail')

  Route.get('/user-referral-code/:id?','Admin/UsersController.referralCode').as('admin.user.referralcode')
  Route.get('/check-referral/:id?','Admin/UsersController.checkTransferReferralAmount').as('admin.user.checkreferral')
  Route.post('/transfer-referral/:id?','Admin/UsersController.transferReferralAmount').as('admin.user.transferReferral')

  Route.get('/settings','Admin/SettingsController.settings').as('admin.settings')
  Route.post('/save-settings','Admin/SettingsController.saveSettings').as('admin.savesettings')
  Route.get('/newsletter-subscribers','Admin/SettingsController.newsletterSubscribers').as('admin.newsletterSubscribers')
  Route.get('/contact-data','Admin/SettingsController.contactData').as('admin.contactData')
  Route.get('/newsletter-export','Admin/SettingsController.newsletterExport').as('admin.newsletter.export')
  Route.get('/newsletter-send','Admin/SettingsController.sendNewsletter').as('admin.newsletter.send')
  Route.post('/newsletter-send-subscribers','Admin/SettingsController.sendToSubscribers').as('admin.newsletter.sendToSubscribers')
  
  Route.get('/contact-data-export','Admin/SettingsController.formDataExport').as('admin.formdata.export')

  Route.get('/refund-list','Admin/RefundController.index').as('admin.refund')
  Route.get('/refunds','Admin/RefundController.refunds').as('admin.refund.list')
  Route.get('/refund-details/:id?','Admin/RefundController.details').as('admin.refund.details')
  Route.post('/send-msg','Admin/RefundController.sendMsg').as('admin.refund.sendMsg')
  Route.post('/refund-approve','Admin/RefundController.approveRefund').as('admin.refund.approve')
  Route.post('/partial-refund-approve','Admin/RefundController.approvePartialRefund').as('admin.refund.approve.partial')
  Route.get('/pending-refund-export','Admin/RefundController.pendingRefundExport').as('admin.refund.pendingExport')
  Route.get('/refund-export','Admin/RefundController.refundExport').as('admin.refund.export')

  Route.get('/users-feedback','Admin/DashboardsController.feedBacks').as('admin.feedback')

  Route.get('/users-appointments','Admin/AppointmentsController.index').as('admin.appointments')
  Route.get('/appointment-details/:id?','Admin/AppointmentsController.view').as('admin.appointments.details')
  Route.post('/appointment-cancel','Admin/AppointmentsController.cancel').as('admin.appointment.cancel')
  Route.get('/appointments-export','Admin/AppointmentsController.export').as('admin.appointments.export')

  Route.get('/posts/:uid?','Admin/PostsController.index').as('admin.posts')
  Route.get('/reported-posts','Admin/PostsController.reportedPosts').as('admin.reported.posts')
  Route.get('/post-detail/:id?','Admin/PostsController.reportedPostDetail').as('admin.reported.post.detail')
  Route.post('/change-post-status','Admin/PostsController.changePostStatus').as('admin.change_post_status')
  Route.post('/post-comment','Admin/PostsController.postComment').as('admin.post.comment')
  Route.post('/post-like','Admin/PostsController.likePost').as('admin.post.like')
  Route.get('/post-export','Admin/PostsController.export').as('admin.post.export')
  
  Route.get('/seekex-wallets','Admin/WalletsController.index').as('admin.wallets')
  Route.get('/seekex-wallet-details/:id?','Admin/WalletsController.details').as('admin.wallet.details')
  Route.post('add-amount-seekex/:id?','Admin/WalletsController.addAmountInWallet').as('admin.wallet.add.seekex')

  Route.get('/seekex-emails','Admin/EmailsController.index').as('admin.emails')
  Route.get('/seekex-emails-detail/:id?','Admin/EmailsController.view').as('admin.emails.details')
  Route.get('/seekex-emails-add/','Admin/EmailsController.add').as('admin.emails.add')
  Route.post('/seekex-emails-save','Admin/EmailsController.save').as('admin.emails.save')
  Route.post('/seekex-emails-status/:id?','Admin/EmailsController.changeStatus').as('admin.change_email_status')

  Route.get('/seekex-cms','Admin/CmsController.index').as('admin.cms')
  Route.get('/seekex-cms-detail/:id?','Admin/CmsController.view').as('admin.cms.details')
  Route.get('/seekex-cms-add/','Admin/CmsController.add').as('admin.cms.add')
  Route.post('/seekex-cms-save','Admin/CmsController.save').as('admin.cms.save')
  Route.post('/seekex-cms-status/:id?','Admin/CmsController.changeStatus').as('admin.change_cms_status')

  Route.get('/faq','Admin/FaqsController.index').as('admin.faq')
  Route.get('/faq-detail/:id?','Admin/FaqsController.view').as('admin.faq.details')
  Route.get('/faq-add/','Admin/FaqsController.add').as('admin.faq.add')
  Route.post('/faq-save','Admin/FaqsController.save').as('admin.faq.save')
  Route.post('/faq-status/:id?','Admin/FaqsController.changeStatus').as('admin.change_faq_status')

  Route.get('/master-godowns','Admin/DashboardsController.masterIndex').as('admin.master')
  Route.post('/change-tag-status','Admin/DashboardsController.changeTagStatus').as('admin.change_tag_status')
  Route.post('/add-tag','Admin/DashboardsController.addNewTag').as('admin.add_new_tag')
  Route.post('/edit-tag','Admin/DashboardsController.editTag').as('admin.edit_tag')
  Route.get('/get-tag','Admin/DashboardsController.getParentTag').as('admin.get_tag')
  Route.get('/tag-export','Admin/DashboardsController.tagExport').as('admin.tag.export')
  Route.post('/tag-import','Admin/DashboardsController.importTagData').as('admin.tag.import')
  

  Route.get('/orgnization','Admin/DashboardsController.orgIndex').as('admin.org')
  Route.post('/change-org-status','Admin/DashboardsController.changeOrgStatus').as('admin.change_org_status')
  Route.post('/add-org','Admin/DashboardsController.addNewOrg').as('admin.add_new_org')
  Route.post('/edit-org','Admin/DashboardsController.editOrg').as('admin.edit_org')

  Route.get('/seekex-blog','Admin/BlogController.index').as('admin.blog')
  Route.get('/seekex-blog-detail/:id?','Admin/BlogController.view').as('admin.blog.details')
  Route.get('/seekex-blog-add/','Admin/BlogController.add').as('admin.blog.add')
  Route.post('/seekex-blog-save','Admin/BlogController.save').as('admin.blog.save')
  Route.post('/seekex-blog-status/:id?','Admin/BlogController.changeStatus').as('admin.change_blog_status')

  Route.get('/agents','Admin/AgentsController.index').as('admin.agents')
  Route.get('/agent/:id?','Admin/AgentsController.view').as('admin.agent.details')
  Route.get('/agent-add','Admin/AgentsController.add').as('admin.agent.add')
  Route.post('/agent-save','Admin/AgentsController.save').as('admin.agent.save')
  Route.post('/agent-status/:id?','Admin/AgentsController.changeStatus').as('admin.change_agent_status')
  Route.get('/agent-export','Admin/AgentsController.export').as('admin.agent.export')

  Route.get('/conversations','Admin/ChatsController.index').as('admin.conversations')
  Route.get('/create-conversations','Admin/ChatsController.create').as('admin.conversations.create')
  Route.get('/get-conversations/:sid?/:rid?','Admin/ChatsController.getConversations').as('admin.conversations.get')
  Route.get('/get-user-list','Admin/ChatsController.searchUserList').as('admin.conversations.users')
  Route.post('/sendChat','Admin/ChatsController.sendChat').as('admin.sendchat')
  Route.get('/chat-export','Admin/ChatsController.export').as('admin.chat.export')

  Route.get('/calls','Admin/CallsController.index').as('admin.calls')
  Route.get('/calls-export','Admin/CallsController.export').as('admin.calls.export')

  Route.get('/sync-system','Admin/AclController.syncSystem').as('admin.syncSystem')
  Route.get('/acl-list','Admin/AclController.aclList').as('admin.aclList')
  Route.get('/groups','Admin/AclController.index').as('admin.groups')
  Route.get('/add-group','Admin/AclController.create').as('admin.group.add')
  Route.get('/view-group/:id?','Admin/AclController.view').as('admin.group.details')
  Route.post('/save-group','Admin/AclController.saveGroup').as('admin.group.save')
  Route.post('/update-group/:id?','Admin/AclController.editGroup').as('admin.group.update')
  Route.post('/change-acl-name','Admin/AclController.changeAclName').as('admin.change.acl')

  Route.get('/audience','Admin/AudienceController.index').as('admin.audience')
  Route.get('/audience-filters','Admin/AudienceController.allEvents').as('admin.audience.filters')
  Route.get('/audience-actions','Admin/AudienceController.allActions').as('admin.audience.allAction')
  Route.get('/audience-action-data/:actionType?','Admin/AudienceController.actionData').as('admin.audience.actionData')
  Route.post('/filter-data','Admin/AudienceController.filterData').as('admin.audience.filterData')
  Route.post('/upload-image','Admin/AudienceController.uploadImage').as('admin.audience.uploadImage')
  Route.get('/audience-campaign','Admin/AudienceController.campaignList').as('admin.campaign')
  Route.get('/campaign-details/:id?','Admin/AudienceController.campaignDetails').as('admin.campaign.details')
  Route.get('/notifications-details/:id?','Admin/AudienceController.notificationsCount').as('admin.campaign.notifications')

  Route.get('/coupons','Admin/CouponController.index').as('admin.coupons')
  Route.get('/add-coupon','Admin/CouponController.add').as('admin.coupon.add')
  Route.post('/save-coupon','Admin/CouponController.add').as('admin.coupon.save')
  Route.post('/coupon-status','Admin/CouponController.changeStatus').as('admin.coupon.status')
  Route.get('/coupons-export','Admin/CouponController.couponExport').as('admin.coupons.export')
  Route.get('/coupons-history/:code?','Admin/CouponController.couponHistory').as('admin.coupons.history')
  Route.get('/system-coupons','Admin/CouponController.getSystemCoupons').as('admin.coupons.systemCoupons')

  Route.get('/videos','Admin/VideosController.index').as('admin.videos')
  Route.get('/video-details/:id?','Admin/VideosController.view').as('admin.video.details')
  Route.get('/video-export','Admin/VideosController.export').as('admin.video.export')
  Route.post('/video-status','Admin/VideosController.changeStatus').as('admin.video.status')
  Route.post('/video-comment','Admin/VideosController.postComment').as('admin.video.comment')
  Route.post('/video-like','Admin/VideosController.likeVideo').as('admin.video.like')

}).middleware('auth:web').middleware('acl').prefix('/admin')
Route.group(() => {
  Route.get('blog/uploaded-images/','Admin/BlogController.uploadedImages').as('admin.blog.uploaded_images')
  Route.post('blog/image-upload/','Admin/BlogController.imageUpload').as('admin.blog.image_upload')
  Route.get('/getMasterData','Api/DashboardController.getMasterData').as('admin.masterData')
  Route.get('/getSkills','Api/DashboardController.getSkillsList').as('admin.skillList')
}).prefix('/admin')
Route.get('/logout','AuthController.logout').as('logout')
/* ===========================Admin routes======================================= */

/* ===========================Agent routes======================================= */
Route.group(() => {
  Route.get('/','Agent/UsersController.login').as('agent.login.home')
  Route.get('/login','Agent/UsersController.login').as('agent.login.form')
  Route.post('/login','Agent/UsersController.agentLogin').as('agent.login')
  Route.get('/logout','Agent/UsersController.logout').as('agent.logout')
}).prefix('/agent')

Route.group(() => {
  Route.get('/dashboard','Agent/UsersController.index').as('agent.dashboard')
}).middleware('auth:agent').middleware('agent:agent').prefix('/agent')

/* ===========================Agent routes======================================= */